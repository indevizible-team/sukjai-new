//
//  DataController.h
//  infostant
//
//  Created by Nattawut Singhchai on 12/29/12 .
//  Copyright (c) 2012 infostant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataController : NSObject
@property (strong,nonatomic) NSMutableArray *controller;
@property (strong,nonatomic) NSMutableDictionary* data; 
@property (strong, nonatomic)NSString *name;
-(id) initWithDictionary:(NSMutableDictionary*)dictionary;
-(NSArray*)key;
-(DataController*)accessKey:(NSString*)key;
-(DataController*)parent;
-(DataController*)insertData:(NSMutableDictionary*)dataToInsert;
-(NSMutableDictionary*)dictionaryAtIndex:(NSUInteger)index;
@end
