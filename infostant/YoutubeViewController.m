//
//  YoutubeViewController.m
//  Youtube
//
//  Created by Nattawut Singhchai on 12/27/12 .
//  Copyright (c) 2012 Nattawut Singhchai. All rights reserved.
//

#import "YoutubeViewController.h"
#import "YoutubeCell.h"
@interface YoutubeViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>

@end

@implementation YoutubeViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	datasource = [[NSMutableArray alloc] initWithObjects:
				   @"http://www.youtube.com/watch?v=tBDlw9jWsOo",
				  @"http://www.youtube.com/watch?v=4T9LNlwScic",
				  @"http://www.youtube.com/watch?v=d0oRsVdr658",
				  @"http://www.youtube.com/watch?v=ge6YYAbyOlY",
				 @"http://www.youtube.com/watch?v=5kA37KB0KoI",
				  nil];
	realLink = [NSMutableDictionary new];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
	NSLog(@"%@",realLink);
}

#pragma mark - UICollectionView
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section	{
	return [datasource count];
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

	YoutubeCell *cell = (YoutubeCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"YoutubeCell" forIndexPath:indexPath];
	cell.youtubeURL = datasource[indexPath.row];
	NSString *yid = [HCYoutubeParser youtubeIDFromYoutubeURL:[NSURL URLWithString:datasource[indexPath.row]]];
	[cell.imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://img.youtube.com/vi/%@/0.jpg",yid]]];
	if ([realLink valueForKey:[NSString stringWithFormat:@"%i",indexPath.row]]==nil) {
	[HCYoutubeParser h264videosWithYoutubeURL:[NSURL URLWithString:[datasource objectAtIndex:indexPath.row]] completeBlock:^(NSDictionary *videoDictionary, NSError *error) {
		[realLink setValue:[videoDictionary valueForKey:@"medium"] forKey:[NSString stringWithFormat:@"%i",indexPath.row]];
	}];
	}
	cell.theLabel.text = @"xfahepgohape'g'apwegpo'wes";
	return cell;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
	return 1;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
	
	if ([realLink valueForKey:[NSString stringWithFormat:@"%i",indexPath.row]]!=nil) {
//		NSURL *url = [NSURL URLWithString:datasource[indexPath.row]];
//		NSDictionary *videoDictionary = [HCYoutubeParser h264videosWithYoutubeURL:url];
		urlToLoad = [NSURL URLWithString:[realLink valueForKey:[NSString stringWithFormat:@"%i",indexPath.row]]];
//		NSLog(@"%@",[videoDictionary objectForKey:@"medium"]);
		mp = [[MPMoviePlayerViewController alloc] initWithContentURL:urlToLoad];
		[self presentMoviePlayerViewControllerAnimated:mp];
	}
	
	
}
@end
