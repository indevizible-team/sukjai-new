//
//  InfostantNavigationViewController.h
//  infostant
//
//  Created by Nattawut Singhchai on 1/1/13 .
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfostantNavigationViewController : UINavigationController
@property BOOL isLandscapeOK;
@end
