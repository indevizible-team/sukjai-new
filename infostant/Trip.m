//
//  Trip.m
//  www
//
//  Created by Trash on 3/12/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "Trip.h"
#import "DataManager.h"
@implementation Trip
+(Trip *)tripWithData:(NSDictionary *)data{
    Trip *trip = [Trip new];
    if (data) {
        trip.title = data[@"title"];
        trip.status = data[@"status"];
        trip.price  = (data[@"price"])?[data[@"price"] floatValue]:-1.0f;
        trip.priceText = (trip.price >0.0f)?[[DataManager numberToStr:(int)trip.price] uppercaseString]:@"N/A";
        trip.code   = data[@"code"];
        trip.currency = data[@"unit"];
        trip.dateString = [data[@"updatedate"] isEqualToString:@"0"]?@"N/A":data[@"updatedate"];
        trip.duration = [data[@"duration"] intValue];
        trip.durationText = (trip.duration)?[NSString stringWithFormat:@"%i",trip.duration]:@"N/A";
        trip.image = [NSURL URLWithString:data[@"photo"]];
        trip.telString = data[@"tel"];
        trip.tel = (data[@"tel"]&&([data[@"tel"] length]>5))?[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",data[@"tel"]]]:nil;
        trip.infoText = data[@"description"];
        trip.id = data[@"infomt"];
        trip.lock = [data[@"lock"] boolValue];
        if ([data[@"tag"] count ]) {
            trip.tagData = data[@"tag"];
            if (data[@"tag"][@"key"]) {
                trip.tagKey = data[@"tag"][@"key"];
            }else{
                trip.tagKey = @[];
            }
        }else{
            trip.tagData = @{@"key":@[]};
            trip.tagKey = @[];
        }
        
        
    }
    return trip;
}
@end
