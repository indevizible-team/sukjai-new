//
//  FileManager.m
//  Mugendai
//
//  Created by Nattawut Singhchai on 12/7/12 .
//  Copyright (c) 2012 Lekk . All rights reserved.
//NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
//NSString *documentsDirectory = [paths objectAtIndex:0];
//documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"InfostantPrivates"];

#import "FileManager.h"
#import "DataManager.h"
@implementation FileManager
+(BOOL)createDirectory:(NSString*)dirname{
	NSFileManager *manager = [NSFileManager new];
	NSString *docDir = [DataManager getDocumentDir];
	BOOL isDirectory;
	NSString *fullDir = [docDir stringByAppendingPathComponent:dirname];
	
	if (![manager fileExistsAtPath:fullDir isDirectory:&isDirectory] || !isDirectory) {
        NSError *error = nil;
        NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete
                                                         forKey:NSFileProtectionKey];
        [manager createDirectoryAtPath:fullDir
           withIntermediateDirectories:YES
                            attributes:attr
                                 error:&error];
        if (error)
			return NO;
    }
	return YES;
}
+(BOOL)createFullDirectory:(NSString*)dirname{
	NSFileManager *manager = [NSFileManager new];
	BOOL isDirectory;
	NSString *fullDir = dirname;
	
	if (![manager fileExistsAtPath:fullDir isDirectory:&isDirectory] || !isDirectory) {
        NSError *error = nil;
        NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete
                                                         forKey:NSFileProtectionKey];
        [manager createDirectoryAtPath:fullDir
           withIntermediateDirectories:YES
                            attributes:attr
                                 error:&error];
        if (error)
			return NO;
    }
	return YES;
}
+(BOOL)removeDirectoryFromUrl:(NSURL*)url{
	return [self removeDirectory:[url path]];
}
+(BOOL)removeDirectory:(NSString*)dirname{
	
	NSFileManager *manager = [NSFileManager new];

	BOOL isDirectory;
	NSString *dir = [[DataManager getDocumentDir] stringByAppendingPathComponent:dirname];
	
	if (![manager fileExistsAtPath:dir isDirectory:&isDirectory] || !isDirectory) {
        NSError *error = nil;
		[manager removeItemAtPath:dir error:&error];
        if (error)
			return NO;
    }
	return YES;
}


//+(void)downloadFileFromURL:(NSString *)url toDocument:(NSString *)dir fileName:(NSString *)filename{
//	NSString *urlpath = url;
//	
//	//set url paths
//	if (filename == nil) {
//		filename = [urlpath lastPathComponent];
//	}
//	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlpath]];
//	AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
//	NSString *downloadDir = dir;
//	if ([self createDirectory:downloadDir]) {
//		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//		NSString *path = [[[paths objectAtIndex:0] stringByAppendingPathComponent:downloadDir] stringByAppendingPathComponent:filename];
//		operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
//		
//		[operation setDownloadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
//			
//		}];
//		[operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//			NSLog(@"Successfully downloaded file to %@", path);
//			
//		} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//			NSLog(@"Error: %@", error);
//			
//		}];
//		[operation start];
//	}
//}
//+(void)uploadImageAndDataToURL:(NSString *)url image:(UIImage *)image data:(NSDictionary *)data fileKey:(NSString *)key success:(void (^)(id))success fail:(void (^)(NSError *))fail{
//	
//	if (key == nil) {
//		key = @"Filedata";
//	}
//	NSDictionary *plist = [[DataManager new] config];
//	
//	AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:[plist valueForKey:@"ajaxDomain"]]];
//	NSDictionary *parameters = data;
//	NSMutableURLRequest *myRequest;
//	if (image==nil) {
//		myRequest = [client requestWithMethod:@"POST" path:url parameters:parameters];
//		NSLog(@"%@",myRequest);
//	}else{
//		myRequest = [client multipartFormRequestWithMethod:@"POST" path:url parameters:parameters constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
//			NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
//			[formData appendPartWithFileData:imageData name:key fileName:@"image.jpg" mimeType:@"image/jpg"];
//		}];
//	}
//
//	AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:myRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
//		success(JSON);
//		
//	} failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
//		NSLog(@"%@\n%@",error,JSON);
//		fail(error);
//	}];
//	NSLog(@"%@",operation);
//	[operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
//		float percentDone = ((float)((int)totalBytesWritten) / (float)((int)totalBytesExpectedToWrite));
//		NSLog(@"%@ ^ %f",url,percentDone);
//	}];
//	[operation setDownloadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
//		
//		float percentDone = ((float)((int)totalBytesWritten) / (float)((int)totalBytesExpectedToWrite));
//		NSLog(@"%@ v %f",url,percentDone);
//		
//	}];
//
//	[operation start];
//}



@end
