//
//  Bitly.h
//  www
//
//  Created by Trash on 3/21/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BitlyData : NSObject
@property (strong,nonatomic) NSURL *longURL;
@property (strong,nonatomic) NSURL *URL;
@property (strong,nonatomic) NSString *hash;
@property (strong,nonatomic) NSString *globalHash;
@property (assign,nonatomic) BOOL isNewHash;
@end

@interface Bitly : NSObject
@property (nonatomic,strong) NSNumber *statusCode;
@property (nonatomic,strong) NSString *statusText;
@property (nonatomic,strong) BitlyData *data;
@property (nonatomic,strong) UIImage *image;
+(void)shortenURL:(NSURL *)url
          success:(void (^)(NSURLRequest *request, NSURL *url,Bitly *bitly))success
          failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error))failure;
+(void)shortenURLWithQRImage:(NSURL *)url
                        size:(int)size
                     success:(void (^)(NSURLRequest *request, NSURL *url,Bitly *bitly,UIImage *qrImage))success
                     failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error))failure;
+(void)shortenURLWithQRImage:(NSURL *)url
          success:(void (^)(NSURLRequest *request, NSURL *url,Bitly *bitly,UIImage *qrImage))success
          failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error))failure;

+(BOOL)expandFromShortURL:(NSURL *)url
                     success:(void (^)(NSURLRequest *request, NSURL *longURL))success
                     failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error))failure;
@end
