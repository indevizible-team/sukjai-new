//
//  Bitly.m
//  www
//
//  Created by Trash on 3/21/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//
#define token @"38d6c371cc56015fa90685df3d02a53a0bbf2e6b"
#import "Bitly.h"
#import "AFNetworking.h"
@implementation BitlyData
@end

@implementation Bitly
+(void)shortenURL:(NSURL *)url
          success:(void (^)(NSURLRequest *, NSURL *, Bitly *))success
          failure:(void (^)(NSURLRequest *, NSHTTPURLResponse *, NSError *))failure{
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:@"https://api-ssl.bitly.com/v3/"]];
    NSDictionary *param = @{@"access_token":token,
                            @"longUrl":url
                            };
    NSURLRequest *request = [client requestWithMethod:@"GET" path:@"shorten" parameters:param];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        Bitly *bl = [[Bitly alloc] initWithData:JSON];
        NSLog(@"bl : %@",bl.statusText);
        success(request,bl.data.URL,bl);
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        failure(request,response,error);
    }];
    [operation start];
}

+(void)shortenURLWithQRImage:(NSURL *)url
                     success:(void (^)(NSURLRequest *, NSURL *, Bitly *, UIImage *))success
                     failure:(void (^)(NSURLRequest *, NSHTTPURLResponse *, NSError *))failure{
    [Bitly shortenURLWithQRImage:url size:0 success:^(NSURLRequest *request, NSURL *url, Bitly *bitly, UIImage *qrImage) {
        success(request,url,bitly,qrImage);
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        failure(request,response,error);
    }];
}

+(void)shortenURLWithQRImage:(NSURL *)url
                        size:(int)size
                     success:(void (^)(NSURLRequest *, NSURL *, Bitly *, UIImage *))success
                     failure:(void (^)(NSURLRequest *, NSHTTPURLResponse *, NSError *))failure{
    [Bitly shortenURL:url success:^(NSURLRequest *request, NSURL *url, Bitly *bitly) {
        AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:@"http://bit.ly"]];
        NSURLRequest *imageRequest = [client requestWithMethod:@"GET" path:[NSString stringWithFormat:@"%@.qrcode",bitly.data.hash] parameters:((size)?@{@"s":[NSNumber numberWithInt:size]}:nil)];
        AFImageRequestOperation *imageOperation = [AFImageRequestOperation imageRequestOperationWithRequest:imageRequest imageProcessingBlock:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            bitly.image = image;
            success(imageRequest,bitly.data.URL,bitly,image);
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            failure(request,response,error);
        }];
        [imageOperation start];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        failure(request,response,error);
    }];
}

-(id)initWithData:(NSDictionary *)dictionaryData{
    if (self && dictionaryData) {
        self.statusCode = dictionaryData[@"status_code"];
        self.statusText = dictionaryData[@"status_txt"];
        if ([self.statusCode isEqualToNumber:@200]) {
            self.data = [[BitlyData alloc] init];
            NSDictionary *dataDetail = dictionaryData[@"data"];
            self.data.longURL       = dataDetail[@"long_url"];
            self.data.URL           = [NSURL URLWithString:dataDetail[@"url"]];
            self.data.hash          = dataDetail[@"hash"];
            self.data.globalHash    = dataDetail[@"global_hash"];
            self.data.isNewHash     = [dataDetail[@"new_hash"] boolValue];
        }
        return self;
    }
    return nil;
}
//+(NSURL *)expandFromShortURL:(NSURL *)url{

//
//}
+(BOOL)expandFromShortURL:(NSURL *)url
                     success:(void (^)(NSURLRequest *, NSURL *))success
                     failure:(void (^)(NSURLRequest *, NSHTTPURLResponse *, NSError *))failure{
    ////    API Address: https://api-ssl.bitly.com
    ////    GET /v3/expand?access_token=ACCESS_TOKEN&shortUrl=http%3A%2F%2Fbit.ly%2Fze6poY
    if ([url.host isEqualToString:@"bit.ly"]) {
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:@"https://api-ssl.bitly.com/v3/"]];
    NSDictionary *param = @{@"access_token":token,
                            @"shortUrl": [[NSURL alloc] initWithScheme:[url scheme]
                                                                                 host:[url host]
                                                                                 path:[url path]]
                            };
    NSURLRequest *request = [client requestWithMethod:@"GET" path:@"expand" parameters:param];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if ([JSON[@"status_code"] isEqualToNumber:@200]) {
            NSLog(@"data : %@ ",JSON);
            success(request,[NSURL URLWithString:JSON[@"data"][@"expand"][0][@"long_url"]]);
        }else{
            failure(request,response,[NSError errorWithDomain:request.URL.host code:[JSON[@"status_code"] intValue] userInfo:nil]);
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        failure(request,response,error);
    }];
    [operation start];
        return YES;
    }
    return NO;
}
@end
