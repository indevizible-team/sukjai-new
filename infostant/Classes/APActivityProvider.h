//
//  APActivityProvider.h
//  www
//
//  Created by Trash on 3/14/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APActivityProvider : UIActivityItemProvider<UIActivityItemSource>

@end

@interface UIActivityTwitter : UIActivity
@property (nonatomic,strong) UIImage *shareImage;
@property (nonatomic,strong) NSString *shareString;
@property (nonatomic,strong) NSURL *shareURL;
@end

@interface UIActivityFacebook : UIActivity
@property (nonatomic,strong) UIImage *shareImage;
@property (nonatomic,strong) NSString *shareString;
@property (nonatomic,strong) NSURL *shareURL;
@end