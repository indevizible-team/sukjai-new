//
//  Route.m
//  www
//
//  Created by Trash on 3/12/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "Route.h"

@implementation Route
+(Route *)routeWithData:(NSDictionary *)data{
    Route *r = [Route new];
    if (data) {
        r.image = [NSURL URLWithString:data[@"photo"]];
        r.title = data[@"title"];
        r.dateString = data[@"startdate"];
        r.productid = data[@"productid"];
        r.infofid   = data[@"infofid"];
        r.infomtit  = data[@"infomtit"];
    }else{
        r.image = [NSURL URLWithString:@"http://www.bangkoktourist.com/data/big-201082223249-MOM00011.jpg"];
        r.title = @"This is title";
        r.description = @"This is description";
        r.dateString = @"date string";
    }
    return r;
}
@end
