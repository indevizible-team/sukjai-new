//
//  Route.h
//  www
//
//  Created by Trash on 3/12/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Route : NSObject
@property (nonatomic,strong) NSURL *image;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *description;
@property (nonatomic,strong) NSString *dateString;
@property (nonatomic,strong) NSString *infofid;
@property (nonatomic,strong) NSString *infomtit;
@property (nonatomic,strong) NSString *productid;

+(Route *)routeWithData:(NSDictionary *)data;
@end
