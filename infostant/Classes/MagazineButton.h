//
//  MagazineButton.h
//  www
//
//  Created by Trash on 3/22/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "HomeButton.h"

@interface MagazineButton : HomeButton
@property (nonatomic,strong) NSURL *downloadLink;
@property (nonatomic,strong) NSURL *appScheme;

@end
