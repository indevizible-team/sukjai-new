//
//  HomeButton.h
//  www
//
//  Created by Trash on 3/22/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface HomeButton : NSObject
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) UIImage *image;
@end
