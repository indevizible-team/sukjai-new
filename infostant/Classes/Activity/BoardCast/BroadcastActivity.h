//
//  BroadcastActivity.h
//  www
//
//  Created by Trash on 3/19/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REComposeViewController.h"
@interface BroadcastActivity : UIActivity
@property (nonatomic,strong) UIImage *shareImage;
@property (nonatomic,strong) NSString *shareString;
@property (nonatomic,strong) NSURL *shareURL;
@property (nonatomic,strong) NSString *socialExtern;
@end
