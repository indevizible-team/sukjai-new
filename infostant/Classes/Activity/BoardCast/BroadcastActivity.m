//
//  BroadcastActivity.m
//  www
//
//  Created by Trash on 3/19/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "BroadcastActivity.h"
#import "User.h"
#import "DataManager.h"
@interface BroadcastActivity ()<UIAlertViewDelegate,REComposeViewControllerDelegate>{
    NSURLRequest *lastRequest;
}
@end
@implementation BroadcastActivity
- (NSString *)activityType { return @"UIActivitryBroadCast"; }
- (NSString *)activityTitle { return @"BroadCast"; }
- (UIImage *) activityImage { return [UIImage imageNamed:@"bcicon"]; }
- (BOOL) canPerformWithActivityItems:(NSArray *)activityItems {
    User *user = [User currentUser];
    NSLog(@"%@",user.data);
    if (![user isAdminUser]) {
        return NO;
    }
    
    for (UIActivityItemProvider *item in activityItems) {
        if ([item isKindOfClass:[NSURL class]]||[item isKindOfClass:[NSString class]]) {
            
            return YES;
        }
    }
    return NO;
}
- (void)prepareWithActivityItems:(NSArray *)activityItems {
    for (id item in activityItems) {
        if ([item isKindOfClass:[UIImage class]]) self.shareImage = item;
        else if ([item isKindOfClass:[NSURL class]]) {
            self.shareURL = item;
        }
        else if ([item isKindOfClass:[NSString class]]) {
            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:item]]) {
                self.shareURL = [NSURL URLWithString:item];
            }
            self.shareString = [(self.shareString ? self.shareString : @"") stringByAppendingFormat:@"%@%@",(self.shareString ? @" " : @""),item]; // concat, with space if already exists.
        }
        
        else NSLog(@"Unknown item type %@", item);
    }
}
- (UIViewController *) activityViewController {
    
    REComposeViewController *composeViewController = [[REComposeViewController alloc] init];
    composeViewController.title = @"Broadcast";
    composeViewController.hasAttachment = (self.shareURL != nil);
    
    composeViewController.delegate = self;
    composeViewController.text = self.shareString;
    return composeViewController;
}
- (void) performActivity {
    
}

-(void)composeViewController:(REComposeViewController *)composeViewController didFinishWithResult:(REComposeResult)result{
    [composeViewController dismissViewControllerAnimated:YES completion:nil];
    
    if (result == REComposeResultPosted) {
        
        User *user = [User currentUser];
        NSMutableDictionary *param = [DataManager param];
        [param addEntriesFromDictionary:@{
         @"oauth_login":user.oauth_login,
         @"msg":composeViewController.text,
         @"link":self.shareURL,
         @"description":composeViewController.text
         }];
        lastRequest = [[DataManager client] requestWithMethod:@"POST" path:@"saveNotificationCenter" parameters:param];
        [self postLastRequest];
        [self activityDidFinish:YES];
        
    }else{
        [self activityDidFinish:NO];
    }
    
}

-(void)postLastRequest{
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:lastRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"%@",JSON);
        if ([[JSON objectForKey:@"error"] isKindOfClass:[NSNumber class]]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoAppName", @"") message:NSLocalizedString(@"infoBroadCastOK", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"infoOK", @"") otherButtonTitles: nil];
            [alert show];
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoError", @"") message:[JSON valueForKey:@"error"] delegate:nil cancelButtonTitle:NSLocalizedString(@"infoOK", @"") otherButtonTitles: nil];
            [alert show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoError", @"") message:NSLocalizedString(@"infoNoInternet", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") otherButtonTitles: @"Try Again",nil];
        [alert show];
    }];
    [operation start];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex != alertView.cancelButtonIndex) {
        [self postLastRequest];
    }
}
@end
