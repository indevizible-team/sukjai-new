//
//  SLComposeActivity.h
//  www
//
//  Created by Trash on 3/14/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//
#import <Social/Social.h>
#import <UIKit/UIKit.h>

@interface SLComposeActivity : UIActivity
@property (nonatomic,strong) UIImage *shareImage;
@property (nonatomic,strong) NSString *shareString;
@property (nonatomic,strong) NSURL *shareURL;
@property (nonatomic,strong) NSString *socialExtern;

+(id)activityWithSocialExtern:(NSString *)ext;
+(NSArray *)getAllActivity;
@end
