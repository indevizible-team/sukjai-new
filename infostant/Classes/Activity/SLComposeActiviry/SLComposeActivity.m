//
//  SLComposeActivity.m
//  www
//
//  Created by Trash on 3/14/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "SLComposeActivity.h"
@implementation SLComposeActivity
- (NSString *)activityType { return @"SLComposeActivity"; }
- (NSString *)activityTitle {
    if ([self.socialExtern isEqualToString:SLServiceTypeFacebook]) {
        return @"Facebook";
    }else if ([self.socialExtern isEqualToString:SLServiceTypeTwitter]){
        return @"Twitter";
    }else if ([self.socialExtern isEqualToString:SLServiceTypeSinaWeibo]){
        return @"Weibo";
    }
    return @"ERROR";

}
- (UIImage *) activityImage {
    if ([self.socialExtern isEqualToString:SLServiceTypeFacebook]) {
        return [UIImage imageNamed:@"f_logo"];
    }
    return [UIImage imageNamed:@"twitterlogo"];
}
- (BOOL) canPerformWithActivityItems:(NSArray *)activityItems { return YES; }
- (void)prepareWithActivityItems:(NSArray *)activityItems {
    for (id item in activityItems) {
        if ([item isKindOfClass:[UIImage class]]) self.shareImage = item;
        else if ([item isKindOfClass:[NSString class]]) {
            self.shareString = [(self.shareString ? self.shareString : @"") stringByAppendingFormat:@"%@%@",(self.shareString ? @" " : @""),item]; // concat, with space if already exists.
        }
        else if ([item isKindOfClass:[NSURL class]]) {
            self.shareURL = item;
//            self.shareString = [(self.shareString ? self.shareString : @"") stringByAppendingFormat:@"%@%@",(self.shareString ? @" " : @""),[(NSURL *)item absoluteString]]; // concat, with space if already exists.
        }
        else NSLog(@"Unknown item type %@", item);
    }
}
- (UIViewController *) activityViewController {
    SLComposeViewController *fbController=[SLComposeViewController composeViewControllerForServiceType:self.socialExtern];
    SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result){
        
        [fbController dismissViewControllerAnimated:YES completion:^{
            switch(result){
                case SLComposeViewControllerResultCancelled:
                default:
                {
                    [self activityDidFinish:NO];
                    return ;
                }
                    break;
                case SLComposeViewControllerResultDone:
                {
                    [self activityDidFinish:YES];
                }
                    break;
            }
        }];
    };
    
    
    [fbController addImage:self.shareImage];
    [fbController setInitialText:self.shareString];
    [fbController addURL:self.shareURL];
    [fbController setCompletionHandler:completionHandler];
    return fbController;
}
- (void) performActivity {
    
}
+(id)activityWithSocialExtern:(NSString *)ext{
    SLComposeActivity *sl = [SLComposeActivity new];
    sl.socialExtern = ext;
    return sl;
}
+(NSArray *)getAllActivity{
    NSMutableArray *tmpArr = [NSMutableArray new];
      if (![SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
          [tmpArr addObject:[SLComposeActivity activityWithSocialExtern:SLServiceTypeFacebook]];
      }
    if (![SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        [tmpArr addObject:[SLComposeActivity activityWithSocialExtern:SLServiceTypeTwitter]];
    }
    return tmpArr;
}


@end
