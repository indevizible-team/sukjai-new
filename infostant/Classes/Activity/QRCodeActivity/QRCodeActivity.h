//
//  QRCodeActivity.h
//  www
//
//  Created by Trash on 3/14/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QRCodeActivityViewController.h"
@interface QRCodeActivity : UIActivity<QRCodeActivityViewController>
@property (nonatomic,strong) UIImage *shareImage;
@property (nonatomic,strong) NSString *shareString;
@property (nonatomic,strong) NSURL *shareURL;
@property (nonatomic,strong) NSString *socialExtern;
@end
