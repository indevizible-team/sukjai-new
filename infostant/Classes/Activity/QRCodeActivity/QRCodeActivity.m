//
//  QRCodeActivity.m
//  www
//
//  Created by Trash on 3/14/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "QRCodeActivity.h"

@implementation QRCodeActivity
- (NSString *)activityType { return @"UIActivityQRCode"; }
- (NSString *)activityTitle { return @"QRCode"; }
- (UIImage *) activityImage { return [UIImage imageNamed:@"qrlogo"]; }
- (BOOL) canPerformWithActivityItems:(NSArray *)activityItems {
    for (UIActivityItemProvider *item in activityItems) {
        if ([item isKindOfClass:[NSURL class]]||[item isKindOfClass:[NSString class]]) {
            return YES;
        }
    }
    return NO;
}
- (void)prepareWithActivityItems:(NSArray *)activityItems {
    for (id item in activityItems) {
        if ([item isKindOfClass:[UIImage class]]) self.shareImage = item;
        else if ([item isKindOfClass:[NSURL class]]) {
            self.shareURL = item;
            self.shareString = [(self.shareString ? self.shareString : @"") stringByAppendingFormat:@"%@%@",(self.shareString ? @" " : @""),[(NSURL *)item absoluteString]]; // concat, with space if already exists.
        }
        else if ([item isKindOfClass:[NSString class]]) {
            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:item]]) {
                self.shareURL = [NSURL URLWithString:item];
            }
            self.shareString = [(self.shareString ? self.shareString : @"") stringByAppendingFormat:@"%@%@",(self.shareString ? @" " : @""),item]; // concat, with space if already exists.
        }
        
        else NSLog(@"Unknown item type %@", item);
    }
}
- (UIViewController *) activityViewController {
    QRCodeActivityViewController *vc =[[QRCodeActivityViewController alloc] initWithNibName:@"QRCodeActivityViewController" bundle:nil];
    vc.modalPresentationStyle = UIModalPresentationFormSheet;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc.delegate = self;
    if (self.shareURL) {
        vc.imageURL = [self.shareURL absoluteString];
    }else{
        vc.imageURL = self.shareString;
    }
    
    return vc;
}
- (void) performActivity {
    
}
-(void)didFinishShareQRCode:(QRCodeActivityViewController *)vc{
    [vc dismissViewControllerAnimated:YES completion:^{
      
        [self activityDidFinish:YES];
        return ;
    }];
}
@end
