//
//  QRCodeActivityViewController.m
//  www
//
//  Created by Trash on 3/14/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//
#define qrSize @240
#import "QRCodeActivityViewController.h"
#import "AFNetworking.h"
#import "ActivitySet.h"
#import "SLComposeActivity.h"
@interface QRCodeActivityViewController (){
    int size;
    NSString *sizeString;
}

@end

@implementation QRCodeActivityViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0)) {
        size = [qrSize intValue]*2;
    }else{
        size = [qrSize intValue];
    }
    sizeString = [NSString stringWithFormat:@"%ix%i",size,size];
    if (self.imageURL) {
  
        AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:@"http://chart.apis.google.com/"]];
        NSDictionary *defaultParam = @{@"chs": sizeString,
                                       @"cht": @"qr",
                                       @"chld":@"|1",
                                       @"chl":self.imageURL
                                       };
        NSURLRequest *urlRequest = [client requestWithMethod:@"GET" path:@"chart" parameters:defaultParam];
        [self.qrView setImageWithURL:urlRequest.URL];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.view.superview.frame = CGRectMake(0, 0, 320, 364);
        self.view.superview.center = self.view.center;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)actionClose:(id)sender {
    if ([self.delegate respondsToSelector:@selector(didFinishShareQRCode:)]) {
        [self.delegate didFinishShareQRCode:self];
    }
}
- (IBAction)actionShare:(id)sender {
    if (self.qrView.image) {
        [self presentViewController:[DataManager activityViewControllerWithArray:@[self.qrView.image,self.imageURL]] animated:YES completion:nil];
    }
    
}

@end
