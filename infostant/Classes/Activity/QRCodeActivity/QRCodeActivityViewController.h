//
//  QRCodeActivityViewController.h
//  www
//
//  Created by Trash on 3/14/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QRCodeActivityViewController;

@interface QRCodeActivityViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *qrView;
@property (nonatomic,strong) NSString *imageURL;
@property (nonatomic,strong) id<QRCodeActivityViewController> delegate;
@end

@protocol QRCodeActivityViewController <NSObject>
-(void)didFinishShareQRCode:(QRCodeActivityViewController *)vc;
@end