//
//  TourPackage.h
//  www
//
//  Created by Trash on 3/22/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "HomeButton.h"
typedef enum {
    kTourPackageTypeDuration,
    kTourPackageTypePrice
}TourPackageType;
@interface TourPackage : HomeButton
@property (nonatomic,assign) TourPackageType type;
@end
