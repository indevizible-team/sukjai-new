//
//  TourSuggestion.h
//  www
//
//  Created by Trash on 3/22/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "HomeButton.h"
typedef enum {
    kTourSuggestionTypeDestination,
    kTourSuggestionTypeSpecial
}TourSuggestionType;
@interface TourSuggestion : HomeButton
@property (nonatomic,assign) TourSuggestionType type;
@end
