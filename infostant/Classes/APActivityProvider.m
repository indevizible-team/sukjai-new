//
//  APActivityProvider.m
//  www
//
//  Created by Trash on 3/14/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//
#import "ActivitySet.h"
#import "APActivityProvider.h"
#import <Social/Social.h>
@implementation APActivityProvider
- (id) activityViewController:(UIActivityViewController *)activityViewController
          itemForActivityType:(NSString *)activityType
{
//    if ( [activityType isEqualToString:UIActivityTypePostToTwitter] )
//        return @"This is a #twitter post!";
//    if ( [activityType isEqualToString:UIActivityTypePostToFacebook] )
//        return @"This is a facebook post!";
//    if ( [activityType isEqualToString:UIActivityTypeMessage] )
//        return @"SMS message text";
//    if ( [activityType isEqualToString:UIActivityTypeMail] )
//        return @"Email text here!";
//    if ( [activityType isEqualToString:@"it.albertopasca.myApp"] )
//        return @"OpenMyapp custom text";
    return nil;
}
- (id) activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController { return @""; }

@end

@implementation UIActivityTwitter : UIActivity 
- (NSString *)activityType { return @"UIActivityTwitter"; }
- (NSString *)activityTitle { return @"Twitter"; }
- (UIImage *) activityImage { return [UIImage imageNamed:@"pinPlace-1.png"]; }
- (BOOL) canPerformWithActivityItems:(NSArray *)activityItems { return YES; }
- (void)prepareWithActivityItems:(NSArray *)activityItems {
    for (id item in activityItems) {
        if ([item isKindOfClass:[UIImage class]]) self.shareImage = item;
        else if ([item isKindOfClass:[NSString class]]) {
            self.shareString = [(self.shareString ? self.shareString : @"") stringByAppendingFormat:@"%@%@",(self.shareString ? @" " : @""),item]; // concat, with space if already exists.
        }
        else if ([item isKindOfClass:[NSURL class]]) {
            self.shareURL = item;
            self.shareString = [(self.shareString ? self.shareString : @"") stringByAppendingFormat:@"%@%@",(self.shareString ? @" " : @""),[(NSURL *)item absoluteString]]; // concat, with space if already exists.
        }
        else NSLog(@"Unknown item type %@", item);
    }
}
- (UIViewController *) activityViewController {
    SLComposeViewController *fbController=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result){
        
        [fbController dismissViewControllerAnimated:YES completion:^{
            switch(result){
                case SLComposeViewControllerResultCancelled:
                default:
                {
                    [self activityDidFinish:NO];
                    NSLog(@"Cancelled.....");
                    return ;
                }
                    break;
                case SLComposeViewControllerResultDone:
                {
                    NSLog(@"Posted....");
                    [self activityDidFinish:YES];
                }
                    break;
            }
        }];
        };
        
    
    [fbController addImage:self.shareImage];
    [fbController setInitialText:self.shareString];
    [fbController addURL:self.shareURL];
    [fbController setCompletionHandler:completionHandler];
    return fbController;
}
- (void) performActivity {
    
}
@end

@implementation UIActivityFacebook : UIActivity
- (NSString *)activityType { return @"UIActivityFacebook"; }
- (NSString *)activityTitle { return @"Twitter"; }
- (UIImage *) activityImage { return [UIImage imageNamed:@"pinPlace-1.png"]; }
- (BOOL) canPerformWithActivityItems:(NSArray *)activityItems { return YES; }
- (void)prepareWithActivityItems:(NSArray *)activityItems {
    for (id item in activityItems) {
        if ([item isKindOfClass:[UIImage class]]) self.shareImage = item;
        else if ([item isKindOfClass:[NSString class]]) {
            self.shareString = [(self.shareString ? self.shareString : @"") stringByAppendingFormat:@"%@%@",(self.shareString ? @" " : @""),item]; // concat, with space if already exists.
        }
        else if ([item isKindOfClass:[NSURL class]]) {
            self.shareURL = item;
            self.shareString = [(self.shareString ? self.shareString : @"") stringByAppendingFormat:@"%@%@",(self.shareString ? @" " : @""),[(NSURL *)item absoluteString]]; // concat, with space if already exists.
        }
        else NSLog(@"Unknown item type %@", item);
    }
}
- (UIViewController *) activityViewController {
    SLComposeViewController *fbController=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result){
        
        [fbController dismissViewControllerAnimated:YES completion:^{
            switch(result){
                case SLComposeViewControllerResultCancelled:
                default:
                {
                    [self activityDidFinish:NO];
                    NSLog(@"Cancelled.....");
                    return ;
                }
                    break;
                case SLComposeViewControllerResultDone:
                {
                    NSLog(@"Posted....");
                    [self activityDidFinish:YES];
                }
                    break;
            }
        }];
    };
    
    
    [fbController addImage:self.shareImage];
    [fbController setInitialText:self.shareString];
    [fbController addURL:self.shareURL];
    [fbController setCompletionHandler:completionHandler];
    return fbController;
}
- (void) performActivity {
    
}


@end