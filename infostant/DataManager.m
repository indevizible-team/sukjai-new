//
//  DataManager.m
//  Mugendai
//
//  Created by Nattawut Singhchai on 12/7/12 .
//  Copyright (c) 2012 Lekk . All rights reserved.
//

#import "DataManager.h"
//#import "NSData+Base64.h"
#import "User.h"
#import "NSStringAdditions.h"
#import "ActivitySet.h"
@implementation DataManager
@synthesize config,client;
-(id)init{
	self = [super init];
    if (self) {
		config =[PLIST readBundle:@"config.plist"];
		client= [DataManager client];
    }
    return self;
}
-(void)updateFeature:(NSString *)feature{
	NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
	[parameters setObject:[[config valueForKey:@"infofid"] valueForKey:feature] forKey:@"infofid"];
	[parameters setObject:@"1" forKey:@"json"];
	NSMutableURLRequest *myRequest = [client requestWithMethod:@"GET" path:@"getNewAllFeaturedata" parameters:parameters];
	AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:myRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
		if ([JSON count]) {
			[PLIST writeFeature:feature withData:JSON];
		}
	} failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
		NSLog(@"%@\n%@",error,JSON);
	}];
	[operation start];
}




+(NSString *)getFeatureDir:(NSString*)feature{
	NSDictionary* p =[PLIST readBundle:@"config.plist"];;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
	NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:[[p valueForKey:@"imageFolderName"] stringByAppendingPathComponent:  [NSString stringWithFormat:@"%@",[[p valueForKey:@"imageFolder"] valueForKey:feature]]]];
	return path;
	
}
+(NSString *)getDocumentDir{
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    //NSString *documentsDirectory = [paths objectAtIndex:0];
    //documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"InfostantPrivates"];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
	return [[paths objectAtIndex:0] stringByAppendingPathComponent:@"InfostantPrivates"];
}
+(CGFloat)txtRectCalc:(UITextView*)textField{
	CGRect frame = textField.frame;
    UIEdgeInsets inset = textField.contentInset;
    frame.size.height = textField.contentSize.height + inset.top + inset.bottom;
	textField.frame = frame;
	
	return textField.frame.size.height;
}
+(NSInteger)textHeight:(NSString*)text fromWidth:(NSInteger)width andFont:(UIFont*)font{
	UITextView *textView = [UITextView new];
	textView.text = text;
	textView.font = font;
	textView.frame = CGRectMake(0, 0, width, 2);
	[self txtRectCalc:textView];
	NSInteger theHeight = textView.frame.size.height;
//	textView = nil;
	return theHeight;
}

#pragma mark Calendar
+(NSString *)getDateNowString{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:@"gregorian"];
	[dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
	[dateFormatter setCalendar:gregorian];
	[dateFormatter setTimeZone: [NSTimeZone timeZoneWithName:@"GMT+7"]];
	NSString *sqlDate = [dateFormatter stringFromDate: [NSDate date]];
	return sqlDate;
}
+(NSString *)getDateFromDateString:(NSString *)date{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:@"gregorian"];
	[dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
	[dateFormatter setCalendar:gregorian];
	[dateFormatter setTimeZone: [NSTimeZone timeZoneWithName:@"GMT+7"]];
	NSDate *setDate = [dateFormatter dateFromString:date];
	[dateFormatter setDateFormat:@"HH:mm"];
	NSString *sqlDate = [dateFormatter stringFromDate: setDate];
	return sqlDate;
}


//+(void)updateImage:(UIProgressView *)pg success:(void (^)(BOOL))success{
//	NSLog(@"Start updating update image data");
//	NSMutableDictionary *param = [NSMutableDictionary new];
//	NSMutableDictionary *updateData = [PLIST readFeature:@"update"];
//	if ([updateData valueForKey:@"updateDate"]!=nil) {
//		NSString *dateString = [updateData valueForKey:@"updateDate"];
//		[param setValue:dateString forKey:@"createdate"];
//	}
//	[param setValue:@"1" forKey:@"json"];
//	NSDictionary *plist = [[DataManager new] config];
//	AFHTTPClient *client= [DataManager client];
//	NSURLRequest *request = [client requestWithMethod:@"GET" path:@"getcheckproduct" parameters:param];
//	[client setParameterEncoding:AFJSONParameterEncoding];
//	NSLog(@"%@",request);
//	AFJSONRequestOperation * operation =
//	[AFJSONRequestOperation JSONRequestOperationWithRequest:request
//													success:^(NSURLRequest * request, NSHTTPURLResponse * response, id JSON) {
//														NSLog(@"Finished get data image update");
//														if ([[JSON valueForKey:@"keydelete"] count]||[[JSON valueForKey:@"keyupdate"] count]) {
//															NSLog(@"Have something to delete...");
//															if ([[JSON valueForKey:@"keydelete"] count]) {
//																for (NSString *link in [JSON valueForKey:@"keydelete"]) {
//																	[FileManager removeDirectoryFromUrl:[NSURL URLWithString:link]];
//																}
//															}
//															if ([[JSON valueForKey:@"keyupdate"] count]) {
//																NSLog(@"Have a new image and updating...");
//																pg.hidden = NO;
//																[FileManager downloadMultiFile:[JSON valueForKey:@"keyupdate"] toProgress:pg success:^{
//																	NSLog(@"Success to download all image and upate date to data");
//																	success(YES);
//																	[PLIST writeFeature:@"update" withData:@{@"updateDate":[DataManager getDateNowString]}];
//																} fail:^{
//																	NSLog(@"Fail to download all image");
//																	success(NO);
//																}];
//															}
//														}else{
//															NSLog(@"Success and no to update");
//															success(YES);
//														}
//														
//													}
//													failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
//														success(NO);
//														NSLog(@"Fail to connect server");
//													}];
//	
//	[operation start];
//	
//
//}

+(NSString *)THB:(NSNumber *)amount{
//	NSNumberFormatter * formatter = [[NSNumberFormatter alloc] init];
//	formatter.numberStyle = NSNumberFormatterCurrencyStyle;
//    formatter.currencyCode = @"THB";
//	return [formatter stringFromNumber:amount];
	return [NSString stringWithFormat:@"%.2f THB",[amount floatValue]];
}
+(NSMutableDictionary *)JSONToMutableDictionary:(id)JSON{
    if (![JSON count]) {
        return [@{@"key": @[]} mutableCopy];
    }
    NSMutableDictionary *mutableCopy = (NSMutableDictionary *)CFBridgingRelease(CFPropertyListCreateDeepCopy(kCFAllocatorDefault, (CFDictionaryRef)JSON, kCFPropertyListMutableContainers));
    return mutableCopy;
}
+(NSMutableArray *)ConvertToMutableArray:(NSArray *)arr{
    NSMutableArray *mutableCopy = (NSMutableArray *)CFBridgingRelease(CFPropertyListCreateDeepCopy(kCFAllocatorDefault, (CFArrayRef)arr, kCFPropertyListMutableContainers));
    return mutableCopy;
}
+(AFHTTPClient *)client{
    NSDictionary *mainInfo = [[NSBundle mainBundle] infoDictionary];
    return [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:[mainInfo valueForKey:@"ajaxDomain"]]];
}
+(NSMutableDictionary *)user{
    NSMutableDictionary *_user = [PLIST readFeature:@"user"];
    if (_user.count > 1) {
        return _user;
    }
    return nil;
}

#pragma mark - font
+(UIFont *)fontSize:(NSString *)size{
    NSString *device;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        device = @"ipad";
    }else{
        device = @"iphone";
    }
    NSDictionary *fontList =  [[[NSBundle mainBundle] infoDictionary] valueForKey:@"font"];
if([[fontList valueForKey:device] valueForKey:size]==nil){
    return nil;
}
return [UIFont fontWithName:[[[fontList valueForKey:device] valueForKey:size] valueForKey:@"font"] size:[[[[fontList valueForKey:device] valueForKey:size] valueForKey:@"size"] floatValue]];
}
+(BOOL)isIpad{
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
}
+(NSMutableDictionary *)param{
    NSDictionary *mainInfo = [[NSBundle mainBundle] infoDictionary];
    NSNumber *infoid = [mainInfo valueForKey:@"infoid"];
    return [NSMutableDictionary dictionaryWithDictionary:@{@"json":@1,@"infoid":infoid}];
}
+(UIStoryboard *)getStoryboard{
    UIStoryboard *storyboard;
    NSDictionary *storyboardList = [[NSBundle mainBundle] infoDictionary];
    storyboard = [UIStoryboard storyboardWithName:[storyboardList valueForKey:@"UIMainStoryboardFile"] bundle:nil];
    return storyboard;
}
+(UIViewController *)controllerByID:(NSString *)name{
    return [[self getStoryboard] instantiateViewControllerWithIdentifier:name];
}

#pragma mark - image convertion
+(NSString *)base64FromUIImage:(UIImage *)image{
    NSData *image64 = UIImageJPEGRepresentation(image, 1.0);
    return [NSString base64StringFromData:image64 length:[image64 length]];
}

#pragma mark - arrayToDictionaryArray converter
+(NSDictionary *)arrayToDictionaryArray:(NSArray *)array{
    NSMutableDictionary *tmpDict = [NSMutableDictionary new];
    int count = 0;
    for (id data in array) {
        [tmpDict setValue:data forKey:[NSString stringWithFormat:@"%i",count++]];
    }
    return tmpDict;
}

#pragma mark - screen size
+(CGSize)getScreenSize{
    return  [[UIScreen mainScreen] bounds].size;
    
}
+(float)getHeightFromRatio:(float)ratio{
    return [self getScreenSize].width/ratio;
}
+(CGSize)getSizeFromRatio:(float)ratio{
    return CGSizeMake([self getScreenSize].width, [self getHeightFromRatio:ratio]);
}

+(void)setProfileImage:(UIImageView *)imageView{
   UIImage *placeHolder = [UIImage imageNamed:@"user_1.png" ];
    User *user =[User currentUser];
    if (user != nil) {
        [imageView setImageWithURL:user.picuser placeholderImage:placeHolder];
    }else{
         [imageView setImage:placeHolder];
    }
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
}

+(NSString *)numberToStr:(int)number{
	return (number > 1000000)?[NSString stringWithFormat:@"%im",(int)number/1000000]:((number > 1000)?[NSString stringWithFormat:@"%ik",(int)number/1000]:[NSString stringWithFormat:@"%i",number]);
}
+ (NSString *)urlEncodeValue:(NSString *)str
{
    NSString *result = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)str, NULL, CFSTR(":/?#[]@!$&’()*+,;="), kCFStringEncodingUTF8));
    return result;
}


+(UIActivityViewController *)activityViewControllerWithArray:(NSArray *)data{
    APActivityProvider *ActivityProvider = [[APActivityProvider alloc] init];
    NSMutableArray *Items = [@[ActivityProvider] mutableCopy];
    [Items addObjectsFromArray:data];
    
    NSMutableArray *Acts = [NSMutableArray new];
    [Acts addObjectsFromArray:[SLComposeActivity getAllActivity]];
    [Acts addObject:[QRCodeActivity new]];
    [Acts addObject:[BroadcastActivity new]];
    UIActivityViewController *ActivityView = [[UIActivityViewController alloc]
                                              initWithActivityItems:Items
                                              applicationActivities:Acts];
    return ActivityView;
}
+(NSString *)webViewString:(NSString *)string{
    return [DataManager webViewString:string  font:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
}
+(NSString *)webViewString:(NSString *)string font:(UIFont *)font{
    return [NSString stringWithFormat:
                   @"<html>" "<style type=\"text/css\">"
                   "body { font-family:%@; font-size:%f;}"
                   "</style>""<body>"
                   "<p>%@</p>"
                   "</body></html>",font.familyName,font.pointSize ,string];
}

@end

