//
//  Feature.h
//  www
//
//  Created by indevizible on 2/18/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Feature : NSObject
+(Feature *)featureWithData:(NSDictionary*)data;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *infofid;
@property (nonatomic, strong) NSString *pic;
@property (nonatomic, strong) NSURL *picURL;
@end
