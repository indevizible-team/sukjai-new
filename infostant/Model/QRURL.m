//
//  QRURL.m
//  เทส
//
//  Created by indevizible on 2/26/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "QRURL.h"

@implementation QRURL

+(QRURL *)dataWithURL:(NSString *)stringUrl{
    QRURL *_QRURL = [QRURL new];
    _QRURL.url = [NSURL URLWithString:stringUrl];
      NSArray *comp = [_QRURL.url pathComponents];
    _QRURL.isLocalLink = NO;
    if (([comp count] < 3)||([[_QRURL.url.query componentsSeparatedByString:@"="] count] < 2)) {
        NSLog(@"%u",comp.count);
        return _QRURL;
    }
    
    _QRURL.infoid = [[_QRURL.url.query componentsSeparatedByString:@"="] lastObject];
    if ([_QRURL.infoid isEqualToString:[[[NSBundle mainBundle] infoDictionary] valueForKey:@"infoid"]]) {
        _QRURL.isLocalLink = YES;
    }
    _QRURL.productid = [comp lastObject];
    _QRURL.infofid = [comp objectAtIndex:comp.count-2];
    return _QRURL;
}
@end
