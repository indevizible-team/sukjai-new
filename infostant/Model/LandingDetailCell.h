//
//  LandingImageCell.h
//  infostant
//
//  Created by INFOSTANT on 12/25/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface LandingDetailCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *subImage1;
@property (weak, nonatomic) IBOutlet UIImageView *subImage2;
@property (weak, nonatomic) IBOutlet UIImageView *subImage3;
@property (weak, nonatomic) IBOutlet UIImageView *subImage4;
@property (weak, nonatomic) IBOutlet UITextView *caption;
@property (weak, nonatomic) IBOutlet UIImageView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *playIcon;
@property (weak, nonatomic) IBOutlet UITextView *description;
@property (strong, nonatomic) IBOutlet UILabel *lblCaption;
@property (strong, nonatomic) IBOutlet UIImageView *bgCaption;
@property (strong, nonatomic) IBOutlet UIView *shadowView;
@property (nonatomic, assign)  int laidOut;
@property (weak, nonatomic) NSURL *youtubeUrl;
@end
