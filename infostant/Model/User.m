//
//  User.m
//  infostant
//
//  Created by indevizible on 2/6/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "User.h"
#import "DataManager.h"
@implementation User

+(User *)currentUser{
    
    NSMutableDictionary *data = [PLIST readFeature:@"user"];
    
    if ([data count]>1) {
        User *user = [[User alloc] init];
        user.data = data;
        user.oauth_login  = [data valueForKey:@"oauth_login"];
        if ([data valueForKey:@"id"]!=nil) {
            user.picuser = [NSURL URLWithString:[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large",[data valueForKey:@"id"]]];
        }else{
            user.picuser = [NSURL URLWithString:[data valueForKey:@"picuser"]];
        }
        user.name = [data valueForKey:@"name"];
        user.displayname = [data valueForKey:@"displayname"];
        user.mid = [data valueForKey:@"mid"];
        user.username = [data valueForKey:@"username"];
        user.countlike = [data valueForKey:@"countlike"];
        user.countfollower = [data valueForKey:@"countfollower"];
        user.countfollowing = [data valueForKey:@"countfollowing"];
        user.oauth_login_dict = @{@"oauth_login":[data valueForKey:@"oauth_login"]};
        user.isAdminUser = [data[@"useradmin"] isEqualToNumber:@1];
        return user;
    }
    return nil;
}

-(id)initWith:(id)data{
    User *user = [User new];
    if (data){
        user.data               = data;
        user.picuser            = ([data valueForKey:@"id"])?
                                    [NSURL URLWithString:[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large",[data valueForKey:@"id"]]]
                                    :[NSURL URLWithString:[data valueForKey:@"picuser"]];
        user.displayname        = [data valueForKey:@"displayname"];
        user.mid                = [data valueForKey:@"mid"];
        user.countlike          = [data valueForKey:@"countlike"];
        user.countfollower      = [data valueForKey:@"countfollower"];
        user.countfollowing     = [data valueForKey:@"countfollowing"];
        user.proname            = data[@"proname"];

    }
    return user;
}
+(void)UserWithId:(id)userId success:(void (^)(NSURLRequest *, NSHTTPURLResponse *, User *))success failure:(void (^)(NSURLRequest *, NSHTTPURLResponse *, NSError *, id))failure{
    NSMutableDictionary *param = [DataManager param];
    param[@"friendid"] = [NSString stringWithFormat:@"%@",userId];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"GET" path:@"getDetailByfriendid" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if([JSON count]){
            User *theUser = [[User alloc] initWith:JSON[JSON[@"key"][0]]];
            success(request,response,theUser);
        }else{
            failure(request,response,nil,JSON);
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        failure(request,response,error,JSON);
    }];
    [operation start];
}

@end
