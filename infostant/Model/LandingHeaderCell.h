//
//  LandingHeaderCell.h
//  infostant
//
//  Created by INFOSTANT on 12/24/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LandingHeaderCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UITextView *title;
@property (strong, nonatomic) IBOutlet UITextView *description;

@end
