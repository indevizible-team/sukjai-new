//
//  Noti.m
//  9Ent
//
//  Created by indevizible on 2/8/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "Noti.h"
#import "DataManager.h"
@implementation Noti
+(Noti *)dataSource{
    Noti *data = [Noti new];
    data.msg = @"0";
    data.friendrequest =@"0";
    data.notification = @"0";
    data.sum = 0;
    NSMutableDictionary *dic = [PLIST readFeature:@"noti"];
    if ([dic count]>1) {
        data.msg = [dic valueForKey:@"msg"];
        data.notification = [dic valueForKey:@"notification"];
        data.friendrequest = [dic valueForKey:@"friendrequest"];
        data.sum = [data.msg intValue]+[data.notification intValue]+[data.friendrequest intValue];
    }
    return data;
}
@end
