//
//  LandingFooterCell.h
//  infostant
//
//  Created by INFOSTANT on 12/24/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LandingFooterCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *profileImg;
@property (strong, nonatomic) IBOutlet UILabel *profileName;
@property (strong, nonatomic) IBOutlet UILabel *dateMod;
@property (strong, nonatomic) IBOutlet UILabel *likeNumber;
@property (strong, nonatomic) IBOutlet UILabel *followNumber;


@end
