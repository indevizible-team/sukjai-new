//
//  MsgChatCell.h
//  infostant
//
//  Created by NINE on 12/27/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MsgChatCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profileName;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UITextView *msgChat;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@property (strong, nonatomic) NSNumber *mid;
@end
