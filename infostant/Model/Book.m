//
//  Book.m
//  9Ent
//
//  Created by indevizible on 2/11/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "Book.h"
#import "DataManager.h"
@implementation Book
+(Book *)dataSourceFromDictionary:(NSDictionary *)data{
    if (data == nil) {
        return nil;
    }
    Book *book = [Book new];
    book.type = ([[data valueForKey:@"type"] isEqualToString:@"zave"])?DPBookTypeZave:DPBookTypePDF;
    book.thumbnailPic = [NSURL URLWithString:[data valueForKey:@"book_thumbnail_pic"]];
    book.name = [data valueForKey:@"book_name"];
    book.screenShot = [data valueForKey:@"screenshot"];
    book.path = [NSURL URLWithString:[data valueForKey:(book.type == DPBookTypeZave)?@"pathzave":@"path"]];
    book.pathc = [NSURL URLWithString:[data valueForKey:@"pathzavec"]];
    book.id = [data valueForKey:@"id"];
    book.price = [data valueForKey:@"price"];
    book.size = [data valueForKey:@"size"];
    book.description = [data valueForKey:@"book_description"];
    book.isNew = ([[data valueForKey:@"new"] isEqualToString:@"old"])?NO:YES;
    return book;
}
@end
