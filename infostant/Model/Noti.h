//
//  Noti.h
//  9Ent
//
//  Created by indevizible on 2/8/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Noti : NSObject
@property (nonatomic, strong) NSString *msg,*friendrequest,*notification;
@property int sum;
+(Noti *)dataSource;

@end
