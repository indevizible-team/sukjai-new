//
//  User.h
//  infostant
//
//  Created by indevizible on 2/6/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
-(id)initWith:(id)data;
+(User *)currentUser;
+(void)UserWithId:(id)userId
                                        success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, User *user))success
                                        failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON))failure;
@property (nonatomic,strong) NSDictionary *oauth_login_dict;
@property (nonatomic,strong)  NSMutableDictionary *data;
@property (nonatomic,strong)  NSString *oauth_login;
@property (nonatomic,strong)   NSURL* picuser;
@property (nonatomic,strong) NSString *name,*mid,*username,*displayname,*countlike,*countfollower,*countfollowing,*proname;
@property (nonatomic,assign) BOOL isAdminUser;
@end
