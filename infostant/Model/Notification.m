//
//  Notification.m
//  www
//
//  Created by indevizible on 2/17/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "Notification.h"

@implementation Notification

+(Notification *)dataSourceWithData:(NSDictionary*)data{
    Notification *noti = [Notification new];
    if (data != nil) {
        if ([[data valueForKey:@"type"] isEqualToString:@"1"]) {
            noti.type = NotiTypeNormal;
            noti.productid = data[@"ref"][@"productid"];
            noti.infofid = data[@"ref"][@"infofid"];
        }else if([[data valueForKey:@"type"] isEqualToString:@"2"]){
            noti.type =  NotiTypeExternalLink;
        }else{
            noti.type = NotiTypeMessage;
        }
        if ([data[@"pic"] length]) {
             noti.pic = [NSURL URLWithString:data[@"pic"]];
        }
        noti.picuser = [NSURL URLWithString:data[@"picuser"]];
        noti.title = data[@"title"];
        noti.createdate = data[@"createdate"];
        noti.displayname = ([data[@"displayname"] length] > 0)?data[@"displayname"]:data[@"username"];
        noti.msg = data[@"msg"];
        noti.link = data[@"link"];
        return noti;
    }

    return nil;
}
@end
