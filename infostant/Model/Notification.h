//
//  Notification.h
//  www
//
//  Created by indevizible on 2/17/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum {
    NotiTypeNormal,
    NotiTypeExternalLink,
    NotiTypeMessage
}NotificationType;
@interface Notification : NSObject
@property (assign, nonatomic) NotificationType type;
@property (strong, nonatomic) NSURL *picuser;
@property (strong, nonatomic) NSString *displayname;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *createdate;
@property (strong, nonatomic) NSURL *pic;
@property (strong, nonatomic) NSString *msg;
@property (strong, nonatomic) NSString *productid;
@property (strong, nonatomic) NSString *infofid;
@property (strong, nonatomic) NSString *link;
+(Notification *)dataSourceWithData:(NSDictionary*)data;
@end
