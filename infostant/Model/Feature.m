//
//  Feature.m
//  www
//
//  Created by indevizible on 2/18/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "Feature.h"

@implementation Feature
+(Feature *)featureWithData:(NSDictionary *)data{
    Feature *_feature = [Feature new];
    _feature.name = data[@"name"];
    _feature.infofid = data[@"infofid"];
    _feature.pic = data[@"pic"];
    _feature.picURL = [NSURL URLWithString:data[@"pic"]];
    return _feature;
}
@end
