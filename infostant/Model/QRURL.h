//
//  QRURL.h
//  เทส
//
//  Created by indevizible on 2/26/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QRURL : NSObject
@property (nonatomic,strong) NSURL *url;
@property (nonatomic,strong) NSString *infoid;
@property (nonatomic,strong) NSString *infofid;
@property (nonatomic,strong) NSString *productid;
@property (nonatomic,assign) BOOL isLocalLink;

+(QRURL *)dataWithURL:(NSString *)stringUrl;
@end
