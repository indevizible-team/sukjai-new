//
//  LandingHeaderCell.m
//  infostant
//
//  Created by INFOSTANT on 12/24/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import "LandingHeaderCell.h"
#import "DataManager.h"
@implementation LandingHeaderCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}
-(void)layoutSubviews{
	[super layoutSubviews];
    [DataManager txtRectCalc:self.title];
    CGRect desrect = self.description.frame;
    desrect.origin.y = self.title.frame.origin.y+self.title.frame.size.height-10;
    self.description.frame = desrect;
	[DataManager txtRectCalc:self.description];
}
@end
