//
//  MainCell.h
//  CVCV
//
//  Created by Nattawut Singhchai on 12/21/12 .
//  Copyright (c) 2012 Nattawut Singhchai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedFeatureCell : UICollectionViewCell<UICollectionViewDataSource,UICollectionViewDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *subCV;
@property (strong, nonatomic) IBOutlet UILabel *lblFeatureName;
@property (strong, nonatomic) IBOutlet UIButton *btnSeeallFeat;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) NSString *infofid;
@property NSUInteger rowCount;
@end
