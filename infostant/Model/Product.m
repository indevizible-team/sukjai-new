//
//  Product.m
//  www
//
//  Created by indevizible on 2/13/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "Product.h"
@implementation Banner
-(id)initWith:(id)data{
    if (self) {
        self.link = [NSURL URLWithString:data[@"link"]];
        self.image = [NSURL URLWithString:data[@"image"]];
        self.width = [data[@"width"] integerValue];
        self.height = [data[@"height"] integerValue];
    }
    return self;
}
@end
@implementation Product

-(Product *)initWithDictionary:(NSDictionary *)data{
    Product *tmpProduct = [Product new];
    tmpProduct.title            = data[@"title"];
    tmpProduct.description      = data[@"description"];
    tmpProduct.address          = data[@"address"];
    tmpProduct.condition        = data[@"conditionname"];
    tmpProduct.email            = data[@"email"];
    tmpProduct.phone            = data[@"phone"];
    tmpProduct.startDate        = data[@"startdate"];
    tmpProduct.endDate          = data[@"enddate"];
    tmpProduct.displayName      = data[@"displayname"];
    tmpProduct.date             = data[@"date"];
    tmpProduct.updateDate       = data[@"updatedate"];
    tmpProduct.likeCount        = data[@"countlike"];
    tmpProduct.followCont       = data[@"countfollow"];
    tmpProduct.salePrice        = data[@"saleprice"];
    tmpProduct.regularPrice     = data[@"regularprice"];
    tmpProduct.productid        = data[@"productid"];
    tmpProduct.mid              = data[@"mid"];
    tmpProduct.shopURL          = [NSURL URLWithString:data[@"fullshopurl"]];
    tmpProduct.pic              = data[@"pic"];
    
    
    if (data[@"bannerlanding"]) {
        tmpProduct.bannerlanding = [[Banner alloc] initWith:data[@"bannerlanding"]];
    }
//    tmpProduct.likeText = @"Like";
//    tmpProduct.followText = @"Follow";
//    

    
    
       return tmpProduct;
}


@end
