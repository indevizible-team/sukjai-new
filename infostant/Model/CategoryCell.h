//
//  CategoryCell.h
//  infostant
//
//  Created by INFOSTANT on 12/25/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *description;
//@property (weak, nonatomic) IBOutlet UILabel *description;
@property (weak, nonatomic) IBOutlet UILabel *ownerName;
@property (weak, nonatomic) IBOutlet UILabel *followNo;
@property (weak, nonatomic) IBOutlet UILabel *likeNo;
@property (strong, nonatomic) NSMutableDictionary *product;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIProgressView *progress;
@property (strong, nonatomic) IBOutlet UIView *shadowView;
@property (nonatomic, assign)  int laidOut;
@end
