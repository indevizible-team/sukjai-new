//
//  FollowListCell.h
//  infostant
//
//  Created by INFOSTANT on 12/25/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowListCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblLocate;
@property (strong, nonatomic) IBOutlet UILabel *caption;
@property (strong, nonatomic) IBOutlet UILabel *lastUpdate;
@property (strong, nonatomic) IBOutlet UIImageView *prifilePic;

@property (strong, nonatomic) NSString *folid;
@end
