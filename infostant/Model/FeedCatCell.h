//
//  SubCell.h
//  CVCV
//
//  Created by Nattawut Singhchai on 12/21/12 .
//  Copyright (c) 2012 Nattawut Singhchai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedCatCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UIImageView *profilePic;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end
