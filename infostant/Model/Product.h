//
//  Product.h
//  www
//
//  Created by indevizible on 2/13/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface Banner : NSObject
@property (nonatomic,strong) NSURL *image,*link;
@property (nonatomic,assign) NSInteger width;
@property (nonatomic,assign) NSInteger height;
@end
@interface Product : NSObject
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *description;
@property (nonatomic,strong) NSString *address;
@property (nonatomic,strong) NSString *condition;
@property (nonatomic,strong) NSString *email;
@property (nonatomic,strong) NSString *phone;
@property (nonatomic,strong) NSString *startDate;
@property (nonatomic,strong) NSString *endDate;
@property (nonatomic,strong) NSString *displayName;
@property (nonatomic,strong) NSString *date;
@property (nonatomic,strong) NSString *updateDate;
@property (nonatomic,strong) NSString *likeCount;
@property (nonatomic,strong) NSString *followCont;
@property (nonatomic,strong) NSString *salePrice;
@property (nonatomic,strong) NSString *regularPrice;
@property (nonatomic,strong) Banner *bannerlanding;
@property BOOL isLike,isFollow;
@property (nonatomic,strong) NSString *likeText,*followText;
-(Product *)initWithDictionary:(NSDictionary *)data;

@property (nonatomic,strong) NSString *mid;
@property (nonatomic,strong) NSURL *shopURL;
@property (nonatomic,strong) NSString *productid;
@property (nonatomic,strong) NSString *pic;
@end
