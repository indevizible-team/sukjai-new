//
//  MainCell.m
//  CVCV
//
//  Created by Nattawut Singhchai on 12/21/12 .
//  Copyright (c) 2012 Nattawut Singhchai. All rights reserved.
//

#import "FeedFeatureCell.h"
#import "FeedCatCell.h"
@implementation FeedFeatureCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code

    }
    return self;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
	return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section	{
    
	return 0;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    FeedCatCell *cell = (FeedCatCell*)[cv dequeueReusableCellWithReuseIdentifier: @"SubCell" forIndexPath:indexPath];
    return cell;
}
@end
