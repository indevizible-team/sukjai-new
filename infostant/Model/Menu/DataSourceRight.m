//
//  DataSource.m
//  socioville
//
//  Created by Valentin Filip on 10.04.2012.
//  Copyright (c) 2012 App Design Vault. All rights reserved.
//

#import "DataSourceRight.h"
#import "MenuItemRight.h"

@implementation DataSourceRight

@synthesize items;

+ (DataSourceRight *)dataSource {
    DataSourceRight *source = [[DataSourceRight alloc] init];
    
    NSArray *rawItemsFeature = @[
                                   @{ @"name": @""   ,@"needToLogin":@NO  ,@"controllerID":@"FeatureVC"},
                                   @{ @"name": @""   ,@"needToLogin":@NO  ,@"controllerID":@"NewFeed"},
                                   @{ @"name": @""    ,@"needToLogin":@NO  ,@"controllerID":@"MessageVC" },
                                   @{ @"name": @"" ,@"needToLogin":@NO  ,@"controllerID":@"FollowVC" },
                                   @{ @"name": @""   ,@"needToLogin":@NO  ,@"controllerID":@"LikeVC"},
                                   @{ @"name": @""  ,@"needToLogin":@NO  ,@"controllerID":@"BookShelf"}];
    
    
    NSArray *rawItems=[[NSArray alloc] initWithObjects:rawItemsFeature, nil];
    source.items = [NSArray arrayWithArray:rawItems];
    
    return source;

}
@end
