//
//  MenuItem.m
//  socioville
//
//  Created by Valentin Filip on 10.04.2012.
//  Copyright (c) 2012 App Design Vault. All rights reserved.
//

#import "MenuItem.h"

@implementation MenuItem

@synthesize name, image, imageName, eventCount ,controllerID,needToLogin;

+ (MenuItem *)itemWithData:(NSDictionary *)data {
    MenuItem *item = [[MenuItem alloc] init];
    item.name = data[@"name"];
    item.eventCount = data[@"eventCount"];
    item.imageName = data[@"imageName"];
    if (item.imageName) {
        item.image = [UIImage imageNamed:item.imageName];
    }
    item.needToLogin = data[@"needToLogin"];
    item.controllerID = data[@"controllerID"];
    item.path = data[@"path"];
    item.loginAction = ([data[@"action"] isEqualToString:@"login"]);
    item.linkShelf = [data[@"linkShelf"] boolValue];
    return item;
}

@end
