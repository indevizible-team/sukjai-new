//
//  DataSource.m
//  socioville
//
//  Created by Valentin Filip on 10.04.2012.
//  Copyright (c) 2012 App Design Vault. All rights reserved.
//

#import "DataSource.h"
#import "MenuItem.h"
#import "DataManager.h"
#import "Noti.h"
@implementation DataSource

@synthesize items;

+ (DataSource *)dataSource {
    DataSource *source = [[DataSource alloc] init];
    NSDictionary *user = [PLIST readFeature:@"user"];
    Noti *noti = [Noti dataSource];
NSDictionary *leftMenu =  [[[NSBundle mainBundle] infoDictionary] valueForKey:@"LeftMenu"];
        leftMenu = ([DataManager isIpad])?[leftMenu valueForKey:@"ipad"]:[leftMenu valueForKey:@"iphone"];
    
    
    NSArray *menu = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"menu"] objectForKey:(([DataManager isIpad])?@"ipad":@"iphone")];
  
    
            UIApplication *application = [UIApplication sharedApplication];
            application.applicationIconBadgeNumber = noti.sum;
    NSArray *indexOfFeature = @[@"home",@"feed",@"msg",@"follow",@"noti",@"like",@"broadcast",@"mytrip",@"qrreader",@"TOC"];
    NSArray *indexOfPanel = @[@"addContent",@"draft",@"settings",@"login"];
    NSArray *indexOfMenu = @[indexOfFeature,indexOfPanel];
    NSArray *protoMenu = @[
                           @[
                               @{ @"name": NSLocalizedString(@"infoHome", @"")      , @"imageName": @"iconMenuHome.png"    ,@"needToLogin":@NO      ,@"controllerID":@"FeatureVC"}
                               ,@{ @"name": NSLocalizedString(@"infoFeed", @"")      , @"imageName": @"iconMenuFeed.png"    ,@"needToLogin":@YES     ,@"controllerID":@"NewFeed"}
                               ,@{ @"name": NSLocalizedString(@"infoMsg", @"")       , @"imageName": @"iconMenuMessage.png" ,@"needToLogin":@YES     ,@"controllerID":@"MessageVC", @"eventCount":noti.msg ,@"path":@"readmsgcenter"}
                               ,@{ @"name": NSLocalizedString(@"infoFollow", @"")    , @"imageName": @"iconMenuFollow.png"  ,@"needToLogin":@YES     ,@"controllerID":@"FollowVC", @"eventCount":noti.friendrequest ,@"path":@"readfollowcenter"}
                               ,@{ @"name": NSLocalizedString(@"infoNoti", @"")      , @"imageName": @"iconMenuFollow.png"  ,@"needToLogin":@YES     ,@"controllerID":@"NotiVC", @"eventCount":noti.notification ,@"path":@"readnotifcationcenter"}
                               ,@{ @"name": NSLocalizedString(@"infoLike", @"")      , @"imageName": @"iconMenuLiked.png"   ,@"needToLogin":@YES     ,@"controllerID":@"LikeVC"}
                               ,@{ @"name": NSLocalizedString(@"infoBroadCast", @""), @"imageName": @"iconMenuFeed.png"    ,@"needToLogin":@YES     ,@"controllerID":@"BroadCastVC",@"needSuper":@YES},
                           
                               @{ @"name": NSLocalizedString(@"infoMyTrip", @"")   , @"imageName": @"iconMenuShelf.png"   ,@"needToLogin":@YES,@"controllerID":@"TripVC"},
                               @{ @"name": NSLocalizedString(@"infoQRReader", @"")   , @"imageName": @"iconMenuShelf.png"   ,@"needToLogin":@YES,@"controllerID":@"QRReaderVC"},
                               @{ @"name": NSLocalizedString(@"infoHome", @"")      , @"imageName": @"iconMenuHome.png"    ,@"needToLogin":@NO      ,@"controllerID":@"FeatureVC"}
                               ],
                                @[
                                @{@"name": NSLocalizedString(@"infoAdd", @"") , @"imageName": @"iconMenuAdd.png"     ,@"needToLogin":@YES  ,@"controllerID":@"AddProductViewController"}
                                ,@{@"name": NSLocalizedString(@"infoDraft", @""), @"imageName": @"iconMenuDraft.png"   ,@"needToLogin":@YES ,@"controllerID":@"DraftViewController"}
                                ,@{@"name": NSLocalizedString(@"infoSettingf", @"")       , @"imageName": @"iconMenuSetting.png" ,@"needToLogin":@NO ,@"controllerID":@"SettingVC"}
                                ,@{@"name": NSLocalizedString(([user count]>1?@"infoLogout":@"infoLogin"), @"")          , @"imageName": ([user count]>1)?@"iconMenuLogout.png" :@"iconMenuLogin.png"   ,@"needToLogin":@NO ,@"controllerID":@"",@"action":@"login"}
                               ]
                           ];
    
    
    NSMutableArray *sectionMenu = [NSMutableArray new];
    for (int section =0; section < menu.count; section++) {
        NSMutableArray *rowMenu = [NSMutableArray new];
        for (int row = 0; row < [menu[section] count]; row++) {
            NSUInteger selectedRow = [indexOfMenu[section] indexOfObject:menu[section][row]];
            BOOL needSuper = protoMenu[section][selectedRow][@"needSuper"]!=nil;
            if ((needSuper && [User currentUser].isAdminUser)||!needSuper) {
                [rowMenu addObject:protoMenu[section][selectedRow]];
            }
        }
        [sectionMenu addObject:rowMenu];
    }
    source.items = [NSArray arrayWithArray:sectionMenu];
    return source;
}
@end
