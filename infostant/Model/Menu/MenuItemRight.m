//
//  MenuItem.m
//  socioville
//
//  Created by Valentin Filip on 10.04.2012.
//  Copyright (c) 2012 App Design Vault. All rights reserved.
//

#import "MenuItemRight.h"

@implementation MenuItemRight

@synthesize name, image, imageName, eventCount;

+ (MenuItemRight *)itemWithData:(NSDictionary *)data {
    MenuItemRight *item = [[MenuItemRight alloc] init];
    item.name = data[@"name"];
    return item;
}

@end
