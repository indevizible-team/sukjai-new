//
//  Book.h
//  9Ent
//
//  Created by indevizible on 2/11/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum{
    DPBookTypeZave,
    DPBookTypePDF
}bookType;
@interface Book : NSObject
@property bookType type;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSURL *thumbnailPic;
@property (nonatomic, strong) NSArray *screenShot;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *size;
@property (nonatomic, strong) NSString *id;
//download Path
@property (nonatomic, strong) NSURL *path;
@property (nonatomic, strong) NSURL *pathc;
@property BOOL isNew;
+(Book *)dataSourceFromDictionary:(NSDictionary *)data;

@end
