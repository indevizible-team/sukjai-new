//
//  DataManager.h
//
//  Created by Nattawut Singhchai on 12/7/12 .
//
#define CONFIG_PLIST @"infoconfig.plist"
#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperation.h"
#import "AFJSONRequestOperation.h"
#import "AFHTTPClient.h"
#import "UIImageView+AFNetworking.h"
#import "FileManager.h"
#import "PLIST.h"
#import "User.h"
#import "DataController.h"
#import "SVProgressHUD.h"
#import "LoginNavController.h"
@interface DataManager : NSObject
@property(retain,nonatomic) NSDictionary *config;
@property(retain,nonatomic) NSString *ajaxPath;
@property(retain,nonatomic) AFHTTPClient* client;

#pragma mark - String Manager
+(NSString*)THB:(NSNumber*)amount;

#pragma mark Calendar
+(NSString*)getDateNowString;
+(NSString*)getDateFromDateString:(NSString*)date;

#pragma mark Back Controller
+(NSString *)getFeatureDir:(NSString*)feature;
+(NSString *)getDocumentDir;
+(CGFloat)txtRectCalc:(UITextView*)textField;
+(NSInteger)textHeight:(NSString*)text fromWidth:(NSInteger)width andFont:(UIFont*)font;
+(NSMutableDictionary *)JSONToMutableDictionary:(id)JSON;
+(AFHTTPClient *)client;
+(NSMutableDictionary *)user;

#pragma mark - font
+(UIFont *)fontSize:(NSString *)size;

#pragma mark - parameter
+(NSMutableDictionary *)param;

#pragma mark - storyboard
+(UIStoryboard *)getStoryboard;
+(UIViewController *)controllerByID:(NSString *)name;

#pragma mark - image convertion
+(NSString *)base64FromUIImage:(UIImage *)image;

#pragma mark - arrayToDictionaryArray converter
+(NSDictionary *)arrayToDictionaryArray:(NSArray *)array;

#pragma mark - screen size
+(CGSize)getScreenSize;
+(float)getHeightFromRatio:(float)ratio;
+(CGSize)getSizeFromRatio:(float)ratio;

+(void)setProfileImage:(UIImageView*)imageView;
+(BOOL)isIpad;
+(NSString *)numberToStr:(int)number;
+ (NSString *)urlEncodeValue:(NSString *)str;

+(UIActivityViewController *)activityViewControllerWithArray:(NSArray *)data;
+(NSString *)webViewString:(NSString *)string;
+(NSString *)webViewString:(NSString *)string font:(UIFont *)font;
@end

