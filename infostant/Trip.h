//
//  Trip.h
//  www
//
//  Created by Trash on 3/12/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Trip : NSObject
@property (nonatomic,strong) NSString *id;
@property (nonatomic,strong) NSURL *image;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *dateString;
@property (nonatomic,assign) BOOL isOwner;
@property (nonatomic,assign) float price;
@property (nonatomic,strong) NSString *priceText;
@property (nonatomic,strong) NSString *currency;
@property (nonatomic,assign) int duration;
@property (nonatomic,strong) NSString *durationText;
@property (nonatomic,strong) NSString *code;
@property (nonatomic,strong) NSNumber *status;
@property (nonatomic,strong) NSURL *tel;
@property (nonatomic,strong) NSString *telString;
@property (nonatomic,strong) NSString *infoText;
@property (nonatomic,assign) BOOL lock;
@property (nonatomic,strong) NSArray *tagKey;
@property (nonatomic,strong) NSDictionary *tagData;
+(Trip *)tripWithData:(NSDictionary *)data;
@end
