//
//  FacebookConnectViewController.m
//  infostant
//
//  Created by indevizible on 1/21/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "FacebookConnectViewController.h"

@interface FacebookConnectViewController ()

@end

@implementation FacebookConnectViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    
}
-(void)fetchData{
    [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary *user, NSError *error) {
        if (!error) {
           
            NSLog(@"user-%@",user);
            NSLog(@"class %@",self.profileImage);
            self.profileImage.profileID =user[@"id"];
        
            NSLog(@"name\t%@",user[@"name"]);
            NSLog(@"first_name\t%@",user[@"first_name"]);
            NSLog(@"last_name\t%@",user[@"last_name"]);
            NSLog(@"gender\t%@",user[@"gender"]);
             NSLog(@"id\t%@",user[@"id"]);
             NSLog(@"email\t%@",user[@"email"]);
             NSLog(@"username\t%@",user[@"username"]);
        }
    }];
    
//    [FBRequestConnection startWithGraphPath:@"/me/feed" completionHandler:^(FBRequestConnection *connection,id<FBGraphUser> user, NSError *error) {
//        if (!error) {
//            NSLog(@"user-%@",user);
//        }
//    }];
//    [FBRequestConnection startWithGraphPath:@"/me" parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id<FBGraphUser> user, NSError *error) {
//        NSLog(@"data : %@ ",user);
//    }];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - FBLoginViewDelegate

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
    // first get the buttons set for login mode
//    self.buttonPostPhoto.enabled = YES;
//    self.buttonPostStatus.enabled = YES;
//    self.buttonPickFriends.enabled = YES;
//    self.buttonPickPlace.enabled = YES;
}

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
    // here we use helper properties of FBGraphUser to dot-through to first_name and
    // id properties of the json response from the server; alternatively we could use
    // NSDictionary methods such as objectForKey to get values from the my json object
//    self.labelFirstName.text = [NSString stringWithFormat:@"Hello %@!", user.first_name];
    // setting the profileID property of the FBProfilePictureView instance
    // causes the control to fetch and display the profile picture for the user
//    self.profilePic.profileID = user.id;
    self.loggedInUser = user;
    NSLog(@"-------%@",user);
}

- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
    BOOL canShareAnyhow = [FBNativeDialogs canPresentShareDialogWithSession:nil];
    NSLog(@"can share any how = %u",canShareAnyhow);
//    self.buttonPostPhoto.enabled = canShareAnyhow;
//    self.buttonPostStatus.enabled = canShareAnyhow;
//    self.buttonPickFriends.enabled = NO;
//    self.buttonPickPlace.enabled = NO;
//    
//    self.profilePic.profileID = nil;
//    self.labelFirstName.text = nil;
//    self.loggedInUser = nil;
}
@end
