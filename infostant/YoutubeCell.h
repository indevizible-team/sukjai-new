//
//  YoutubeCell.h
//  Youtube
//
//  Created by Nattawut Singhchai on 12/27/12 .
//  Copyright (c) 2012 Nattawut Singhchai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <QuartzCore/QuartzCore.h>
#import "HCYoutubeParser.h"
@interface YoutubeCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *playButton;
@property (strong, nonatomic) NSString *youtubeURL;
@property (strong, nonatomic) IBOutlet UILabel *theLabel;
@property (strong, nonatomic) NSURL *urlToLoad;
@end
