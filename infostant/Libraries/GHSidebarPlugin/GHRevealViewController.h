//
//  GHSidebarViewController.h
//  GHSidebarNav
//
//  Created by Greg Haines on 11/20/11.
//

#import <Foundation/Foundation.h>
#import "MenuViewController.h"
#import "MenuRightViewController.h"
#import "InfostantNavigationViewController.h"
extern const NSTimeInterval kGHRevealSidebarDefaultAnimationDuration;
extern const CGFloat kGHRevealSidebarWidth;

@interface GHRevealViewController : UIViewController {
@private
    UIView *_sidebarRightView;
	UIView *_sidebarView;
	UIView *_contentView;
	UITapGestureRecognizer *_tapRecog;
}

@property (nonatomic, readonly, getter = isSidebarShowing) BOOL leftSidebarShowing;
@property (nonatomic, readonly, getter = isSidebarShowing) BOOL rightSidebarShowing;
@property (nonatomic, readonly, getter = isSearching) BOOL searching;
@property (strong, nonatomic) MenuViewController *sidebarViewController;
@property (strong, nonatomic) MenuRightViewController *sidebarRightViewController;
@property (strong, nonatomic) InfostantNavigationViewController *contentViewController;

- (void)dragContentView:(UIPanGestureRecognizer *)panGesture;
- (void)leftDragContentView:(UIPanGestureRecognizer *)panGesture;
- (void)toggleSidebar:(BOOL)show duration:(NSTimeInterval)duration;
- (void)toggleSidebar:(BOOL)show duration:(NSTimeInterval)duration completion:(void (^)(BOOL finished))completion;
- (void)toggleRightSidebar:(BOOL)show duration:(NSTimeInterval)duration;
- (void)toggleRightSidebar:(BOOL)show duration:(NSTimeInterval)duration completion:(void (^)(BOOL finished))completion;

@end
