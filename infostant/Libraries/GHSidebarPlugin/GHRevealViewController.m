//
//  GHSidebarViewController.m
//  GHSidebarNav
//
//  Created by Greg Haines on 11/20/11.
//

#import "GHRevealViewController.h"
#import <QuartzCore/QuartzCore.h>


#pragma mark -
#pragma mark Constants
const NSTimeInterval kGHRevealSidebarDefaultAnimationDuration = 0.25;
const CGFloat kGHRevealSidebarWidth = 260.0f;
const CGFloat kGHRevealSidebarFlickVelocity = 1000.0f;


#pragma mark -
#pragma mark Private Interface
@interface GHRevealViewController ()
@property (nonatomic, readwrite, getter = isLeftSidebarShowing) BOOL leftSidebarShowing;
@property (nonatomic, readwrite, getter = isRightSidebarShowing) BOOL rightSidebarShowing;
@property (nonatomic, readwrite, getter = isSearching) BOOL searching;
@property (nonatomic, strong) UIView *searchView;
- (void)hideSidebar;
@end


#pragma mark -
#pragma mark Implementation
@implementation GHRevealViewController

#pragma mark Properties
@synthesize leftSidebarShowing;
@synthesize rightSidebarShowing;
@synthesize sidebarViewController;
@synthesize sidebarRightViewController;
@synthesize contentViewController;

- (void)setSidebarViewController:(MenuViewController *)svc {
    
	if (sidebarViewController == nil) {
        CGRect tmpBound = _sidebarView.bounds;
        tmpBound.size.width = kGHRevealSidebarWidth;
        svc.view.frame = tmpBound;
		sidebarViewController = svc;
		[self addChildViewController:sidebarViewController];
		[_sidebarView addSubview:sidebarViewController.view];
		[sidebarViewController didMoveToParentViewController:self];
        
	} else if (sidebarViewController != svc) {
        CGRect tmpBound = _sidebarView.bounds;
        tmpBound.size.width = kGHRevealSidebarWidth;
        svc.view.frame = tmpBound;
        
        [sidebarViewController willMoveToParentViewController:nil];
		[self addChildViewController:svc];
		self.view.userInteractionEnabled = NO;
		[self transitionFromViewController:sidebarViewController
						  toViewController:svc
								  duration:0
								   options:UIViewAnimationOptionTransitionNone
								animations:^{}
								completion:^(BOOL finished){
									self.view.userInteractionEnabled = YES;
									[sidebarViewController removeFromParentViewController];
									[svc didMoveToParentViewController:self];
									sidebarViewController = svc;
								}
		 ];
	}
}

- (void)setSidebarRightViewController:(MenuRightViewController *)src {
    
	if (sidebarRightViewController == nil) {
        CGRect tmpBound = _sidebarRightView.bounds;
        tmpBound.size.width = kGHRevealSidebarWidth;
        src.view.frame = tmpBound;
		sidebarRightViewController = src;
		[self addChildViewController:sidebarRightViewController];
		[_sidebarRightView addSubview:sidebarRightViewController.view];
		[sidebarRightViewController didMoveToParentViewController:self];
        
	} else if (sidebarRightViewController != src) {
        CGRect tmpBound = _sidebarRightView.bounds;
        tmpBound.size.width = kGHRevealSidebarWidth;
        src.view.frame = tmpBound;
        
        [sidebarRightViewController willMoveToParentViewController:nil];
		[self addChildViewController:src];
		self.view.userInteractionEnabled = NO;
		[self transitionFromViewController:sidebarRightViewController
						  toViewController:src
								  duration:0
								   options:UIViewAnimationOptionTransitionNone
								animations:^{}
								completion:^(BOOL finished){
									self.view.userInteractionEnabled = YES;
									[sidebarRightViewController removeFromParentViewController];
									[src didMoveToParentViewController:self];
									sidebarRightViewController = src;
								}
		 ];
	}
}

- (void)setContentViewController:(UIViewController *)cvc {
	if (contentViewController == nil) {
		cvc.view.frame = _contentView.bounds;
		contentViewController = cvc;
		[self addChildViewController:contentViewController];
		[_contentView addSubview:contentViewController.view];
		[contentViewController didMoveToParentViewController:self];
	} else if (contentViewController != cvc) {
		cvc.view.frame = _contentView.bounds;
		[contentViewController willMoveToParentViewController:nil];
		[self addChildViewController:cvc];
		self.view.userInteractionEnabled = NO;
		[self transitionFromViewController:contentViewController
						  toViewController:cvc
								  duration:0
								   options:UIViewAnimationOptionTransitionNone
								animations:^{}
								completion:^(BOOL finished){
									self.view.userInteractionEnabled = YES;
									[contentViewController removeFromParentViewController];
									[cvc didMoveToParentViewController:self];
									contentViewController = cvc;
								}
         ];
	}
}

- (void)viewDidLoad
{
    
    self.leftSidebarShowing = NO;
    self.rightSidebarShowing = NO;
    self.searching = NO;
    _tapRecog = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideSidebar)];
    _tapRecog.cancelsTouchesInView = YES;
    
    self.view.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    
    _sidebarView = [[UIView alloc] initWithFrame:self.view.bounds];
    _sidebarView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    _sidebarView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_sidebarView];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        _sidebarRightView = [[UIView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x+(self.view.bounds.size.width-kGHRevealSidebarWidth), self.view.bounds.origin.y, kGHRevealSidebarWidth, self.view.bounds.size.height)];
    }
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        _sidebarRightView = [[UIView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x+60, self.view.bounds.origin.y, kGHRevealSidebarWidth, self.view.bounds.size.height)];
    }
    _sidebarRightView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    _sidebarRightView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_sidebarRightView];
    
    _contentView = [[UIView alloc] initWithFrame:self.view.bounds];
    _contentView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    _contentView.backgroundColor = [UIColor clearColor];
    _contentView.layer.masksToBounds = NO;
    _contentView.layer.shadowColor = [UIColor blackColor].CGColor;
    _contentView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    _contentView.layer.shadowOpacity = 1.0f;
    _contentView.layer.shadowRadius = 2.5f;
    _contentView.layer.shadowPath = [UIBezierPath bezierPathWithRect:_contentView.bounds].CGPath;
    [self.view addSubview:_contentView];
    
    if (sidebarViewController) {
        MenuViewController *temp = sidebarViewController;
        sidebarViewController = nil;
        self.sidebarViewController = temp;
    }
    
    if (sidebarRightViewController) {
        MenuRightViewController *temp = sidebarRightViewController;
        sidebarRightViewController = nil;
        self.sidebarRightViewController = temp;
    }
    
    if (contentViewController) {
        UIViewController *temp = contentViewController;
        contentViewController = nil;
        self.contentViewController = temp;
    }
}

#pragma mark UIViewController
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)orientation {
	return (orientation == UIInterfaceOrientationPortraitUpsideDown)
    ? (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    : YES;
}

#pragma mark Public Methods
- (void)leftDragContentView:(UIPanGestureRecognizer *)panGesture {
	CGFloat translation = [panGesture translationInView:self.view].x;
	if (panGesture.state == UIGestureRecognizerStateChanged) {
		if (leftSidebarShowing) {
			if (translation > 0.0f) {
				_contentView.frame = CGRectOffset(_contentView.bounds, kGHRevealSidebarWidth, 0.0f);
				self.leftSidebarShowing = YES;
			} else if (translation < -kGHRevealSidebarWidth) {
				_contentView.frame = _contentView.bounds;
				self.leftSidebarShowing = NO;
			} else {
				_contentView.frame = CGRectOffset(_contentView.bounds, (kGHRevealSidebarWidth + translation), 0.0f);
			}
		} else {
			if (translation < 0.0f) {
				_contentView.frame = _contentView.bounds;
				self.leftSidebarShowing = NO;
			} else if (translation > kGHRevealSidebarWidth) {
				_contentView.frame = CGRectOffset(_contentView.bounds, kGHRevealSidebarWidth, 0.0f);
				self.leftSidebarShowing = YES;
			} else {
				_contentView.frame = CGRectOffset(_contentView.bounds, translation, 0.0f);
			}
		}
	} else if (panGesture.state == UIGestureRecognizerStateEnded) {
		CGFloat velocity = [panGesture velocityInView:self.view].x;
		BOOL show = (fabs(velocity) > kGHRevealSidebarFlickVelocity)
        ? (velocity > 0)
        : (translation > (kGHRevealSidebarWidth / 2));
		[self toggleSidebar:show duration:kGHRevealSidebarDefaultAnimationDuration];
		
	}
}

- (void)dragContentView:(UIPanGestureRecognizer *)panGesture {
	CGFloat translation = [panGesture translationInView:self.view].x;
    if (_contentView.frame.origin.x >0.0f) {
        _sidebarRightView.hidden=YES;
        _sidebarView.hidden=NO;
    }else{
        _sidebarRightView.hidden=NO;
        _sidebarView.hidden=YES;
    }

	if (panGesture.state == UIGestureRecognizerStateChanged) {
		if (leftSidebarShowing) {
            if (fabs(_contentView.frame.origin.x)<=kGHRevealSidebarWidth) {
                if (_contentView.frame.origin.x >0) {
                    if (translation < 0) {
                        _contentView.frame = CGRectOffset(_contentView.bounds, (translation + kGHRevealSidebarWidth), 0.0f);
                    }
                }else{
                    if (translation > 0) {
                         _contentView.frame = CGRectOffset(_contentView.bounds, (translation - kGHRevealSidebarWidth), 0.0f);
                    }
                   
                }
            }
		} else {
            float bh = _contentView.frame.origin.x;
            if (bh==0.0f) {
                _contentView.frame = CGRectOffset(_contentView.bounds, translation, 0.0f);
            }else if (fabs(bh) >= kGHRevealSidebarWidth) {
                    self.leftSidebarShowing = YES;
            }else{
                if (fabs(translation)<=kGHRevealSidebarWidth) {
                    _contentView.frame = CGRectOffset(_contentView.bounds, translation, 0.0f);
                }else{
                    if (bh>0) {
                        _contentView.frame = CGRectOffset(_contentView.bounds, kGHRevealSidebarWidth, 0.0f);
                    }else{
                        _contentView.frame = CGRectOffset(_contentView.bounds, -kGHRevealSidebarWidth, 0.0f);
                    }
                    
                }
                
                self.leftSidebarShowing = NO;
            }
		}
	} else if (panGesture.state == UIGestureRecognizerStateEnded) {
//		CGFloat velocity = [panGesture velocityInView:self.view].x;
		BOOL show =  (labs(_contentView.frame.origin.x) > (kGHRevealSidebarWidth / 2));
        if (_contentView.frame.origin.x >0.0f) {
            [self toggleSidebar:show duration:kGHRevealSidebarDefaultAnimationDuration];
        }else{
            [self toggleRightSidebar:show duration:kGHRevealSidebarDefaultAnimationDuration];
        }
		
	}


}

- (void)toggleSidebar:(BOOL)show duration:(NSTimeInterval)duration {
    _sidebarRightView.hidden=YES;
    _sidebarView.hidden=NO;
    
	[self toggleSidebar:show duration:duration completion:^(BOOL finshed){}];
}

- (void)toggleSidebar:(BOOL)show duration:(NSTimeInterval)duration completion:(void (^)(BOOL finsihed))completion {
	void (^animations)(void) = ^{
		if (show) {
			_contentView.frame = CGRectOffset(_contentView.bounds, kGHRevealSidebarWidth, 0.0f);
			[_contentView addGestureRecognizer:_tapRecog];
		} else {
			if (self.isSearching) {
				_sidebarView.frame = CGRectMake(0.0f, 0.0f, kGHRevealSidebarWidth, CGRectGetHeight(self.view.bounds));
			} else {
				[_contentView removeGestureRecognizer:_tapRecog];
			}
			_contentView.frame = _contentView.bounds;
		}
		self.leftSidebarShowing = show;
	};
	if (duration > 0.0) {
		[UIView animateWithDuration:duration
							  delay:0
							options:UIViewAnimationOptionCurveEaseInOut
						 animations:animations
						 completion:completion];
	} else {
		animations();
		completion(YES);
	}
}

- (void)toggleRightSidebar:(BOOL)show duration:(NSTimeInterval)duration {
    _sidebarView.hidden=YES;
    _sidebarRightView.hidden=NO;
	[self toggleRightSidebar:show duration:duration completion:^(BOOL finshed){}];
}

- (void)toggleRightSidebar:(BOOL)show duration:(NSTimeInterval)duration completion:(void (^)(BOOL finsihed))completion {
	void (^animations)(void) = ^{
		if (show) {
			_contentView.frame = CGRectOffset(_contentView.bounds, -kGHRevealSidebarWidth, 0.0f);
			[_contentView addGestureRecognizer:_tapRecog];
		} else {
			if (self.isSearching) {
				_sidebarView.frame = CGRectMake(0.0f, 0.0f, -kGHRevealSidebarWidth, CGRectGetHeight(self.view.bounds));
			} else {
				[_contentView removeGestureRecognizer:_tapRecog];
			}
			_contentView.frame = _contentView.bounds;
		}
		self.leftSidebarShowing = show;
	};
	if (duration > 0.0) {
		[UIView animateWithDuration:duration
							  delay:0
							options:UIViewAnimationOptionCurveEaseInOut
						 animations:animations
						 completion:completion];
	} else {
		animations();
		completion(YES);
	}
}

#pragma mark Private Methods
- (void)hideSidebar {
    if (_contentView.frame.origin.x >0.0f) {
        [self toggleSidebar:NO duration:kGHRevealSidebarDefaultAnimationDuration];
    }else{
        [self toggleRightSidebar:NO duration:kGHRevealSidebarDefaultAnimationDuration];
    }
	
}

@end
