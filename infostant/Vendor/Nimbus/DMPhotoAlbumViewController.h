//
//  DMPhotoAlbumViewController.h
//  www
//
//  Created by Trash on 3/29/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "NetworkPhotoAlbumViewController.h"
@protocol DMPhotoAlbumDataSource,DMPhotoAlbumDelegate;
@interface DMPhotoAlbumViewController : NetworkPhotoAlbumViewController <
    NIPhotoAlbumScrollViewDataSource,
    NIPhotoScrubberViewDataSource,
    NIOperationDelegate
>
{@private
//NSString* _facebookAlbumId;

NSArray* _photoInformation;
}
- (id)initWith:(id)object;
@property (nonatomic,strong) NSArray *imageData;
@property (nonatomic,strong) id<DMPhotoAlbumDelegate,DMPhotoAlbumDataSource> delegate;
//-(id)initWithDelegate:(id)delegate;
+(id)viewWithDelegate:(id)delegate;
@end

@protocol DMPhotoAlbumDataSource <NSObject>

-(NSUInteger)numberOfPhotoAlbum:(DMPhotoAlbumViewController *)photoAlbum;
-(NSString *)photoAlbum:(DMPhotoAlbumViewController *)photoAlbum captionForIndex:(NSUInteger)index;
-(NSString *)photoAlbum:(DMPhotoAlbumViewController *)photoAlbum imageLinkForIndex:(NSUInteger)index;

@end

@protocol DMPhotoAlbumDelegate <NSObject>

-(void)didTaptoAlbumDoneButton:(DMPhotoAlbumViewController *)photoAlbum;

@end