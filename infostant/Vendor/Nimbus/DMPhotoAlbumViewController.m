//
//  DMPhotoAlbumViewController.m
//  www
//
//  Created by Trash on 3/29/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "DMPhotoAlbumViewController.h"

#import "CaptionedPhotoView.h"
#import "AFNetworking.h"

@interface DMPhotoAlbumViewController (){
    UIBarButtonItem * doneButton;
}

@end

@implementation DMPhotoAlbumViewController
@synthesize imageData;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWith:(id)object{
    if ((self = [self initWithNibName:nil bundle:nil])) {
//        self.imageData = object;
        _photoInformation = object;
    }
    return self;
}

-(id)initWithDelegate:(id)delegate{
    if ((self = [self initWithNibName:nil bundle:nil])) {
        self.delegate = delegate;
        doneButton=
        [[UIBarButtonItem alloc]
         initWithBarButtonSystemItem:UIBarButtonSystemItemDone
         target:self
         action:@selector( doneFunc ) ];
        self.navigationItem.rightBarButtonItem = doneButton;
    }
    return self;
}

+(id)viewWithDelegate:(id)delegate{
        DMPhotoAlbumViewController *vc = [[DMPhotoAlbumViewController alloc] initWithDelegate:delegate];
    UINavigationController *vcNav = [[UINavigationController alloc] initWithRootViewController:vc];
    
    return vcNav;
}
-(void)doneFunc{
    if ([self.delegate respondsToSelector:@selector(didTaptoAlbumDoneButton:)]) {
        [self.delegate didTaptoAlbumDoneButton:self];
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UIViewController


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)loadView {
    [super loadView];
    
    self.photoAlbumView.dataSource = self;
    self.photoScrubberView.dataSource = self;
    
    // This title will be displayed until we get the results back for the album information.
    self.title = NSLocalizedString(@"Loading...", @"Navigation bar title - Loading a photo album");
//    [self loadThumbnails];
    [self.photoAlbumView reloadData];
    [self.photoScrubberView reloadData];
    
    [self refreshChromeState];
//    [self loadAlbumInformation];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)viewDidUnload {
    imageData = nil;
    
    [super viewDidUnload];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark NIPhotoScrubberViewDataSource


///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)numberOfPhotosInScrubberView:(NIPhotoScrubberView *)photoScrubberView {
    return ([self.delegate respondsToSelector:@selector(numberOfPhotoAlbum:)])?[self.delegate numberOfPhotoAlbum:self]:0;

}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (UIImage *)photoScrubberView: (NIPhotoScrubberView *)photoScrubberView
              thumbnailAtIndex: (NSInteger)thumbnailIndex {
    NSString* photoIndexKey = [self cacheKeyForPhotoIndex:thumbnailIndex];
    
    UIImage* image = [self.thumbnailImageCache objectWithName:photoIndexKey];
    if (nil == image) {
        NSDictionary* photo = [_photoInformation objectAtIndex:thumbnailIndex];
        
        NSString* thumbnailSource = [photo objectForKey:@"thumbnailSource"];
        [self requestImageFromSource: thumbnailSource
                           photoSize: NIPhotoScrollViewPhotoSizeThumbnail
                          photoIndex: thumbnailIndex];
    }
    image = [UIImage imageNamed:@"twitterlogo"];
    return image;
}

- (void)setChromeVisibility:(BOOL)isVisible animated:(BOOL)animated {
    [super setChromeVisibility:isVisible animated:animated];
    
    // TODO(jverkoey June 19, 2012): This is not the ideal way to do access the visible pages.
    // Let's consider adding a new API for this.
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:NIStatusBarAnimationCurve()];
    [UIView setAnimationDuration:NIStatusBarAnimationDuration()];
    for (CaptionedPhotoView* captionedPageView in self.photoAlbumView.visiblePages) {
        captionedPageView.captionWell.alpha = isVisible ? 1 : 0;
    }
    [UIView commitAnimations];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark NIPhotoAlbumScrollViewDataSource


///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)numberOfPagesInPagingScrollView:(NIPhotoAlbumScrollView *)photoScrollView {
    return ([self.delegate respondsToSelector:@selector(numberOfPhotoAlbum:)])?[self.delegate numberOfPhotoAlbum:self]:0;
}
///////////////////////////////////////////////////////////////////////////////////////////////////
- (UIImage *)photoAlbumScrollView: (NIPhotoAlbumScrollView *)photoAlbumScrollView
                     photoAtIndex: (NSInteger)photoIndex
                        photoSize: (NIPhotoScrollViewPhotoSize *)photoSize
                        isLoading: (BOOL *)isLoading
          originalPhotoDimensions: (CGSize *)originalPhotoDimensions {
    UIImage* image = nil;
    
    NSString* photoIndexKey = [self cacheKeyForPhotoIndex:photoIndex];
   
    image = [self.highQualityImageCache objectWithName:photoIndexKey];
    if (nil != image) {
        *photoSize = NIPhotoScrollViewPhotoSizeOriginal;
        
    } else {
    [self requestImageFromSource:  (([self.delegate respondsToSelector:@selector(photoAlbum:imageLinkForIndex:)])?[self.delegate photoAlbum:self imageLinkForIndex:photoIndex]:nil)
                           photoSize: NIPhotoScrollViewPhotoSizeOriginal
                          photoIndex: photoIndex];
        
        *isLoading = YES;
    }
    return image;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)photoAlbumScrollView: (NIPhotoAlbumScrollView *)photoAlbumScrollView
     stopLoadingPhotoAtIndex: (NSInteger)photoIndex {
    // TODO: Figure out how to implement this with AFNetworking.
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (UIView<NIPagingScrollViewPage>*)pagingScrollView:(NIPagingScrollView *)pagingScrollView pageViewForIndex:(NSInteger)pageIndex {
    // TODO (jverkoey Nov 27, 2011): We should make this sort of custom logic easier to build.
    UIView<NIPagingScrollViewPage>* pageView = nil;
    NSString* reuseIdentifier = NSStringFromClass([CaptionedPhotoView class]);
    
   
    
    pageView = [pagingScrollView dequeueReusablePageWithIdentifier:reuseIdentifier];
    if (nil == pageView) {
        pageView = [[CaptionedPhotoView alloc] init];
        pageView.reuseIdentifier = reuseIdentifier;
    }
    
    NIPhotoScrollView* photoScrollView = (NIPhotoScrollView *)pageView;
    photoScrollView.photoScrollViewDelegate = self.photoAlbumView;
    photoScrollView.zoomingAboveOriginalSizeIsEnabled = [self.photoAlbumView isZoomingAboveOriginalSizeEnabled];
    
    
    CaptionedPhotoView* captionedView = (CaptionedPhotoView *)pageView;
    captionedView.captionWell.hidden = YES;
    if ([self.delegate respondsToSelector:@selector(photoAlbum:captionForIndex:)]) {
        NSString *caption = [self.delegate photoAlbum:self captionForIndex:pageIndex];
        captionedView.caption = caption;
        captionedView.captionWell.hidden = !(caption.length);
    }
    
    
    return pageView;
}
@end
