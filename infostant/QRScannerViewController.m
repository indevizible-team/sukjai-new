//
//  QRScannerViewController.m
//  www
//
//  Created by indevizible on 2/23/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "QRScannerViewController.h"
#import "QRURL.h"
#import "WebViewController.h"
#import "DataManager.h"
#import "CategoryViewController.h"
#import "Bitly.h"
#import "DMURL.h"
#import "MyTripListViewController.h"
@interface QRScannerViewController (){
    BOOL processing;
}

@end

@implementation QRScannerViewController
@synthesize readerView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	// Do any additional setup after loading the view
    // the delegate receives decode results
    readerView.readerDelegate = self;
    readerView.autoresizesSubviews = NO;
    // you can use this to support the simulator
    if(TARGET_IPHONE_SIMULATOR) {
        cameraSim = [[ZBarCameraSimulator alloc]
                     initWithViewController: self];
        cameraSim.readerView = readerView;
    }
    
}
-(void)viewDidAppear:(BOOL)animated{
    [readerView start];
}
- (void) viewWillDisappear: (BOOL) animated
{
    [readerView stop];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL) shouldAutorotateToInterfaceOrientation: (UIInterfaceOrientation) orient
{
    // auto-rotation is supported
    return(YES);
}

- (void) willRotateToInterfaceOrientation: (UIInterfaceOrientation) orient
                                 duration: (NSTimeInterval) duration
{
    // compensate for view rotation so camera preview is not rotated
    [readerView willRotateToInterfaceOrientation: orient
                                        duration: duration];
}
- (void) readerView: (ZBarReaderView*) view
     didReadSymbols: (ZBarSymbolSet*) syms
          fromImage: (UIImage*) img
{
    
    // do something useful with results
    for(ZBarSymbol *sym in syms) {
        if ([self.delegate respondsToSelector:@selector(QRscannerDidRecieveData:data:image:)]) {
            [self.delegate QRscannerDidRecieveData:self data:sym.data image:img];
        }else{
            if (!processing) {
                [self goToURL:sym.data];
            }
        }
        break;
    }
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}
-(void)goToURL:(NSString *)stringURL{
    
    
    QRURL *url = [QRURL dataWithURL:stringURL];
    UIApplication *ourApplication = [UIApplication sharedApplication];
    NSURL *ourURL = url.url;
    
    if ([ourURL.host isEqualToString:@"bit.ly"]) {
        [SVProgressHUD show];
        processing = YES;
        [Bitly expandFromShortURL:ourURL success:^(NSURLRequest *request, NSURL *longURL) {
            processing = NO;
            NSLog(@"long url = %@",longURL);
            NSMutableDictionary *query = [longURL dictionaryForQueryString];
            if (query[@"code"]&&query[@"tour"]) {
                Trip *myTrip= [Trip new];
                myTrip.code = query[@"code"];
                MyTripListViewController *vc = (MyTripListViewController *)[DataManager controllerByID:@"MyTripListViewController"];
                vc.trip = myTrip;
                [self.navigationController pushViewController:vc animated:NO];
            }else{
                WebViewController *wv  = (WebViewController *)[DataManager controllerByID:@"LandingD"];
                wv.address = longURL.absoluteString;
                [self.navigationController pushViewController:wv animated:YES];
            }
           
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            [SVProgressHUD showErrorWithStatus:@"Some thing wrong"];
        }];
    }else  if ([ourApplication canOpenURL:ourURL]) {
        if ([url isLocalLink]) {
            [SVProgressHUD show];
            NSMutableDictionary *param = [DataManager param];
            [param addEntriesFromDictionary:[User currentUser].oauth_login_dict];
            [param addEntriesFromDictionary:@{
             @"page":@1,
             @"infofid":url.infofid,
             @"productid":url.productid
             }];
            NSLog(@"reqesting %@",[[DataManager client] requestWithMethod:@"GET" path:@"getNewAllFeature" parameters:param].URL);
            AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"GET" path:@"getNewAllFeature" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                [SVProgressHUD dismiss];
                if ([JSON count]) {
                    DataController *dataCtrl = [[DataController alloc] initWithDictionary:[DataManager JSONToMutableDictionary:JSON]];
                    NSMutableDictionary *selectedCat = [dataCtrl dictionaryAtIndex:0];
                    if (([url.infofid isEqualToString:@"1"] && ![[selectedCat valueForKey:@"data"] count])||[[selectedCat valueForKey:@"banner"] integerValue]==1) {
                        WebViewController *wv  = (WebViewController *)[DataManager controllerByID:@"LandingD"];
                        wv.address = [selectedCat valueForKey:@"fullshopurl"];
                        wv.title = NSLocalizedString([selectedCat valueForKey:@"title"],[selectedCat valueForKey:@"title"]);
                        [self.navigationController pushViewController:wv animated:YES];
                    }else if ([url.infofid isEqualToString:@"3"]){
                        NSMutableArray *TmpImage = [NSMutableArray new];
                        for (NSString *image in [selectedCat valueForKey:@"pdfdata"]) {
                            NSURL *_url = [NSURL URLWithString:image];
                            [TmpImage addObject:_url];
                        }
                        ImageSlideViewController *iv = (ImageSlideViewController *)[DataManager controllerByID:@"LandingE"];
                        iv.imageList = TmpImage;
                        
                        [self presentViewController:iv animated:YES completion:nil];
                    }else{
                        LandingNewViewController *lv =(LandingNewViewController *) [DataManager controllerByID:@"LandingNew"];
                        lv.product = dataCtrl;
                        lv.accessKey = dataCtrl.key[0];
                        lv.landing = [dataCtrl accessKey:lv.accessKey];
                        lv.feature = url.infofid;
                        [self.navigationController pushViewController:lv animated:YES];
                    }
                }else{
                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNoContent", @"")];
                }
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNoInternet", @"")];
            }];
            [operation start];
            
            
        }else{
            WebViewController *wvc = (WebViewController *)[DataManager controllerByID:@"LandingD"];
            wvc.address = stringURL;
            [self.navigationController pushViewController:wvc animated:YES];
        }
    }
    else {
        //Display error
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoMessage", @"") message:stringURL delegate:nil cancelButtonTitle:NSLocalizedString(@"infoOK", @"") otherButtonTitles:nil];
        [alertView show];
    }
    

}

+(QRScannerViewController *)QRScanner:(id<QRScannerDelegate>)delegate{
    QRScannerViewController *vc = [[QRScannerViewController alloc] initWithNibName:@"QRScannerViewController" bundle:nil];
    vc.delegate = delegate;
    return vc;
}

@end
