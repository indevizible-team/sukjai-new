//
//  FacebookConnectViewController.h
//  infostant
//
//  Created by indevizible on 1/21/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
@interface FacebookConnectViewController : UIViewController<FBLoginViewDelegate>
@property (strong, nonatomic) IBOutlet FBProfilePictureView *profileImage;

@property (strong, nonatomic) id<FBGraphUser> loggedInUser;
@end
