//
//  QRScannerViewController.h
//  www
//
//  Created by indevizible on 2/23/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol QRScannerDelegate;
@interface QRScannerViewController : UIViewController<ZBarReaderViewDelegate>{
    ZBarReaderView *readerView;
    UITextView *resultText;
    ZBarCameraSimulator *cameraSim;
}
@property (strong, nonatomic) IBOutlet ZBarReaderView *readerView;
@property (strong, nonatomic) id<QRScannerDelegate> delegate;
@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;
+(QRScannerViewController *)QRScanner:(id<QRScannerDelegate>)delegate;
@end

@protocol QRScannerDelegate <NSObject>
@optional
-(void)QRscannerDidRecieveData:(QRScannerViewController *)vc data:(NSString *)data image:(UIImage *)image;

@end