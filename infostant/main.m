//
//  main.m
//  infostant
//
//  Created by INFOSTANT on 12/26/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
