//
//  ProfileFeedViewController.m
//  infostant
//
//  Created by INFOSTANT on 12/25/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import "FollowDetailViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <QuartzCore/QuartzCore.h>
#import "AFPhotoEditorController.h"
#import "AFPhotoEditorCustomization.h"
#import "AFOpenGLManager.h"
#import "AppDelegate.h"
#import "FollowDetailCell.h"
#import "SubCatViewController.h"
#import "ProfileHeader.h"
#import "GalleryImageCell.h"
#import <CoreLocation/CoreLocation.h>
#import "MBAlertView.h"
#import "MBHUDView.h"
#import "IDMPhotoBrowser.h"
#import "CustomCollectionFlowLayout.h"
@interface FollowDetailViewController ()<SelectFeatureDelegate, LoginDelegate, UIAlertViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, AFPhotoEditorControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UIPopoverControllerDelegate, UIActionSheetDelegate,UIAlertViewDelegate,UICollectionViewDelegateFlowLayout>{
    User *thisUser;
    NSMutableArray *dataSource,*photoList;
    CLLocationManager *locationManager;
    UIImage *finishImage;

}
@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;
@property (nonatomic, strong) UIPopoverController * popover;
@property (nonatomic, assign) BOOL shouldReleasePopover;
@property (nonatomic, nonatomic) UIImage *imgFromCamera;

@property (nonatomic, strong) ALAssetsLibrary * assetLibrary;
@property (nonatomic, strong) NSMutableArray * sessions;

- (IBAction)messageAction:(UIButton *)sender;

@end


@implementation FollowDetailViewController
@synthesize navigationBarPanGestureRecognizer;
@synthesize imgFromCamera;

- (void)viewDidUnload {
    [super viewDidUnload];
     [locationManager stopUpdatingLocation];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    NSLog(@"didload");
    locationManager  = [[CLLocationManager alloc] init];
    [locationManager startUpdatingLocation];
    page = 1;
    canLoadMore = YES;
    dataSource = [ NSMutableArray new];
    photoList = [NSMutableArray new];
    isMe = NO;
    _allFeature = [[DataController alloc] initWithDictionary:[PLIST readFeature:@"feature"]];
    infofid = _allFeature.key[0];
    
    ALAssetsLibrary * assetLibrary = [[ALAssetsLibrary alloc] init];
    [self setAssetLibrary:assetLibrary];
    NSMutableArray * sessions = [NSMutableArray new];
    [self setSessions:sessions];
    [AFOpenGLManager beginOpenGLLoad];
    
    if (_personProfile) {
        if ([[User currentUser].mid isEqualToString:[NSString stringWithFormat:@"%@",_personProfile[@"mid"]]]) {
            thisUser = [User currentUser];
            isMe = YES;
            _followBtn.hidden = YES;
        }else{
            thisUser = [[User alloc] initWith:_personProfile];
            self.navigationItem.rightBarButtonItem = nil;
        }
        [self display];
    }else{
        [SVProgressHUD showWithStatus:@"XYZ"];
        if (self.friendid) {

            [User UserWithId:self.friendid success:^(NSURLRequest *request, NSHTTPURLResponse *response, User *user) {
                [SVProgressHUD dismiss];
                thisUser = user;
                [self display];
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                if (error) {
                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNoInternet", @"")];
                }else{
                    [SVProgressHUD showErrorWithStatus:@"This user may be delete from server"];
                }
                
            }];
        }else{
            [SVProgressHUD dismiss];
            thisUser= [User currentUser];
            [self display];
        }
        

    }
    
    self.lblFollower.text = NSLocalizedString(@"infoFollower", @"");
    self.lblFollowing.text = NSLocalizedString(@"infoFollowing", @"");
    self.lblLikes.text = NSLocalizedString(@"infoLikes", @"");
}

-(void)loadMore{
    canLoadMore = NO;
    NSMutableDictionary *param = [DataManager param];
    [param addEntriesFromDictionary:@{@"page":@(page++),@"friendid":thisUser.mid,@"infofid":@11}];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"GET" path:@"getNewAllFeature" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"rf : %@ ",request.URL);
        if ([JSON count]) {
            canLoadMore = YES;
            for (NSString *key in JSON[@"key"]) {
                NSURL *url = [NSURL URLWithString:[JSON[key][@"pic"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                [dataSource addObject:url];
                IDMPhoto *photo = [IDMPhoto photoWithURL:url];
                photo.caption = JSON[key][@"title"];
                [photoList addObject:photo];
            }
            [self.collectionFollow reloadData];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"fail : %@ - %@ - %@",error.localizedDescription,JSON,request.URL);
    }];
    
    [operation start];
}

-(void)display{
    [self.profilePic setImageWithURL:thisUser.picuser];
    self.lblName.text = thisUser.displayname;
    self.lblLocation.text = thisUser.proname;
    self.flwerNumber.text = thisUser.countfollower;
    self.flwngNumber.text = thisUser.countfollowing;
    self.likeNumber.text = thisUser.countlike;
    self.title = thisUser.displayname;
    [self loadMore];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}


- (void)viewDidAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	
	
	
    UINavigationController *nav = self.navigationController;
    UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			self.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
		}
		// Check if we have a revealButton already.
        NSLog(@"count : %i",self.navigationController.viewControllers.count);
		if (![self.navigationItem leftBarButtonItem]&&(self.navigationController.viewControllers.count==1)) {
			// If not, allocate one and add it.
			UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
            
            UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [menuButton setImage:imageMenu forState:UIControlStateNormal];
            menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
            [menuButton addTarget:controller action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
		}

	}
    
    
}

- (void)arrangeCollectionView {
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.collectionFollow.collectionViewLayout;
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        flowLayout.scrollDirection =  UICollectionViewScrollDirectionVertical;
    } else {
        flowLayout.scrollDirection =  UICollectionViewScrollDirectionHorizontal;
    }
    
    self.collectionFollow.collectionViewLayout = flowLayout;
//    [self.collectionFollow reloadData];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self arrangeCollectionView];
}

- (BOOL) hasValidAPIKey{
    NSString * key = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Aviary-API-Key"];
    if ([key isEqualToString:@"<YOUR_API_KEY>"]) {
        [[[UIAlertView alloc] initWithTitle:@"Oops!" message:@"You forgot to add your API key!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return NO;
    }
    return YES;
}


#pragma mark - UICollectionView Datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
//    return [Repository dataIPad].count;
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
//    if (_profile!=nil) {
//        return _profile.key.count;
//    }
    return [dataSource count];
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    ProfileHeader *profileHeader;
    if ([kind isEqual:UICollectionElementKindSectionHeader])
    {
        profileHeader = [self.collectionFollow dequeueReusableSupplementaryViewOfKind:kind
                                                         withReuseIdentifier:@"ProfileHeader"
                                                                forIndexPath:indexPath];
    }
    
    [profileHeader.imgUserProfile setImageWithURL:thisUser.picuser];
    profileHeader.lblUsername.text = thisUser.displayname;
    profileHeader.labelLocate.text = thisUser.proname;
    profileHeader.follerNumber.text = thisUser.countfollower;
    profileHeader.follingNumber.text = thisUser.countfollowing;
    profileHeader.likeNumber.text = thisUser.countlike;
    self.title = thisUser.displayname;
    
    return profileHeader;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    NSDictionary *each =[_profile dictionaryAtIndex:indexPath.row];
//    
    GalleryImageCell *gallerycell;
    gallerycell = [cv dequeueReusableCellWithReuseIdentifier:@"Image" forIndexPath:indexPath];
    
    if ([dataSource[indexPath.row] isKindOfClass:[UIImage class]]) {
        [gallerycell.imgGalleryView setImage:dataSource[indexPath.row]];
    }else{
        [gallerycell.imgGalleryView setImageWithURL:dataSource[indexPath.row]];
    }
    
    if ((indexPath.row > dataSource.count - 5 )&&canLoadMore) {
        [self loadMore];
    }
    
    return gallerycell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photoList animatedFromView:[collectionView cellForItemAtIndexPath:indexPath  ]];
    [browser setInitialPageIndex:indexPath.row];
    [self presentViewController:browser animated:YES completion:nil];
}

- (BOOL) shouldInvalidateLayoutForBoundsChange:(CGRect)newBound {
    
    return YES;
    
}

-(void)didLogin:(LoginNavController *)vc{
    canLoadMore = YES;
    page = 1;
    _profile = nil;
//    [self reloadData];
}

#pragma mark - Popover Methods

- (void) presentViewControllerInPopover:(UIViewController *)controller
{
    CGRect sourceRect = CGRectMake(0, 0, 60, 32);
    UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:controller];
    [popover setDelegate:self];
    [self setPopover:popover];
    [self setShouldReleasePopover:YES];
    
    [popover presentPopoverFromRect:sourceRect inView:[self view] permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

- (void) dismissPopoverWithCompletion:(void(^)(void))completion
{
    [[self popover] dismissPopoverAnimated:YES];
    [self setPopover:nil];
    
    NSTimeInterval delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        completion();
    });
}

- (void) popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    if ([self shouldReleasePopover]){
        [self setPopover:nil];
    }
    [self setShouldReleasePopover:YES];
}

#pragma mark - Camera Button Action
- (IBAction)openCamera:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select image source"
                                                             delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Camera", @"Photo Library", @"Cancel", nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleAutomatic;
    actionSheet.destructiveButtonIndex = 2;
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self TakePhotoWithCamera];
    }
    else if (buttonIndex == 1)
    {
        [self SelectPhotoFromLibrary];
    }
    
    else if (buttonIndex == 2)
    {
        NSLog(@"cancel");
    }
}

-(void) TakePhotoWithCamera{
    if ([self hasValidAPIKey]) {
        UIImagePickerController * imagePicker = [UIImagePickerController new];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [imagePicker setDelegate:self];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            [self presentViewController:imagePicker animated:YES completion:nil];
        }else{
            [self presentViewControllerInPopover:imagePicker];
        }
    }
}

-(void) SelectPhotoFromLibrary
{
    if ([self hasValidAPIKey]) {
        UIImagePickerController * imagePicker = [UIImagePickerController new];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [imagePicker setDelegate:self];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            [self presentViewController:imagePicker animated:YES completion:nil];
        }else{
            [self presentViewControllerInPopover:imagePicker];
        }
    }
}


#pragma mark - Photo Editor Creation and Presentation
- (void) launchPhotoEditorWithImage:(UIImage *)editingResImage highResolutionImage:(UIImage *)highResImage
{
    // Initialize the photo editor and set its delegate
    AFPhotoEditorController * photoEditor = [[AFPhotoEditorController alloc] initWithImage:editingResImage];
    [photoEditor setDelegate:self];
    
    // Customize the editor's apperance. The customization options really only need to be set once in this case since they are never changing, so we used dispatch once here.
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self setPhotoEditorCustomizationOptions];
    });
    
    // If a high res image is passed, create the high res context with the image and the photo editor.
    if (highResImage) {
        [self setupHighResContextForPhotoEditor:photoEditor withImage:highResImage];
    }
    
    // Present the photo editor.
    [self presentViewController:photoEditor animated:NO completion:nil];
}

- (void) setupHighResContextForPhotoEditor:(AFPhotoEditorController *)photoEditor withImage:(UIImage *)highResImage
{
    // Capture a reference to the editor's session, which internally tracks user actions on a photo.
    __block AFPhotoEditorSession *session = [photoEditor session];
    
    // Add the session to our sessions array. We need to retain the session until all contexts we create from it are finished rendering.
    [[self sessions] addObject:session];
    
    // Create a context from the session with the high res image.
    AFPhotoEditorContext *context = [session createContextWithImage:highResImage];
    
    __block FollowDetailViewController * blockSelf = self;
    
    // Call render on the context. The render will asynchronously apply all changes made in the session (and therefore editor)
    // to the context's image. It will not complete until some point after the session closes (i.e. the editor hits done or
    // cancel in the editor). When rendering does complete, the completion block will be called with the result image if changes
    // were made to it, or `nil` if no changes were made. In this case, we write the image to the user's photo album, and release
    // our reference to the session.
    [context render:^(UIImage *result) {
        if (result) {
//            UIImageWriteToSavedPhotosAlbum(result, nil, nil, NULL);
        }
        
        [[blockSelf sessions] removeObject:session];
        
        blockSelf = nil;
        session = nil;
        
    }];
}

#pragma Photo Editor Delegate Methods

// This is called when the user taps "Done" in the photo editor.
- (void) photoEditor:(AFPhotoEditorController *)editor finishedWithImage:(UIImage *)image
{
    finishImage = image;
    UIAlertView *settitle = [[UIAlertView alloc] initWithTitle:@"Enter image caption" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Send", nil];
    settitle.alertViewStyle = UIAlertViewStylePlainTextInput;
     [editor dismissViewControllerAnimated:YES completion:nil];
    [settitle show];

}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex != alertView.cancelButtonIndex) {
        NSString *caption = @"";
        for (UIView* view in alertView.subviews)
        {
            if ([view isKindOfClass:[UITextField class]])
            {
                UITextField* textField = (UITextField*)view;
                caption = textField.text;
                break;
            }
        }
        NSDictionary *param =  @{@"lat":@(locationManager.location.coordinate.latitude),
                                 @"lng":@(locationManager.location.coordinate.longitude),
                                 @"photo":[DataManager base64FromUIImage:finishImage],
                                 @"infofid":@11,
                                 @"title":caption};
        NSMutableDictionary *realParam = [DataManager param];
        [realParam addEntriesFromDictionary:param];
        NSMutableDictionary *user = [PLIST readFeature:@"user"];
        [realParam setValue:[user valueForKey:@"oauth_login"] forKey:@"oauth_login"];
        [MBHUDView hudWithBody:@"Uploading..." type:MBAlertViewHUDTypeActivityIndicator hidesAfter:0 show:YES];
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"POST" path:@"savePhotoImage" parameters:realParam] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            [MBHUDView dismissCurrentHUD];
            [MBHUDView hudWithBody:@"Successfully upload image" type:MBAlertViewHUDTypeCheckmark hidesAfter:2.0 show:YES];
            
            [dataSource insertObject:finishImage atIndex:0];
            IDMPhoto *photo = [IDMPhoto photoWithImage:finishImage] ;
            photo.caption = caption;
            [photoList insertObject:photo atIndex:0];
            [self.collectionFollow insertItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:0]]];
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
            [MBHUDView dismissCurrentHUD];
            NSLog(@"ERR : %@",error.localizedDescription);
            [[MBAlertView alertWithBody:@"Cannot upload image right now. please try again later." cancelTitle:@"OK" cancelBlock:nil] addToDisplayQueue];
            
        }];
        [operation start];
    }
}

// This is called when the user taps "Cancel" in the photo editor.
- (void) photoEditorCanceled:(AFPhotoEditorController *)editor
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Photo Editor Customization
- (void) setPhotoEditorCustomizationOptions
{
    // Set Tool Order
    NSArray * toolOrder = @[kAFEffects, kAFFocus, kAFFrames, kAFStickers, kAFEnhance, kAFOrientation, kAFCrop, kAFAdjustments, kAFSplash, kAFDraw, kAFText, kAFRedeye, kAFWhiten, kAFBlemish, kAFMeme];
    [AFPhotoEditorCustomization setToolOrder:toolOrder];
    
    // Set Custom Crop Sizes
    [AFPhotoEditorCustomization setCropToolOriginalEnabled:NO];
    [AFPhotoEditorCustomization setCropToolCustomEnabled:YES];
    NSDictionary * fourBySix = @{kAFCropPresetHeight : @(4.0f), kAFCropPresetWidth : @(6.0f)};
    NSDictionary * fiveBySeven = @{kAFCropPresetHeight : @(5.0f), kAFCropPresetWidth : @(7.0f)};
    NSDictionary * square = @{kAFCropPresetName: @"Square", kAFCropPresetHeight : @(1.0f), kAFCropPresetWidth : @(1.0f)};
    [AFPhotoEditorCustomization setCropToolPresets:@[fourBySix, fiveBySeven, square]];
    
    // Set Supported Orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        NSArray * supportedOrientations = @[@(UIInterfaceOrientationPortrait), @(UIInterfaceOrientationPortraitUpsideDown), @(UIInterfaceOrientationLandscapeLeft), @(UIInterfaceOrientationLandscapeRight)];
        [AFPhotoEditorCustomization setSupportedIpadOrientations:supportedOrientations];
    }
}

#pragma mark - UIImagePicker Delegate

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSURL * assetURL = [info objectForKey:UIImagePickerControllerReferenceURL];
    imgFromCamera = [info objectForKey: UIImagePickerControllerOriginalImage];
    void(^completion)(void)  = ^(void){
        
        [[self assetLibrary] assetForURL:assetURL resultBlock:^(ALAsset *asset) {
            if (asset){
                [self launchEditorWithAsset:asset];
            }
        } failureBlock:^(NSError *error) {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enable access to your device's photos." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }];
    };
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self dismissViewControllerAnimated:YES completion:completion];
        [self performSelector:@selector(launchEditorWithSampleImage) withObject:nil afterDelay:1.0f];
    }else{
        [self dismissPopoverWithCompletion:completion];
    }
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Photo Editor Launch Methods

- (void) launchEditorWithAsset:(ALAsset *)asset
{
    UIImage * editingResImage = [self editingResImageForAsset:asset];
    UIImage * highResImage = [self highResImageForAsset:asset];
    
    [self launchPhotoEditorWithImage:editingResImage highResolutionImage:highResImage];
}

- (void) launchEditorWithSampleImage
{
//    UIImage * sampleImage = [UIImage imageNamed:@"Demo.png"];
//    UIImageWriteToSavedPhotosAlbum(imgFromCamera, nil, nil, NULL);
    [self launchPhotoEditorWithImage:imgFromCamera highResolutionImage:nil];
    
}

#pragma mark - ALAssets Helper Methods

- (UIImage *)editingResImageForAsset:(ALAsset*)asset
{
    CGImageRef image = [[asset defaultRepresentation] fullScreenImage];
    
    return [UIImage imageWithCGImage:image scale:1.0 orientation:UIImageOrientationUp];
}

- (UIImage *)highResImageForAsset:(ALAsset*)asset
{
    ALAssetRepresentation * representation = [asset defaultRepresentation];
    
    CGImageRef image = [representation fullResolutionImage];
    UIImageOrientation orientation = [representation orientation];
    CGFloat scale = [representation scale];
    
    return [UIImage imageWithCGImage:image scale:scale orientation:orientation];
}

#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationIsPortrait(interfaceOrientation);
    }else{
        return YES;
    }
}

- (BOOL) shouldAutorotate
{
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self setShouldReleasePopover:NO];
    [[self popover] dismissPopoverAnimated:YES];
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if ([self popover]) {
        CGRect popoverRef = CGRectMake(0, 0, 60, 32);
        [[self popover] presentPopoverFromRect:popoverRef inView:[self view] permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }
}


#pragma mark -
- (IBAction)messageAction:(UIButton *)sender {
    NSMutableDictionary *param = [DataManager param];
    NSMutableDictionary *user = [PLIST readFeature:@"user"];
    [param setValue:[user valueForKey:@"oauth_login"] forKey:@"oauth_login"];
    [param setValue:thisUser.mid forKey:@"friendid"];
    NSURLRequest *request = [[DataManager client] requestWithMethod:@"POST" path:@"checkFollow" parameters:param];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"%@",JSON);
        if ([[JSON valueForKey:@"follow"] isEqualToNumber:@1]) {
            MsgDetailViewController *vc = [[DataManager getStoryboard] instantiateViewControllerWithIdentifier:@"MsgDetailViewController"];
            vc.friendid = [_personProfile valueForKey:@"mid"];
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoAppName", @"") message:NSLocalizedString(@"infoMsgFollowBefore", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"infoOK", @"") otherButtonTitles: nil];
            [alert show];
            
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoAppName", @"") message:[NSString stringWithFormat:@"%@",error] delegate:nil cancelButtonTitle:NSLocalizedString(@"infoOK", @"") otherButtonTitles: nil];
        [alert show];
    }];
    [operation start];
}

@end
