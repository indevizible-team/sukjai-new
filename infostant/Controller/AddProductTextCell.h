//
//  AddProductTextCell.h
//  infostant
//
//  Created by indevizible on 1/24/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddProductTextCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextView *description;

@end
