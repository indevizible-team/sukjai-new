//
//  SubCatViewController.m
//  infostant
//
//  Created by indevizible on 1/11/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "CategoryCell.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "SelectFeatureViewController.h"
#import "WebViewController.h"
#import "ImageSlideViewController.h"
#import "LandingNewViewController.h"
#import "SubCatViewController.h"

@interface SubCatViewController ()<SelectFeatureDelegate>

@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;

@end



@implementation SubCatViewController
@synthesize infofid;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    canLoadMore = YES;
    page = 1;
    if (infofid==nil) {
        infofid = @"1";
    }
    
    user = [User currentUser];
    if (user!=nil) {
            client = [DataManager client];
            [self loadMore];
    }
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)loadMore{
    if (canLoadMore) {
        canLoadMore = NO;
        if (page == 1) {
            [SVProgressHUD show];
        }
        
        NSMutableDictionary *param = [@{
                                      @"infofid":infofid,
                                      @"friendid":_friendid,
                                      @"subcatid":_subcatid
                                      } mutableCopy];
        [param addEntriesFromDictionary:[DataManager param]];
        [param setValue:[NSNumber numberWithInteger:page++] forKey:@"page"];
        NSMutableURLRequest *dataRequest = [client requestWithMethod:@"GET" path:@"getNewAllFeature" parameters:param];
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:dataRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            NSLog(@"finish %@",request.URL);
            if ([JSON count]) {
                if (_subCat==nil) {
                    _subCat = [[DataController alloc] initWithDictionary:[[DataManager JSONToMutableDictionary:JSON] mutableCopy]];
                }else{
                    [_subCat insertData:[[DataManager JSONToMutableDictionary:JSON] mutableCopy]];
                }
                canLoadMore = YES;
            }else{
                canLoadMore = NO;
            }
           
            [SVProgressHUD dismiss];
            [_collectionSubCat reloadData];
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
            NSLog(@"error : %@",request.URL);
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNetworkError", @"")];
        }];
        [operation start];
    }
}
- (void)viewDidAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	
	
    UINavigationController *nav = self.navigationController;
    UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			self.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
		}
		

	}
}
- (void)arrangeCollectionView {
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.collectionSubCat.collectionViewLayout;
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        flowLayout.scrollDirection =  UICollectionViewScrollDirectionVertical;
    } else {
        flowLayout.scrollDirection =  UICollectionViewScrollDirectionHorizontal;
    }
    
    self.collectionSubCat.collectionViewLayout = flowLayout;
    [self.collectionSubCat reloadData];
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self arrangeCollectionView];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	CGSize retval;
    int cellMod = indexPath.row%5;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        retval = CGSizeMake( 306, 290);
    }
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        if (cellMod>=2) {
            retval = CGSizeMake(236, 328);
        }
        if (cellMod<2) {
            retval = CGSizeMake(361, 328);
        }
    }
	return retval;
}


#pragma mark - UICollectionView Datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    if (_subCat!=nil) {
        return _subCat.key.count;
    }
    return 0;
}

- (CategoryCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *each = [_subCat dictionaryAtIndex:indexPath.row];
    CategoryCell *cell;
    int cellMod = indexPath.row%5;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        if (cellMod<2) {
            cell = [cv dequeueReusableCellWithReuseIdentifier: @"DuoCateCell" forIndexPath:indexPath];
        }
        if (cellMod>=2) {
            cell = [cv dequeueReusableCellWithReuseIdentifier: @"TripleCateCell" forIndexPath:indexPath];
        }
    }
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
    {
        cell = (CategoryCell *)[cv dequeueReusableCellWithReuseIdentifier:@"likelistM" forIndexPath:indexPath];
    }
    
    [cell.imageView setImageWithURL:[NSURL URLWithString:[each valueForKey:@"pic"]]];
    cell.ownerName.text = [NSString stringWithFormat:@"By %@",[each valueForKey:@"displayname"]];
    cell.description.text = [each valueForKey:@"title"];
    cell.followNo.text = [DataManager numberToStr:[[each valueForKey:@"countfollow"] intValue]];
    cell.likeNo.text = [DataManager numberToStr:[[each valueForKey:@"countlike"] intValue]];
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:cell.bounds];
    cell.layer.masksToBounds = NO;
    cell.layer.borderColor =[UIColor blackColor].CGColor;
    cell.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.layer.shadowOffset = CGSizeMake(0.0f, 0.05f);
    cell.layer.shadowOpacity = 0.5f;
    cell.layer.shadowRadius = 1.0f;
    cell.layer.shadowPath = shadowPath.CGPath;
    
    if (indexPath.row  >=  _subCat.key.count-5) {
        [self loadMore];
    }
    return cell;
}
- (void)collectionView:(UICollectionView *)colView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor colorWithRed:105.0f/255 green:105.0f/255 blue:105.0f/255 alpha:0.2f];
}

- (void)collectionView:(UICollectionView *)colView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = nil;
}
-(NSArray *)selectFeatureList:(SelectFeatureViewController *)controller{
    featureList = [NSMutableArray new];
    for (NSString *key in _allSubCat.key) {
        [featureList addObject:[[_allSubCat.data valueForKey:key] valueForKey:@"subcatname"]];
    }
    
    return featureList;
}
-(void)didSelectFeature:(SelectFeatureViewController *)controller selected:(NSInteger *)selected{
    page =1;
    NSDictionary *data = [_allSubCat dictionaryAtIndex:selected];
    _subcatid = [data valueForKey:@"subcatid"];
    self.title = [data valueForKey:@"subcatname"];
    [_collectionSubCat scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
    _subCat = nil;
    canLoadMore = YES;
    self.title= NSLocalizedString([data valueForKey:@"subcatname"], [data valueForKey:@"subcatname"]);
    [self loadMore];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    SelectFeatureViewController *vc = [segue destinationViewController];
    vc.delegate = self;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *selectedInfofid = [_subCat dictionaryAtIndex:indexPath.row];
    
    UIStoryboard *storyboard = [DataManager getStoryboard];
    
	if ([infofid isEqualToString:@"1"] && ![[selectedInfofid valueForKey:@"data"] count]) {
		WebViewController *wv  = [storyboard instantiateViewControllerWithIdentifier:@"LandingD"];
		wv.address = [selectedInfofid valueForKey:@"fullshopurl"];
		wv.title = [selectedInfofid valueForKey:@"title"];
		[self.navigationController pushViewController:wv animated:YES];
	}else if ([infofid isEqualToString:@"3"]){
		NSMutableArray *TmpImage = [NSMutableArray new];
		for (NSString *image in [selectedInfofid valueForKey:@"pdfdata"]) {
			NSURL *_url = [NSURL URLWithString:image];
			[TmpImage addObject:_url];
		}
		ImageSlideViewController *iv = [storyboard instantiateViewControllerWithIdentifier:@"LandingE"];
		iv.imageList = TmpImage;
		
		[self presentViewController:iv animated:YES completion:nil];
	}else{
		LandingNewViewController *lv = [storyboard instantiateViewControllerWithIdentifier:@"LandingNew"];
		lv.product = _subCat;
		lv.accessKey = _subCat.key[indexPath.row];
        lv.landing = [_subCat accessKey:lv.accessKey];
        lv.feature = self.infofid;
		[self.navigationController pushViewController:lv animated:YES];
        
	}
}

@end
