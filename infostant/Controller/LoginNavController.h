//
//  LoginNavController.h
//  infostant
//
//  Created by indevizible on 1/21/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoginDelegate;
@interface LoginNavController : UINavigationController
@property (nonatomic, assign) id<LoginDelegate> ldelegate;
@property BOOL canRotation;
+(id)loginViewDelegate:(id<LoginDelegate>)delegate;
-(void)didLogin;
@end

@protocol LoginDelegate <NSObject>

-(void)didLogin:(LoginNavController *)vc;

@end