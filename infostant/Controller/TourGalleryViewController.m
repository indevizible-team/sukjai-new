//
//  TourGalleryViewController.m
//  www
//
//  Created by Trash on 3/22/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//
#define kDestination @1
#define kSpecial @2
#import "TourGalleryViewController.h"
#import "ImageGalleryCell.h"
#import "MyTourViewController.h"
#import "Trip.h"
@interface TourGalleryViewController (){
    NSURLRequest *lastRequest;
    NSMutableArray *myTripList;
    int page;
    NSNumber *whereIs;
}

@end

@implementation TourGalleryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.tour.type == kTourSuggestionTypeDestination) {
        whereIs = kDestination;
    }else if(self.tour.type == kTourSuggestionTypeSpecial){
        whereIs = kSpecial;
    }
   
	// Do any additional setup after loading the view.
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing: ) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];
    [self reset];
    [self loadMore];
}

-(void)reset{
    myTripList = nil;
    page = 1;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dropViewDidBeginRefreshing:(UIRefreshControl *)refreshControl
{
    [self reset];
    [self loadMore];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    Trip *trip = myTripList[indexPath.row];
    ImageGalleryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GalleryCell" forIndexPath:indexPath];
    [cell.imageView setImageWithURL:trip.image];
    return cell;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
   return  myTripList.count;
}

-(void)loadMore{
    NSMutableDictionary *param = [DataManager param];
    [param setValue:[NSNumber numberWithInt:page++] forKey:@"page"];
    [param setValue:whereIs forKey:@"whereis"];
    [param setValue:@"catid" forKey:@"where"];
    [param setValue:@"rand()" forKey:@"orderby"];
    lastRequest = [[DataManager client] requestWithMethod:@"GET" path:@"getMyTrips" parameters:param];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:lastRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"req : %@",request.URL);
        [self.refreshControl endRefreshing];
        DataController *tmp = [[DataController alloc] initWithDictionary:[DataManager JSONToMutableDictionary:JSON]];
        if ([[param valueForKey:@"page"] isEqualToNumber:@1]) {
            myTripList = [NSMutableArray new];
        }
        for (int i=0; i<tmp.key.count; i++) {
            [myTripList addObject:[Trip tripWithData:[tmp dictionaryAtIndex:i]]];
        }
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        [self.collectionView reloadData];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"error , %@",request.URL);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [self.refreshControl endRefreshing];
    }];
    [operation start];
    
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:sender];
    Trip *trip = myTripList[indexPath.row];
    MyTourViewController *vc = [segue destinationViewController];
    unsigned int rand = arc4random()%trip.tagKey.count;
    vc.tag = trip.tagKey[rand];
    vc.sortType = SortTypeDESC;
    vc.sortMethod = SortMethodCreate;
    vc.whereIs = whereIs;
    vc.title = [NSString stringWithFormat:@"#%@",trip.tagData[trip.tagKey[rand]][@"tag"]];
}

@end
