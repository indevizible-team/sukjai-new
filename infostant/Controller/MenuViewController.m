//
//  MenuViewController.m
//  socioville
//
//  Created by Valentin Filip on 09.04.2012.
//  Copyright (c) 2012 App Design Vault. All rights reserved.
//

#import "MenuViewController.h"
#import "MainViewController.h"
#import "ProfileViewController.h"
#import "FeatureViewController.h"
#import "FeedPageViewController.h"
#import "MessageViewController.h"
#import "FollowListViewController.h"
#import "LikeListViewController.h"
#import "RegisterViewController.h"

#import "SettingViewController.h"
#import "FollowDetailViewController.h"
#import "AddContentViewController.h"
#import "AddContentFirstViewController.h"
#import "DataSource.h"
#import "MenuItem.h"
#import "AppDelegate.h"
#import "DataManager.h"
#import "LoginNavController.h"
#import "AppDelegate.h"
@interface MenuViewController ()<UIAlertViewDelegate,LoginDelegate>{
    AppDelegate *appDelegate;
}

@property (nonatomic, strong) DataSource    *dataSource;
@property (nonatomic, strong) NSIndexPath   *currentSelection;
//@property (nonatomic, strong) User *user;
@end

@implementation MenuViewController 

//@synthesize searchBar;
@synthesize tableView;
@synthesize currentSelection;
@synthesize dataSource;

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
   
    appDelegate.menuView = self;
    
    self.dataSource = [DataSource dataSource];
//    _user = [User currentUser];
    [self.tableView reloadData];
}

- (void)viewDidUnload
{
//    [self setSearchBar:nil];
    [self setTableView:nil];
    [super viewDidUnload];
}
-(void)reloadUser{
//    _user = [User currentUser];
    self.dataSource = [DataSource dataSource];
    [self.tableView reloadData];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) return 1;
    else
        return [(self.dataSource.items)[section-1] count];

    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section==0) return 0;
    return 24;
}
-(NSString *)setSectionName:(NSString *)name{
    return [NSString stringWithFormat:@"  %@",[name uppercaseString]];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

        NSString *sectionName = nil;
        switch(section)
        {
            case 1:
                sectionName = [self setSectionName:NSLocalizedString(@"infoAppName", @"")];
                break;
            case 2:
                sectionName = [self setSectionName:NSLocalizedString(@"infoSectionAcc", @"")];
                break;
        }
    
        UILabel *sectionHeader = [[UILabel alloc] init];
        sectionHeader.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"menuSectionBg.png"]];
        sectionHeader.font = [UIFont boldSystemFontOfSize:13];
        sectionHeader.textColor = [UIColor whiteColor];
        sectionHeader.text = sectionName;
        return sectionHeader;

}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        cell = [aTableView dequeueReusableCellWithIdentifier:@"MenuUserCell"];
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgSideBarCell.png"]];
        cell.textLabel.text = ([User currentUser])?[User currentUser].displayname:NSLocalizedString(@"infoGuestName", @"");
        [DataManager setProfileImage:cell.imageView];
        
        
    } else {
        cell = [aTableView dequeueReusableCellWithIdentifier:@"MenuCell"];
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgSideBarCell.png"]];
        MenuItem *item =[MenuItem itemWithData:(self.dataSource.items)[indexPath.section-1][indexPath.row]] ;
//        UIImageView *imgRow = (UIImageView *)[cell viewWithTag:1];
        UILabel *lblText = (UILabel *)[cell viewWithTag:2];
//        imgRow.image = item.image;
        lblText.text = item.name;
        UIView *countView = nil;
        if ([item.eventCount intValue] > 0) {
            NSString *countString = [NSString stringWithFormat:@"%@", item.eventCount];
            CGSize sizeCount = [countString sizeWithFont:[UIFont systemFontOfSize:14.0f]];
            
            UIImage *bkgImg = [UIImage imageNamed:@"sidemenu-count.png"];
            countView = [[UIImageView alloc] initWithImage:[bkgImg stretchableImageWithLeftCapWidth:10 topCapHeight:10]];
            countView.frame = CGRectIntegral(CGRectMake(0, 0, sizeCount.width + 2*10, bkgImg.size.height-5));
            
            UILabel *lblCount = [[UILabel alloc] initWithFrame:CGRectIntegral(CGRectMake(10, ((bkgImg.size.height-sizeCount.height)/2)-2, sizeCount.width, sizeCount.height))];
            lblCount.text = countString;
            lblCount.backgroundColor = [UIColor clearColor];
            lblCount.textColor = [UIColor whiteColor];
            lblCount.textAlignment = NSTextAlignmentCenter;
            lblCount.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f];
            lblCount.shadowColor = [UIColor darkGrayColor];
            lblCount.shadowOffset = CGSizeMake(0, 1);
            [countView addSubview:lblCount];
        }
        cell.accessoryView = countView;
    }
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL login = ([User currentUser])?YES:NO;
    MainViewController *mainController = (MainViewController *)self.parentViewController;
    if((indexPath.row == 0)&&(indexPath.section == 0)){
        if (!login) {
            [self openLoginPage];
        }else{
            UIViewController *profileVC = [DataManager controllerByID:@"ProfileDisplay"];
            InfostantNavigationViewController *nav = [[InfostantNavigationViewController alloc] initWithRootViewController:profileVC];
            mainController.contentViewController = nav;
        }
        [mainController toggleSidebar:!mainController.leftSidebarShowing duration:kGHRevealSidebarDefaultAnimationDuration];
    }else{
        MenuItem *item =[MenuItem itemWithData:(self.dataSource.items)[indexPath.section-1][indexPath.row]] ;
        UITableViewCell *cell = [aTableView cellForRowAtIndexPath:currentSelection];
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgSideBarCell.png"]];
        currentSelection = indexPath;
        cell = [aTableView cellForRowAtIndexPath:indexPath];
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgSideBarCellSelect.png"]];
        
        if (item.loginAction) {
            if (!login) {
                [self openLoginPage];
                [mainController toggleSidebar:!mainController.leftSidebarShowing duration:kGHRevealSidebarDefaultAnimationDuration];
            }else{
                UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoAppName", @"") message:NSLocalizedString(@"infoMsgLogout", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") otherButtonTitles:@"Logout", nil];
                [alert show];
            }
        }else{
            if (!login && [item.needToLogin boolValue]) {
                [self openLoginPage];
                [mainController toggleSidebar:!mainController.leftSidebarShowing duration:kGHRevealSidebarDefaultAnimationDuration];
            }else{
                if (item.path != nil) {
                    NSMutableDictionary *param = [DataManager param];
                    [param addEntriesFromDictionary:[User currentUser].oauth_login_dict];
                    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"POST" path:item.path parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                        AFJSONRequestOperation *change = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"POST" path:@"getCountAlldata" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                            if ([JSON valueForKey:@"expire"]==nil) {
                                [PLIST writeFeature:@"noti" withData:JSON];
                                [self reloadUser];
                            }else{
                                [self openLoginPage];
                            }
                        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                            
                        }];
                        [change start];
                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                        
                    }];
                    [operation start];
                }
                [self setFrontViewByStoryboardID:item.controllerID];
                [mainController toggleSidebar:!mainController.leftSidebarShowing duration:kGHRevealSidebarDefaultAnimationDuration];
            }
        }
    }
}

-(void)setFrontViewByStoryboardID:(NSString *)storyboardID{
    MainViewController *mainController = (MainViewController *)self.parentViewController;
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:storyboardID];
    InfostantNavigationViewController *nav = [[InfostantNavigationViewController alloc] initWithRootViewController:vc];
    mainController.contentViewController = nav;
}

-(void)openLoginPage{
    LoginNavController *nvc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginNav"];
    nvc.ldelegate = self;
    [self presentViewController:nvc animated:YES completion:nil];
}

-(void)didLogin:(LoginNavController *)vc{
    [self reloadUser];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex) {
        MainViewController *mainController = (MainViewController *)self.parentViewController;
        [PLIST writeFeature:@"user" withData:@{}];
        [PLIST writeFeature:@"draft" withData:@{}];
        FeatureViewController *featureVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FeatureVC"];
        InfostantNavigationViewController *nav = [[InfostantNavigationViewController alloc] initWithRootViewController:featureVC];
        mainController.contentViewController = nav;
        [mainController.sidebarViewController reloadUser];
        [mainController toggleSidebar:!mainController.leftSidebarShowing duration:kGHRevealSidebarDefaultAnimationDuration];
        UIApplication *application = [UIApplication sharedApplication];
        application.applicationIconBadgeNumber = 0;
        [self.tableView reloadData];
         
    }
}
@end
