//
//  LoginPopViewController.h
//  infostant
//
//  Created by NINE on 1/21/56 BE.
//  Copyright (c) 2556 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginNavController.h"
#import <FacebookSDK/FacebookSDK.h>
@interface LoginPopViewController : UIViewController <UITextFieldDelegate> {
    LoginNavController *nc;
}
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnClose;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollLogin;
@property (strong, nonatomic) IBOutlet UITextField *txtUsername;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UIButton *btnLogin;
@property (strong, nonatomic) IBOutlet UIButton *btnForgetpass;
@property (strong, nonatomic) IBOutlet UIButton *butnRegis;

@property (strong, nonatomic) id <FBGraphUser> loggedInUser;
@end
