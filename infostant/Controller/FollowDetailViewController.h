//
//  FollowDetailViewController.h
//  infostant
//
//  Created by INFOSTANT on 12/25/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"
#import "SelectFeatureViewController.h"
@interface FollowDetailViewController : UIViewController {
    NSString *infofid;
    BOOL isMe,canLoadMore;
    int page;
    
}
@property (strong, nonatomic) IBOutlet UILabel *lblLikes;
@property (strong, nonatomic) IBOutlet UILabel *lblFollowing;
@property (strong, nonatomic) IBOutlet UILabel *lblFollower;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionFollow;
@property (strong, nonatomic) IBOutlet UIImageView *profilePic;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblLocation;
@property (strong, nonatomic) IBOutlet UILabel *flwerNumber;
@property (strong, nonatomic) IBOutlet UILabel *flwngNumber;
@property (strong, nonatomic) IBOutlet UILabel *likeNumber;


@property (strong, nonatomic) NSMutableDictionary *personProfile;
@property (strong, nonatomic) DataController *profile;
@property (strong, nonatomic) DataController *allFeature;
@property (strong, nonatomic) NSString *friendid;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *openCamera;
@property (strong, nonatomic) IBOutlet UIButton *followBtn;
- (IBAction)followAction:(id)sender;

@end
