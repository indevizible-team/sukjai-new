//
//  ListViewController.m
//  socioville
//
//  Created by Valentin Filip on 05.04.2012.
//  Copyright (c) 2012 App Design Vault. All rights reserved.
//

#import "NotiListViewController.h"
#import "AppDelegate.h"
#import "User.h"
#import "NotiListCell.h"
#import "Notification.h"
#import "NotificationMsgViewController.h"
#import "CategoryViewController.h"
#import "MyTripListViewController.h"
#import "Bitly.h"   
#import "Trip.h"
#import "DMURL.h"
@interface NotiListViewController (){
    int page;
    BOOL canLoadMore;
    User *user;
    DataController *notiData;
    UIRefreshControl *refreshControl;
}
@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;
@end

@implementation NotiListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)refresh:(id)sender {
    notiData = nil;
    user = [User currentUser];
    page =1;
    canLoadMore = YES;
    [self loadNotification];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing: ) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    [self refresh:nil];
}
- (void)dropViewDidBeginRefreshing:(UIRefreshControl *)refreshControl
{
    [self refresh:nil];

}
-(void)loadNotification{
    if (canLoadMore) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        canLoadMore = !canLoadMore;
        NSMutableDictionary *param = [DataManager param];
        [param addEntriesFromDictionary:user.oauth_login_dict];
        [param setValue:[NSNumber numberWithInt:page++] forKey:@"page"];
        
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"GET" path:@"getNotificationCenter" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            [refreshControl endRefreshing];
            NSLog(@"finish load %@",request.URL);
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            if ([JSON count]) {
                if (notiData == nil) {
                    notiData = [[DataController alloc] initWithDictionary:[DataManager JSONToMutableDictionary:JSON]];
                    
                }else{
                    [notiData insertData:[DataManager JSONToMutableDictionary:JSON]];
                }
                [self.tableView reloadData];
               
                canLoadMore = !canLoadMore;
            }
           
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
            [refreshControl endRefreshing];
        }];
         [operation start];
    }
}

- (void)viewDidUnload {
    [super viewDidUnload];
}
- (void)viewDidAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.title = @"Notfications";
	
    UINavigationController *nav = self.navigationController;
    UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			self.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
		}
		
		// Check if we have a revealButton already.
		if (![self.navigationItem leftBarButtonItem]&&(self.navigationController.viewControllers.count==1)) {
			// If not, allocate one and add it.
			UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
            
            UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [menuButton setImage:imageMenu forState:UIControlStateNormal];
            menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
            [menuButton addTarget:controller action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
		}
	}
    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (notiData == nil) {
        return 0;
    }
    return notiData.key.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        return 60;
    }
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        return 80;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Notification *noti = [Notification dataSourceWithData:[notiData dictionaryAtIndex:indexPath.row]];
    NotiListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NotiListCell"];
    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"listTable.png"]];
    [cell.lblName setText:noti.displayname];
    [cell.lblMessage setText:noti.msg];
    [cell.lblTime setText:noti.createdate];
    [cell.profilePic setImageWithURL:noti.picuser];
    if (noti.pic!=nil) {
        [cell.screenPic setImageWithURL:noti.pic];
    }else{
        [cell.screenPic setImage:nil];
    }
    if (indexPath.row > notiData.key.count-5) {
        [self loadNotification];
    }

    return cell;
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Notification *noti = [Notification dataSourceWithData:[notiData dictionaryAtIndex:indexPath.row]];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (noti.type == NotiTypeMessage) {
        NotificationMsgViewController *vc =(NotificationMsgViewController *) [DataManager controllerByID:@"NotificationMsgViewController"];
        [self.navigationController pushViewController:vc animated:YES];
        [vc.userImage setImageWithURL:noti.picuser];
        [vc.txtMessage setText:noti.msg];
        [vc.lblDisplayName setText:noti.displayname];
        [vc.lblCreateDate setText:noti.createdate];
       
        
    }
    if (noti.type == NotiTypeNormal) {
        [SVProgressHUD show];
        NSMutableDictionary *param = [DataManager param];
        [param addEntriesFromDictionary:[User currentUser].oauth_login_dict];
        [param addEntriesFromDictionary:@{
                @"page":@1,
                @"infofid":noti.infofid,
                @"productid":noti.productid
         }];
        NSLog(@"reqesting %@",[[DataManager client] requestWithMethod:@"GET" path:@"getNewAllFeature" parameters:param].URL);
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"GET" path:@"getNewAllFeature" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            [SVProgressHUD dismiss];

            if ([JSON count]) {
                DataController *dataCtrl = [[DataController alloc] initWithDictionary:[DataManager JSONToMutableDictionary:JSON]];
                NSMutableDictionary *selectedCat = [dataCtrl dictionaryAtIndex:0];
                if (([noti.infofid isEqualToString:@"1"] && ![[selectedCat valueForKey:@"data"] count])||[[selectedCat valueForKey:@"banner"] integerValue]==1) {
                    WebViewController *wv  = (WebViewController *)[DataManager controllerByID:@"LandingD"];
                    wv.address = [selectedCat valueForKey:@"fullshopurl"];
                    wv.title = NSLocalizedString([selectedCat valueForKey:@"title"],[selectedCat valueForKey:@"title"]);
                    [self.navigationController pushViewController:wv animated:YES];
                }else if ([noti.infofid isEqualToString:@"3"]){
                    NSMutableArray *TmpImage = [NSMutableArray new];
                    for (NSString *image in [selectedCat valueForKey:@"pdfdata"]) {
                        NSURL *_url = [NSURL URLWithString:image];
                        [TmpImage addObject:_url];
                    }
                    ImageSlideViewController *iv = (ImageSlideViewController *)[DataManager controllerByID:@"LandingE"];
                    iv.imageList = TmpImage;
                    
                    [self presentViewController:iv animated:YES completion:nil];
                }else{
                    LandingNewViewController *lv =(LandingNewViewController *) [DataManager controllerByID:@"LandingNew"];
                    lv.product = dataCtrl;
                    lv.accessKey = dataCtrl.key[0];
                    lv.landing = [dataCtrl accessKey:lv.accessKey];
                    lv.feature = noti.infofid;
                    [self.navigationController pushViewController:lv animated:YES];
                }
            }else{
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNoContent", @"")];
            }
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNoInternet", @"")];
        }];
        [operation start];
   

    }
    if (noti.type == NotiTypeExternalLink) {
        
        
            NSURL *ourURL = [NSURL URLWithString:noti.link];
            if ([ourURL.host isEqualToString:@"bit.ly"]) {
                [SVProgressHUD show];
                [Bitly expandFromShortURL:ourURL success:^(NSURLRequest *request, NSURL *longURL) {
                    NSMutableDictionary *query = [longURL dictionaryForQueryString];
                    Trip *myTrip= [Trip new];
                    if (query[@"code"]&&query[@"tour"]) {
                        [SVProgressHUD dismiss];
                        [self.navigationController popViewControllerAnimated:NO];
                        myTrip.code = query[@"code"];
                        MyTripListViewController *vc = (MyTripListViewController *)[DataManager controllerByID:@"MyTripListViewController"];
                        vc.trip = myTrip;
                        [self.navigationController pushViewController:vc animated:NO];
                    }else{
                        WebViewController *wv  = [WebViewController webViewWithURLString:noti.link];
                        [self.navigationController pushViewController:wv animated:YES];
                    }
        
                } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                    [SVProgressHUD showErrorWithStatus:@"Something wrong"];
                }];
                
            }else{
                WebViewController *wv  = [WebViewController webViewWithURLString:noti.link];
                [self.navigationController pushViewController:wv animated:YES];
            }
        

        
        
        
    }
 
}

@end
