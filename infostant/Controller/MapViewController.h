//
//  MapViewController.h
//  infostant
//
//  Created by indevizible on 1/6/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>
#import "DisplayMap.h"
#import "WebViewController.h"
@interface MapViewController : UIViewController{
    DisplayMap *ann;
    CLLocationManager *locationManager;
}
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnGetDirection;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property float lat;
@property float lng;
@property (strong,nonatomic) NSString *placeName;
@end
