//
//  AddContentViewController.h
//  infostant
//
//  Created by indevizible on 1/22/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"
@interface AddContentViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>{
    DataController *dc;
    UIAlertView *alert;
}

@property (strong, nonatomic) IBOutlet UICollectionView *collectionFeature;
@property (strong, nonatomic) NSArray *dataKey;
@property (strong, nonatomic) NSMutableDictionary *dataSource;
@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;

@end