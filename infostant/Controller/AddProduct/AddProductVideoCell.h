//
//  AddProductVideoCell.h
//  infostant
//
//  Created by indevizible on 1/24/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddProductVideoCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *videoThumbnail;


@end
