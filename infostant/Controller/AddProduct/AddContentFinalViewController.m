//
//  AddContentFinalViewController.m
//  infostant
//
//  Created by indevizible on 1/29/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "AddContentFinalViewController.h"
#import "DataManager.h" 
#import "AddProductSelectCategoryViewController.h"
#import "AddProductFinishCell.h"
#import "FeatureViewController.h"
#import "FollowDetailViewController.h"
#import "DraftViewController.h"
#define thumbnailRaio  420.0f/350.0f
typedef enum{
    kFinish,
    kTakePhoto
}UIActionSheetTag;
typedef enum{
    kTryConnect,
    kSaveDraft
}UIAlertViewTag;
@interface AddContentFinalViewController (){
    NSArray *dataSoruce;
    DataController *category;
    NSString *shopName;
    NSString *keywords;
    NSString *shopUrl;
}

@end

@implementation AddContentFinalViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadCategory];
    dataSoruce = @[@{@"title":NSLocalizedString(@"infoShopName", @""),         @"cellId":@"KeywordsCell"},
                   @{@"title":NSLocalizedString(@"infoSelectCate", @""),   @"cellId":@"CatSelectCell"},
                   @{@"title":NSLocalizedString(@"infoKeywords", @""),          @"cellId":@"KeywordsCell"},
                   @{@"title":NSLocalizedString(@"infoShopURL", @""),          @"cellId":@"ShopURLCell"},
                   @{@"title":NSLocalizedString(@"infoShopThumbnail", @""),    @"cellId":@"AddProductTakePhotoCell"}];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if (category != nil) {
        return dataSoruce.count;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *CellIdentifier = [[dataSoruce objectAtIndex:indexPath.section] valueForKey:@"cellId"];
    AddProductFinishCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (indexPath.section != 1) {
        if (self.thumbnailImage!=nil && indexPath.section ==4) {
            [cell.image setImage:self.thumbnailImage];
        }else{
            if (indexPath.section == 0) {
                shopName = cell.value.text;
                cell.textLabel.font = [DataManager fontSize:@"small"];
            }else if (indexPath.section == 2){
                keywords = cell.value.text;
                cell.textLabel.font = [DataManager fontSize:@"small"];
            }
            else{
//                cell.value.text = shopUrl;
                shopUrl = cell.value.text;
                cell.textLabel.font = [DataManager fontSize:@"small"];
            }
            
        }
        
//        cell.backgroundColor = [UIColor clearColor];
    }else{
        if (self.selectedCategory!=nil) {
            cell.textLabel.text = [NSString stringWithFormat:@"%@/%@",[self.selectedCategory valueForKey:@"catname"],[self.selectedCategory valueForKey:@"subcatname"]];
        }else{
            cell.textLabel.text = @"Select Category";
            cell.textLabel.font = [DataManager fontSize:@"small"];
        }
//        cell.backgroundColor = [UIColor clearColor];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 4) {
        return 260.0f;
    }
    return 44.0f;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 4) {
        [self takePhoto];
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [[dataSoruce objectAtIndex:section] valueForKey:@"title"];
}


#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == kTakePhoto) {
        if (buttonIndex == 0) {
            if ([UIImagePickerController isSourceTypeAvailable:
                 UIImagePickerControllerSourceTypeCamera])
            {
                
                [self preparePickerType:UIImagePickerControllerSourceTypeCamera];
                [self presentViewController:self.imagePicker.imagePickerController
                                   animated:YES completion:nil];
                newMedia = YES;
            }
        }else if (buttonIndex == 1){
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                if ([self.myPopoverController isPopoverVisible]) {
                    [self.myPopoverController dismissPopoverAnimated:YES];
                } else {
                    if ([UIImagePickerController isSourceTypeAvailable:
                         UIImagePickerControllerSourceTypeSavedPhotosAlbum])
                    {
                        
                        [self preparePickerType:UIImagePickerControllerSourceTypePhotoLibrary];
                        self.myPopoverController = [[UIPopoverController alloc]
                                                    initWithContentViewController:self.imagePicker.imagePickerController];
                        self.myPopoverController.delegate = self;
                        [self.myPopoverController presentPopoverFromRect:CGRectMake(44, 20, 200, 200) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
                        newMedia = NO;
                    }
                }
                
            } else {
                if ([UIImagePickerController isSourceTypeAvailable:
                     UIImagePickerControllerSourceTypeSavedPhotosAlbum])
                {
                    [self preparePickerType:UIImagePickerControllerSourceTypePhotoLibrary];
                    [self presentViewController:self.imagePicker.imagePickerController animated:YES completion:nil];
                    newMedia = NO;
                }
                
            }
        }

    }else{
        if (buttonIndex<2) {
            
            [self prepareDataToPost];
            
            if (buttonIndex) {//save draft
                [self saveDraft];
                
            }else{//send to server
                [self postToServer];
            }
        }
    }
}

-(void)postToServer{
    NSURLRequest *reqest  = [[DataManager client] requestWithMethod:@"POST" path:@"saveFeature" parameters:param];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:reqest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [SVProgressHUD dismiss];
        
        FollowDetailViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileDisplay"];
        [self.navigationController pushViewController:profileVC animated:YES];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        //connection error
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoNoInternet", @"") message:NSLocalizedString(@"infoSaveDraft", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel",@"") otherButtonTitles:NSLocalizedString(@"infoSaveDraft",@"") , nil];
        alert.tag = kSaveDraft;
        [alert show];
    }];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        float percentDone = ((float)((int)totalBytesWritten) / (float)((int)totalBytesExpectedToWrite));
        [SVProgressHUD showProgress:percentDone];
    }];
    [operation start];

}
#pragma  mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == kTryConnect) {
        if (buttonIndex) {
            [self loadCategory];
        }
    }else if (alertView.tag == kSaveDraft){
        if (buttonIndex) {
            [self saveDraft];
        }
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self.myPopoverController dismissPopoverAnimated:true];
    }else{
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
	
    NSString *mediaType = [info
						   objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image = [info
						  objectForKey:UIImagePickerControllerEditedImage];
		didChangeImage = YES;
        
        self.thumbnailImage = image;
      

        [self.tableView reloadData];
        if (newMedia){
			[self dismissViewControllerAnimated:YES completion:nil];
            
            //			[self refreshSize];
            //saveimage
            UIImageWriteToSavedPhotosAlbum(image,
										   self,
										   @selector(image:finishedSavingWithError:contextInfo:),
										   nil);
		}
    }
	
}




#pragma mark - private function
- (void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
	if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
							  initWithTitle: NSLocalizedString(@"infoTitleSaveFail", @"")
							  message: NSLocalizedString(@"infoMsgSaveFail", @"")
							  delegate: nil
							  cancelButtonTitle:NSLocalizedString(@"infoOK",@"")
							  otherButtonTitles:nil];
        [alert show];
	}
}
- (void)prepareDataToPost{
//    NSMutableDictionary *firstData = [self.allData objectAtIndex:0];
    DataController *secData = self.landingData;
//    NSMutableDictionary *user = [DataManager user];
    User *user = [User currentUser];
    param = [DataManager param];
    [param setValue:@1 forKey:@"infofid"];
    [param setValue:user.oauth_login forKey:@"oauth_login"];
    [param setValue:shopUrl forKey:@"shopurl"];
    [param setValue:keywords forKey:@"keyword"];
    [param setValue:shopName forKey:@"title"];
    [param setValue:[self.selectedCategory valueForKey:@"catid"] forKey:@"catid"];
    [param setValue:[self.selectedCategory valueForKey:@"subcatid"] forKey:@"subcatid"];
    [param setValue:[DataManager base64FromUIImage:self.thumbnailImage] forKey:@"photo"];
    int n=0;
    NSMutableArray *convertedSecData = [NSMutableArray new];
    NSMutableDictionary *postData = [NSMutableDictionary new];
    
    for (int i=0; i<secData.key.count; i++) {
        BOOL add = YES;
        NSMutableDictionary *tmpData = [NSMutableDictionary new];
        NSMutableDictionary *each = [secData dictionaryAtIndex:i];
        if (each.count>1) {
            
            int infotid = [[each valueForKey:@"infotid"] intValue];
            if (infotid == 1) {
                [tmpData setValue:@1 forKey:@"infotid"];
                [tmpData setValue:[DataManager base64FromUIImage:[each valueForKey:@"pic"]] forKey:@"image"];
            }else if(infotid ==2){
                [tmpData setValue:@2 forKey:@"infotid"];
                NSMutableArray *galleryGroup = [NSMutableArray new];
                
                for (int i=1; i<=4; i++) {
                    NSString *imageKey =[NSString stringWithFormat:@"pic-%i",i];
                    if ([each valueForKey:imageKey]!=nil) {
                        [galleryGroup addObject:[DataManager base64FromUIImage:[each valueForKey:imageKey]]];
                    }
                }
                if (![galleryGroup count]) {
                    add = NO;
                }else{
                    [tmpData setValue:galleryGroup forKey:@"gallarygroup"];
                    
                }
            }else if (infotid == 3){
                [tmpData setValue:@3 forKey:@"infotypeid"];
                [tmpData setValue:@5 forKey:@"infotid"];
                [tmpData setValue:@"" forKey:@"title"];
                [tmpData setValue:[each valueForKey:@"detail"] forKey:@"description"];
            }else if (infotid ==4){
                [tmpData setValue:@1 forKey:@"infotypeid"];
                [tmpData setValue:@3 forKey:@"infotid"];
                [tmpData setValue:[each valueForKey:@"link"] forKey:@"embed"];
                
            }else if (infotid == 5){
                [tmpData setValue:@4 forKey:@"infotypeid"];
                [tmpData setValue:@5 forKey:@"infotid"];
                [tmpData setValue:[each valueForKey:@"title"] forKey:@"title"];
                [tmpData setValue:[each valueForKey:@"description"] forKey:@"description"];
            }else if (infotid == 6){
                [tmpData setValue:[each valueForKey:@"lat"] forKey:@"lat"];
                [tmpData setValue:[each valueForKey:@"lng"] forKey:@"lng"];
                [tmpData setValue:@4 forKey:@"infotid"];
            }
            if (add) {
                [postData setValue:tmpData forKey:[NSString stringWithFormat:@"%i",n]];
                n++;
                [convertedSecData addObject:tmpData];
            }
        }
        
    }
    [param setValue:postData forKey:@"arraydata"];
}
-(void)takePhoto{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel",@"")  destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"infoTakePhoto",@"") ,NSLocalizedString(@"infoChoosePhoto",@"") , nil];
    popup.tag=kTakePhoto;
    popup.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [popup showInView:self.view];
}
-(void)sendData{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"infoVerify", @"")];
    NSMutableDictionary *paramShopURLCheker = [DataManager param];
    [paramShopURLCheker setValue:shopUrl forKey:@"shopurl"];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"POST" path:@"checkshopurlfeature" parameters:paramShopURLCheker] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if ([[JSON valueForKey:@"error"] isKindOfClass:[NSString class]]) {
            UIAlertView *err = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoError", @"")  message:[JSON valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"infoOK",@"")  otherButtonTitles: nil];
            [err show];
            
        }else{
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel",@"") destructiveButtonTitle:NSLocalizedString(@"infoUpload",@"")otherButtonTitles:NSLocalizedString(@"infoSaveDraft",@"") , nil];
            
            actionSheet.tag= kFinish;
            [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
            [actionSheet showInView:self.view];
            
        }
        [SVProgressHUD dismiss];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD dismiss];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoNoInternet", @"") message:NSLocalizedString(@"infoSaveDraft", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel",@"") otherButtonTitles:NSLocalizedString(@"infoSaveDraft",@""), nil];
        alert.tag = kSaveDraft;
        [alert show];
    }];
    [operation start];
}
-(void)saveDraft{
    NSMutableDictionary *draft = [PLIST readFeature:@"draft"];
    if ([draft valueForKey:@"data"]!=nil) {
        if ([[draft valueForKey:@"data"] count]) {
            NSMutableArray *tmpDraft = [draft valueForKey:@"data"];
            [tmpDraft    addObject:param];
            [PLIST writeFeature:@"draft" withData:draft];
        }else{
            [PLIST writeFeature:@"draft" withData:@{@"data":@[param]}];
        }
    }else{
        [PLIST writeFeature:@"draft" withData:@{@"data":@[param]}];
    }
    [self.navigationController pushViewController:[DataManager controllerByID:@"DraftViewController"] animated:YES];
}
-(void)loadCategory{
    NSMutableDictionary *paramGetCatList = [DataManager param];
    [paramGetCatList setValue:@1 forKey:@"infofid"];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"GET" path:@"getCategorylist" parameters:paramGetCatList] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        category = [[DataController alloc] initWithDictionary:[DataManager JSONToMutableDictionary:JSON]];
        
        [self.tableView reloadData];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoNoInternet", @"") message:NSLocalizedString(@"infoTryAgain", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel",@"") otherButtonTitles:NSLocalizedString(@"infoYes",@""), nil];
        alert.tag = kTryConnect;
        [alert show];
    }];
    [operation start];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    AddProductSelectCategoryViewController *catVC = [segue destinationViewController];
    catVC.category = category;
    
}

- (IBAction)finishBtn:(id)sender {
//    AddProductFinishCell *tmpCell;
    UIAlertView *errMsg;
    NSString *errMsgText;
    
//    tmpCell= (AddProductFinishCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
//    shopName = tmpCell.value.text;
//    tmpCell = (AddProductFinishCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:2]];
//    keywords = tmpCell.value.text;
//    tmpCell = (AddProductFinishCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:3]];
//    shopUrl = tmpCell.value.text;
//
    NSLog(@"%@-%@-%@",shopName,keywords,shopUrl);
    if (self.thumbnailImage == nil) {
        errMsgText = NSLocalizedString(@"infoErrMsgSetThumbnail", @"");
    }else if (shopUrl == nil ){
        errMsgText = NSLocalizedString(@"infoErrMsgURLEmpty", @"");
    }else if (self.selectedCategory == nil){
        errMsgText = NSLocalizedString(@"infoErrMsgSelectCate", @"");
    }else if (shopName == nil){
        errMsgText = NSLocalizedString(@"infoErrMsgShopName", @"");
    }
    if (errMsgText != nil) {
        errMsg = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoError", @"") message:errMsgText delegate:self cancelButtonTitle:NSLocalizedString(@"infoOK",@"") otherButtonTitles:nil];
        [errMsg show];
    }else{
        [self sendData];
    }
}

# pragma mark -
# pragma mark GKImagePicker Delegate Methods

- (void)imagePicker:(GKImagePicker *)imagePicker pickedImage:(UIImage *)image{
    
    if (imagePicker.imagePickerController.sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImageWriteToSavedPhotosAlbum(image,
                                       self,
                                       @selector(image:finishedSavingWithError:contextInfo:),
                                       nil);
    }
    
    self.thumbnailImage = image;
    
    [self.tableView reloadData];
    
    [self hideImagePicker];
    
}

- (void)hideImagePicker{
    if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM()) {
        
        [self.myPopoverController dismissPopoverAnimated:YES];
        
    } else {
        
        [self.imagePicker.imagePickerController dismissViewControllerAnimated:YES completion:nil];
        
    }
}
-(void)preparePickerType:(UIImagePickerControllerSourceType)type{
    self.imagePicker = [[GKImagePicker alloc] init];
    self.imagePicker.cropSize = [DataManager getSizeFromRatio:thumbnailRaio];
    self.imagePicker.delegate = self;
    self.imagePicker.imagePickerController.sourceType = type;
    self.imagePicker.imagePickerController.mediaTypes = [NSArray arrayWithObjects:
                                                         (NSString *) kUTTypeImage,
                                                         nil];
}
@end
