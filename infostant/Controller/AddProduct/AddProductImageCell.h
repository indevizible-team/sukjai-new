//
//  AddProductImageCell.h
//  infostant
//
//  Created by indevizible on 1/23/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddProductImageCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *image;

@end
