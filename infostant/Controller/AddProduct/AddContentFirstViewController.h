//
//  AddContentFirstViewController.h
//  infostant
//
//  Created by indevizible on 1/29/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GKImagePicker.h"
#import <MobileCoreServices/MobileCoreServices.h>
@interface AddContentFirstViewController : UIViewController<UIActionSheetDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,UITextFieldDelegate>{
    BOOL didChangeImage,newMedia;
}
@property (strong, nonatomic) IBOutlet UITextField *txtShopName;
@property (strong, nonatomic) IBOutlet UIButton *btnImage;
//@property (strong, nonatomic) IBOutlet UITextView *txtShopName;

@property (nonatomic, strong) UIPopoverController *myPopoverController;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) NSMutableArray *allData;
@property (nonatomic, strong) NSMutableDictionary *firstData;
@end
