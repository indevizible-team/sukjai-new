//
//  EditAddDataViewController.h
//  infostant
//
//  Created by indevizible on 1/29/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditAddDataViewController : UITableViewController{
   __strong NSString *title,*description;
 
}

@property (nonatomic,strong) NSMutableDictionary *each;
@property (nonatomic,strong) NSArray *dataStructure;
@property (nonatomic,strong) UITableView *parentTable;
@end
