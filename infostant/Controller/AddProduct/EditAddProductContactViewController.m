//
//  EditAddProductContactViewController.m
//  infostant
//
//  Created by indevizible on 1/29/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "EditAddProductContactViewController.h"
#import "AddDataCell.h"
@interface EditAddProductContactViewController ()

@end

@implementation EditAddProductContactViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
   
    dataStructure = @[
                      @{@"type": @"TextField",
                        @"name":@"title",
                        @"title":@"Open hour"},
                      @{@"type": @"TextView",
                        @"name":@"description",
                        @"title":@"Contact"}
                      ];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    AddDataCell *cell  = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%@Cell",[[dataStructure objectAtIndex:indexPath.section] valueForKey:@"type"]] forIndexPath:indexPath];
    
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if (dataStructure==nil) {
        return 0;
    }
    return [dataStructure count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   return 1;
}



-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [[dataStructure objectAtIndex:section] valueForKey:@"title"];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[[dataStructure objectAtIndex:indexPath.section] valueForKey:@"type"] isEqualToString:@"TextView"]) {
        return 150.0f;
    }
    return 45.0f;
}

- (void)keyboardWasShown:(NSNotification *)aNotification {
    //if (keyboardShown) return;

}

@end
