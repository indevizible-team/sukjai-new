//
//  AddProductSelectCategoryCell.h
//  infostant
//
//  Created by indevizible on 1/29/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddProductSelectCategoryCell : UITableViewCell
@property (nonatomic,weak) NSMutableDictionary *data;
@end
