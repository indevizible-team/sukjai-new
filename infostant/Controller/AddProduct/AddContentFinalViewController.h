//
//  AddContentFinalViewController.h
//  infostant
//
//  Created by indevizible on 1/29/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//
#import "LoginNavController.h"
#import "GKImagePicker.h"
#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "DataManager.h"
@interface AddContentFinalViewController : UITableViewController<UIAlertViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,UITextFieldDelegate,GKImagePickerDelegate>{
    BOOL didChangeImage,newMedia;
    NSMutableDictionary *param;
}
@property (nonatomic,strong) NSMutableDictionary *selectedCategory;
@property (nonatomic, strong) UIPopoverController *myPopoverController;
@property (nonatomic, strong) UIImage *thumbnailImage;
@property (nonatomic, strong) DataController *landingData;
@property (nonatomic, strong) GKImagePicker *imagePicker;
@end
