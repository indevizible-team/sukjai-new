//
//  MapDropPinViewController.m
//  infostant
//
//  Created by indevizible on 1/29/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "MapDropPinViewController.h"

@interface MapDropPinViewController ()

@end

@implementation MapDropPinViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = NSLocalizedString(@"infoPinmap", @"");
    self.btnSave.title = NSLocalizedString(@"infoSave", @"");
    self.btnDroppin.title = NSLocalizedString(@"infoDroppin", @"");
    self.btnSave.title = NSLocalizedString(@"infoCurLocation", @"");
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [locationManager startUpdatingLocation];
    ann = [[DisplayMap alloc] init];
    MKCoordinateRegion region;
    if ([self.each valueForKey:@"custom"]!=nil) {
        region.center.latitude = [[self.each valueForKey:@"lat"] floatValue];
        region.center.longitude = [[self.each valueForKey:@"lng"] floatValue];
    }else{
        if (self.currentLocation.latitude != 0.0f) {
            region.center = self.currentLocation;
        }else{
            region.center = locationManager.location.coordinate;
        }
    }
    
    region.span.longitudeDelta = 0.01f;
    region.span.latitudeDelta = 0.01f;
    ann.coordinate = region.center;
    [_mapView addAnnotation:ann];

}

-(void)viewDidAppear:(BOOL)animated{
    MKCoordinateRegion region;
    region.span.longitudeDelta = 0.01f;
    region.span.latitudeDelta = 0.01f;
    region.center = ann.coordinate;
    [_mapView setRegion:region animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)dropPin:(id)sender {
    ann.coordinate = self.mapView.centerCoordinate;
    [self.mapView addAnnotation:ann];
}

- (IBAction)getCurrent:(id)sender {
    
    ann.coordinate = locationManager.location.coordinate;
    MKCoordinateRegion region;
    region.center = locationManager.location.coordinate;
    region.span.longitudeDelta = 0.01f;
    region.span.latitudeDelta = 0.01f;
    [_mapView setRegion:region animated:YES];
}
- (IBAction)btnSave:(id)sender {
    [self.each setValue:[NSNumber numberWithFloat:ann.coordinate.latitude] forKey:@"lat"];
    [self.each setValue:[NSNumber numberWithFloat:ann.coordinate.longitude] forKey:@"lng"];
    [self.each setValue:@YES forKey:@"custom"];
    [self.parentTable reloadData];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
