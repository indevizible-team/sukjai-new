//
//  AddProductSelectSubCategoryViewController.h
//  infostant
//
//  Created by indevizible on 1/29/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddProductSelectSubCategoryViewController : UITableViewController
@property (nonatomic,strong) NSMutableDictionary *category;

@end
