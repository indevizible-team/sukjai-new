//
//  MapDropPinViewController.h
//  infostant
//
//  Created by indevizible on 1/29/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "DisplayMap.h"
@interface MapDropPinViewController : UIViewController{
    DisplayMap *ann;
    CLLocationManager *locationManager;
}
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnCurLocation;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnDroppin;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnSave;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic ,strong) UITableView *parentTable;
@property (nonatomic,strong) NSMutableDictionary *each;
@property CLLocationCoordinate2D currentLocation;
@end
