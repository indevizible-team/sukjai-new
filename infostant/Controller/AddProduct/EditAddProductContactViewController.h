//
//  EditAddProductContactViewController.h
//  infostant
//
//  Created by indevizible on 1/29/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditAddProductContactViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>{
    NSArray *dataStructure;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;


@end
