//
//  AddProductContactCell.h
//  infostant
//
//  Created by indevizible on 1/29/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddProductContactCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *txtOpenHour;
@property (strong, nonatomic) IBOutlet UITextView *txtContactDescription;

@end
