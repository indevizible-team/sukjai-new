//
//  AddContentFirstViewController.m
//  infostant
//
//  Created by indevizible on 1/29/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "AddContentFirstViewController.h"
#import "AddProductViewController.h"
#import "GKImagePicker.h"
@interface AddContentFirstViewController ()<GKImagePickerDelegate>
@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;
@property (nonatomic, strong) GKImagePicker *imagePicker;
@end

@implementation AddContentFirstViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.firstData = [NSMutableDictionary new];
	// Do any additional setup after loading the view.
    
    UINavigationController *nav = self.navigationController;
    UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			self.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
		}
		
		// Check if we have a revealButton already.
		if (![self.navigationItem leftBarButtonItem]&&(self.navigationController.viewControllers.count==1)) {
			// If not, allocate one and add it.
			UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
            
            UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [menuButton setImage:imageMenu forState:UIControlStateNormal];
            menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
            [menuButton addTarget:controller action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
		}
	}

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)takeBtn:(id)sender {
    [self takePhoto];
}
-(void)takePhoto{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel",@"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"infoTakePhoto",@""),NSLocalizedString(@"infoChoosePhoto",@""), nil];
    
    popup.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [popup showInView:self.view];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
	if (buttonIndex == 0) {
		if ([UIImagePickerController isSourceTypeAvailable:
			 UIImagePickerControllerSourceTypeCamera])
		{
            
            [self preparePickerType:UIImagePickerControllerSourceTypeCamera];
			[self presentViewController:self.imagePicker.imagePickerController
                               animated:YES completion:nil];
			newMedia = YES;
		}
	}else if (buttonIndex == 1){
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            if ([self.myPopoverController isPopoverVisible]) {
                [self.myPopoverController dismissPopoverAnimated:YES];
            } else {
                if ([UIImagePickerController isSourceTypeAvailable:
                     UIImagePickerControllerSourceTypeSavedPhotosAlbum])
                {
                    
                    [self preparePickerType:UIImagePickerControllerSourceTypePhotoLibrary];
                    self.myPopoverController = [[UIPopoverController alloc]
                                                initWithContentViewController:self.imagePicker.imagePickerController];
                    self.myPopoverController.delegate = self;
                    [self.myPopoverController presentPopoverFromRect:CGRectMake(44, 20, 200, 200) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
                    newMedia = NO;
                }
            }
            
        } else {
            if ([UIImagePickerController isSourceTypeAvailable:
                 UIImagePickerControllerSourceTypeSavedPhotosAlbum])
            {
                [self preparePickerType:UIImagePickerControllerSourceTypePhotoLibrary];
                [self presentViewController:self.imagePicker.imagePickerController animated:YES completion:nil];
                newMedia = NO;
            }
            
        }
    }
}


-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
	if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
							  initWithTitle: NSLocalizedString(@"infoTitleSaveFail", @"")
							  message: NSLocalizedString(@"infoMsgSaveFail", @"")
							  delegate: nil
							  cancelButtonTitle:NSLocalizedString(@"infoOK",@"")
							  otherButtonTitles:nil];
        [alert show];
	}
}
- (IBAction)nextBtnClick:(id)sender {
    NSString *errMsg;
    if ([self.firstData valueForKey:@"photo"]==nil) {
        errMsg = NSLocalizedString(@"infoErrorMsgSetlogo", @"");
    }else if (![self.txtShopName.text length]){
        errMsg = NSLocalizedString(@"infoErrorMsgEmpty", @"");
    }else if ([self.txtShopName.text length] > 55){
        errMsg = NSLocalizedString(@"infoErrorMaxLength", @"");
    }
    if (errMsg != nil) {
        UIAlertView *alert = [[UIAlertView alloc]
							  initWithTitle: NSLocalizedString(@"infoError", @"")
							  message: errMsg
							  delegate: nil
							  cancelButtonTitle:NSLocalizedString(@"infoOK",@"")
							  otherButtonTitles:nil];
        [alert show];
    }else{
        [self.firstData setValue:self.txtShopName.text forKey:@"title"];
        self.allData = [NSMutableArray new];
        [self.allData addObject:self.firstData];
        
        AddProductViewController *vc = [[DataManager getStoryboard] instantiateViewControllerWithIdentifier:@"AddProductViewController"];
        vc.allData = self.allData;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    
   
   
}
# pragma mark -
# pragma mark GKImagePicker Delegate Methods

- (void)imagePicker:(GKImagePicker *)imagePicker pickedImage:(UIImage *)image{
    
    if (imagePicker.imagePickerController.sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImageWriteToSavedPhotosAlbum(image,
                                       self,
                                       @selector(image:finishedSavingWithError:contextInfo:),
                                       nil);
    }
    
    self.imageView.image = image;
    [self.firstData setValue:image forKey:@"photo"];
    
    [self hideImagePicker];
   
}

- (void)hideImagePicker{
    if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM()) {
        
        [self.myPopoverController dismissPopoverAnimated:YES];
        
    } else {
        
        [self.imagePicker.imagePickerController dismissViewControllerAnimated:YES completion:nil];
        
    }
}
-(void)preparePickerType:(UIImagePickerControllerSourceType)type{
    self.imagePicker = [[GKImagePicker alloc] init];
    self.imagePicker.cropSize = CGSizeMake(320, 145);
    self.imagePicker.delegate = self;
    self.imagePicker.imagePickerController.sourceType = type;
    self.imagePicker.imagePickerController.mediaTypes = [NSArray arrayWithObjects:
                                                         (NSString *) kUTTypeImage,
                                                         nil];
}

@end
