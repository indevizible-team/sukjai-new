//
//  EditAddDataViewController.m
//  infostant
//
//  Created by indevizible on 1/29/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "EditAddDataViewController.h"
#import "AddDataCell.h"
@interface EditAddDataViewController ()

@end

@implementation EditAddDataViewController
@synthesize dataStructure;

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {


    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
//    dataStructure = @[
//                      @{@"type": @"TextField",
//                        @"name":@"title",
//                        @"title":@"Open hour"},
//                      @{@"type": @"TextView",
//                        @"name":@"description",
//                        @"title":@"Contact"}
//                      ];
//    self.each = [[NSMutableDictionary alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return [dataStructure count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *item = [dataStructure objectAtIndex:indexPath.section];
    AddDataCell *cell  = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%@Cell",[item valueForKey:@"type"]] forIndexPath:indexPath];
    
    if ([[item valueForKey:@"type"] isEqualToString:@"TextField"]) {
//        cell.txtInput.text = [self.each valueForKey:[item valueForKey:@"name" ]];
        self.each[@"name"] = cell.txtInput.text;
    }else{
//        cell.txtViewInput.text = [self.each valueForKey:[item valueForKey:@"name"]];
        self.each[@"name"] = cell.txtViewInput.text;
    }
    
    
    return cell;
}
- (IBAction)btnSave:(id)sender {
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
//    [self.parentTable beginUpdates];
    for (int i=0; i<[dataStructure count]; i++) {
         NSMutableDictionary *item = [dataStructure objectAtIndex:i];
        AddDataCell *cell = (AddDataCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:i]];
        if ([[item valueForKey:@"type"] isEqualToString:@"TextField"]) {
            [self.each setValue:cell.txtInput.text forKey:[[dataStructure objectAtIndex:i] valueForKey:@"name"]];
            
        }else{
            [self.each setValue:cell.txtViewInput.text forKey:[[dataStructure objectAtIndex:i] valueForKey:@"name"]];
        }
    }
//    [self.parentTable endUpdates];
    [self.parentTable reloadData];
    NSLog(@"each - %@",self.each);
    [self.navigationController popViewControllerAnimated:YES];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([[[dataStructure objectAtIndex:indexPath.section] valueForKey:@"type"] isEqualToString:@"TextView"]) {
        return 150.0f;
    }
    return 45.0f;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [[dataStructure objectAtIndex:section] valueForKey:@"title"];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    NSLog(@"xxx");
//    [self.tableView reloadData];
}
@end
