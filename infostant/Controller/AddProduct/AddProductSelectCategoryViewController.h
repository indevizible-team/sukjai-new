//
//  AddProductSelectCategoryViewController.h
//  infostant
//
//  Created by indevizible on 1/29/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"

@interface AddProductSelectCategoryViewController : UITableViewController<UIAlertViewDelegate>
@property (nonatomic,strong) DataController *category;

@end
