//
//  FeedPageViewController.h
//  infostant
//
//  Created by indevizible on 1/17/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"
@interface FeedPageViewController : UITableViewController{
    DataController *featureDataController;
}
@property (strong,nonatomic) DataController *feature;
@property (strong,nonatomic) NSMutableArray *featureList;

@end
