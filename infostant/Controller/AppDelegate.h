//
//  AppDelegate.h
//  infostant
//
//  Created by INFOSTANT on 12/24/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "MenuViewController.h"
#import "MenuRightViewController.h"
#import "MsgDetailViewController.h"
#import "MainViewController.h"
#import "GAI.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) MenuViewController *menuView;
@property (strong, nonatomic) MenuRightViewController *menuRigntView;
@property (strong, nonatomic) MsgDetailViewController *messageView;
@property (strong, nonatomic) UIColor *defaultColor;
@property (strong, nonatomic) NSDictionary *infoDictionary;
@property (strong, nonatomic) NSURL *tmpURL;
@property(nonatomic, retain) id<GAITracker> tracker;
@end
