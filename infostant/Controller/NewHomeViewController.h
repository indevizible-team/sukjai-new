//
//  NewHomeViewController.h
//  www
//
//  Created by Trash on 3/22/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TourPackage.h"
#import "TourSuggestion.h"
#import "CategoryButton.h"
#import "MagazineButton.h"
@interface NewHomeViewController : UICollectionViewController<UICollectionViewDelegateFlowLayout>

@end
