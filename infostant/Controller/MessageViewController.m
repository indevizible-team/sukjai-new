//
//  MessageViewController.m
//  infostant
//
//  Created by INFOSTANT on 12/25/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import "MessageViewController.h"
#import "AppDelegate.h"
#import "DataManager.h" 
#import "SVProgressHUD.h"
#import "MessageListCell.h"
#import "MsgDetailViewController.h"
@interface MessageViewController ()<LoginDelegate>{
    UIRefreshControl *refreshControl;
    BOOL firstTime;
}
@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;
@end

@implementation MessageViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    firstTime = YES;
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing: ) forControlEvents:UIControlEventValueChanged];
    [refreshControl setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Pull to refresh"]];
    [self.tableView addSubview:refreshControl];
    page = 1;
    canLoadMore = YES;
    
    [self loadMore];
}
- (void)dropViewDidBeginRefreshing:(UIRefreshControl *)refreshControl
{
    chatList=nil;
    page = 1;
    canLoadMore = YES;
    [self loadMore];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}
-(void)loadMore{
    
    canLoadMore = NO;
    User *user = [User currentUser];
    if (user!=nil) {
            if (page==1 && firstTime) {
                firstTime = NO;
            }
            AFHTTPClient *client = [DataManager client];
            NSMutableDictionary *param = [@{
                                          @"page":[NSString stringWithFormat:@"%i",page++],
                                          
                                          } mutableCopy];
            [param addEntriesFromDictionary:[DataManager param]];
            [param addEntriesFromDictionary:user.oauth_login_dict];
           
            
            NSURLRequest *request = [client requestWithMethod:@"GET" path:@"getmsgcenter" parameters:param];
            NSLog(@"requesting : %@",request.URL);
            AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                 [refreshControl endRefreshing];
                if ([JSON count]) {
                    canLoadMore = YES;
                    if ([JSON valueForKey:@"expire"]!=nil) {
                        [self openLogin];
                    }else{
                        if (chatList == nil) {
                            chatList = [[DataController alloc] initWithDictionary:[DataManager JSONToMutableDictionary:JSON]];
                        }else{
                            [chatList insertData:[DataManager JSONToMutableDictionary:JSON]];
                        }
                        
                        [_chatCenterTable reloadData];
                    }
                }
                
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                [refreshControl endRefreshing];
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNetworkError", @"")];
            }];
            [operation start];
        
    }

}
-(void)openLogin{
    LoginNavController *nvc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginNav"];
    nvc.ldelegate = self;
    [self presentViewController:nvc animated:YES completion:nil];
    
}
-(void)didLogin:(LoginNavController *)vc{
    chatList = nil;
    page =1;
    [self loadMore];
}
- (void)viewDidAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.title = NSLocalizedString(@"infoMessage", @"");
	
    UINavigationController *nav = self.navigationController;
    UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			self.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
		}
		
		// Check if we have a revealButton already.
		if (![self.navigationItem leftBarButtonItem]&&(self.navigationController.viewControllers.count==1)) {
			// If not, allocate one and add it.
			UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
            
            UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [menuButton setImage:imageMenu forState:UIControlStateNormal];
            menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
            [menuButton addTarget:controller action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
		}
        
//        if (![self.navigationItem rightBarButtonItem]&&[[[[NSBundle mainBundle] infoDictionary] valueForKey:@"RightMenu"] boolValue])
//        {
//            self.navigationItem.rightBarButtonItem = self.editButtonItem;
//        }
        
	}
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (chatList == nil) {
        return 0;
    }
    return chatList.key.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        return 60;
    }
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        return 80;
    }
    return 0;
}

//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingstyleForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return UITableViewCellEditingStyleDelete;
//}

- (BOOL)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSLog(@"%@", indexPath);
    chatList = nil;
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}


- (MessageListCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *chatRow = [chatList.data valueForKey:chatList.key[indexPath.row]];
    NSLog(@"chatRow : %@",chatRow);
    MessageListCell *cell = (MessageListCell *)[tableView dequeueReusableCellWithIdentifier:@"MessageListCell"];
    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"listTable.png"]];
    [cell.profilePic setImageWithURL:[NSURL URLWithString:[chatRow valueForKey:@"picuser"]]];
    cell.lblName.text = ([[chatRow valueForKey:@"displayname"] length])?[chatRow valueForKey:@"displayname"]:[chatRow valueForKey:@"username"];
    cell.lblTime.text = [chatRow valueForKey:@"ago"];
    cell.lblMessage.text = [chatRow valueForKey:@"msg"];
    cell.friendid = [chatRow valueForKey:@"mid"];
    
    if (canLoadMore && indexPath.row > chatList.key.count) {
        [self loadMore];
    }
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(MessageListCell *)sender{
    MsgDetailViewController *vc = (MsgDetailViewController *)[segue destinationViewController];
    vc.friendid = sender.friendid;
}

@end
