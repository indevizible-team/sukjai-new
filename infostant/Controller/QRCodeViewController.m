//
//  QRCodeViewController.m
//  www
//
//  Created by indevizible on 2/23/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "QRCodeViewController.h"
#import "QRScannerViewController.h"
@interface QRCodeViewController ()

@end

@implementation QRCodeViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view
    // the delegate receives decode results
    QRScannerViewController *qrvc = [[QRScannerViewController alloc] initWithNibName:@"QRScannerViewController" bundle:nil];
    
//    [self presentViewController:qrvc animated:NO completion:nil];
    [self.navigationController pushViewController:qrvc animated:NO];
    [self setSlideBar:qrvc];
  }
-(void)viewDidAppear:(BOOL)animated{
   
}
- (void) viewWillDisappear: (BOOL) animated
{
   
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL) shouldAutorotateToInterfaceOrientation: (UIInterfaceOrientation) orient
{
    // auto-rotation is supported
    return(YES);
}

- (void) willRotateToInterfaceOrientation: (UIInterfaceOrientation) orient
                                 duration: (NSTimeInterval) duration
{
    // compensate for view rotation so camera preview is not rotated
}

-(void)setSlideBar:(QRScannerViewController *)vc{
    UINavigationController *nav = vc.navigationController;
    UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[vc.navigationController.navigationBar gestureRecognizers] containsObject:vc.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			vc.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[vc.navigationController.navigationBar addGestureRecognizer:vc.navigationBarPanGestureRecognizer];
		}
		
		// Check if we have a revealButton already.
		if (![vc.navigationItem leftBarButtonItem]) {
			// If not, allocate one and add it.
			UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
            
            UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [menuButton setImage:imageMenu forState:UIControlStateNormal];
            menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
            [menuButton addTarget:controller action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            vc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
		}
        if (![vc.navigationItem rightBarButtonItem]&&[[[[NSBundle mainBundle] infoDictionary] valueForKey:@"RightMenu"] boolValue]) {
			// If not, allocate one and add it.
			UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
            
            UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [menuButton setImage:imageMenu forState:UIControlStateNormal];
            menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
            [menuButton addTarget:controller action:@selector(revealRightToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            vc.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
		}
	}
}


@end
