//
//  AddContentViewController.m
//  infostant
//
//  Created by indevizible on 1/22/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "AddContentViewController.h"
#import "AddContentSelectViewController.h"
#import "FeatureCell.h"
#import "AppDelegate.h"
#import "CategoryViewController.h"
#import "SVProgressHUD.h"
@interface AddContentViewController ()

@end

@implementation AddContentViewController


@synthesize navigationBarPanGestureRecognizer;
@synthesize dataSource, dataKey;

- (void)viewDidUnload {
    [super viewDidUnload];
}
-(void)viewDidLoad{
    
    dc = [DataController new];
	[SVProgressHUD show];
    self.view.userInteractionEnabled = NO;
    dataKey = dc.key;
    
    [self saveFeature];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}
-(void)saveFeature{
    AFHTTPClient *client = [DataManager client];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:@"getAllFeatureBanner" parameters:[DataManager param] ];
    
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [PLIST writeFeature:@"feature" withData:JSON];
//        [self loadFeatureData];
        [self arrangeCollectionView];
        self.view.userInteractionEnabled = YES;
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoAppName", @"") message:NSLocalizedString(@"infoNetworkError", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"infoTryAain", @"") otherButtonTitles: nil];
        [alert show];
    }];
    [operation start];
}

//-(void)loadFeatureData{
//    AFHTTPClient *client = [DataManager client];
//    NSMutableDictionary *param = [DataManager param];
//    [param setValue:@1 forKey:@"infofid"];
//	NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:@"getAllCategorylist" parameters:param ];
//	AFJSONRequestOperation *operation = [[AFJSONRequestOperation alloc] initWithRequest:request];
//	[operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//		[SVProgressHUD dismiss];
//        NSMutableDictionary *mutableCopy = (NSMutableDictionary *)CFBridgingRelease(CFPropertyListCreateDeepCopy(kCFAllocatorDefault, (CFDictionaryRef)responseObject, kCFPropertyListMutableContainers));
//        
//        dc = [[DataController alloc] initWithDictionary:mutableCopy];
//		dataKey = dc.key;
//		[self.collectionFeature reloadData];
//        self.view.userInteractionEnabled = YES;
//	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        alert = [[UIAlertView alloc] initWithTitle:@"Infostant" message:@"Network error" delegate:self cancelButtonTitle:@"Try Again" otherButtonTitles: nil];
//        [alert show];
//    }];
//	[operation start];
//    [self arrangeCollectionView];
//}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self saveFeature];
}


- (void)viewDidAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.title = NSLocalizedString(@"Infostant", @"Infostant");
	
    UINavigationController *nav = self.navigationController;
    UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			self.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
		}
		
		// Check if we have a revealButton already.
		if (![self.navigationItem leftBarButtonItem]&&(self.navigationController.viewControllers.count==1)) {
			// If not, allocate one and add it.
			UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
            
            UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [menuButton setImage:imageMenu forState:UIControlStateNormal];
            menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
            [menuButton addTarget:controller action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
		}
	}
}
- (void)arrangeCollectionView {
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.collectionFeature.collectionViewLayout;

    
    self.collectionFeature.collectionViewLayout = flowLayout;
    [self.collectionFeature reloadData];
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self arrangeCollectionView];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
    }
}

#pragma mark – UICollectionViewDelegateFlowLayout

//// 1
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//	CGSize retval = CGSizeMake(152, 140);
//	return retval;
//}
#pragma mark - UICollectionView Datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    //    return [Repository dataIPad].count;
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    //    return [[Repository dataIPad][section][@"rows"] count];
    //    return dc.key.count;
    if (self.collectionFeature==nil) {
        return 0;
    }
    return 6;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = dc.key[indexPath.row];
    FeatureCell *cell;
    cell = [cv dequeueReusableCellWithReuseIdentifier: @"featureCell" forIndexPath:indexPath];
    cell.lblTitle.text = [[dc.data valueForKey:key] valueForKey:@"name"];
    cell.lblTitle.font = [DataManager fontSize:@"head"];

    NSArray *imageInfostant = @[
                                @"catShop.png",
                                @"catMarKet.png",
                                @"catCatalog.png",
                                @"catPromotion.png",
                                @"catPlace.png",
                                @"catEvent.png"
                                ];
    [cell.imageView setImage:[UIImage imageNamed:imageInfostant[indexPath.row]]];
    cell.feature = key;
    return cell;
}
- (UICollectionReusableView *)collectionView: (UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *catBanner = [[UICollectionReusableView alloc] init];
    catBanner= [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader                                                       withReuseIdentifier:@"Banner1" forIndexPath:indexPath];
    
    return catBanner;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(FeatureCell*)sender
{

        AddContentSelectViewController *vc = [segue destinationViewController];
        vc.infofid = sender.feature;
    
}
@end
