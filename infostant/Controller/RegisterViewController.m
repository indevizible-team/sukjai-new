//
//  RegisterViewController.m
//  infostant
//
//  Created by INFOSTANT on 12/26/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import "RegisterViewController.h"
#import "AppDelegate.h"
#import "DataManager.h"
#import "MainViewController.h"
#import "FeatureViewController.h"
@interface RegisterViewController ()

@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation RegisterViewController
@synthesize navigationBarPanGestureRecognizer;
@synthesize labels;
@synthesize placeholders;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.labels = [NSArray arrayWithObjects:@"Username",
                   @"Password",
                   @"Re-Password",
                   @"Email",
                   nil];
	
	self.placeholders = [NSArray arrayWithObjects:@"Enter username",
                         @"Enter password",
                         @"Confirm password",
                         @"example@infostant.com",
                         nil];
}


- (void)viewDidAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	self.title = NSLocalizedString(@"Register", @"Register");
	
    UINavigationController *nav = self.navigationController;
    UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			self.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
		}
		
		// Check if we have a revealButton already.
		if (![self.navigationItem leftBarButtonItem]&&(self.navigationController.viewControllers.count==1)) {
			// If not, allocate one and add it.
			UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
            
            UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [menuButton setImage:imageMenu forState:UIControlStateNormal];
            menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
            [menuButton addTarget:controller action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
		}

	}
}


#pragma mark -
#pragma mark Table view data source

- (void)configureCell:(ELCTextFieldCell *)cell atIndexPath:(NSIndexPath *)indexPath {
	
	cell.leftLabel.text = [self.labels objectAtIndex:indexPath.row];
	cell.rightTextField.placeholder = [self.placeholders objectAtIndex:indexPath.row];
    if (indexPath.row==1||indexPath.row==2) {
        cell.rightTextField.secureTextEntry=YES;
    }
    else if (indexPath.row==3){
        [cell.rightTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    }
    
	cell.indexPath = indexPath;
	cell.delegate = self;
    //Disables UITableViewCell from accidentally becoming selected.
    cell.selectionStyle = UITableViewCellEditingStyleNone;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 5;
            break;

        default:
            return 0;
            break;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 25;
            break;
            
        default:
            return 0;
            break;
    }
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell;
    if (indexPath.section == 0&&indexPath.row<4) {
        cell = (ELCTextFieldCell*)[tableView dequeueReusableCellWithIdentifier:@"RegisCell"];
        if (cell == nil) {
            cell = [[ELCTextFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RegisCell"];
        }
        
        [self configureCell:cell atIndexPath:indexPath];
    }
    if (indexPath.section == 0 && indexPath.row==4)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"CountryCell"];
    }
    [cell resignFirstResponder];
    return cell;
}


#pragma mark ELCTextFieldCellDelegate Methods

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    ELCTextFieldCell *textFieldCell = (ELCTextFieldCell*)textField.superview;
    if (![textFieldCell isKindOfClass:ELCTextFieldCell.class]) {
        return;
    }
    //It's a better method to get the indexPath like this, in case you are rearranging / removing / adding rows,
    //the set indexPath wouldn't change
    NSIndexPath *indexPath = [self.tableView indexPathForCell:textFieldCell];
	if(indexPath != nil && indexPath.row < [labels count]-1) {
		NSIndexPath *path = [NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section];
		[[(ELCTextFieldCell*)[self.tableView cellForRowAtIndexPath:path] rightTextField] becomeFirstResponder];
		[self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
	}
	else {
		[[(ELCTextFieldCell*)[self.tableView cellForRowAtIndexPath:indexPath] rightTextField] resignFirstResponder];
	}
}

- (void)updateTextLabelAtIndexPath:(NSIndexPath*)indexPath string:(NSString*)string {
    
	NSLog(@"See input: %@ from section: %d row: %d, should update models appropriately", string, indexPath.section, indexPath.row);
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (IBAction)reg:(id)sender {
    UIAlertView *alert;
    ELCTextFieldCell *cellUser =(ELCTextFieldCell *)[_regisTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    ELCTextFieldCell *cellPass = (ELCTextFieldCell *)[_regisTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    ELCTextFieldCell *cellPass1 = (ELCTextFieldCell *)[_regisTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    ELCTextFieldCell *cellEmail = (ELCTextFieldCell *)[_regisTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    
    if (![cellPass.rightTextField.text isEqualToString:cellPass1.rightTextField.text]) {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoError", @"") message:@"Password not match" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }else if([cellPass.rightTextField.text length] < 1){
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoError", @"") message:@"Please enter password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }else if([cellEmail.rightTextField.text length] < 1){
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoError", @"") message:@"Please enter Email" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }else if([cellUser.rightTextField.text length] < 1){
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoError", @"") message:@"Please enter Email" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }else{
        NSMutableDictionary *regis = [@{
                                   
                                      @"username":cellUser.rightTextField.text,
                                      @"password1":cellPass.rightTextField.text,
                                    
                                      @"email":cellEmail.rightTextField.text
                                      } mutableCopy];
        [regis addEntriesFromDictionary:[DataManager param]];
        NSDictionary *device = [PLIST readFeature:@"device"];
        [regis addEntriesFromDictionary:device];
        
        AFHTTPClient *client = [DataManager client];
        NSURLRequest *request = [client requestWithMethod:@"POST" path:@"saveregister" parameters:regis];
        [SVProgressHUD showWithStatus:NSLocalizedString(@"infoRegisStatus", @"")];
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            
            if (![[JSON valueForKey:@"error"] isKindOfClass:[NSNumber class]]) {
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNetworkError", @"")];
            }else{
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"infoRegisComplete", @"")];
                [PLIST writeFeature:@"user" withData:JSON];
                
                MainViewController *mainController = (MainViewController *)self.parentViewController.parentViewController;
                FeatureViewController *featureVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FeatureVC"];
                InfostantNavigationViewController *nav = [[InfostantNavigationViewController alloc] initWithRootViewController:featureVC];
                mainController.contentViewController = nav;
                [mainController.sidebarViewController reloadUser];
                
            }
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNoInternet", @"")];
        }];
        [operation start];

        
    }
    
    

}
@end
