//
//  EditAddProductTextViewController.h
//  infostant
//
//  Created by indevizible on 1/24/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditAddProductTextViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnDone;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnClear;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnCancel;
@property (strong, nonatomic) IBOutlet UITextView *description;
@property (nonatomic, strong) NSMutableDictionary *targetData;
@property (nonatomic, strong) UITableView *parentTable;
@end
