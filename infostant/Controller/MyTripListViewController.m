//
//  MyTripListViewController.m
//  www
//
//  Created by KEEP CALM AND CODE OK on 3/8/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//
#import "MyTripListViewController.h"
#import "Route.h"
#import "MyTripMapCell.h"
#import "MyTripListCell.h"
#import "WebViewController.h"
#import "ActivitySet.h"
#import "CategoryViewController.h"
#import "SVProgressHUD.h"
#import "Bitly.h"
#import "LoginNavController.h"
#import "DMURL.h"
typedef enum{
    kDatePickerTag,
    SelectButtonIndex,
    CancelButtonIndex
}
DatePickerTag;

typedef enum{
    kActionSheetLongPress,
    kActionSheetDatePicker,
    kActionSheetGlobal
}kActionSheet;
@interface MyTripListViewController ()<UIActionSheetDelegate,UIGestureRecognizerDelegate,LoginDelegate>{
    UIDatePicker *pickerView;
    NSIndexPath *selectedIndex;
    NSMutableArray *routeList,*sectionName;
    NSURL *map;
    Bitly *shorten;
    CLLocationManager *locationManager;
}
@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;

@end

@implementation MyTripListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [locationManager startUpdatingLocation];
    
    
    [SVProgressHUD show];
    routeList = [NSMutableArray new];
    sectionName = [NSMutableArray new];
    if(_canEdit){
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handleLongPress:)];
        lpgr.minimumPressDuration = 1.0; //seconds
        lpgr.delegate = self;
        [self.tableView addGestureRecognizer:lpgr];
    }
    
    NSMutableDictionary *param = [DataManager param];
    [param setValue:_trip.code forKey:@"code"];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"GET" path:@"getSubMyTrips" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
 
        if (JSON[@"map"]) {
            float lat,lng;
            lat =locationManager.location.coordinate.latitude;
            lng =locationManager.location.coordinate.longitude;
            NSURL *url;
            url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@",JSON[@"map"],[NSString stringWithFormat:@"&my=%f|%f",lat,lng]] stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
            map = url;
            NSLog(@"map :%@",map);
  
        }
        _trip.title =  self.title = JSON[@"title"];
       
        for (NSString *sectionKey in JSON[@"key"]) {
            [sectionName addObject:[NSString stringWithFormat:@"Day %@",sectionKey]];
            NSDictionary  *eachSection = JSON[sectionKey];
            NSMutableArray *rowRoute = [NSMutableArray new];
            NSLog(@"->%@",eachSection);
            if (eachSection.count) {
                for (NSString *rowKey in eachSection[@"key"]) {
                    [rowRoute addObject:[Route routeWithData:eachSection[rowKey]]];
                }
            }
           
            NSLog(@"->%@",eachSection);
            [routeList addObject:rowRoute];
        }
        [SVProgressHUD dismiss];
        [self.tableView reloadData];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD showErrorWithStatus:@"Can not fetch data from server"];
    }];
    [operation start];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return routeList.count+1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (!section) {
        return 1;
    }
    return [routeList[section-1] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!indexPath.section) {
        static NSString *CellIdentifier = @"MyTripMapCell";
        MyTripMapCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        if (!cell.webView.request) {
            
                
          
            NSLog(@"%@",map.absoluteString);
            [cell.webView loadRequest:[NSURLRequest requestWithURL:map]];
        }
        
        
        return cell;

    }
    
    Route *route = routeList[indexPath.section-1][indexPath.row];
    
    static NSString *CellIdentifier = @"MyTripListCell";
    MyTripListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    [cell.thumbnailView setImageWithURL:route.image];
    cell.title.text = route.title;
    cell.date.text = route.dateString;
    
    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (!section) {
        return nil;
    }
    return sectionName[section-1];
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    
}

// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section) {
        Route *route = routeList[indexPath.section-1][indexPath.row];
        
        [SVProgressHUD show];
        NSMutableDictionary *param = [DataManager param];
        [param addEntriesFromDictionary:[User currentUser].oauth_login_dict];
        [param addEntriesFromDictionary:@{
         @"page":@1,
         @"infofid":route.infofid,
         @"productid":route.productid
         }];
        NSLog(@"reqesting %@",[[DataManager client] requestWithMethod:@"GET" path:@"getNewAllFeature" parameters:param].URL);
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"GET" path:@"getNewAllFeature" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            [SVProgressHUD dismiss];
          
            if ([JSON count]) {
                DataController *dataCtrl = [[DataController alloc] initWithDictionary:[DataManager JSONToMutableDictionary:JSON]];
                NSMutableDictionary *selectedCat = [dataCtrl dictionaryAtIndex:0];
                if (([route.infofid isEqualToString:@"1"] && ![[selectedCat valueForKey:@"data"] count])||[[selectedCat valueForKey:@"banner"] integerValue]==1) {
                    WebViewController *wv  = (WebViewController *)[DataManager controllerByID:@"LandingD"];
                    wv.address = [selectedCat valueForKey:@"fullshopurl"];
                    wv.title = NSLocalizedString([selectedCat valueForKey:@"title"],[selectedCat valueForKey:@"title"]);
                    [self.navigationController pushViewController:wv animated:YES];
                }else if ([route.infofid isEqualToString:@"3"]){
                    NSMutableArray *TmpImage = [NSMutableArray new];
                    for (NSString *image in [selectedCat valueForKey:@"pdfdata"]) {
                        NSURL *_url = [NSURL URLWithString:image];
                        [TmpImage addObject:_url];
                    }
                    ImageSlideViewController *iv = (ImageSlideViewController *)[DataManager controllerByID:@"LandingE"];
                    iv.imageList = TmpImage;
                    
                    [self presentViewController:iv animated:YES completion:nil];
                }else{
                    LandingNewViewController *lv =(LandingNewViewController *) [DataManager controllerByID:@"LandingNew"];
                    lv.product = dataCtrl;
                    lv.accessKey = dataCtrl.key[0];
                    lv.landing = [dataCtrl accessKey:lv.accessKey];
                    lv.feature = route.infofid;
                    [self.navigationController pushViewController:lv animated:YES];
                }
            }else{
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNoContent", @"")];
            }
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNoInternet", @"")];
        }];
        [operation start];
        

    }
    
    

}
- (IBAction)action:(UIBarButtonItem *)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"infoAddTrip", @""),NSLocalizedString(@"infoShare", @""), nil];
    actionSheet.tag = kActionSheetGlobal;
     [actionSheet showFromBarButtonItem:sender animated:YES];
    
//    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!indexPath.section) {
        return 207.0f;
    }
    return 101.0f;
}
- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    if (actionSheet.tag==kActionSheetDatePicker) {
        pickerView = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 40, 320, 216)];
        
        //Configure picker...
        [pickerView setMinuteInterval:5];
        [pickerView setTag: kDatePickerTag];
        [pickerView addTarget:self action:@selector(didChangeDate:) forControlEvents:UIControlEventValueChanged];
        //Add picker to action sheet
        [actionSheet addSubview:pickerView];
        
        
        
        //Gets an array af all of the subviews of our actionSheet
        NSArray *subviews = [actionSheet subviews];
        
        [[subviews objectAtIndex:SelectButtonIndex] setFrame:CGRectMake(20, 266, 280, 46)];
        [[subviews objectAtIndex:CancelButtonIndex] setFrame:CGRectMake(20, 317, 280, 46)];
    }
}
-(void)didChangeDate:(UIDatePicker *)picker{
    NSLog(@"%@",picker.date);
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
//    CGPoint p = [gestureRecognizer locationInView:self.tableView];
//    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
//        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
//        if (indexPath ){
//            selectedIndex = indexPath;
//            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") destructiveButtonTitle:NSLocalizedString(@"infoDelete", @"") otherButtonTitles:NSLocalizedString(@"infoEdit", @""), nil];
//            
//            actionSheet.tag = kActionSheetLongPress;
//            [actionSheet showInView:self.view];
//        }
//    }
}


-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex!=[actionSheet cancelButtonIndex]) {
        if (actionSheet.tag == kActionSheetDatePicker) {
            NSLog(@"%@",pickerView.date);
        }else if (actionSheet.tag == kActionSheetLongPress){
            if ([actionSheet destructiveButtonIndex]==buttonIndex) {
                UIAlertView *deleteAlert = [[UIAlertView alloc] initWithTitle:@"Confirm" message:@"Confirm to delete xxxxx" delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") otherButtonTitles:NSLocalizedString(@"infoConfirm", @"") , nil];
                [deleteAlert show];
            }else if (buttonIndex == 1){
                UIActionSheet *asheet = [[UIActionSheet alloc] initWithTitle:@"Pick the date." delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Select", nil];
                asheet.tag =kActionSheetDatePicker;
                [asheet showFromBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES];
                [asheet setFrame:CGRectMake(0, self.view.bounds.size.height-338, 320, 383)];
            }
        }else if(actionSheet.tag == kActionSheetGlobal){
            if (buttonIndex == 0) {
                if ([User currentUser]) {
                    [SVProgressHUD show];
                    NSMutableDictionary *param = [DataManager param];
                    [param setValue:_trip.code forKey:@"code"];
                    [param addEntriesFromDictionary:[User currentUser].oauth_login_dict];
                    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"POST" path:@"saveFriendTrips" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                        NSLog(@"%@->%@",param, JSON);
                        if ([JSON[@"error"] isKindOfClass:[NSString class]]) {
                            [SVProgressHUD showErrorWithStatus:JSON[@"error"]];
                        }else
                        [SVProgressHUD showSuccessWithStatus:@"Complete"];
                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                        [SVProgressHUD showErrorWithStatus:@"Can not fetch data from server"];
                    }];
                    [operation start];
                }else{
                    LoginNavController *nvc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginNav"];
                    nvc.ldelegate = self;
                    [self presentViewController:nvc animated:YES completion:nil];
                }
            }else if (buttonIndex == 1){
                if (!shorten && routeList) {
                    [SVProgressHUD show];
                    AFHTTPClient *client = [DataManager client];
                    NSMutableDictionary *param = [DataManager param];
                    [param setValue:nil forKey:@"json"];
                    [param addEntriesFromDictionary:@{@"tour":@1,@"code":_trip.code}];
                    NSURLRequest *req = [client requestWithMethod:@"GET" path:@"pathredirecttonew" parameters:param];
                    [Bitly shortenURLWithQRImage:req.URL success:^(NSURLRequest *request, NSURL *url, Bitly *bitly, UIImage *qrImage) {
                        shorten = bitly;
                        [SVProgressHUD dismiss];
                        [self showActivity];
                        
                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                        [SVProgressHUD showErrorWithStatus:@"Can not fetch data from server"];
                    }];
                }else if (shorten){
                    [self showActivity];
                }
                
             }
        }else{
            
        }
    }
}

-(void)showActivity{
    APActivityProvider *ActivityProvider = [[APActivityProvider alloc] init];
    
    NSString *text = [NSString stringWithFormat:@"Thailand One Click\n%@\ncode : %@\nGet this trip here >> %@",_trip.title,_trip.code,shorten.data.URL.absoluteString];
    NSArray *Items = @[ActivityProvider,text,shorten.data.URL,shorten.image];
    
    NSMutableArray *Acts = [NSMutableArray new];
    [Acts addObjectsFromArray:[SLComposeActivity getAllActivity]];
    [Acts addObject:[QRCodeActivity new]];
    [Acts addObject:[BroadcastActivity new]];
    UIActivityViewController *ActivityView = [[UIActivityViewController alloc]
                                              initWithActivityItems:Items
                                              applicationActivities:Acts];
    
    [self presentViewController:ActivityView animated:YES completion:nil];

}

-(void)didLogin:(LoginNavController *)vc{
    [vc dismissViewControllerAnimated:YES completion:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([sender isKindOfClass:[MyTripMapCell class]]) {
        NSString *url = ((MyTripMapCell *)sender).webView.request.URL.absoluteString;
        WebViewController *wv = [segue destinationViewController];
        wv.address = url;
    }
}

-(void)setShowSlider:(BOOL)showSlider{
    if (showSlider) {
        UINavigationController *nav = self.navigationController;
        UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
        if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
        {
            // Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
            if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
            {
                // If not, allocate one and add it.
                UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
                self.navigationBarPanGestureRecognizer = panGestureRecognizer;
                
                [self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
            }
            
            // Check if we have a revealButton already.
            if (![self.navigationItem leftBarButtonItem]&&(self.navigationController.viewControllers.count==1)) {
                // If not, allocate one and add it.
                UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
                
                UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [menuButton setImage:imageMenu forState:UIControlStateNormal];
                menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
                [menuButton addTarget:controller action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
                
                self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
            }
            
            if (![self.navigationItem rightBarButtonItem]&&[[[[NSBundle mainBundle] infoDictionary] valueForKey:@"RightMenu"] boolValue]) {
                // If not, allocate one and add it.
                UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
                
                UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [menuButton setImage:imageMenu forState:UIControlStateNormal];
                menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
                [menuButton addTarget:controller action:@selector(revealRightToggle:) forControlEvents:UIControlEventTouchUpInside];
                
                self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
            }
        }

    }
}
@end
