//
//  ImageSlideViewController.h
//  infostant
//
//  Created by Nattawut Singhchai on 1/2/13 .
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageSlideViewController : UIViewController
{
    NSMutableArray *imageArray;
	//    UIScrollView *myScroller;
	IBOutlet UIScrollView *myScroller;
    int nowPageInt;
    int oldPageInt;
    int rotationPageInt;
	CGSize viewSize;
	BOOL showDoneBar;
}
//input array of NSString or UIImage
- (IBAction)DoneButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnDone;
@property (strong, nonatomic) IBOutlet UIToolbar *doneBar;
@property (strong, nonatomic) IBOutlet UITextView *description;
@property (strong, nonatomic) NSString *txtDescription;
@property (nonatomic, strong) NSArray *imageList;
@end
