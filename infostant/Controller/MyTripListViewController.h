//
//  MyTripListViewController.h
//  www
//
//  Created by KEEP CALM AND CODE OK on 3/8/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Trip.h"
@interface MyTripListViewController : UITableViewController
@property (nonatomic,strong) Trip *trip;
@property (nonatomic,assign) BOOL canEdit,showSlider;
@end
