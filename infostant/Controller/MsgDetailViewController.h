//
//  MsgDetailViewController.h
//  infostant
//
//  Created by INFOSTANT on 12/25/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"
#import "DataController.h"
#import "DataManager.h" 
@interface MsgDetailViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UITextFieldDelegate>{
    DataController *chat;
    User *user;
    CGRect _tmpRect;
    int tmpCount;
    AFHTTPClient *client;
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionMsgDetail;

@property (strong, nonatomic) IBOutlet UIView *msgField;
@property (strong, nonatomic) IBOutlet UITextField *txtfieldMsg;
@property (strong, nonatomic) IBOutlet UIButton *btnSend;
- (IBAction)SendText:(id)sender;

@property (strong, nonatomic) NSString *friendid;
-(void)reloadMessage;

@end
