//
//  CategoryViewController.h
//  infostant
//
//  Created by INFOSTANT on 12/24/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectCatViewController.h"
#import "DataManager.h"
#import "SVPullToRefresh.h"
#import "WebViewController.h"
#import "ImageSlideViewController.h"
#import "LandingNewViewController.h"
#import "DataManager.h"
#import "FeatureViewController.h"
@interface CategoryViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,SelectCatViewControllerDelegate>{
	BOOL emptyData,searchMode;
    NSString *searchString;
}

@property (strong, nonatomic) IBOutlet UICollectionView *collectionCategory;
@property (strong, nonatomic) NSArray *dataKey;
@property (strong,nonatomic) NSMutableDictionary *datasource;
@property (strong, nonatomic) NSMutableDictionary *product;
@property (strong, nonatomic) NSArray *productKey;
@property (strong, nonatomic) DataController *dc;
@property (strong, nonatomic) DataController *catData;
@property (strong, nonatomic) NSNumber *page;
@property (strong, nonatomic) NSString *infofid;
@property (strong, nonatomic) NSString *selectedCat;
@property (nonatomic , assign) BOOL isFirstPage;
@end