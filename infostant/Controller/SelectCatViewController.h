//
//  SelectCatViewController.h
//  infostant
//
//  Created by Nattawut Singhchai on 12/29/12 .
//  Copyright (c) 2012 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SelectCatViewControllerDelegate;
@interface SelectCatViewController : UITableViewController{
	NSArray *catList;
}

@property (nonatomic, assign) id<SelectCatViewControllerDelegate> delegate;

@end

@protocol SelectCatViewControllerDelegate <NSObject>

-(NSArray*)selectCatList:(SelectCatViewController*)controller;
-(void)didSelectCat:(SelectCatViewController*)controller selected:(NSInteger*)selected;
-(void)didSearchCat:(SelectCatViewController*)controller forValue:(NSString*)val;
@end