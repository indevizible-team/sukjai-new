//
//  FeatureBanner.h
//  dmconnex
//
//  Created by indevizible on 2/11/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeatureBanner : UICollectionReusableView
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end
