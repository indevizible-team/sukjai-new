//
//  CategoryViewController.m
//  infostant
//
//  Created by INFOSTANT on 12/24/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import "CategoryViewController.h"
#import "AppDelegate.h"
#import "CategoryCell.h"
#import <QuartzCore/QuartzCore.h>
#import "InfostantNavigationViewController.h"
#import "DMPhotoAlbumViewController.h"
@interface CategoryViewController()<DMPhotoAlbumDataSource,DMPhotoAlbumDelegate>{
    UIRefreshControl *refreshControl;
    NSArray *selectedImage;
}
@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;

@end

@implementation CategoryViewController
@synthesize navigationBarPanGestureRecognizer,dataKey,datasource,product, productKey,page;

-(void)viewDidLoad{
	emptyData = NO;
	page = @1;
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing: ) forControlEvents:UIControlEventValueChanged];
    [self.collectionCategory addSubview:refreshControl];
    [refreshControl beginRefreshing];
	[self getCategorylist:self.infofid];
    
}

- (void)dropViewDidBeginRefreshing:(UIRefreshControl *)refreshControl
{
    emptyData = NO;
    self.page = @1;
    [self changeCatKey:self.catData.name];
    
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
  
}
-(void)getCategorylist:(NSString *)catid{
    NSMutableDictionary *param = [DataManager param];
    [param setValue:self.infofid forKey:@"infofid"];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"GET" path:@"getCategorylist" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"req : %@",request.URL);
        self.dc = [[DataController alloc] initWithDictionary:[DataManager JSONToMutableDictionary:JSON]];
        [[self.dc.data valueForKey:@"key"] insertObject:@"0" atIndex:0];
        dataKey = @[];
        productKey = @[];
        if (self.selectedCat) {
            [self changeCatKey:self.selectedCat];
        }else{
            [self changeCatKey:@"0"];
        }
        [self arrangeCollectionView];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"error - %@ from %@",error,request.URL);
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNetworkError", @"")];
        [refreshControl endRefreshing];
    }];
    [operation start];
}

-(void)changeCatKey:(NSString*)catKey{
    NSString *katKey;
    if (catKey != nil) {
        katKey = catKey;
    }else{
        katKey = self.catData.name;
    }
    self.catData = [self.dc accessKey:katKey];
    if ([page integerValue]>1) {
        
    }
    
    if ([catKey isEqualToString:@"0"]) {
        self.title = [[[PLIST readFeature:@"feature"] valueForKey:self.infofid] valueForKey:@"name"];
    }else{
        self.title = [[self.dc.data valueForKey:catKey] valueForKey:@"catname"];
    }
	User *user = [User currentUser];
	
	AFHTTPClient *client = [DataManager client];
	NSMutableDictionary *param = [@{@"page":page,@"catid":katKey,@"infofid":self.infofid} mutableCopy];
    [param addEntriesFromDictionary:[DataManager param]];
    if (searchMode) {
        [param setValue:searchString forKey:@"search"];
    }
    if (user!=nil) {
        [param addEntriesFromDictionary:user.oauth_login_dict];
    }
	NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:@"getNewAllFeature" parameters:param];
	AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"req : %@",request.URL);
       
        [SVProgressHUD dismiss];
		NSMutableDictionary *mutableCopy = (NSMutableDictionary *)CFBridgingRelease(CFPropertyListCreateDeepCopy(kCFAllocatorDefault, (CFDictionaryRef)JSON, kCFPropertyListMutableContainers));
        if ([JSON count]) {
             [self.catData insertData:mutableCopy];
        }
       
		[self.collectionCategory reloadData];

		
        [refreshControl endRefreshing];
		page = [NSNumber numberWithInt:1+[page intValue]];
		if (self.catData.key.count) {
			[self.collectionCategory setContentOffset:CGPointZero];
		}
		
	} failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNoInternet", @"")];
		[refreshControl endRefreshing];
        NSLog(@"error - %@ - %@",error,request.URL);
	}];
	
	[operation start];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewWillAppear:animated];

    UINavigationController *nav = self.navigationController;
    UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			self.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
		}
        if (self.isFirstPage) {
            
            // Check if we have a revealButton already.
            if (![self.navigationItem leftBarButtonItem]&&(self.navigationController.viewControllers.count==1)) {
                // If not, allocate one and add it.
                UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
                
                UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [menuButton setImage:imageMenu forState:UIControlStateNormal];
                menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
                [menuButton addTarget:controller action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
                
                self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
            }
            if (![self.navigationItem rightBarButtonItem]&&[[[[NSBundle mainBundle] infoDictionary] valueForKey:@"RightMenu"] boolValue]) {
                // If not, allocate one and add it.
                UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
                
                UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [menuButton setImage:imageMenu forState:UIControlStateNormal];
                menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
                [menuButton addTarget:controller action:@selector(revealRightToggle:) forControlEvents:UIControlEventTouchUpInside];
                
                self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
            }
        }
	}
}

- (void)arrangeCollectionView {
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.collectionCategory.collectionViewLayout;
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        flowLayout.scrollDirection =  UICollectionViewScrollDirectionVertical;
    } else {
        flowLayout.scrollDirection =  UICollectionViewScrollDirectionHorizontal;
    }
    
    self.collectionCategory.collectionViewLayout = flowLayout;
    [self.collectionCategory reloadData];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self arrangeCollectionView];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
    }
}

#pragma mark – UICollectionViewDelegateFlowLayout

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	CGSize retval;
    int cellMod = indexPath.row%5;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        retval = CGSizeMake( 306, 290);
    }
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        if (cellMod>=2) {
            retval = CGSizeMake(236, 328);
        }
        if (cellMod<2) {
            retval = CGSizeMake(361, 328);
        }
    }
	return retval;
}
#pragma mark - UICollectionView Datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    //    return [Repository dataIPad].count;
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    //    return [[Repository dataIPad][section][@"rows"] count];
    if(self.collectionCategory==nil){
        if ([page isEqualToNumber:@1]) {
            return 0;
        }
        return 1;
    }
    if (self.catData.key.count == 0) {
        if ([page isEqualToNumber:@1]) {
            return 0;
        }
        return 1;
    }
    return self.catData.key.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.collectionCategory == nil ) {
        return [cv dequeueReusableCellWithReuseIdentifier:@"NoRstCell" forIndexPath:indexPath];
    }
    if (self.catData.key.count == 0) {
        return [cv dequeueReusableCellWithReuseIdentifier:@"NoRstCell" forIndexPath:indexPath];
    }
    
    NSString *key = self.catData.key[indexPath.row];
    
    
    
    NSMutableDictionary *productItem = [self.catData.data valueForKey:key];
    
    CategoryCell *cell;
    
    if ([[productItem valueForKey:@"banner"] integerValue]==1) {
        cell = [cv dequeueReusableCellWithReuseIdentifier:@"AdsCell" forIndexPath:indexPath];
        [cell.imageView setImageWithURL:[NSURL URLWithString:[productItem valueForKey:@"pic"]]];
        
        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:cell.bounds];
        cell.layer.masksToBounds = NO;
        cell.layer.borderColor =[UIColor blackColor].CGColor;
        cell.layer.shadowColor = [UIColor blackColor].CGColor;
        cell.layer.shadowOffset = CGSizeMake(0.0f, 0.05f);
        cell.layer.shadowOpacity = 0.5f;
        cell.layer.shadowRadius = 1.0f;
        cell.layer.shadowPath = shadowPath.CGPath;
        
        return cell;
    }
    
    
    int cellMod = indexPath.row%5;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        if (cellMod<2) {
            cell = [cv dequeueReusableCellWithReuseIdentifier: @"DuoCateCell" forIndexPath:indexPath];
        }
        if (cellMod>=2) {
            cell = [cv dequeueReusableCellWithReuseIdentifier: @"TripleCateCell" forIndexPath:indexPath];
        }
    }
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
    {
        cell = [cv dequeueReusableCellWithReuseIdentifier: @"categoryCell" forIndexPath:indexPath];
    }
    

    cell.description.text = [[productItem valueForKey:@"title"] stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceCharacterSet]];
    
    [cell.imageView setImageWithURL:[NSURL URLWithString:[productItem valueForKey:@"pic"]] progressView:cell.progress placeholderImage:nil];
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:cell.bounds];
    cell.layer.masksToBounds = NO;
    cell.layer.borderColor =[UIColor blackColor].CGColor;
    cell.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.layer.shadowOffset = CGSizeMake(0.0f, 0.05f);
    cell.layer.shadowOpacity = 0.5f;
    cell.layer.shadowRadius = 1.0f;
    cell.layer.shadowPath = shadowPath.CGPath;


    cell.product = productItem;
	cell.ownerName.text = [NSString stringWithFormat:@"By %@", [productItem valueForKey:@"displayname"]];
	cell.likeNo.text = [DataManager numberToStr:[[productItem valueForKey:@"countlike"] intValue]];
	cell.followNo.text	=[DataManager numberToStr: [[productItem valueForKey:@"countfollow"] intValue]];
    productItem = nil;
    

    if (self.catData.key.count - indexPath.row < 10) {
        if (!emptyData) {
            [self loadNextPage];
        }else{
            
        }
        
    }
    
    return cell;
    
}


- (void)collectionView:(UICollectionView *)colView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor colorWithRed:105.0f/255 green:105.0f/255 blue:105.0f/255 alpha:0.2f];
}

- (void)collectionView:(UICollectionView *)colView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = nil;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
   
    if (!(self.collectionCategory == nil || self.catData.key.count==0)) {
        NSDictionary *selectedCat = [self.catData.data valueForKey:self.catData.key[indexPath.row]];
        UIStoryboard *storyboard = [DataManager getStoryboard];
        
        if (([self.infofid isEqualToString:@"1"] && ![[selectedCat valueForKey:@"data"] count])||[[selectedCat valueForKey:@"banner"] integerValue]==1) {
            WebViewController *wv  = [storyboard instantiateViewControllerWithIdentifier:@"LandingD"];
            wv.address = [selectedCat valueForKey:@"fullshopurl"];
            wv.title = NSLocalizedString([selectedCat valueForKey:@"title"],[selectedCat valueForKey:@"title"]);
            [self.navigationController pushViewController:wv animated:YES];
        }else if ([self.infofid isEqualToString:@"3"]){
            NSMutableArray *TmpImage = [NSMutableArray new];
            for (NSString *image in [selectedCat valueForKey:@"pdfdata"]) {
//                NSURL *_url = [NSURL URLWithString:image];
                [TmpImage addObject:image];
            }
            selectedImage = TmpImage;
//            ImageSlideViewController *iv = [storyboard instantiateViewControllerWithIdentifier:@"LandingE"];
            DMPhotoAlbumViewController *iv = [DMPhotoAlbumViewController viewWithDelegate:self];
//            iv.imageList = TmpImage;
            
            [self presentViewController:iv animated:YES completion:nil];
        }else{
           
            LandingNewViewController *lv = [storyboard instantiateViewControllerWithIdentifier:@"LandingNew"];
            lv.product = self.catData;
            lv.accessKey = self.catData.key[indexPath.row];
            lv.landing = [self.catData accessKey:lv.accessKey];
            lv.feature = self.infofid;
            [SVProgressHUD animateWithDuration:100 animations:nil completion:^(BOOL finished) {
                [self.navigationController pushViewController:lv animated:YES];
            }];
            [SVProgressHUD show];
            
        }
    }
}

-(NSUInteger)numberOfPhotoAlbum:(DMPhotoAlbumViewController *)photoAlbum{
    return selectedImage.count;
}

-(NSString *)photoAlbum:(DMPhotoAlbumViewController *)photoAlbum captionForIndex:(NSUInteger)index{
    return nil;
}

-(NSString *)photoAlbum:(DMPhotoAlbumViewController *)photoAlbum imageLinkForIndex:(NSUInteger)index{
    return selectedImage[index];
}

-(void)didTaptoAlbumDoneButton:(DMPhotoAlbumViewController *)photoAlbum{
    [photoAlbum dismissViewControllerAnimated:YES completion:nil];
}

-(NSArray *)selectCatList:(SelectCatViewController *)controller{
	NSMutableArray *catList = [NSMutableArray new];
	for (int i=0; i<self.dc.key.count; i++) {
//		NSLog(@"%@",[[self.dc.data valueForKey:[self.dc.key objectAtIndex:i]] valueForKey:@"catname"]);
		[catList addObject:[[self.dc.data valueForKey:[self.dc.key objectAtIndex:i]] valueForKey:@"catname"]];
	}
	return catList;
}
-(void)didSearchCat:(SelectCatViewController *)controller forValue:(NSString *)val{
	NSLog(@"Searching : %@",val);
    
    page = @1;
    emptyData = NO;
    searchMode = YES;
    searchString = val;
    [self.navigationController popViewControllerAnimated:YES];
    [self changeCatKey:nil];
    self.title = val;
	
}
-(void)didSelectCat:(SelectCatViewController *)controller selected:(NSInteger *)selected{
	NSLog(@"Select Cat : %d",(int)selected);
	page = @1;
	emptyData = NO;
    searchMode = NO;
	[self changeCatKey:[self.dc.key objectAtIndex:selected]];
	[self.navigationController popViewControllerAnimated:YES];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
	if ([segue.identifier isEqualToString:@"SelectCat"]) {
		SelectCatViewController *vc = [segue destinationViewController];
		vc.delegate=self;
	}
}

- (NSInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
-(void)loadNextPage{
    emptyData = YES;
    AFHTTPClient *client =[DataManager client];
    NSMutableDictionary *param = [@{@"page":self.page,@"catid":self.catData.name,@"infofid":self.infofid} mutableCopy];
    [param addEntriesFromDictionary:[DataManager param]];
    if (searchMode) {
        [param setValue:searchString forKey:@"search"];
    }
    NSMutableURLRequest *pageRequest = [client requestWithMethod:@"GET" path:@"getNewAllFeature" parameters:param];
    AFJSONRequestOperation *pageOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:pageRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSMutableDictionary *mutableCopy = (NSMutableDictionary *)CFBridgingRelease(CFPropertyListCreateDeepCopy(kCFAllocatorDefault, (CFDictionaryRef)JSON, kCFPropertyListMutableContainers));
        if (![mutableCopy count]) {
            emptyData = YES;
        }else{
            emptyData = NO;
            NSMutableDictionary *tmpProduct = mutableCopy;
            [self.catData insertData:tmpProduct];
            NSLog(@"request finish -> %@",request);
            [self.collectionCategory reloadData];
            NSLog(@"next page->>%@",self.page);
            self.page = [NSNumber numberWithInt:1+[self.page intValue]];
        }
        [self.collectionCategory.infiniteScrollingView stopAnimating];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"%@",request);
        [self.collectionCategory.infiniteScrollingView stopAnimating];
    }];
    [pageOperation start];
}
@end
