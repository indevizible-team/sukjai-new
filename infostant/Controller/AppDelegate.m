//
//  AppDelegate.m
//  infostant
//
//  Created by INFOSTANT on 12/24/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//
//GoogleAnalyticIdentifier
#import "AppDelegate.h"
#import "DataManager.h"
#import "MenuItem.h"
#import "MainViewController.h"
#import "MyTripListViewController.h"
#import "Trip.h"
#import "DMURL.h"
#import <FacebookSDK/FacebookSDK.h>
@implementation AppDelegate
NSString *newStringDataNotification;
NSString *newshelfid=@"1";
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.infoDictionary = [[NSBundle mainBundle] infoDictionary];
    // Let the device know we want to receive push notifications
	[[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    self.defaultColor = UIColorFromRGB([[self.infoDictionary objectForKey:@"defaultColor"] intValue]);

    [self customizeGlobalTheme];
//    NSMutableDictionary *user = [PLIST readFeature:@"user"];
    User *user = [User currentUser];
    NSLog(@"%@",user);
    if (user != nil) {
        NSMutableDictionary *param = [DataManager param];
        [param addEntriesFromDictionary:user.oauth_login_dict];
            NSURLRequest *request= [[DataManager client] requestWithMethod:@"POST" path:@"checkoauth" parameters:param];
            NSLog(@"request : %@",request.URL);
            AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                if ([JSON valueForKey:@"expire"]!=nil) {
                    [PLIST emptyFeature:@"user"];
                }
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                NSLog(@"error %@",error);
            }];
            [operation start];
    }
    [ZBarReaderView class];
    [FBProfilePictureView class];
    return YES;

}

-(void)setGAITracker{
    // Initialize Google Analytics with a 120-second dispatch interval. There is a
    // tradeoff between battery usage and timely dispatch.
    [GAI sharedInstance].debug = YES;
    [GAI sharedInstance].dispatchInterval = 120;
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    self.tracker = [[GAI sharedInstance] trackerWithTrackingId:self.infoDictionary[@"GoogleAnalyticIdentifier"]];
}


- (void)customizeGlobalTheme {
    UIImage *navBarImage = [UIImage imageNamed:@"bgNavBar.png"];
    
    [[UINavigationBar appearance] setBackgroundImage:navBarImage
                                       forBarMetrics:UIBarMetricsDefault];
    
    
    UIImage *barButton = [[UIImage imageNamed:@"buttonDefault.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 5)];
    UIImage *barButtonx = [[UIImage imageNamed:@"back.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 15, 0, 5)];
//
    [[UIBarButtonItem appearance] setBackgroundImage:barButton forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:barButtonx forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
//
//    
//    [[UIBarButtonItem appearance] setTitleTextAttributes:
//     [NSDictionary dictionaryWithObjectsAndKeys: [UIColor colorWithRed:112.0/255.0f green:112.0/255.0f blue:112.0/255.0f alpha:1.0f],
//      UITextAttributeTextColor, [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0],
//      UITextAttributeTextShadowColor, [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
//      UITextAttributeTextShadowOffset, [UIFont fontWithName:@"Helvetica-Bold" size:14],
//      UITextAttributeFont, nil] forState:UIControlStateNormal];
//    
//    [[UIBarButtonItem appearance] setTitleTextAttributes:
//     [NSDictionary dictionaryWithObjectsAndKeys: [UIColor colorWithRed:255.0/255.0f green:102.0/255.0f blue:0.0/255.0f alpha:1.0f],
//      UITextAttributeTextColor, [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0],
//      UITextAttributeTextShadowColor, [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
//      UITextAttributeTextShadowOffset, [UIFont fontWithName:@"Helvetica-Bold" size:14],
//      UITextAttributeFont, nil] forState:UIControlStateHighlighted];
//    [[UISearchBar appearance] setBackgroundImage:[UIImage imageNamed:@"bgMenu.png"]];

    
    [[UITableViewHeaderFooterView appearance] setTintColor:self.defaultColor];
//    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTintColor:self.defaultColor];
    
    [[UISegmentedControl appearance] setTintColor:self.defaultColor];
    [[UIRefreshControl appearance] setTintColor:self.defaultColor];
    
    [[UINavigationBar appearance] setTitleTextAttributes:
     @{UITextAttributeTextColor: [UIColor whiteColor],
//    [UIColor colorWithRed:38.0f/255 green:38.0f/255 blue:38.0f/255 alpha:1.0f],
                          UITextAttributeTextShadowColor: [UIColor grayColor],
                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, -1)],
                                     UITextAttributeFont: [UIFont fontWithName:@"Helvetica-Bold" size:20]}];
}

							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [self reloadNotificationCount:nil];
//    application.applicationIconBadgeNumber = 0;
    [FBSession.activeSession handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [FBSession.activeSession close];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *str = [NSString stringWithFormat:@"%@",deviceToken];
    newStringDataNotification = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    newStringDataNotification = [newStringDataNotification stringByReplacingOccurrencesOfString:@"<" withString:@""];
    newStringDataNotification = [newStringDataNotification stringByReplacingOccurrencesOfString:@">" withString:@""];
    NSLog(@"token receive - > %@",newStringDataNotification);
    [PLIST writeFeature:@"device" withData:@{@"token": newStringDataNotification}];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    if ([url.scheme isEqualToString:@"dmconnex"]) {
        if ([url.host isEqualToString:@"q"]) {
            NSLog(@"Root = %@",[self.window.rootViewController class]);
            NSLog(@"%@",[self.window.rootViewController class]);
            if ([self.window.rootViewController isKindOfClass:[MainViewController class]]) {
                MainViewController *mainVC =(MainViewController *) self.window.rootViewController;
                [mainVC.navigationController popToRootViewControllerAnimated:NO];
                NSMutableDictionary *query = [url dictionaryForQueryString];
                if (query[@"tour"]) {
                    Trip *myTrip= [Trip new];
                    myTrip.code = query[@"code"];
                    MyTripListViewController *vc = (MyTripListViewController *)[DataManager controllerByID:@"MyTripListViewController"];
                    vc.showSlider = YES;
                    vc.trip = myTrip;
                    NSLog(@"%@",mainVC.contentViewController.navigationController.viewControllers);
                    [mainVC.contentViewController pushViewController:vc animated:NO];

                }
            }else{
                self.tmpURL = url;
            }
        }

        return YES;
    }
    
    // attempt to extract a token from the url
    return [FBSession.activeSession handleOpenURL:url];
}

-(void)application:(UIApplication *)app  didFailToRegisterForRemoteNotificationsWithError:(NSData *)deviceToken {
//    NSLog(@"Test");
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    NSLog(@"noti recieve - %@",userInfo);
    NSLog(@"BBBBB");
    [self reloadNotificationCount:userInfo[@"aps"]];
}

-(void)reloadNotificationCount:(NSDictionary *)data{
    User *user = [User currentUser];
    if (user!=nil) {
        NSMutableDictionary *param = [DataManager param];
        [param addEntriesFromDictionary:user.oauth_login_dict];

        if (self.messageView != nil) {
                       AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:[[DataManager client] requestWithMethod:@"POST" path:@"readmsgcenter" parameters:param]];
            [operation setCompletionBlock:^{
                [self.messageView reloadMessage];
            }];
            [operation start];
        }
        
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"POST" path:@"getCountAlldata" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            [PLIST writeFeature:@"noti" withData:JSON];
            NSLog(@"GET Some %@",JSON);
            if (self.menuView !=nil) {
                [self.menuView reloadUser];
            }
            if (data!=nil) {
                NSArray *datasource = @[
                                        @{ @"controllerID":@"FollowVC",@"path":@"readfollowcenter"}
                                        ,@{@"controllerID":@"MessageVC",@"path":@"readmsgcenter"}
                                        ,@{@"controllerID":@"NotiVC",@"path":@"readnotifcationcenter"}
                                        ];
                int type = [data[@"type"] intValue]-1;
                
                MenuItem *item = [MenuItem itemWithData:datasource[type]];
                
                   
                        NSMutableDictionary *param = [DataManager param];
                        [param addEntriesFromDictionary:[User currentUser].oauth_login_dict];
                        
                        
                        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"POST" path:item.path parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                            AFJSONRequestOperation *change = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"POST" path:@"getCountAlldata" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                if ([JSON valueForKey:@"expire"]==nil) {
                                    [PLIST writeFeature:@"noti" withData:JSON];
                                    [self.menuView reloadUser];
                                }else{
                                    [self.menuView openLoginPage];
                                }
                            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                
                            }];
                            [change start];
                        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                            
                        }];
                        [operation start];
                    
                    
                
                        [self.menuView setFrontViewByStoryboardID:item.controllerID];
                
                    
//                    [mainController toggleSidebar:!mainController.leftSidebarShowing duration:kGHRevealSidebarDefaultAnimationDuration];
            }
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
            
        }];
        [operation start];
    }
    
}

@end
