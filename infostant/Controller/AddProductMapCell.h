//
//  AddProductMapCell.h
//  infostant
//
//  Created by indevizible on 1/29/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddProductMapCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *mapImage;

@end

