//
//  MyTripViewController.m
//  www
//
//  Created by KEEP CALM AND CODE OK on 3/8/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//
#define kUnlock @1
#define kLock @2
#import "MyTripViewController.h"
#import "MyTripListViewController.h"
#import "MyTripCell.h"
#import "MyTripMapCell.h"
#import "Trip.h"
#import "DataManager.h"
#import "QRScannerViewController.h"
#import "DMURL.h"
#import "Bitly.h"
#import "WebViewController.h"
#import "LoginNavController.h"
typedef enum{
    kActionSheetShare,
    kActionSheetEdit,
    kActionSheetAdd
} kActionSheet;

typedef enum{
    kAlertConfirmDelete,
    kAlertInput,
    kAlertCall,
    kAlertAddByCode
    
}kAlert;
@interface MyTripViewController ()<UIAlertViewDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,QRScannerDelegate,LoginDelegate>{
    
    NSIndexPath *selectedIndex;
    int tableCount;
    NSMutableArray *myTripList;
    BOOL isMyTrip,canLoadMore,scanning;
    int page;
    NSURLRequest *lastRequest;
    Trip *selectedTrip;
    User *user;
    NSIndexPath *infoIndex;
    NSString *tmpCode;

}
- (IBAction)switchTripType:(UISegmentedControl *)sender;
- (IBAction)callAddTrip:(id)sender;
@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;
@end

@implementation MyTripViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setSlider];
    user = [User currentUser];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing: ) forControlEvents:UIControlEventValueChanged];
    
//    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
//                                          initWithTarget:self action:@selector(handleLongPress:)];
//    lpgr.minimumPressDuration = 1.0; //seconds
//    lpgr.delegate = self;
//    [self.tableView addGestureRecognizer:lpgr];
    
    
    [self.tableView addSubview:self.refreshControl];
   
    
    [self.refreshControl beginRefreshing];
    [self dropViewDidBeginRefreshing:self.refreshControl];
}
-(void)viewDidDisappear:(BOOL)animated{
     [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}
- (void)dropViewDidBeginRefreshing:(UIRefreshControl *)refreshControl
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    page = 1;
    canLoadMore =YES;
    [self loadMore];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
- (IBAction)information:(UIButton *)sender {
    NSIndexPath *current = [NSIndexPath indexPathForItem:sender.tag inSection:0];
    if (infoIndex) {
        if ([current isEqual:infoIndex]) {
            infoIndex = nil;
        }else{
            __strong NSIndexPath *tmp = infoIndex;
            infoIndex = current;
            [self.tableView reloadRowsAtIndexPaths:@[tmp] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        [self.tableView reloadRowsAtIndexPaths:@[current] withRowAnimation:UITableViewRowAnimationAutomatic];
    }else{
        infoIndex = current;
        [self.tableView reloadRowsAtIndexPaths:@[current] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return myTripList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([myTripList[indexPath.row] isKindOfClass:[NSURL class]]) {
        MyTripMapCell *mapCell = [tableView dequeueReusableCellWithIdentifier:@"MyTripMapCell" forIndexPath:indexPath];
        [mapCell.webView loadRequest:[NSURLRequest requestWithURL:myTripList[indexPath.row]]];
        return mapCell;
    }
   
    static NSString *CellIdentifier = @"MyTripCell";
    MyTripCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Trip *myTrip = (Trip *)[myTripList objectAtIndex:indexPath.row];
    cell.title.text = myTrip.title;
    cell.description.text = myTrip.dateString;
    [cell.imageThumbnail setImageWithURL:myTrip.image];
    cell.priceLabel.text = myTrip.priceText;
    cell.currencyType.text = myTrip.currency;
    cell.dayCount.text = myTrip.durationText;
    cell.callBtn.enabled = (myTrip.tel != nil);
    cell.infoBtn.tag =  cell.callBtn.tag = indexPath.row;
    cell.information.text = myTrip.infoText;
    cell.infoBtn.hidden = !(myTrip.infoText.length);
    if (indexPath.row+3 > myTripList.count && canLoadMore) {
        [self loadMore];
    }
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // Return NO if you do not want the specified item to be editable.
    if (([((self.status.selectedSegmentIndex)?kLock:kUnlock) isEqualToNumber:kUnlock] && [User currentUser].isAdminUser)||([((self.status.selectedSegmentIndex)?kLock:kUnlock) isEqualToNumber:kLock])) {
        return YES;
    }
    return NO;
    
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        selectedIndex = indexPath;
        selectedTrip = myTripList[selectedIndex.row];
        // Delete the row from the data source
       [self showDeleteSeletedIndexPathConfirm];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/


// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
[self.refreshControl endRefreshing];
}

- (IBAction)switchTripType:(UISegmentedControl *)sender {
    NSLog(@"%i",sender.selectedSegmentIndex);
    [self.refreshControl beginRefreshing];
    [self dropViewDidBeginRefreshing:self.refreshControl];
   
}

- (IBAction)callAddTrip:(id)sender {
    UIActionSheet *sheet =[[UIActionSheet alloc] initWithTitle:@"Add with:" delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") destructiveButtonTitle:nil otherButtonTitles:@"Tour code",@"QRCode", nil];
    sheet.tag = kActionSheetAdd;
    [sheet showInView:self.view];
    
//    
//    UIAlertView *inputBox = [[UIAlertView alloc] initWithTitle:@"New Trip" message:@"Design your trip name" delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") otherButtonTitles:NSLocalizedString(@"infoOK", @"") , nil];
//    [inputBox setAlertViewStyle:UIAlertViewStylePlainTextInput];
//    inputBox.tag = kAlertInput;
//    [inputBox show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex != [alertView cancelButtonIndex]) {
        if (alertView.tag == kAlertConfirmDelete) {
            
            NSMutableDictionary *param = [DataManager param];
            [param addEntriesFromDictionary:@{@"infomt":selectedTrip.id}];
            [param addEntriesFromDictionary:[User currentUser].oauth_login_dict];
            
            AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"POST" path:@"deleteMyTrips" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                NSLog(@"JSON : %@\n param : %@",JSON,param);
                if ([JSON[@"error"] isKindOfClass:[NSString class]]) {
                     [SVProgressHUD showErrorWithStatus:JSON[@"error"]];
                }else{
                    [myTripList removeObjectAtIndex:selectedIndex.row];
                    ;            [self.tableView deleteRowsAtIndexPaths:@[selectedIndex] withRowAnimation:UITableViewRowAnimationAutomatic];
                }
               
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                NSLog(@"error :%@",JSON);
            }];
            [operation start];
            
          
            

        }else if (alertView.tag == kAlertAddByCode){
            if ([User currentUser]) {
                [self addCodeWithCode:[alertView textFieldAtIndex:0].text];
            }else{
                [self presentViewController:[LoginNavController loginViewDelegate:self] animated:YES completion:nil];
            }
        }else if (alertView.tag == kAlertCall){
            [[UIApplication sharedApplication] openURL:selectedTrip.tel];
        }
    }
}
-(void)didLogin:(LoginNavController *)vc{
    [vc dismissViewControllerAnimated:YES completion:^{
        if (tmpCode && [User currentUser]) {
            [self addCodeWithCode:tmpCode];
        }else{
            tmpCode = nil;
        }
        
    }];
}

-(void)addCodeWithCode:(NSString *)code{
    [SVProgressHUD show];
    NSMutableDictionary *param = [DataManager param];
    [param setValue:code forKey:@"code"];
    [param addEntriesFromDictionary:[User currentUser].oauth_login_dict];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"POST" path:@"saveFriendTrips" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"%@->%@",param, JSON);
        if ([JSON[@"error"] isKindOfClass:[NSString class]]) {
            [SVProgressHUD showErrorWithStatus:JSON[@"error"]];
        }else{
            [SVProgressHUD showSuccessWithStatus:@"Complete"];
            page = 1;
            canLoadMore =YES;
            [self loadMore];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD showErrorWithStatus:@"Can not fetch data from server"];
    }];
    [operation start];
    tmpCode = nil;
}
-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan && [((self.status.selectedSegmentIndex)?kLock:kUnlock) isEqualToNumber:kUnlock]) {
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
        if (indexPath ){
            selectedIndex = indexPath;
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") destructiveButtonTitle:NSLocalizedString(@"infoDelete", @"") otherButtonTitles:NSLocalizedString(@"infoEdit", @""),NSLocalizedString(@"infoShare", @""), nil];
            actionSheet.tag = kActionSheetEdit;
            [actionSheet showInView:self.view];
        }
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ([actionSheet cancelButtonIndex]!=buttonIndex) {
        if (actionSheet.tag == kActionSheetEdit) {
            if (buttonIndex == 0) {
                [self showDeleteSeletedIndexPathConfirm];
            }else if(buttonIndex == 1){
                UIAlertView *inputBox = [[UIAlertView alloc] initWithTitle:@"Edit Trip" message:@"Design your trip name" delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") otherButtonTitles:NSLocalizedString(@"infoOK", @"") , nil];
                [inputBox setAlertViewStyle:UIAlertViewStylePlainTextInput];
                inputBox.tag = kAlertInput;
                [inputBox show];
            }else if(buttonIndex == 2){
                UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"infoFacebook", @""),NSLocalizedString(@"infoShareInTrip", @""), nil];
                actionSheet.tag = kActionSheetShare;
                [actionSheet showInView:self.view];
            }
        }else if (actionSheet.tag == kActionSheetShare){
            
        }else if (actionSheet.tag == kActionSheetAdd){
            if (buttonIndex == 1) {
                QRScannerViewController *vc = [QRScannerViewController QRScanner:self];
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                UIAlertView *inputBox = [[UIAlertView alloc] initWithTitle:@"Add Tour" message:@"Enter Code" delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") otherButtonTitles:NSLocalizedString(@"infoOK", @"") , nil];
                [inputBox setAlertViewStyle:UIAlertViewStylePlainTextInput];
                inputBox.tag = kAlertAddByCode;
                [inputBox show];
                
            }
        }

    }
    }

-(void)showDeleteSeletedIndexPathConfirm{
    UIAlertView *deleteConfirm = [[UIAlertView alloc] initWithTitle:@"Confirm To delete" message:@"Do you wanna delete" delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") otherButtonTitles:NSLocalizedString(@"infoConfirm", @"") , nil];
    deleteConfirm.tag = kAlertConfirmDelete;
    [deleteConfirm show];
}

-(void)setSlider{
    UIViewController *controller = self.navigationController.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			self.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
		}
		
		// Check if we have a revealButton already.
		if (![self.navigationItem leftBarButtonItem]&&(self.navigationController.viewControllers.count==1)) {
			// If not, allocate one and add it.
			UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
            
            UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [menuButton setImage:imageMenu forState:UIControlStateNormal];
            menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
            [menuButton addTarget:controller action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
		}
        
        if (![self.navigationItem rightBarButtonItem]&&[[[[NSBundle mainBundle] infoDictionary] valueForKey:@"RightMenu"] boolValue]) {
			// If not, allocate one and add it.
			UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
            
            UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [menuButton setImage:imageMenu forState:UIControlStateNormal];
            menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
            [menuButton addTarget:controller action:@selector(revealRightToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
		}
	}

}
- (IBAction)ClickBtn:(UIButton *)sender {
    selectedTrip = [myTripList objectAtIndex:sender.tag];
    UIAlertView *alertCall = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoAppName", @"") message:[NSString stringWithFormat:@"Call %@",selectedTrip.telString] delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") otherButtonTitles:@"Call", nil];
    alertCall.tag = kAlertCall;
    [alertCall show];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([myTripList[indexPath.row] isKindOfClass:[NSURL class]]) {
        return tableView.bounds.size.height;
    }
    if ([infoIndex isEqual:indexPath]) {
        return 250.0f;
    }
    return 117.0f;
}


-(void)loadMore{
    canLoadMore = NO;
    NSMutableDictionary *param = [DataManager param];
    [param addEntriesFromDictionary:[User currentUser].oauth_login_dict];
    [param setValue:[NSNumber numberWithInt:page++] forKey:@"page"];
    [param setValue:((self.status.selectedSegmentIndex)?kLock:kUnlock) forKey:@"status"];
    lastRequest = [[DataManager client] requestWithMethod:@"GET" path:@"getMyTrips" parameters:param];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:lastRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"%@ res : %@",request.URL, JSON);
        if ([[param valueForKey:@"page"] isEqualToNumber:@1]) {
            myTripList = [NSMutableArray new];
            infoIndex = nil;
        }
        
        if ([JSON count]) {
            DataController *tmp = [[DataController alloc] initWithDictionary:[DataManager JSONToMutableDictionary:JSON]];
            for (int i=0; i<tmp.key.count; i++) {
                [myTripList addObject:[Trip tripWithData:[tmp dictionaryAtIndex:i]]];
            }
            canLoadMore = YES;
        }else{
            
            if (![User currentUser].isAdminUser&&[((self.status.selectedSegmentIndex)?kLock:kUnlock) isEqualToNumber:kUnlock]) {
                [myTripList addObject:[NSURL URLWithString:@"http://www.infostant.com"]];
            }
            
//            if ((![JSON count])&&[((self.status.selectedSegmentIndex)?kLock:kUnlock) isEqualToNumber:kUnlock]) {
//                
//            }

        }
        
        [self.refreshControl endRefreshing];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [self.tableView reloadData];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"error , %@",request.URL);
         [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
         [self.refreshControl endRefreshing];
    }];
    [operation start];
    
   
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
  
        MyTripListViewController *vc = [segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForCell:((MyTripCell *)sender)];
        vc.trip = myTripList[indexPath.row];
        vc.canEdit = [((self.status.selectedSegmentIndex)?kLock:kUnlock) isEqualToNumber:kUnlock];
    
}

-(void)QRscannerDidRecieveData:(QRScannerViewController *)vc data:(NSString *)data image:(UIImage *)image{
    if (!scanning) {
        NSURL *ourURL = [NSURL URLWithString:data];
        if ([ourURL.host isEqualToString:@"bit.ly"]) {
            scanning = YES;
            [SVProgressHUD show];
            [Bitly expandFromShortURL:ourURL success:^(NSURLRequest *request, NSURL *longURL) {
                
                NSLog(@"long url = %@",longURL);
                NSMutableDictionary *query = [longURL dictionaryForQueryString];
                Trip *myTrip= [Trip new];
                if (query[@"code"]&&query[@"tour"]) {
                    [SVProgressHUD dismiss];
                    [self.navigationController popViewControllerAnimated:NO];
                    myTrip.code = query[@"code"];
                    MyTripListViewController *vc = (MyTripListViewController *)[DataManager controllerByID:@"MyTripListViewController"];
                    vc.trip = myTrip;
                    [self.navigationController pushViewController:vc animated:NO];
                }else{
                    [SVProgressHUD showErrorWithStatus:@"invalid code"];
                }
                scanning = NO;
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                [SVProgressHUD showErrorWithStatus:@"Some thing wrong"];
                scanning = NO;
            }];

        }
    }
}

@end
