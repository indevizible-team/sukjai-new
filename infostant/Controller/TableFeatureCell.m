//
//  TableFeatureCell.m
//  infostant
//
//  Created by indevizible on 1/17/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "TableFeatureCell.h"
#import "HorizonFeatureCell.h"
#import "SelectFeatureViewController.h"
#import "WebViewController.h"
#import "ImageSlideViewController.h"
#import "LandingNewViewController.h"
#import "CategoryViewController.h"
#import "LikeListViewController.h"
@implementation TableFeatureCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.datasource==nil) {
        return 1;
    }
    return self.datasource.key.count+1;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    HorizonFeatureCell *cell;

    if (self.datasource.key.count == indexPath.row) {
        cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"SeeAllCell" forIndexPath:indexPath];
        cell.infofid =[self.datasource.data valueForKey:@"infofid"];
    }else{
        NSMutableDictionary *each = [self.datasource dictionaryAtIndex:indexPath.row];
        if (([[each valueForKey:@"banner"] integerValue]==1)) {
            cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"FeatureAdsProduct" forIndexPath:indexPath];
            [cell.imageView setImageWithURL:[NSURL URLWithString:[each valueForKey:@"pic"]]];
            cell.each = each;
        }
        else{
            cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"FeatureProduct" forIndexPath:indexPath];
            [cell.imageView setImageWithURL:[NSURL URLWithString:[each valueForKey:@"pic"]]];
            cell.productName.text = [NSString stringWithFormat:@" %@",[each valueForKey:@"title"]];
            cell.each = each;
        }
        

        
    }
//    NSLog(@"datasouce.data = %@",self.datasource.data);
    cell.infofid = self.infofid;


    return cell;
}
-(void)prepareForReuse{
    [super prepareForReuse];
    if (self.datasource != nil) {
        if ([self.datasource.data valueForKey:@"indexPath"]!=nil) {
            if (self.datasource.key.count) {
                 [self.collectionView scrollToItemAtIndexPath:[self.datasource.data valueForKey:@"indexPath"] atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
            }
           
        }
        
    }
    
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSUInteger min = INT_MAX;
    for (NSIndexPath *indexPath in [self.collectionView indexPathsForVisibleItems]) {
            if (indexPath.row < min) {
                min = indexPath.row;
            }
    }
    
    NSIndexPath *minIndexPath = [NSIndexPath indexPathForItem:min inSection:0] ;
    [self.datasource.data setValue:minIndexPath forKey:@"indexPath"];
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:min inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    HorizonFeatureCell *cell = (HorizonFeatureCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if (indexPath.row<self.datasource.key.count) {
        
        NSString *infofid = cell.infofid;
        NSMutableDictionary *selectedInfofid = cell.each;
        UIStoryboard *storyboard = [DataManager getStoryboard];
        if (([infofid isEqualToString:@"1"] && ![[selectedInfofid valueForKey:@"data"] count])||([selectedInfofid valueForKey:@"banner"]!=nil)) {
            WebViewController *wv  = [storyboard instantiateViewControllerWithIdentifier:@"LandingD"];
            wv.address = [selectedInfofid valueForKey:@"fullshopurl"];
            wv.title = NSLocalizedString([selectedInfofid valueForKey:@"title"],[selectedCat valueForKey:@"title"]);
            [self.nav pushViewController:wv animated:YES];
        }else if ([infofid isEqualToString:@"3"]){
            NSMutableArray *TmpImage = [NSMutableArray new];
            for (NSString *image in [selectedInfofid valueForKey:@"pdfdata"]) {
                NSURL *_url = [NSURL URLWithString:image];
                [TmpImage addObject:_url];
            }
            ImageSlideViewController *iv = [storyboard instantiateViewControllerWithIdentifier:@"LandingE"];
            iv.imageList = TmpImage;
            
            [self.delegate presentViewController:iv animated:YES completion:nil];
        }else{
            LandingNewViewController *lv = [storyboard instantiateViewControllerWithIdentifier:@"LandingNew"];
            NSLog(@">>>>>%@",self.datasource.data);
            lv.product = self.datasource;
            lv.accessKey = [cell.each valueForKey:@"productid"];
            lv.landing = [ self.datasource accessKey:lv.accessKey];
            lv.feature = cell.infofid;
            [self.nav pushViewController:lv animated:YES];
        }

    }
    else{
        if (self.datasource.key.count == 0) {
            CategoryViewController *vc =(CategoryViewController *) [DataManager controllerByID:@"CategoryVC"];
            vc.infofid = cell.infofid;
            [self.nav pushViewController:vc animated:YES];
        }else{
            LikeListViewController *vc = (LikeListViewController *)[DataManager controllerByID:@"LikeVC"];
            
            vc.infofid = cell.infofid;
            vc.feed = YES;
            [self.nav pushViewController:vc animated:YES];
        }
    }


    
}
@end
