//
//  AddProductViewController.m
//  infostant
//
//  Created by indevizible on 1/23/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//
#define photoRatio 420.0f/350.0f
#import "AddProductViewController.h"

//cell image
#import "AddProductImageCell.h"
#import "PhotoTakerViewController.h"

#import "AddProductGalleryCell.h"
#import "AddProductContactCell.h"
#import "EditAddProductTextViewController.h"
#import "AddProductTextCell.h"
#import "HCYoutubeParser.h"
#import "AddProductVideoCell.h"
#import "EditAddDataViewController.h"
#import "AddProductMapCell.h"
#import "MapDropPinViewController.h"
#import "AddContentFinalViewController.h"
#import "GKImagePicker.h"
@interface AddProductViewController ()<GKImagePickerDelegate>
@property (nonatomic, strong) GKImagePicker *imagePicker;
@end

@implementation AddProductViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"infoAddContent", @"");
    postData = [DataController new];
    NSMutableDictionary *imageDataSchema = [@{@"key": @[@"x-1",@"x-2",@"x-3",@"x-4",@"x-5",@"x-6"],
                                            @"x-1":@{@"infotid": @1},
                                            @"x-2":@{@"infotid": @2},
                                            @"x-3":@{@"infotid":@3},
                                            @"x-4":@{@"infotid":@4},
                                            @"x-5":@{@"infotid":@5},
                                            @"x-6":@{@"infotid":@6},
                                            @"x-7":@{@"infotid":@1}
                                            } mutableCopy];
    cellIdentifierList = @[@"AddProductImageCell",@"AddProductGalleryCell",@"AddProductTextCell",@"AddProductYoutubeCell",@"AddProductContactCell",@"AddProductMapCell"];
    storyboardIdentifierList = @[@"",@"",@"EditAddProductTextViewController",@"",@"EditAddDataViewController",@"MapDropPinViewController"];
    [postData insertData:imageDataSchema];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [locationManager startUpdatingLocation];
    [self.tableView reloadData];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    UINavigationController *nav = self.navigationController;
    UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			self.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
		}
		
		// Check if we have a revealButton already.
		if (![self.navigationItem leftBarButtonItem]&&(self.navigationController.viewControllers.count==1)) {
			// If not, allocate one and add it.
			UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
            
            UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [menuButton setImage:imageMenu forState:UIControlStateNormal];
            menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
            [menuButton addTarget:controller action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
		}
	}

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return postData.key.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIImage *tmpImage = [UIImage imageNamed:@"imgLargePH.png"];
    UIImage *tmpImageS = [UIImage imageNamed:@"imgSmallPH.png"];
    NSMutableDictionary *each = [postData dictionaryAtIndex:indexPath.row];
    NSInteger infotid = [[each valueForKey:@"infotid"] integerValue];
    if (infotid == 1) {
        AddProductImageCell *cell = [tableView dequeueReusableCellWithIdentifier:[cellIdentifierList objectAtIndex:infotid-1] forIndexPath:indexPath];
        if ([each valueForKey:@"pic"]!=nil) {
            [cell.image setImage:[each valueForKey:@"pic"]];
        }else{
            [cell.image setImage:tmpImage];
        }
        return cell;
    }else if(infotid ==2){
        AddProductGalleryCell *cell = [tableView dequeueReusableCellWithIdentifier:[cellIdentifierList objectAtIndex:infotid-1] forIndexPath:indexPath];
        cell.targetData = each;
        cell.parent = self;
   
        [cell.btn1Outlet setImage:tmpImageS forState:UIControlStateNormal];
        [cell.btn2Outlet setImage:tmpImageS forState:UIControlStateNormal];
        [cell.btn3Outlet setImage:tmpImageS forState:UIControlStateNormal];
        [cell.btn4Outlet setImage:tmpImageS forState:UIControlStateNormal];
        
        if ([each valueForKey:@"pic-1"]!=nil) {
            [cell.btn1Outlet setImage:[each valueForKey:@"pic-1"] forState:UIControlStateNormal];
        }
        if ([each valueForKey:@"pic-2"]!=nil) {
            [cell.btn2Outlet setImage:[each valueForKey:@"pic-2"] forState:UIControlStateNormal];
        }
        if ([each valueForKey:@"pic-3"]!=nil) {
            [cell.btn3Outlet setImage:[each valueForKey:@"pic-3"] forState:UIControlStateNormal];
        }
        if ([each valueForKey:@"pic-4"]!=nil) {
            [cell.btn4Outlet setImage:[each valueForKey:@"pic-4"] forState:UIControlStateNormal];
        }
        return cell;
    }else if (infotid ==3){
        AddProductTextCell *cell = [tableView dequeueReusableCellWithIdentifier:[cellIdentifierList objectAtIndex:infotid-1] forIndexPath:indexPath];
        if ([each valueForKey:@"detail"]!=nil) {
            [cell.description setText:[each valueForKey:@"detail"]];
        
        }else{
            [cell.description setText:@"Description : Explain everything that you want to say about your shop or your place. This is a detailed description of the idea, fact, emotion, or something that can be helpful for reader to understand about your place. So feel free to write it down and it can be edited every time you want. Have Fun! (recommend 450 Characters)"];
        }
        return cell;
    }else if(infotid ==4){
         AddProductVideoCell *cell = [tableView dequeueReusableCellWithIdentifier:[cellIdentifierList objectAtIndex:infotid-1] forIndexPath:indexPath];
        if ([each valueForKey:@"link"]!=nil) {
            NSString *youtubeId = [each valueForKey:@"link"];
            [cell.videoThumbnail setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://img.youtube.com/vi/%@/0.jpg",youtubeId]]];

        }else{

        }
        return  cell;
        
    }else if (infotid==5){
        AddProductContactCell *cell = [tableView dequeueReusableCellWithIdentifier:[cellIdentifierList objectAtIndex:infotid-1] forIndexPath:indexPath];
        cell.txtOpenHour.text = [each valueForKey:@"title"];
        cell.txtContactDescription.text = [each valueForKey:@"description"];
        return cell;
        
    }else if (infotid==6){
        AddProductMapCell *cell = [tableView dequeueReusableCellWithIdentifier:[cellIdentifierList objectAtIndex:infotid-1] forIndexPath:indexPath];
        float lat,lng;
        if ([each valueForKey:@"custom"]==nil) {
            lat =locationManager.location.coordinate.latitude;
            lng =locationManager.location.coordinate.longitude;
            [each setValue:[NSString stringWithFormat:@"%f",lat] forKey:@"lat"];
            [each setValue:[NSString stringWithFormat:@"%f",lng] forKey:@"lng"];
            
        }else{
            lat = [[each valueForKey:@"lat"] floatValue];
            lng = [[each valueForKey:@"lng"] floatValue];
        }
        [cell.mapImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/staticmap?center=%f,%f&zoom=13&size=640x640&maptype=roadmap&markers=color:yellow%%7Clabel:!%%7C%f,%f&sensor=false",lat,lng,lat,lng]] placeholderImage:[UIImage imageNamed:@"staticmaps.png"]];
        return cell;
    }

    
    
    return nil;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    currentEditingCell = indexPath.row;
    NSMutableDictionary *each = [postData dictionaryAtIndex:indexPath.row];
    NSInteger infotid = [[each valueForKey:@"infotid"] integerValue];
    if (infotid == 1) {
        [self takePhoto];
    }else if(infotid == 3){
        EditAddProductTextViewController *vc = [[DataManager getStoryboard] instantiateViewControllerWithIdentifier:[storyboardIdentifierList objectAtIndex:infotid-1]];
        vc.targetData = each;
        vc.description.font = [DataManager fontSize:@"small"];
        [vc setParentTable:self.tableView];
        [self presentViewController:vc animated:YES completion:nil];
        
    }else if(infotid==4){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoYoutube", @"") message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") otherButtonTitles:NSLocalizedString(@"infoOK", @""), nil];
        [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [alert show];
    }else if (infotid==5){
        EditAddDataViewController *vc = [[DataManager getStoryboard] instantiateViewControllerWithIdentifier:[storyboardIdentifierList objectAtIndex:infotid-1]];
        vc.each = each;
        vc.parentTable= self.tableView;
        vc.dataStructure = @[
                                 @{@"type": @"TextField",
                                   @"name":@"title",
                                   @"title":NSLocalizedString(@"infoTitleOpen", @"")},
                                 @{@"type": @"TextView",
                                   @"name":@"description",
                                   @"title":NSLocalizedString(@"infoTitleContact", @"")}
                                 ];
        [self.navigationController pushViewController:vc animated:YES];

    }else if (infotid==6){
        MapDropPinViewController *vc  = [[DataManager getStoryboard] instantiateViewControllerWithIdentifier:[storyboardIdentifierList objectAtIndex:infotid-1]];
        vc.each = each;
        vc.parentTable = self.tableView;
        vc.currentLocation = locationManager.location.coordinate;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    float height = 320.0f;
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    NSMutableDictionary *each = [postData dictionaryAtIndex:indexPath.row];
    NSInteger infotid = [[each valueForKey:@"infotid"] integerValue];
    if (infotid == 1) {
        height = [DataManager getHeightFromRatio:photoRatio];
        height = 320.0f;
    }else if (infotid  == 2){
        height = [DataManager getHeightFromRatio:galleryRatio];
        height = 250.0f;
    }else if(infotid == 3){
        if ([each valueForKey:@"detail"]!=nil) {
                height = [DataManager textHeight:[each valueForKey:@"detail"] fromWidth:screenBound.size.width andFont:[DataManager fontSize:@"small"]];
        }else{
            height = 180.0f;
        }
    
    }else if(infotid == 4){
        float ratio = 4.0f/3.0f;
        height = screenBound.size.width/ratio;
        height = 270.0f;
    }else if (infotid == 5){
        height = 100.0f + [DataManager textHeight:[each valueForKey:@"description"] fromWidth:285 andFont:[UIFont systemFontOfSize:14.0f]];
    }else if (infotid == 6){
        height = 320.0f;
    }
    return height;
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
	if (buttonIndex == 0) {
		if ([UIImagePickerController isSourceTypeAvailable:
			 UIImagePickerControllerSourceTypeCamera])
		{
            
            [self preparePickerType:UIImagePickerControllerSourceTypeCamera];
			[self presentViewController:self.imagePicker.imagePickerController
                               animated:YES completion:nil];
			newMedia = YES;
		}
	}else if (buttonIndex == 1){
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            if ([self.myPopoverController isPopoverVisible]) {
                [self.myPopoverController dismissPopoverAnimated:YES];
            } else {
                if ([UIImagePickerController isSourceTypeAvailable:
                     UIImagePickerControllerSourceTypeSavedPhotosAlbum])
                {
                    
                    [self preparePickerType:UIImagePickerControllerSourceTypePhotoLibrary];
                    self.myPopoverController = [[UIPopoverController alloc]
                                                initWithContentViewController:self.imagePicker.imagePickerController];
                    self.myPopoverController.delegate = self;
                    
                    [self.myPopoverController presentPopoverFromRect:CGRectMake(44, 20, 200, 200) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
                    newMedia = NO;
                }
            }
            
        } else {
            if ([UIImagePickerController isSourceTypeAvailable:
                 UIImagePickerControllerSourceTypeSavedPhotosAlbum])
            {
                [self preparePickerType:UIImagePickerControllerSourceTypePhotoLibrary];
                [self presentViewController:self.imagePicker.imagePickerController animated:YES completion:nil];
                newMedia = NO;
            }
            
        }
    }
}

#pragma mark -
#pragma mark UIImagePickerControllerDelegate

-(void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self.myPopoverController dismissPopoverAnimated:true];
    }else{
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
	
    NSString *mediaType = [info
						   objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image = [info
						  objectForKey:UIImagePickerControllerEditedImage];
		didChangeImage = YES;
  
        NSDictionary *each = [postData dictionaryAtIndex:currentEditingCell];
        [each setValue:image forKey:@"pic"];
        [self.tableView reloadData];
        if (newMedia){
			[self dismissViewControllerAnimated:YES completion:nil];
            
            //			[self refreshSize];
            //saveimage
            UIImageWriteToSavedPhotosAlbum(image,
										   self,
										   @selector(image:finishedSavingWithError:contextInfo:),
										   nil);
		}
    }
	
}

-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
	if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
							  initWithTitle: NSLocalizedString(@"infoTitleSaveFail", @"")
							  message: NSLocalizedString(@"infoMsgSaveFail", @"")
							  delegate: nil
							  cancelButtonTitle:NSLocalizedString(@"infoOK", @"")
							  otherButtonTitles:nil];
        [alert show];
	}
}
-(void)takePhoto{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"infoTakePhoto", @""),NSLocalizedString(@"infoChoosePhoto", @""), nil];
    popup.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [popup showInView:self.view];
}

#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSString *inputText = [alertView textFieldAtIndex:0].text;
    if ([inputText length] && buttonIndex) {
        NSDictionary *each = [postData dictionaryAtIndex:currentEditingCell];
        [each setValue:[HCYoutubeParser youtubeIDFromYoutubeURL:[NSURL URLWithString:inputText]] forKey:@"link"];
        [self.tableView reloadData];
    }
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    AddContentFinalViewController *vc = [segue destinationViewController];
    vc.landingData =postData;
}




# pragma mark -
# pragma mark GKImagePicker Delegate Methods

- (void)imagePicker:(GKImagePicker *)imagePicker pickedImage:(UIImage *)image{
    
    if (imagePicker.imagePickerController.sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImageWriteToSavedPhotosAlbum(image,
                                       self,
                                       @selector(image:finishedSavingWithError:contextInfo:),
                                       nil);
    }
    
    NSDictionary *each = [postData dictionaryAtIndex:currentEditingCell];
    [each setValue:image forKey:@"pic"];
    [self.tableView reloadData];
    
    [self hideImagePicker];
    
}

- (void)hideImagePicker{
    if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM()) {
        
        [self.myPopoverController dismissPopoverAnimated:YES];
        
    } else {
        
        [self.imagePicker.imagePickerController dismissViewControllerAnimated:YES completion:nil];
        
    }
}
-(void)preparePickerType:(UIImagePickerControllerSourceType)type{
    self.imagePicker = [[GKImagePicker alloc] init];
    self.imagePicker.cropSize = [DataManager getSizeFromRatio:photoRatio];
    self.imagePicker.delegate = self;
    self.imagePicker.imagePickerController.sourceType = type;
    self.imagePicker.imagePickerController.mediaTypes = [NSArray arrayWithObjects:
                                                         (NSString *) kUTTypeImage,
                                                         nil];
}

@end
