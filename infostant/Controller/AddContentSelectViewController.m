//
//  AddContentSelectViewController.m
//  infostant
//
//  Created by indevizible on 1/22/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "AddContentSelectViewController.h"

@interface AddContentSelectViewController ()<UIPickerViewDataSource,UIPickerViewDelegate>


@end

@implementation AddContentSelectViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [self loadAllSubCat:^{}];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}

-(void)loadAllSubCat:(void (^)(void))success{
    User *user = [User currentUser];
    
    self.view.userInteractionEnabled = NO;
    [SVProgressHUD showWithStatus:NSLocalizedString(@"infoLoadData", @"")];
    NSMutableDictionary *param = [DataManager param];
    [param addEntriesFromDictionary:user.oauth_login_dict];
    [param setValue:self.infofid forKey:@"infofid"];
    AFJSONRequestOperation *getAllCat = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"GET" path:@"getSubCategorylist" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        subCatData = [[DataController alloc] initWithDictionary:[DataManager JSONToMutableDictionary:JSON]];
        success();

        self.view.userInteractionEnabled =YES;
        [SVProgressHUD dismiss];
        [self.pickerView reloadAllComponents];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNetworkError", @"")];
        self.view.userInteractionEnabled =YES;
    }];
    [getAllCat start];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)xx:(id)sender {

    NSString *stringURL = [NSString stringWithFormat:@"mykiosk://"];
    NSURL *url = [NSURL URLWithString:stringURL];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)subCatSelect:(id)sender {
    if (subCatData == nil) {
        [self loadAllSubCat:^{
            [self openList];
        }];
    }else{
        if (subCatData.key.count) {
            [self openList];
        }else{
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNotFound", @"")];
        }
        
    }
}
-(void)openList{
    _doneBar.hidden=NO;
    _pickerView.hidden=NO;
}

- (IBAction)closeList:(id)sender {
    _doneBar.hidden=YES;
    _pickerView.hidden=YES;
}
#pragma mark - UIPickerViewDataSource
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
  
        if ( subCatData == nil) {
            return 0;
        }
        return subCatData.key.count;
    
   
}


#pragma mark - UIPickerViewDelegate
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [[subCatData dictionaryAtIndex:row] valueForKey:@"subcatname"];
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
   
        [self.btnSubCatSel setTitle:[[subCatData dictionaryAtIndex:row] valueForKey:@"subcatname"] forState:UIControlStateNormal];
    
}
@end
