//
//  NotiListCell.h
//  www
//
//  Created by MINDNINE on 2/14/56 BE.
//  Copyright (c) 2556 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotiListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UIImageView *screenPic;

@end
