//
//  WebViewController.h
//  WebView
//
//  Created by Nattawut Singhchai on 12/28/12 .
//  Copyright (c) 2012 Nattawut Singhchai. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <Social/Social.h>
@interface WebViewController : UIViewController<UIWebViewDelegate>{
//	SLComposeViewController *sl;
}
@property (strong, nonatomic) IBOutlet UIWebView *web;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activity;
- (IBAction)back:(id)sender;
- (IBAction)fwd:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *backBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *fwdBtn;
- (IBAction)share:(id)sender;
@property (strong, nonatomic) NSString *address;
+(id)webViewWithURLString:(NSString *)string;
@end
