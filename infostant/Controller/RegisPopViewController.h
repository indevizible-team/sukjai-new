//
//  RegisPopViewController.h
//  infostant
//
//  Created by NINE on 1/21/56 BE.
//  Copyright (c) 2556 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginNavController.h"

@interface RegisPopViewController : UIViewController <UITextFieldDelegate> {
    LoginNavController *nc;
}

@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnClose;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollRegis;
@property (strong, nonatomic) IBOutlet UITextField *txtUsername;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UITextField *txtRepass;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
@end
