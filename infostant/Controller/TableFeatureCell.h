//
//  TableFeatureCell.h
//  infostant
//
//  Created by indevizible on 1/17/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HorizonFeature.h"
#import "DataManager.h"
@interface TableFeatureCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet HorizonFeature *collectionView;
@property (strong, nonatomic) DataController *datasource;
@property (strong, nonatomic) NSString *infofid;
@property (strong,nonatomic) UINavigationController *nav;
@property (strong,nonatomic) UIViewController *delegate;
@end
