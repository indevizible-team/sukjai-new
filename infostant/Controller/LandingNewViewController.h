//
//  LandingNewViewController.h
//  www
//
//  Created by indevizible on 2/28/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "HCYoutubeParser.h"
#import "DataController.h"
#import "Product.h"
@class LandingHeadCell;
@interface LandingNewViewController : UITableViewController{
    NSMutableArray *headerPack;
    NSMutableArray *footerPack;
    NSMutableArray *landingPack;
    NSArray *landingCellList;
    LandingHeadCell *headCellData;
    
    NSMutableDictionary *theProduct;
    NSArray *imgGallery;
    UIFont *normalFont;
    NSString *txtLike;
    NSString *txtFollow;
    NSString *txtLikeCount,*txtFollowCount;
}
@property (strong, nonatomic) NSString *feature;
@property (strong, nonatomic) DataController *product;
@property (strong, nonatomic) DataController *landing;
@property (strong, nonatomic) NSString *accessKey;

@end
