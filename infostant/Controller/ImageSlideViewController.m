//
//  ImageSlideViewController.m
//  infostant
//
//  Created by Nattawut Singhchai on 1/2/13 .
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "ImageSlideViewController.h"
#import "AFNetworking.h"
#import "DataManager.h"
@interface ImageSlideViewController ()<UIScrollViewDelegate>{
    CGRect defaultFrame;
    CGRect defaultBound;
}

@end

@implementation ImageSlideViewController
@synthesize imageList;
-(void)setRotation:(UIInterfaceOrientation)toInterfaceOrientation
{
	int widthInt = 0;
    int heightInt = 0;
    if(toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation== UIInterfaceOrientationPortraitUpsideDown){
        widthInt = viewSize.width;
        heightInt = viewSize.height;
    }
    else{
        widthInt = viewSize.height;
        heightInt = viewSize.width;
    }
    NSLog(@"b-%@",NSStringFromCGRect(self.view.bounds));
	NSLog(@"f-%@",NSStringFromCGRect(self.view.frame));
    {
        [myScroller setFrame:CGRectMake(0, 0, widthInt, heightInt)];
        [myScroller setContentSize:CGSizeMake(widthInt*[imageList count], heightInt)];
        [myScroller setContentOffset:CGPointMake(rotationPageInt*widthInt, 0)];
        // NSLog(@"nowPageInt %d",nowPageInt);
        
        for (int i=0; i<[imageList count]; i++)
        {
            NSDictionary *tempDic= [imageArray objectAtIndex:i];
            UIScrollView *tempZoomScrollView = [tempDic objectForKey:@"zoomScrollView"];
            [tempZoomScrollView setFrame:CGRectMake(i*widthInt, 0, widthInt, heightInt)];
            [tempZoomScrollView setZoomScale:1];
            [tempZoomScrollView setContentSize:CGSizeMake(widthInt, heightInt)];
            
            UIView *tempZoomView = [tempDic objectForKey:@"zoomView"];
            [tempZoomView setFrame:CGRectMake(0, 0, widthInt, heightInt)];
            
            UIImageView *tempImageView = [tempDic objectForKey:@"imageView"];
            [tempImageView setFrame:CGRectMake(0, 0, widthInt, heightInt)];
			
        }
    }//setUp
 
	
}
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	[self setRotation:toInterfaceOrientation];
}
-(BOOL)shouldAutorotate{
	NSLog(@"nowPageInt = %d",nowPageInt);
	rotationPageInt = nowPageInt;
    return YES;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	NSLog(@"nowPageInt = %d",nowPageInt);
	rotationPageInt = nowPageInt;
    return YES;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.btnDone.title = NSLocalizedString(@"infoDone", @"");
    
    if (self.txtDescription != nil) {
        self.description.hidden = NO;
        [self.description setText:self.txtDescription];
        self.description.font = [DataManager fontSize:@"small"];
    }
    
	
	
    viewSize = self.view.frame.size;
    [self.view setBackgroundColor:[UIColor blackColor]];
    imageArray = [[NSMutableArray alloc] init];
    
    [myScroller setTag:1];
    
    for (int i=0; i<[imageList count]; i++)
    {
        NSMutableDictionary *tempImageDic = [[NSMutableDictionary alloc] init];
        
		UIScrollView *tempZoomScrollView = [[UIScrollView alloc] init];
		tempZoomScrollView.scrollEnabled = YES;
		
		tempZoomScrollView.userInteractionEnabled = YES;
		[tempZoomScrollView setFrame:CGRectMake(i*viewSize.width, 0, viewSize.width, viewSize.height)];
		[tempZoomScrollView setMaximumZoomScale:2.0f];
		[tempZoomScrollView setMinimumZoomScale:1.0f];
		tempZoomScrollView.bouncesZoom = YES;
		tempZoomScrollView.delegate =self;
		tempZoomScrollView.clipsToBounds = YES;
        [tempImageDic setObject:tempZoomScrollView forKey:@"zoomScrollView"];
        
		
		UIView *tempZoomView = [[UIView alloc] init];
		[tempZoomView setFrame:CGRectMake(0, 0, viewSize.width, viewSize.height)];
        UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
		[singleTap setNumberOfTapsRequired:1];
		[doubleTap setNumberOfTapsRequired:2];
        [tempZoomView addGestureRecognizer:doubleTap];
		[tempZoomView addGestureRecognizer:singleTap];
        [tempZoomView setTag:1];
        [tempImageDic setObject:tempZoomView forKey:@"zoomView"];
        
        UIImageView *tempImageView = [[UIImageView alloc] init];
        [tempImageView setContentMode:UIViewContentModeScaleAspectFit];
        [tempImageView setFrame:CGRectMake(0, 0, viewSize.width, viewSize.height)];
        [tempImageView setTag:1];
		if ([imageList[i] isKindOfClass:[NSString class]]) {
			[tempImageView setImage:[UIImage imageNamed:imageList[i]]];
		}else if ([imageList[i] isKindOfClass:[UIImage class]]){
			[tempImageView setImage:imageList[i]];
		}else if([imageList[i] isKindOfClass:[NSURL class]]){
			[tempImageView setImageWithURL:imageList[i]];
		}
        
		[tempImageDic setObject:tempImageView forKey:@"imageView"];
        
        [tempZoomView addSubview:tempImageView];
        [tempZoomScrollView addSubview:tempZoomView];
        
        [myScroller addSubview:tempZoomScrollView];
		
        [imageArray addObject:tempImageDic];
    }
	
	
	[self setRotation:[[UIApplication sharedApplication] statusBarOrientation]];
}
-(void)viewDidAppear:(BOOL)animated{
    defaultFrame = self.description.frame;
    defaultBound = self.description.bounds;
    
    NSLog(@"F %@ - B %@",NSStringFromCGRect(defaultFrame),NSStringFromCGRect(defaultBound));
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Zoom
-(UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView2
{
    UIView *tempZoomView = (UIView*)[scrollView2 viewWithTag:1];
    return tempZoomView;
}
- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center {
    CGRect zoomRect;
    
    // the zoom rect is in the content view's coordinates.
    //    At a zoom scale of 1.0, it would be the size of the imageScrollView's bounds.
    //    As the zoom scale decreases, so more content is visible, the size of the rect grows.
    zoomRect.size.height = [[[imageArray objectAtIndex:nowPageInt] objectForKey:@"zoomScrollView"] frame].size.height / scale;
    zoomRect.size.width  = [[[imageArray objectAtIndex:nowPageInt] objectForKey:@"zoomScrollView"] frame].size.width  / scale;
    
    // choose an origin so as to get the right center.
    zoomRect.origin.x    = center.x - (zoomRect.size.width  / 2.0);
    zoomRect.origin.y    = center.y - (zoomRect.size.height / 2.0);
    
    return zoomRect;
}
- (void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer {
    // zoom in
    float newScale;
    if ([[[imageArray objectAtIndex:nowPageInt] objectForKey:@"zoomScrollView"] zoomScale]<1.5) {
        newScale = 2.0f;
    }
    else
    {
        newScale = 1.0f;
    }
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
    [[[imageArray objectAtIndex:nowPageInt] objectForKey:@"zoomScrollView"] zoomToRect:zoomRect animated:YES];
    
}
-(void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer{
	NSLog(@"xxxxx");
	
	[UIView animateWithDuration:0.4 delay:0.0 options:0
					 animations:^{
						 //						 if (_doneBar.hidden) {
						 //							 _doneBar.hidden = NO;
						 //						 }else{
						 //							 _doneBar.hidden = YES;
						 //						 }
						 if(_doneBar.alpha == 1.0){
							 _doneBar.alpha =0;
                             _description.alpha =0;
						 }else{
							 _doneBar.alpha =1;
                             _description.alpha = 1;
						 }
					 }
					 completion:nil];
	
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([scrollView tag]==1)
    {
		CGFloat pageWidth = scrollView.frame.size.width;
		int page = ((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
		// pageControl.currentPage = page;
		nowPageInt = page;
        UIInterfaceOrientation tempInterface;
        tempInterface = [[UIApplication sharedApplication] statusBarOrientation];
        if( tempInterface == UIInterfaceOrientationPortrait || tempInterface == UIInterfaceOrientationPortraitUpsideDown) {
            if(scrollView.contentOffset.x>=(oldPageInt+1)*viewSize.width || scrollView.contentOffset.x<=(oldPageInt-1)*viewSize.width){
                [[[imageArray objectAtIndex:oldPageInt] objectForKey:@"zoomScrollView"] setZoomScale:1];
                oldPageInt = nowPageInt;
            }
        }
        else{
            if(scrollView.contentOffset.x>=(oldPageInt+1)*viewSize.height || scrollView.contentOffset.x<=(oldPageInt-1)*viewSize.height){
                [[[imageArray objectAtIndex:oldPageInt] objectForKey:@"zoomScrollView"] setZoomScale:1];
                oldPageInt = nowPageInt;
            }
        }
        
    }

    if ([[[scrollView superclass] superclass] isSubclassOfClass:[UIView class]]) {
//         NSLog(@"B %@ - %@",NSStringFromCGRect(scrollView.frame),NSStringFromCGRect(scrollView.bounds));
    }
   
}
#pragma mark -
- (void)viewDidUnload {
	myScroller = nil;
	[self setDoneBar:nil];
	[super viewDidUnload];
}
- (IBAction)DoneButton:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
	NSLog(@"touched");
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
     if ([[[scrollView superclass] superclass] isSubclassOfClass:[UIView class]]) {
//         self.description.bounds = defaultFrame;
//         self.description.frame
       
     }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    
    if ([[[scrollView superclass] superclass] isSubclassOfClass:[UIView class]]) {
        NSLog(@"%@",NSStringFromCGSize(scrollView.frame.size));
//          NSLog(@"A F %@ - B %@",NSStringFromCGRect(scrollView.frame),NSStringFromCGRect(scrollView.bounds));
//        CGRect tmpFrame = self.description.bounds;
//        tmpFrame.size.height = scrollView.contentSize.height;
//        self.description.frame = tmpFrame;
//        NSLog(@"%@",NSStringFromCGSize(scrollView.frame.size));
//        CGRect tmpBound = self.description.frame;
//        tmpBound.size.height = self.description.frame.size.height+self.description.bounds.origin.y;
//        
//        self.description.frame = tmpBound;
//        self.description.bounds = tmpBound;
//        self.description.frame = defaultFrame;
    }
}
@end
