//
//  RegisterViewController.h
//  infostant
//
//  Created by INFOSTANT on 12/26/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ELCTextFieldCell.h"
@interface RegisterViewController : UITableViewController <ELCTextFieldDelegate,UIAlertViewDelegate>{
	
	NSArray *labels;
	NSArray *placeholders;
    
}
@property (nonatomic, retain) NSArray *labels;
@property (nonatomic, retain) NSArray *placeholders;
@property (strong, nonatomic) IBOutlet UITableView *regisTable;

- (IBAction)reg:(id)sender;


@end
