//
//  MainViewController.m
//  socioville
//
//  Created by Valentin Filip on 09.04.2012.
//  Copyright (c) 2012 App Design Vault. All rights reserved.
//

#import "MainViewController.h"
#import "FeatureViewController.h"
#import "MenuViewController.h"
#import "MenuRightViewController.h"
#import "AppDelegate.h"
#import "InfostantNavigationViewController.h"
#import "AFJSONRequestOperation.h"
#import "AFHTTPClient.h"
#import "DataManager.h"
#import "MyTripListViewController.h"
#import "DMURL.h"
@interface MainViewController (){
    BOOL rightMenuEnable;
}

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
   [super viewDidLoad];
    rightMenuEnable =[[[[NSBundle mainBundle] infoDictionary] valueForKey:@"RightMenu"] boolValue];
	self.canRotation = NO;
    UIViewController *frontVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FeatureVC"];
    MenuViewController *rearVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RearVC"];
    MenuRightViewController *rightVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MRightVC"];
    
    InfostantNavigationViewController *nav = [[InfostantNavigationViewController alloc]initWithRootViewController:frontVC];
    
    self.contentViewController = nav;
    self.sidebarViewController = rearVC;
    self.sidebarRightViewController = rightVC;
    
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.window.rootViewController = self;
    if (appDelegate.tmpURL) {
        NSMutableDictionary *query = [appDelegate.tmpURL dictionaryForQueryString];
        Trip *myTrip= [Trip new];
        myTrip.code = query[@"code"];
          MyTripListViewController *vc = (MyTripListViewController *)[DataManager controllerByID:@"MyTripListViewController"];
        vc.trip = myTrip;
        [self.contentViewController pushViewController:vc animated:NO];
        appDelegate.tmpURL = nil;
    }
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
	self.title = self.contentViewController.title;
}

- (void)revealToggle:(id)sender {
    [super toggleSidebar:!self.leftSidebarShowing duration:kGHRevealSidebarDefaultAnimationDuration];
}

- (void)revealRightToggle:(id)sender {
    if (rightMenuEnable) {
          [super toggleRightSidebar:!self.rightSidebarShowing duration:kGHRevealSidebarDefaultAnimationDuration];
    }
  
}

- (void)revealGesture:(UIPanGestureRecognizer *)recognizer {
    if ( rightMenuEnable) {
        [super dragContentView:recognizer];
    }else{
        [super leftDragContentView:recognizer];

    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
	
	if (self.canRotation) {
        // for iPhone, you could also return UIInterfaceOrientationMaskAllButUpsideDown
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
    return UIInterfaceOrientationMaskPortrait;
    
}
@end
