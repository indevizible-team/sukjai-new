//
//  ProfileHeader.h
//  sukjai
//
//  Created by MINDNINE on 7/17/56 BE.
//  Copyright (c) 2556 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileHeader : UICollectionReusableView
@property (strong, nonatomic) IBOutlet UIImageView *imgUserProfile;
@property (strong, nonatomic) IBOutlet UILabel *labelLocate;
@property (strong, nonatomic) IBOutlet UILabel *lblUsername;
@property (strong, nonatomic) IBOutlet UILabel *follerNumber;
@property (strong, nonatomic) IBOutlet UILabel *follingNumber;
@property (strong, nonatomic) IBOutlet UILabel *likeNumber;

@end
