//
//  FollowListViewController.m
//  infostant
//
//  Created by INFOSTANT on 12/25/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import "FollowListViewController.h"
#import "AppDelegate.h"
#import "DataManager.h"
#import "FollowListCell.h"
#import "FollowDetailViewController.h"
@interface FollowListViewController ()<LoginDelegate>{
    UIRefreshControl *refreshControl;
}
@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;
@end

@implementation FollowListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing: ) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    [refreshControl beginRefreshing];
    following = NO;
    page = 1;
    canLoadMore = YES;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self loadMore];
}
- (void)dropViewDidBeginRefreshing:(UIRefreshControl *)refreshControl
{
    page = 1;
    follow = nil;
    [self loadMore];
}
-(void)loadMore{
    canLoadMore = NO;
    User *user = [User currentUser];
    if (user !=nil) {
      
            
            AFHTTPClient *client = [DataManager client];
            NSMutableDictionary *param = [@{
                                          @"page":[NSNumber numberWithInt:page++],
                                          @"oauth_login":user.oauth_login
                                          } mutableCopy];
            [param addEntriesFromDictionary:[DataManager param]];
            if (following) {
                [param setValue:@1 forKey:@"following"];
            }
//            [SVProgressHUD showWithStatus:NSLocalizedString(@"infoLoadMessage", @"")];
            NSURLRequest *request = [client requestWithMethod:@"GET" path:@"getFollow" parameters:param];
            NSLog(@"requesting : %@",request.URL);
            AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                if ([JSON count]) {
                    canLoadMore = YES;
                    if ([JSON valueForKey:@"expire"]==nil) {
                        if (follow == nil) {
                            follow = [[DataController alloc] initWithDictionary:[DataManager JSONToMutableDictionary:JSON]];
                        }else{
                            [follow insertData:[DataManager JSONToMutableDictionary:JSON]];
                        }
                        
                    }else{
                        
                    }
                }
                                                 [self.tableView reloadData];
                [refreshControl endRefreshing];
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                [refreshControl endRefreshing];
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNetworkError", @"")];

            }];
            [operation start];
        
    }

}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}
-(void)openLogin{
    LoginNavController *nvc =(LoginNavController *) [DataManager controllerByID:@"LoginNav"];
    nvc.ldelegate = self;
    [self presentViewController:nvc animated:YES completion:nil];
}

-(void)didLogin:(LoginNavController *)vc{
    page = 1;
    follow = nil;
    [self loadMore];
}

- (void)viewDidUnload {
    
    [super viewDidUnload];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.title = NSLocalizedString(@"infoFollower", @"");
	
    UINavigationController *nav = self.navigationController;
    UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			self.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
		}
		
		// Check if we have a revealButton already.
		if (![self.navigationItem leftBarButtonItem]&&(self.navigationController.viewControllers.count==1)) {
			// If not, allocate one and add it.
			UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
            
            UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [menuButton setImage:imageMenu forState:UIControlStateNormal];
            menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
            [menuButton addTarget:controller action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
		}
	}
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (follow != nil) {
        return follow.key.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        return 60;
    }
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        return 80;
    }
    return 60;
}

- (FollowListCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *each = [follow.data valueForKey:follow.key[indexPath.row]];
    FollowListCell *cell = (FollowListCell *)[tableView dequeueReusableCellWithIdentifier:@"FollowListCell"];
    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"listTable.png"]];
    cell.lblName.text = [each valueForKey:@"displayname"];
    cell.lblLocate.text = [each valueForKey:@"proname"];
    cell.caption.text = [each valueForKey:@"caption"];
    [cell.prifilePic setImageWithURL:[NSURL URLWithString:[each valueForKey:@"picuser"]]];
    cell.folid = [each valueForKey:@"folid"];
    
    if (canLoadMore && ( indexPath.row > follow.key.count-2)) {
        [self loadMore];
    }
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(FollowListCell *)sender{
    FollowDetailViewController *vc = [segue destinationViewController];
    vc.personProfile = [follow.data valueForKey:sender.folid];
}
- (IBAction)changeModeButton:(UIBarButtonItem *)sender {
    if (!following) {
        sender.title = NSLocalizedString(@"infoFollower", @"");
        self.title = NSLocalizedString(@"infoFollowing", @"");
    }else{
        sender.title = NSLocalizedString(@"infoFollowing", @"");
        self.title = NSLocalizedString(@"infoFollower", @"");
    }
    [refreshControl beginRefreshing];
    follow = nil;
    following = !following;
    page = 1;
    canLoadMore = YES;
    [self loadMore];
}
@end
