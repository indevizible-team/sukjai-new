//
//  SelectCatViewController.m
//  infostant
//
//  Created by Nattawut Singhchai on 12/29/12 .
//  Copyright (c) 2012 infostant. All rights reserved.
//

#import "SelectCatViewController.h"
#import "SelectCatCell.h"
@interface SelectCatViewController ()<UISearchBarDelegate>

@end

@implementation SelectCatViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"infoSelectCate", @"");
	if ([self.delegate respondsToSelector:@selector(selectCatList:)]) {
		catList  = [self.delegate selectCatList:self];
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [catList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SelectCatCell";
    SelectCatCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.catLabel.text = catList[indexPath.row];
    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgTableListA.png"]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	if ([self.delegate respondsToSelector:@selector(didSelectCat:selected:)]) {
		[self.delegate didSelectCat:self selected:indexPath.row];
	}
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
	if ([self.delegate respondsToSelector:@selector(didSearchCat:forValue:)]) {
		[self.delegate didSearchCat:self forValue:searchBar.text];
	}
}

@end
