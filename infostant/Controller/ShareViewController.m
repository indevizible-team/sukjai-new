//
//  ShareViewController.m
//  www
//
//  Created by Trash on 3/14/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "ShareViewController.h"
#import <Social/Social.h>
//#import "FacebookPhotoAlbumViewController.h"
#import "DMPhotoAlbumViewController.h"
@interface ShareViewController ()<DMPhotoAlbumDataSource>{
    NSArray *testData;
}
- (IBAction)shareFacebook:(UIButton *)sender;

@end

@implementation ShareViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)shareFacebook:(UIButton *)sender {
    testData= @[
  @{@"caption": @"cap1",
    @"image":@"http://sphotos-f.ak.fbcdn.net/hphotos-ak-ash3/580192_320591048070036_420529414_n.jpg"},
  @{@"caption":@"แวบนึงกรูคิด นี่กรูปล่อยตัวเองโทรมหรอวะเนี่ย ดูดิ๊ อีวุดยังดูแลผมซะ...=[]=",
    @"image":@"http://sphotos-h.ak.fbcdn.net/hphotos-ak-snc7/578310_577762928914056_596995654_n.jpg"}
  ,
  @{@"caption":@"ชาบูเจี๊ยบน้อย ให้มีแรงทำงานวันนี้ >w<",
    @"image":@"http://sphotos-a.ak.fbcdn.net/hphotos-ak-snc6/269306_577014688988880_586037735_n.jpg"}
  ];
//    DMPhotoAlbumViewController *vc = [[DMPhotoAlbumViewController alloc] initWithDelegate:self];
//    [self.navigationController pushViewController:vc animated:YES];
}

-(NSUInteger)numberOfPhotoAlbum:(DMPhotoAlbumViewController *)photoAlbum{
    return testData.count;
}

-(NSString *)photoAlbum:(DMPhotoAlbumViewController *)photoAlbum imageLinkForIndex:(NSUInteger)index{
    return testData[index][@"image"];
}

-(NSString *)photoAlbum:(DMPhotoAlbumViewController *)photoAlbum captionForIndex:(NSUInteger)index{
    return testData[index][@"caption"];
}
@end
