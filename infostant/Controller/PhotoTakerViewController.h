//
//  PhotoTakerViewController.h
//  AddContentProto
//
//  Created by indevizible on 1/24/13.
//  Copyright (c) 2013 indevizible. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
@interface PhotoTakerViewController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate>{
    BOOL didChangeImage,newMedia;
}
- (IBAction)btnTaken:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) UIPopoverController *myPopoverController;
@property (nonatomic, weak) NSMutableDictionary *targetSource;
@property (nonatomic, weak) UITableView *targetTable;
@end
