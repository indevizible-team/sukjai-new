//
//  MessageViewController.h
//  infostant
//
//  Created by INFOSTANT on 12/25/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataController.h"
@interface MessageViewController : UITableViewController{
    DataController *chatList;
    int page;
    BOOL canLoadMore;
}
@property (strong, nonatomic) IBOutlet UITableView *chatCenterTable;

@end
