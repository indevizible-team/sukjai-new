//
//  LikeListViewController.h
//  infostant
//
//  Created by INFOSTANT on 12/25/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"

@interface LikeListViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>{
    BOOL canLoadMore;
    User *user;
    
    NSUInteger page;
    AFHTTPClient *client;
    NSMutableArray *featureList;
    NSString *mode;
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionLike;
@property (strong, nonatomic) NSString *infofid;
@property (strong, nonatomic) DataController *allFeature;
@property (strong, nonatomic) DataController *like;
@property BOOL feed;
@end
