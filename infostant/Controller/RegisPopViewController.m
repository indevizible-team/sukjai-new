//
//  RegisPopViewController.m
//  infostant
//
//  Created by NINE on 1/21/56 BE.
//  Copyright (c) 2556 infostant. All rights reserved.
//

#import "RegisPopViewController.h"
#import "DataManager.h"
#define kOFFSET_FOR_KEYBOARD 216
#define kOFFSET_FOR_KEYBOARD_PAD 264

@interface RegisPopViewController ()

@end

@implementation RegisPopViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"infoRegis", @"");
    self.btnClose.title = NSLocalizedString(@"infoClose", @"");
    self.txtEmail.placeholder = NSLocalizedString(@"infoUsername", @"");
    self.txtPassword.placeholder = NSLocalizedString(@"infoPassword", @"");
    self.txtRepass.placeholder = NSLocalizedString(@"infoRepass", @"");
    self.txtUsername.placeholder = NSLocalizedString(@"infoEmail", @"");
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldBeginEditing");
    [UIView animateWithDuration:0.2
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGRect rect =_scrollRegis.frame;
                         NSLog(@"=> %f",rect.origin.y);
                         if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone){
                             rect.origin.y = self.view.frame.size.height-kOFFSET_FOR_KEYBOARD-rect.size.height+80;
                         }
                         if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
                             rect.origin.y = self.view.frame.size.height-kOFFSET_FOR_KEYBOARD_PAD-rect.size.height;
                         }
                         _scrollRegis.frame = rect;
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done");
                     }];
    
    return YES;
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldEndEditing");
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSLog(@"textFieldShouldReturn");
    [textField resignFirstResponder];
    [UIView animateWithDuration:0.2
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGRect rect =_scrollRegis.frame;
                         rect.origin.y = self.view.frame.size.height-rect.size.height;
                         _scrollRegis.frame = rect;
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done");
                     }];

    return  YES;
}
//@"Please enter password"
- (IBAction)closeBtn:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)submitBtn:(id)sender {
    UIAlertView *alert;

    if (![self.txtPassword.text isEqualToString:self.txtRepass.text]) {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoError", @"") message:NSLocalizedString(@"infoPassNotMatch", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"infoOK", @"") otherButtonTitles:nil];
        [alert show];
    }else if([self.txtPassword.text length] < 1){
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoError", @"") message:NSLocalizedString(@"infoPleasePass", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"infoOK", @"") otherButtonTitles:nil];
        [alert show];
    }else if([self.txtEmail.text length] < 1){
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoError", @"") message:NSLocalizedString(@"infoEnterMail", @"")  delegate:self cancelButtonTitle:NSLocalizedString(@"infoOK", @"") otherButtonTitles:nil];
        [alert show];
    }else if([self.txtUsername.text length] < 1){
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoError", @"") message:NSLocalizedString(@"infoEnterUser", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"infoOK", @"") otherButtonTitles:nil];
        [alert show];
    }else{
        NSMutableDictionary *regis = [@{
                                     
                                      @"username":self.txtUsername.text,
                                      @"password1":self.txtPassword.text,
                                      @"email":self.txtEmail.text
                                      } mutableCopy];
        [regis addEntriesFromDictionary:[DataManager param]];
        NSDictionary *device = [PLIST readFeature:@"device"];
        [regis addEntriesFromDictionary:device];
        
        AFHTTPClient *client = [DataManager client];
        NSURLRequest *request = [client requestWithMethod:@"POST" path:@"saveregister" parameters:regis];
        [SVProgressHUD showWithStatus:NSLocalizedString(@"infoRegisStatus", @"")];
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            NSLog(@"JSON : %@",JSON);
            if ([[JSON valueForKey:@"error"] isKindOfClass:[NSString class]]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoError", @"") message:[JSON valueForKey:@"error"] delegate:nil cancelButtonTitle:NSLocalizedString(@"infoOK", @"") otherButtonTitles: nil];
                [alert show];
            }else{
                [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"infoRegisComplete", @"")];
                [PLIST writeFeature:@"user" withData:JSON];
                [nc dismissViewControllerAnimated:YES completion:^{
                    [nc didLogin];
                }];
            }
            [SVProgressHUD dismiss];
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNoInternet", @"")];
        }];
        [operation start];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    NSLog(@"-------%u",interfaceOrientation);
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
    }
}

@end
