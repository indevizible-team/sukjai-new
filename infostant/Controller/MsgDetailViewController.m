//
//  MsgDetailViewController.m
//  infostant
//
//  Created by INFOSTANT on 12/25/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import "MsgDetailViewController.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "MsgChatCell.h"
#import "SVProgressHUD.h"
#define kOFFSET_FOR_KEYBOARD 216
#define kOFFSET_FOR_KEYBOARD_PAD 264

@interface MsgDetailViewController ()<LoginDelegate>
@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;
@end

@implementation MsgDetailViewController
@synthesize navigationBarPanGestureRecognizer;



-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldBeginEditing");
    [UIView animateWithDuration:0.2
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGRect rect =_msgField.frame;
                         if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone){
                             rect.origin.y = self.view.frame.size.height-kOFFSET_FOR_KEYBOARD-rect.size.height;
                         }
                         if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
                             rect.origin.y = self.view.frame.size.height-kOFFSET_FOR_KEYBOARD_PAD-rect.size.height;
                         }
                        
                         _msgField.frame = rect;
                         
                     }
                     completion:^(BOOL finished){
                         if (finished) {
                             [self expandCollection];
                             [self scrollToBottom];
                         }
                         
                     }];
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldEndEditing");
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSLog(@"textFieldShouldReturn");
    [_txtfieldMsg resignFirstResponder];
    [UIView animateWithDuration:0.2
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGRect rect =_msgField.frame;
                         rect.origin.y = self.view.frame.size.height-rect.size.height;
                         _msgField.frame = rect;
                         
                     }
                     completion:^(BOOL finished){
                         if (finished) {
                             [self expandCollection];
                             
                         }
                         
                     }];
    return  YES;
}


-(void)expandCollection{
    CGRect oriCollection = _collectionMsgDetail.frame;
    oriCollection.size.height = _msgField.frame.origin.y;
    _collectionMsgDetail.frame = oriCollection;
}


- (void)viewDidUnload {
    
    [super viewDidUnload];

}
-(void)viewDidLoad{
    tmpCount = 0;
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    delegate.messageView = self;
    user = [User currentUser];
    self.title = NSLocalizedString(@"infoMsgDetails", @"");
	
    UINavigationController *nav = self.navigationController;
    UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			self.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
           
            
		}
	}
    [self reloadMessage];
}

-(void)reloadMessage{
    client = [DataManager client];
    NSMutableDictionary *param = [@{
                                  @"page":@1,
                                  @"oauth_login":user.oauth_login,
                                  @"friendid":_friendid
                                  } mutableCopy];

    [param addEntriesFromDictionary:[DataManager param]];
    NSURLRequest *request = [client requestWithMethod:@"GET" path:@"getChatlist" parameters:param];
    NSLog(@"requesting : %@",request.URL);
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        if ([JSON count]) {
            if ([JSON valueForKey:@"expire"]!=nil) {
                [self openLoginPage];
            }else{
                chat = [[DataController alloc] initWithDictionary:[DataManager JSONToMutableDictionary:JSON]];
                [_collectionMsgDetail reloadData];
                [self scrollToBottom];
                [SVProgressHUD dismiss];
            }
        }
   
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNetworkError", @"")];
    }];
    [operation start];
    

}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}
-(void)openLoginPage{
    LoginNavController *nvc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginNav"];
    nvc.ldelegate = self;
    [self presentViewController:nvc animated:YES completion:nil];
}

-(void)didLogin:(LoginNavController *)vc{
    tmpCount = 0;
    user = [User currentUser];
    [self reloadMessage];
}
-(void)scrollToBottom{
    if (chat.key.count>0) {
        [_collectionMsgDetail scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:chat.key.count-1 inSection:0] atScrollPosition:UICollectionViewScrollPositionBottom animated:NO];
    }
    
}

#pragma mark - UICollectionView Datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    if (chat!=nil) {
        return chat.key.count;
        
    }
    return 0;
}

- (MsgChatCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableDictionary *chatRow = [chat.data valueForKey:chat.key[indexPath.row]];
    
    MsgChatCell *cell = (MsgChatCell *)[cv dequeueReusableCellWithReuseIdentifier: @"MessageCell" forIndexPath:indexPath];
    if([[chatRow valueForKey:@"createby"] isEqualToString:user.mid]){
        [DataManager setProfileImage:cell.profileName];
    }else{
        if ([chatRow[@"pic"] isKindOfClass:[NSString class]]) {
            [cell.profileName setImageWithURL:[NSURL URLWithString:chatRow[@"pic"]]];
        }else{
            [cell.profileName setImageWithURL:chatRow[@"pic"]];
        }
    }
    cell.msgChat.text = chatRow[@"msg"];
    cell.lblName.text = chatRow[@"displayname"];
    cell.lblTime.text = chatRow[@"ago"];
    
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    float width;
    NSMutableDictionary *chatRow = [chat.data valueForKey:chat.key[indexPath.row]];
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        width = 768.0f;
    }
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
    {
        width = 320.0f;
    }
    float height = [DataManager textHeight:chatRow[@"msg"] fromWidth:width andFont:[DataManager fontSize:@"small"]];
    if (height+15 < 60) {
        height =60;
    }else{
        height = height+25;
    }   
    
    return CGSizeMake(width, height);
}
//-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
//    [_msgField resignFirstResponder];
//}
- (IBAction)SendText:(id)sender {
    NSLog(@"move from %@",NSStringFromCGRect(_txtfieldMsg.frame));
    NSString *displayname =[user valueForKey:@"displayname"];
    NSMutableDictionary *val;

        val = [@{@"ago": @"a moment ago",
               @"displayname":displayname,
               @"pic":user.picuser,
               @"msg":[_txtfieldMsg.text mutableCopy],
               
               } mutableCopy];
    
    
    NSString *tmpKey = [NSString stringWithFormat:@"x-%i",tmpCount++];
    NSMutableDictionary *tmpChat = [@{
                                    @"key": [@[tmpKey] mutableCopy],
                                    tmpKey:val
                                    } mutableCopy];
    NSMutableDictionary *param = [@{
                                  @"page":@1,
                                  @"oauth_login":user.oauth_login,
                                  @"friendid":_friendid,
                                  @"msg":[val valueForKey:@"msg"]
                                  } mutableCopy];
    [param addEntriesFromDictionary:[DataManager param]];
    NSURLRequest *request = [client requestWithMethod:@"POST" path:@"savechatcenterdata" parameters:param];

    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"success send with url :%@",request.URL);
        if ([JSON count]) {
            if ([JSON valueForKey:@"expire"]!=nil) {
                [self openLoginPage];
            }
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"fail send with url :%@",request.URL);
    }];
    [operation start];
    
    _txtfieldMsg.text=@"";
    if (chat==nil) {
        chat = [[DataController alloc] initWithDictionary:tmpChat];
    }else{
         [chat insertData:tmpChat];
        
    }
    NSLog(@"chat data \n %@",chat.data);
   
    [_txtfieldMsg resignFirstResponder];
     _tmpRect = _collectionMsgDetail.frame;
    NSLog(@"count : %i ,%@",chat.key.count,chat.data);
    if (chat.key.count <= 1) {
        [self.collectionMsgDetail reloadData];
    }else{
        [self.collectionMsgDetail insertItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:chat.key.count-1 inSection:0]]];
    }

    [self.collectionMsgDetail performBatchUpdates:^{
       
    } completion:^(BOOL finished) {
//        NSLog(@"move to %@",NSStringFromCGRect(_txtfieldMsg.frame));
        [self scrollToBottom];
    }];
    
}
@end
