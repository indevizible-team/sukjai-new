//
//  ForgetPassPopViewController.m
//  infostant
//
//  Created by NINE on 1/21/56 BE.
//  Copyright (c) 2556 infostant. All rights reserved.
//

#import "ForgetPassPopViewController.h"
#import "DataManager.h"
@interface ForgetPassPopViewController ()

@end

@implementation ForgetPassPopViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"infoForgotPass", @"");
    self.btnClose.title = NSLocalizedString(@"infoClose", @"");
    NSDictionary *mainInfo = [[NSBundle mainBundle] infoDictionary];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:[mainInfo valueForKey:@"domain"]]];
    NSURLRequest *request = [client requestWithMethod:@"GET" path:@"resetpass" parameters:nil];
   [_webView loadRequest:request];
    
  
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    NSLog(@"-------%u",interfaceOrientation);
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)closeBtn:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


@end
