//
//  AddProductGalleryCell.m
//  infostant
//
//  Created by indevizible on 1/24/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "AddProductGalleryCell.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "AddProductViewController.h"
@implementation AddProductGalleryCell{
    AddProductViewController *sup;
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self.parent) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btn1:(id)sender {
    currentEditingCell = 1;
    [self takePhoto];
}

- (IBAction)btn2:(id)sender {
    currentEditingCell = 2;
    [self takePhoto];
}

- (IBAction)btn3:(id)sender {
    currentEditingCell = 3;
    [self takePhoto];
}

- (IBAction)btn4:(id)sender {
    currentEditingCell = 4;
    [self takePhoto];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
		if ([UIImagePickerController isSourceTypeAvailable:
			 UIImagePickerControllerSourceTypeCamera])
		{
            
            [self preparePickerType:UIImagePickerControllerSourceTypeCamera];
			[self.parent presentViewController:self.imagePicker.imagePickerController
                               animated:YES completion:nil];
			newMedia = YES;
		}
	}else if (buttonIndex == 1){
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            if ([self.myPopoverController isPopoverVisible]) {
                [self.myPopoverController dismissPopoverAnimated:YES];
            } else {
                if ([UIImagePickerController isSourceTypeAvailable:
                     UIImagePickerControllerSourceTypeSavedPhotosAlbum])
                {
                    
                    [self preparePickerType:UIImagePickerControllerSourceTypePhotoLibrary];
                    self.myPopoverController = [[UIPopoverController alloc]
                                                initWithContentViewController:self.imagePicker.imagePickerController];
                    self.myPopoverController.delegate = self;
                    [self.myPopoverController presentPopoverFromRect:CGRectMake(44, 20, 200, 200) inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
                    newMedia = NO;
                }
            }
            
        } else {
            if ([UIImagePickerController isSourceTypeAvailable:
                 UIImagePickerControllerSourceTypeSavedPhotosAlbum])
            {
                [self preparePickerType:UIImagePickerControllerSourceTypePhotoLibrary];
                [self.parent presentViewController:self.imagePicker.imagePickerController animated:YES completion:nil];
                newMedia = NO;
            }
            
        }
    }
}


-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
	if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
							  initWithTitle: NSLocalizedString(@"infoTitleSaveFail", @"")
							  message: NSLocalizedString(@"infoMsgSaveFail", @"")
							  delegate: nil
							  cancelButtonTitle:NSLocalizedString(@"infoOK", @"")
							  otherButtonTitles:nil];
        [alert show];
	}
}
-(void)takePhoto{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose Existing", nil];
    
    popup.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [popup showInView:self.parent.view];
}


# pragma mark -
# pragma mark GKImagePicker Delegate Methods

- (void)imagePicker:(GKImagePicker *)imagePicker pickedImage:(UIImage *)image{
    
    if (imagePicker.imagePickerController.sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImageWriteToSavedPhotosAlbum(image,
                                       self,
                                       @selector(image:finishedSavingWithError:contextInfo:),
                                       nil);
    }
    
    [self.targetData setValue:image forKey:[NSString stringWithFormat:@"pic-%u",currentEditingCell]];
    [self.parent.tableView reloadData];
    
    [self hideImagePicker];
    
}

- (void)hideImagePicker{
    if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM()) {
        
        [self.myPopoverController dismissPopoverAnimated:YES];
        
    } else {
        
        [self.imagePicker.imagePickerController dismissViewControllerAnimated:YES completion:nil];
        
    }
}
-(void)preparePickerType:(UIImagePickerControllerSourceType)type{
    self.imagePicker = [[GKImagePicker alloc] init];
    self.imagePicker.cropSize = [DataManager getSizeFromRatio:galleryRatio];
    self.imagePicker.delegate = self;
    self.imagePicker.imagePickerController.sourceType = type;
    self.imagePicker.imagePickerController.mediaTypes = [NSArray arrayWithObjects:
                                                         (NSString *) kUTTypeImage,
                                                         nil];
}

@end
