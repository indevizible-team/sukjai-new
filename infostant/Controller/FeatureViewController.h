//
//  FeatureViewController.h
//  infostant
//
//  Created by INFOSTANT on 12/24/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"


@interface FeatureViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UIAlertViewDelegate>{
	DataController *dc;
    UIAlertView *alert;
    DataController *feature;
}

@property (strong, nonatomic) IBOutlet UICollectionView *collectionFeature;
@property (strong, nonatomic) NSArray *dataKey;
@property (strong, nonatomic) NSMutableDictionary *dataSource;


@end
