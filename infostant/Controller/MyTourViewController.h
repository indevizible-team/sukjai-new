//
//  MyTourViewController.h
//  www
//
//  Created by Trash on 3/22/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum {
    SortTypeASD,
    SortTypeDESC
}SortType;
typedef enum {
    SortMethodCreate,
    SortMethodDuration,
    SortMethodPrice,
    SortMethodRandom
}SortMethod;
@interface MyTourViewController : UITableViewController
@property (nonatomic,assign) SortType sortType;
@property (nonatomic,assign) SortMethod sortMethod;
@property (nonatomic,strong) NSNumber *whereIs;
@property (nonatomic,strong) NSString *tag;
@end
