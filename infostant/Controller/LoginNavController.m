//
//  LoginNavController.m
//  infostant
//
//  Created by indevizible on 1/21/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "LoginNavController.h"

@interface LoginNavController ()

@end

@implementation LoginNavController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.canRotation = NO;
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)didLogin{
    if ([self.ldelegate respondsToSelector:@selector(didLogin:)]) {
		[self.ldelegate didLogin:self];
	}
}

- (NSUInteger)supportedInterfaceOrientations
{
	
	if (self.canRotation) {
        // for iPhone, you could also return UIInterfaceOrientationMaskAllButUpsideDown
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
    return UIInterfaceOrientationMaskPortrait;
    
}

+(id)loginViewDelegate:(id<LoginDelegate>)delegate{
    LoginNavController *nvc =(LoginNavController *) [DataManager controllerByID:@"LoginNav"];
    nvc.ldelegate = delegate;
    return nvc;

}
@end
