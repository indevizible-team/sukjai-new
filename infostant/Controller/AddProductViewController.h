//
//  AddProductViewController.h
//  infostant
//
//  Created by indevizible on 1/23/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "DataManager.h"
@interface AddProductViewController : UITableViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate>{
    DataController *postData;
    NSArray *cellIdentifierList;
    NSArray *storyboardIdentifierList;
    BOOL didChangeImage,newMedia;
    NSUInteger currentEditingCell;
    CLLocationManager *locationManager;
}
@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;
@property (nonatomic, strong) UIPopoverController *myPopoverController;
@property (nonatomic, strong) NSMutableArray *allData;
@end
