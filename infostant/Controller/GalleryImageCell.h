//
//  GalleryImageCell.h
//  sukjai
//
//  Created by MINDNINE on 7/17/56 BE.
//  Copyright (c) 2556 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GalleryImageCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgGalleryView;

@end
