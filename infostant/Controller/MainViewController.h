//
//  MainViewController.h
//  socioville
//
//  Created by Valentin Filip on 09.04.2012.
//  Copyright (c) 2012 App Design Vault. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GHRevealViewController.h"
#import "DataController.h"
@interface MainViewController : GHRevealViewController
@property BOOL canRotation;
@end
