//
//  LikeLandingViewController.h
//  infostant
//
//  Created by INFOSTANT on 12/26/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LikeLandingViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) IBOutlet UICollectionView *collectionLikelnd;
@end
