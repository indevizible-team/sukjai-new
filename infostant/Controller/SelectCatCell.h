//
//  SelectCatCell.h
//  infostant
//
//  Created by Nattawut Singhchai on 12/29/12 .
//  Copyright (c) 2012 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectCatCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *catLabel;
@property (strong, nonatomic) IBOutlet UIImageView *catIcon;

@end
