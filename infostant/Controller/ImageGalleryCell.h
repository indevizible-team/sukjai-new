//
//  ImageGalleryCell.h
//  www
//
//  Created by Trash on 3/22/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageGalleryCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end
