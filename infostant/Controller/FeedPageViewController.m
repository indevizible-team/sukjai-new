//
//  FeedPageViewController.m
//  infostant
//
//  Created by indevizible on 1/17/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "FeedPageViewController.h"
#import "TableFeatureCell.h"
#import "DataManager.h"
#import "LikeListViewController.h"
#import "HorizonFeatureCell.h"
#import "CategoryViewController.h"
@interface FeedPageViewController ()<LoginDelegate>
@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;
@end

@implementation FeedPageViewController
@synthesize navigationBarPanGestureRecognizer;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    [SVProgressHUD show];
    self.featureList = [NSMutableArray new];
    self.feature  = [[DataController alloc] initWithDictionary:[PLIST readFeature:@"feature"]];
    [self loadFeature:0];
   

}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}
-(void)loadFeature:(NSUInteger)index{
    User *user = [User currentUser];
    NSMutableDictionary *param = [DataManager param];
    
    [param setValue:user.oauth_login forKey:@"oauth_login"];
    [param setValue:[[self.feature dictionaryAtIndex:index] valueForKey:@"infofid"] forKey:@"infofid"];
    [param setValue:@1 forKey:@"feedall"];
    [param setValue:@1 forKey:@"page"];
    
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"GET" path:@"getNewAllFeature" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"feed - %@",JSON);
        [SVProgressHUD dismiss];
        if ([JSON count]) {
            if ([JSON valueForKey:@"expire"]==nil) {
                DataController *tmpController = [[DataController alloc] initWithDictionary:[DataManager JSONToMutableDictionary:JSON]];
                
                tmpController.name = [param valueForKey:@"infofid"];
                [self.featureList addObject:tmpController];
                
                if (index+1 == self.feature.key.count) {
                    
                    for (DataController *dc in self.featureList) {
                        NSLog(@"%@",[dc.data valueForKey:@"infofid"]);
                    }
                   
                    
                }else{
                    [self loadFeature:index+1];
                }
                
            }else{
                [self openLogin];
            }

        }
         [self.tableView reloadData];
               
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"Fail");
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNetworkError", @"")];
    }];
    [operation start];
}
-(void)openLogin{
    LoginNavController *nvc =(LoginNavController *) [DataManager controllerByID:@"LoginNav"];
    nvc.ldelegate = self;
    [self presentViewController:nvc animated:YES completion:nil];
}
-(void)didLogin:(LoginNavController *)vc{
    [self loadFeature:0];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
	self.title = NSLocalizedString(@"infoFeed", @"");
	
    UINavigationController *nav = self.navigationController;
    UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			self.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
		}
		
		// Check if we have a revealButton already.
		if (![self.navigationItem leftBarButtonItem]&&(self.navigationController.viewControllers.count==1)) {
			// If not, allocate one and add it.
			UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
            
            UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [menuButton setImage:imageMenu forState:UIControlStateNormal];
            menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
            [menuButton addTarget:controller action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
		}
	}
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.feature == nil) {
        return 0;
    }
    return self.feature.key.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    
//    tableView.backgroundColor = [UIColor whiteColor];
//
//    return tableView;
//    
//}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"FeedArticleCell";

    TableFeatureCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    
    if (self.featureList.count&&(self.featureList.count > indexPath.section )) {
        
            cell.datasource = [self.featureList objectAtIndex:indexPath.section];
            [cell.collectionView reloadData];
        
    }else{
        cell.datasource = [[DataController alloc] initWithDictionary:[@{} mutableCopy]];
        [cell.collectionView reloadData];
    }
    cell.nav = self.navigationController;
    cell.delegate = self;
    cell.infofid = [[self.feature dictionaryAtIndex:indexPath.section] valueForKey:@"infofid"];
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    //    vc.feed = YES;
    //    vc.infofid =
    if ([[segue identifier] isEqualToString:@"SeeAll"]) {
        
//        LikeListViewController *vc = [segue destinationViewController];
//         HorizonFeatureCell *cell = (HorizonFeatureCell*) sender;
//        NSLog(@"%@",cell.infofid);
//        
//         vc.infofid = cell.infofid;
//         vc.feed = YES;
//        NSLog(@"%i",vc.feed);
    }

    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}
//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//    
//    NSString *titleHeader = [[self.feature dictionaryAtIndex:section] valueForKey:@"name"];
//
//
//    return titleHeader;
//}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSString *sectionName = [[self.feature dictionaryAtIndex:section] valueForKey:@"name"];
    
    UILabel *sectionHeader = [[UILabel alloc] init];
    sectionHeader.backgroundColor = [UIColor whiteColor];
    sectionHeader.textColor = [UIColor colorWithRed:68.0/255.0f green:68.0/255.0f blue:68.0/255.0f alpha:1.0f];
    sectionHeader.frame = CGRectMake(50, 0, 200, 20);
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        sectionHeader.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:19.0];
        sectionHeader.text = [NSString stringWithFormat:@"  %@",sectionName];
    }
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        sectionHeader.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0];
        sectionHeader.text = [NSString stringWithFormat:@" %@",sectionName];
    }



    return sectionHeader;
}

@end
