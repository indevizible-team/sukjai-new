//
//  SubCatViewController.h
//  infostant
//
//  Created by indevizible on 1/11/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"
@interface SubCatViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>{
    BOOL canLoadMore;
    User *user;
    NSString *infofid;
    NSUInteger page;
    AFHTTPClient *client;
    NSMutableArray *featureList;
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionSubCat;
@property (strong, nonatomic) NSString *subcatid;
@property (strong, nonatomic) NSString *friendid;
@property (strong, nonatomic) NSString *infofid;
@property (strong, nonatomic) DataController *subCat;
@property (strong, nonatomic) DataController *allSubCat;
@end
