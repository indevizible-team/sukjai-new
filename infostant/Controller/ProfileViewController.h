//
//  ProfileFeedViewController.h
//  infostant
//
//  Created by INFOSTANT on 12/25/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) IBOutlet UICollectionView *collectionProfile;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblLocation;
@property (strong, nonatomic) IBOutlet UILabel *flwerNumber;
@property (strong, nonatomic) IBOutlet UILabel *flwngNumber;
@property (strong, nonatomic) IBOutlet UILabel *likeNumber;
@property (strong, nonatomic) IBOutlet UIImageView *profilePic;

@property (strong, nonatomic) NSString *friendId;
@end
