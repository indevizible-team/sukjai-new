//
//  FeatureViewController.m
//  infostant
//
//  Created by INFOSTANT on 12/24/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//
#import "FeatureBanner.h"
#import "FeatureViewController.h"
#import "FeatureCell.h"
#import "AppDelegate.h"
#import "CategoryViewController.h"
#import "SVProgressHUD.h"
@interface FeatureViewController (){
    NSMutableDictionary *image;
}
@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;

@end



@implementation FeatureViewController

@synthesize navigationBarPanGestureRecognizer;
@synthesize dataSource, dataKey;

- (void)viewDidUnload {
    [super viewDidUnload];
}
-(void)viewDidLoad{
    feature = [[DataController alloc] initWithDictionary:[PLIST readFeature:@"feature"]];

    image = [PLIST readFeature:@"image"];

}


- (void)viewDidAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.title = NSLocalizedString(@"infoAppName", @"");
	
    UINavigationController *nav = self.navigationController;
    UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			self.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
		}
		NSLog(@"navigation stack = %i",self.navigationController.viewControllers.count);
		// Check if we have a revealButton already.
		if (![self.navigationItem leftBarButtonItem]&&(self.navigationController.viewControllers.count==1)) {
			// If not, allocate one and add it.
			UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
            
            UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [menuButton setImage:imageMenu forState:UIControlStateNormal];
            menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
            [menuButton addTarget:controller action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
		}

        if (![self.navigationItem rightBarButtonItem]&&[[[[NSBundle mainBundle] infoDictionary] valueForKey:@"RightMenu"] boolValue]) {
			// If not, allocate one and add it.
			UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
            
            UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [menuButton setImage:imageMenu forState:UIControlStateNormal];
            menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
            [menuButton addTarget:controller action:@selector(revealRightToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
		}
	}
}

- (void)arrangeCollectionView {
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.collectionFeature.collectionViewLayout;
//    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
//        flowLayout.scrollDirection =  UICollectionViewScrollDirectionVertical;
//    } else {
//        flowLayout.scrollDirection =  UICollectionViewScrollDirectionHorizontal;
//    }
    
    self.collectionFeature.collectionViewLayout = flowLayout;
    [self.collectionFeature reloadData];
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
     NSLog(@"will-------%u",toInterfaceOrientation);
    [self arrangeCollectionView];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    NSLog(@"-------%u",interfaceOrientation);
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
//        return (interfaceOrientation == UIInterfaceOrientationPortrait);
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
    }
}



#pragma mark – UICollectionViewDelegateFlowLayout

//// 1
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//	CGSize retval = CGSizeMake(152, 140);
//	return retval;
//}
#pragma mark - UICollectionView Datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    //    return [Repository dataIPad].count;
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    //    return [[Repository dataIPad][section][@"rows"] count];

    if (self.collectionFeature==nil) {
        return 0;
    }
    return feature.key.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = feature.key[indexPath.row];
    if (!feature.key[indexPath.row])
    {
        UICollectionViewCell *cell;
        cell = [cv dequeueReusableCellWithReuseIdentifier: @"MenutitleCell" forIndexPath:indexPath];
        return cell;
    }
    else
    {
    FeatureCell *cell;
    cell = [cv dequeueReusableCellWithReuseIdentifier: @"featureCell" forIndexPath:indexPath];
    cell.lblTitle.text = [[feature.data valueForKey:key] valueForKey:@"name"];
    cell.lblTitle.font = [DataManager fontSize:@"head"];
    [cell.imageView setImageWithURL:[NSURL URLWithString:[[feature.data valueForKey:key] valueForKey:@"pic"]] placeholderImage:[UIImage imageWithData:[image valueForKey:[[feature.data valueForKey:key] valueForKey:@"pic"]]]];

    cell.feature = key;
        return cell;
    }
    
    
}
- (UICollectionReusableView *)collectionView: (UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    FeatureBanner *catBanner = [[FeatureBanner alloc] init];
    catBanner= [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader                                                       withReuseIdentifier:@"Banner1" forIndexPath:indexPath];
    NSDictionary *banner = [PLIST readFeature:@"feature"];
    NSString *device = ([DataManager isIpad])?@"ipad":@"iphone";
   

    [catBanner.imageView setImageWithURL:[NSURL URLWithString:banner[@"banner"][@"image"][device]] placeholderImage:[UIImage imageWithData:image[banner[@"banner"][@"image"][device]]]];
    return catBanner;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(FeatureCell*)sender
{
    if ([[segue identifier] isEqualToString:@"Feature"]) {
        CategoryViewController *vc = [segue destinationViewController];
        vc.infofid = sender.feature;
        NSLog(@"sender.feature %@",sender.feature);
        
    }
}


@end





