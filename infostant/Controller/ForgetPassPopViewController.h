//
//  ForgetPassPopViewController.h
//  infostant
//
//  Created by NINE on 1/21/56 BE.
//  Copyright (c) 2556 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgetPassPopViewController : UIViewController<UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnClose;
@property (strong, nonatomic) IBOutlet UITextField *txtForgetMail;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end
