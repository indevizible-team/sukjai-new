//
//  LandingNewViewController.m
//  www
//
//  Created by indevizible on 2/28/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//
#define qrSize @"500x500"
#import "LandingNewViewController.h"
#import "DisplayMap.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "LandingHeadCell.h" //
#import "DataManager.h"
#import "LandingBodyCell.h"//
#import "DisplayMap.h"
#import "ImageSlideViewController.h"
#import "MapViewController.h"
#import "MsgDetailViewController.h"
#import "MainViewController.h"
#import "MenuViewController.h"
#import "LoginNavController.h"
#import "TTOpenInAppActivity.h"
#import "User.h"
#import "FollowDetailViewController.h"
#import "APActivityProvider.h"
#import "SLComposeActivity.h"
#import "QRCodeActivity.h"
#import "BroadcastActivity.h"

#import "DMPhotoAlbumViewController.h"
typedef enum{
    LandingActionTypeLike,
    LandingActionTypeFollow,
    LandingActionTypeVote
} LandingAction;

typedef enum{
    ActionSheetSeeMore,
    ActionSheetAddToMyTrip
} ActionSheet;
@interface LandingNewViewController ()<LoginDelegate,UIAlertViewDelegate,UIActionSheetDelegate,DMPhotoAlbumDataSource,DMPhotoAlbumDelegate>{
     Product *thisProduct;
    AppDelegate *appDelegate;
    NSMutableDictionary *tmpHeight;
    NSMutableArray *imageSliderData;
}
@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;
@property (nonatomic, strong) UIPopoverController *activityPopoverController;
@end

@implementation LandingNewViewController
@synthesize feature;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    tmpHeight = [NSMutableDictionary new];
    appDelegate = [UIApplication sharedApplication].delegate;
    if(![[[appDelegate.infoDictionary objectForKey:@"menu"] objectForKey:(([DataManager isIpad])?@"ipad":@"iphone")][0] containsObject:@"mytrip"]){
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    self.landing = [self.product accessKey:self.accessKey];
    
    thisProduct = [[Product alloc] initWithDictionary:[self.product.data valueForKey:self.accessKey]];
    
    normalFont = [DataManager fontSize:@"small"];
    landingCellList = @[@"landingImg",@"landingGallery",@"landingVdo",@"landingMap",@"landingText",@"landingVote",@"landingVote",@"landingVote"];
    if (self.product.parent.name != nil) {
        feature = self.product.parent.name;
    }
    
    NSLog(@"product - %@",self.product.data[self.accessKey]);
    headerPack = [NSMutableArray new];
    landingPack = [NSMutableArray new];
    if (thisProduct.bannerlanding) {
        [headerPack addObject:@"BannerCell"];
    }
    [headerPack addObject:@"TitleCell"];
   
    if (thisProduct.description) {
        [headerPack addObject:@"TextCell"];
    }
    if (thisProduct.regularPrice) {
        [headerPack addObject:@"PriceCell"];
    }
    if (thisProduct.address) {
        [headerPack addObject:@"ContactCell"];
    }
    if (thisProduct.startDate) {
        [headerPack addObject:@"DateCell"];
    }
    
    txtLikeCount = thisProduct.likeCount;
    txtFollowCount = thisProduct.followCont;
    for (NSString* infotidString in self.landing.key) {
        NSInteger infotid = [[[self.landing.data valueForKey:infotidString] valueForKey:@"infotid"] integerValue];
        [landingPack addObject:landingCellList[infotid-1]];
    }
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}
- (void)viewDidAppear:(BOOL)animated
{
    
	[super viewWillAppear:animated];
    [SVProgressHUD dismiss];
    UINavigationController *nav = self.navigationController;
    UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			self.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
		}
	}
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return headerPack.count;
    }else if (section == 2)
    {
        return 1;
    }
    return self.landing.key.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSString *groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
    [formatter setGroupingSeparator:groupingSeparator];
    [formatter setGroupingSize:3];
    [formatter setAlwaysShowsDecimalSeparator:NO];
    [formatter setUsesGroupingSeparator:YES];
    [formatter setCurrencySymbol:@"฿"];
    if (indexPath.section!=1) {
        LandingHeadCell *cell;
        if (indexPath.section == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:headerPack[indexPath.row] forIndexPath:indexPath];
        }else{
            cell= [tableView dequeueReusableCellWithIdentifier:@"FooterCell" forIndexPath:indexPath];
        }
        
        cell.title.text         = thisProduct.title;
        cell.title.font = [DataManager fontSize:@"large"];
        
//        cell.description.text   = thisProduct.description;
//        cell.description.font = [DataManager fontSize:@"small"];
        [cell.description loadHTMLString:[DataManager webViewString:thisProduct.description font:[UIFont fontWithName:@"Helvetica" size:14]] baseURL:nil];
        
        cell.address.text       = thisProduct.address;
        cell.address.font = [DataManager fontSize:@"small"];
        
        cell.condition.text     = thisProduct.condition;
        cell.condition.font = [DataManager fontSize:@"small"];
        
        cell.email.text         = thisProduct.email;
        cell.email.font = [DataManager fontSize:@"small"];
        
        cell.tel.text           = thisProduct.phone;
        cell.tel.font = [DataManager fontSize:@"small"];
        
        cell.dateBegin.text     = thisProduct.startDate;
        cell.dateBegin.font = [DataManager fontSize:@"small"];
        
        cell.dateEnd.text       = thisProduct.endDate;
        cell.dateEnd.font = [DataManager fontSize:@"small"];
        
        cell.username.text      = thisProduct.displayName;
        cell.username.font = [DataManager fontSize:@"smalldes"];
        
        cell.date.text          =  thisProduct.date;
        cell.date.font = [DataManager fontSize:@"smalldes"];
        
        NSString *updateText=(thisProduct.updateDate)? [NSString stringWithFormat:@"  |  Updated %@ ",thisProduct.updateDate]:@"";
        cell.date.text          = [NSString stringWithFormat:@"By %@%@",thisProduct.displayName,updateText];
        cell.date.font = [DataManager fontSize:@"smalldes"];
        
        [cell.imageBanner setImageWithURL:thisProduct.bannerlanding.image];
        
        cell.likeCount.text = txtLikeCount;
        cell.likeCount.font = [DataManager fontSize:@"small"];
        cell.folCount.text = txtFollowCount;
        cell.folCount.font = [DataManager fontSize:@"small"];
        
        
        [cell.likeBtn setTitle:[NSString stringWithFormat:@" %@", txtLikeCount] forState:UIControlStateNormal];
        [cell.followBtn setTitle:[NSString stringWithFormat:@" %@", txtFollowCount] forState:UIControlStateNormal];
        
        
        cell.regularPrice.hidden = [thisProduct.regularPrice isEqualToString:@"0"];
        
        cell.salePrice.text     = [NSString stringWithFormat:@"Now: %@",[formatter stringFromNumber:[NSNumber numberWithFloat:[thisProduct.salePrice floatValue]]]];
        cell.salePrice.font = [DataManager fontSize:@"small"];
        
        cell.salePrice.hidden =  [thisProduct.salePrice isEqualToString:thisProduct.regularPrice];

        cell.regularPrice.text  =[NSString stringWithFormat:@"Regular: %@",[formatter stringFromNumber:[NSNumber numberWithFloat:[thisProduct.regularPrice floatValue]]]] ;
        cell.regularPrice.font = [DataManager fontSize:@"small"];
        if ([[User currentUser].mid isEqualToString:thisProduct.mid]) {
            cell.megBtn.enabled = cell.followBtn.enabled = NO;
            
        }else{
            
            cell.megBtn.enabled = cell.followBtn.enabled = YES;
        }
        return cell;
    }
    LandingBodyCell *cell;
    NSMutableDictionary *eachLanding = [self.landing.data valueForKey:self.landing.key[indexPath.row]];
    NSString *infotidString = [eachLanding valueForKey:@"infotid"];
    NSInteger infotid = [infotidString integerValue];
    NSString *lat = [eachLanding valueForKey:@"lat"];
    NSString *lng = [eachLanding valueForKey:@"lng"];
    cell = [tableView dequeueReusableCellWithIdentifier:landingCellList[infotid-1] forIndexPath:indexPath];
    imgGallery = [eachLanding valueForKey:@"imggallery"];
    switch (infotid) {
        case 1:
            [cell.imageView setImageWithURL:[NSURL URLWithString:[eachLanding valueForKey:@"img"]]];
            break;
        case 2:
            
            if (imgGallery.count>0) {
                [cell.subImage1 setImageWithURL:[NSURL URLWithString:[imgGallery objectAtIndex:0]]];
            }
            if (imgGallery.count>1) {
                [cell.subImage2 setImageWithURL:[NSURL URLWithString:[imgGallery objectAtIndex:1]]];
            }
            if (imgGallery.count>2) {
                [cell.subImage3 setImageWithURL:[NSURL URLWithString:[imgGallery objectAtIndex:2]]];
            }
            if (imgGallery.count>3) {
                [cell.subImage4 setImageWithURL:[NSURL URLWithString:[imgGallery objectAtIndex:3]]];
            }
            
            break;
        case 3:
            [cell.imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://img.youtube.com/vi/%@/0.jpg",[eachLanding valueForKey:@"embed"]]]];
            break;
        case 4:
            [cell.mapView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/staticmap?center=%@,%@&zoom=15&size=600x504&maptype=roadmap&sensor=false",lat,lng]]];
            break;
        case 5:
           
            NSLog(@"setting");
//            [cell.description setText:[eachLanding valueForKey:@"description"]];
//            cell.description.font = [UIFont systemFontOfSize:14.0];
//            [cell.description sizeToFit];
            NSString* plainContent =[eachLanding valueForKey:@"description"];
            NSString* htmlContentString = [NSString stringWithFormat:
                                        @"<html><style type=\"text/css\"> body { font-family:Helvetica; font-size:14;}</style>"
"<body><p>%@</p></body></html>", plainContent];
            [cell.webView loadHTMLString:htmlContentString baseURL:nil];
            break;
//        default:
//            break;
    }
    if ([[eachLanding valueForKey:@"detail"] length]) {
        cell.lblCaption.hidden = cell.caption.hidden = cell.bgCaption.hidden = NO;
        cell.lblCaption.text =
        cell.caption.text = [NSString stringWithFormat:@"  %@", [eachLanding valueForKey:@"detail"]];
        cell.caption.font = [DataManager fontSize:@"smalldes"];
    }else{
        cell.lblCaption.hidden = cell.caption.hidden = cell.bgCaption.hidden = YES;
    }
    return cell;

}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (indexPath.section == 0) {
        if ([headerPack[indexPath.row] isEqualToString:@"TitleCell"]) {
            FollowDetailViewController *vc =(FollowDetailViewController *) [DataManager controllerByID:@"ProfileDisplay"];
            vc.friendid = thisProduct.mid;
            [self.navigationController pushViewController:vc animated:YES ];
        }
        if ([headerPack[indexPath.row] isEqualToString:@"BannerCell"]) {
            WebViewController *wv = [WebViewController webViewWithURLString:thisProduct.bannerlanding.link.absoluteString];
            [self.navigationController pushViewController:wv animated:YES];
        }
    }
    if (indexPath.section == 1) {
        UIStoryboard *storyboard = [DataManager getStoryboard];
        NSInteger infotid = [[[self.landing.data valueForKey:self.landing.key[indexPath.row]] valueForKey:@"infotid"] integerValue];
        
        NSDictionary *tapData = [self.landing.data valueForKey:self.landing.key[indexPath.row]];
        if (infotid==1) {
           
            imageSliderData = [
                               @[@{@"caption": tapData[@"detail"],@"image":tapData[@"img"]}]
                               mutableCopy];
            
             UINavigationController *vc = [DMPhotoAlbumViewController viewWithDelegate:self];
            [self presentViewController:vc animated:YES completion:nil];
            
//            ImageSlideViewController *iv = [storyboard instantiateViewControllerWithIdentifier:@"LandingE"];
//            NSArray *img =
//            [NSArray arrayWithObjects:
//             [NSURL URLWithString:[tapData valueForKey:@"img"]],
//             nil];
//            iv.imageList = img;
//            iv.txtDescription = [tapData valueForKey:@"detail"];
//            [self presentViewController:iv animated:YES completion:nil];
        }else if (infotid==2) {
//            ImageSlideViewController *iv = [storyboard instantiateViewControllerWithIdentifier:@"LandingE"];
//            NSMutableArray *img = [NSMutableArray new];
            imageSliderData = [NSMutableArray new];
            for (NSString *imgUrl in [tapData valueForKey:@"imggallery"]) {
//                [img addObject:[NSURL URLWithString:imgUrl]];
                [imageSliderData addObject:@{@"caption":[tapData valueForKey:@"detail"],@"image":imgUrl}];
            }
//            DMPhotoAlbumViewController *vc = [[DMPhotoAlbumViewController alloc] initWithDelegate:self];
            UINavigationController *vc = [DMPhotoAlbumViewController viewWithDelegate:self];
//            [self.navigationController pushViewController:vc animated:YES];
            [self presentViewController:vc animated:YES completion:nil];
//            iv.imageList = img;
//            
//            iv.txtDescription = [tapData valueForKey:@"detail"];
//            [self presentViewController:iv animated:YES completion:nil];
        }else if (infotid ==3){
            [HCYoutubeParser h264videosWithYoutubeID:tapData[@"embed"] completeBlock:^(NSDictionary *videoDictionary, NSError *error) {
                NSURL *youtubeLink = [NSURL URLWithString:[videoDictionary valueForKey:@"medium"]];
                MPMoviePlayerViewController *mp = [[MPMoviePlayerViewController alloc] initWithContentURL:youtubeLink];
                [self presentMoviePlayerViewControllerAnimated:mp];
            }];
        }else if (infotid == 4){
            MapViewController *mv = [storyboard instantiateViewControllerWithIdentifier:@"LandingMap"];
            mv.lat = [[tapData valueForKey:@"lat"] floatValue];
            mv.lng = [[tapData valueForKey:@"lng"] floatValue];
            mv.placeName = thisProduct.title;
            [self.navigationController pushViewController:mv animated:YES];
        }
    }
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
    }
}
#pragma mark - Login
-(void)didLogin:(LoginNavController *)vc{
    MainViewController *mainController = (MainViewController *)self.navigationController.parentViewController;
    
    [mainController.sidebarViewController reloadUser];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat width=[UIScreen mainScreen].bounds.size.width;
    CGFloat height=0.0f;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        if (indexPath.section == 0) {
            NSString *cellName = headerPack[indexPath.row];
            if ([cellName isEqualToString:@"TitleCell"]) {
                if (thisProduct.title.length > 1) {
                    height = 65+[DataManager textHeight:thisProduct.title fromWidth:width andFont:[DataManager fontSize:@"head"]];
                }else{
                    height = 30;
                }
                NSLog(@"len = %i",thisProduct.title.length);
                
            }else if ([cellName isEqualToString:@"TextCell"]){
                if ([thisProduct.description length]>1) {
                    UIWebView *tmpWebView = [[UIWebView alloc] initWithFrame:self.view.bounds];
                    [tmpWebView loadHTMLString:[DataManager webViewString:thisProduct.description font:[UIFont fontWithName:@"Helvetica" size:14]] baseURL:nil];
                    
//                    height = [DataManager textHeight:thisProduct.description fromWidth:width andFont:[DataManager fontSize:@"small"]]
//                    + [DataManager textHeight:thisProduct.description fromWidth:width andFont:[UIFont fontWithName:@"HelveticaNeue" size:1.0]];
                    height = tmpWebView.frame.size.height;
                }
            }else if ([cellName isEqualToString:@"ContactCell"]){
                height = 155.0f;
            }else if ([cellName isEqualToString:@"DateCell"]){
                height =40.0f;
            }else if ([cellName isEqualToString:@"PriceCell"]){
                height =40.0f;
            }
        }else if(indexPath.section ==2){
            height = 76.0;
        }else{
            NSDictionary *eachData = [self.landing.data valueForKey:[self.landing.key objectAtIndex:indexPath.row]];
            if ([[eachData valueForKey:@"infotid"] integerValue]==5) {
                UIWebView *tmpWebView = [[UIWebView alloc] initWithFrame:self.view.bounds];
//                [self.view addSubview:tmpWebView];
                NSString* plainContent =[eachData valueForKey:@"description"];
                NSString* htmlContentString = [NSString stringWithFormat:
                                               @"<html>" "<style type=\"text/css\">"
                                               "body { font-family:Helvetica; font-size:14;}"
                                               "</style>"
                                               "<body>"
                                               "<p>%@</p>"
                                               "</body></html>", plainContent];
                [tmpWebView loadHTMLString:htmlContentString baseURL:nil];
//                height =
//                  [DataManager textHeight:[eachData valueForKey:@"description"] fromWidth:width andFont:[DataManager fontSize:@"small"]]
//                + [DataManager textHeight:[eachData valueForKey:@"description"] fromWidth:width andFont:[UIFont fontWithName:@"HelveticaNeue" size:0.5f]];
                height = tmpWebView.frame.size.height;
//                [tmpWebView removeFromSuperview];
            }else if([[eachData valueForKey:@"infotid"] integerValue]==1){
                float _w = [[eachData valueForKey:@"width"] floatValue];
                float _h = [[eachData valueForKey:@"height"] floatValue];
                float ratio = _w/_h;
                
                height = width/ratio;
                
            }else{
                int infotid =[[eachData valueForKey:@"infotid"] integerValue];
                width = 768.0f;
                if (infotid==2) {
                    height = 580.0f;
                }
                if (infotid==3) {
                    height = 600.0f;
                }
                if (infotid==4) {
                    height = 600.0f + [DataManager textHeight:[eachData valueForKey:@"detail"] fromWidth:width andFont:[DataManager fontSize:@"small"]];
                }
                if (infotid == 7) {
                    height = 76.0f;
                }
                
            }
        }
    }
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
    {
        height = 0;
        if (indexPath.section == 0) {
            NSString *cellName = headerPack[indexPath.row];
            if ([cellName isEqualToString:@"TitleCell"]) {
                if (thisProduct.title.length >1) {
                    height = 20 + [DataManager textHeight:thisProduct.title fromWidth:width andFont:[DataManager fontSize:@"head"]];
                }else{
                    height = 60;
                }
                
            }
            else if ([cellName isEqualToString:@"TextCell"]){
                if ([thisProduct.description length]>1) {
                    height = [DataManager textHeight:thisProduct.description fromWidth:width andFont:[DataManager fontSize:@"small"]]+30;
                }
                
            }
            else if ([cellName isEqualToString:@"ContactCell"]){
                height = 135.0f;
            }else if ([cellName isEqualToString:@"DateCell"]){
                height =35.0f;
            }else if ([cellName isEqualToString:@"PriceCell"]){
                height =35.0f;
            }else if([cellName isEqualToString:@"BannerCell"]){
                float _w = thisProduct.bannerlanding.width;
                float _h = thisProduct.bannerlanding.height;
                if (_h == 0.0) {
                    _h = 100000000000.0f;
                }
                float ratio = _w/_h;
                height = width/ratio;
            }
        }else if(indexPath.section ==2){
            height = 65.0;
        }else{
            NSDictionary *eachData = [self.landing.data valueForKey:[self.landing.key objectAtIndex:indexPath.row]];
            if ([[eachData valueForKey:@"infotid"] integerValue]==5) {
                height =
                [DataManager textHeight:[eachData valueForKey:@"description"] fromWidth:width andFont:[DataManager fontSize:@"small"]]+
                [DataManager textHeight:thisProduct.description fromWidth:width andFont:[UIFont fontWithName:@"HelveticaNeue" size:5.0f]];
            }else if([[eachData valueForKey:@"infotid"] integerValue]==1){
                float _w = [[eachData valueForKey:@"width"] floatValue];
                float _h = [[eachData valueForKey:@"height"] floatValue];
                float ratio = _w/_h;
                height = width/ratio;
            }else{
                int infotid =[[eachData valueForKey:@"infotid"] integerValue];
                float _w,_h,ratio;
                switch (infotid) {
                    case 1:
                         _w = [[eachData valueForKey:@"width"] floatValue];
                         _h = [[eachData valueForKey:@"height"] floatValue];
                        ratio = _w/_h;
                        height = width/ratio;
                        break;
                    case 2:
                        height = 260.0f;
                        break;
                    case 3:
                        height = 248.0f;
                        break;
                    case 4:
                        height = 225.0f + [DataManager textHeight:[eachData valueForKey:@"detail"] fromWidth:width andFont:[DataManager fontSize:@"small"]];
                        break;
                    case 7:
                        height = 50.0f;
                        break;
                        
                    default:
                        height = 0.0f;
                        break;
                }

            }
            
        }
        
    }
	return height;
}

#pragma mark - accessory
- (IBAction)like:(id)sender {
    NSMutableDictionary *param = [DataManager param];
    User *user = [User currentUser];
    [param setValue:user.oauth_login forKey:@"oauth_login"];
    [param setValue:thisProduct.productid forKey:@"productid"];
    [param setValue:feature forKey:@"infofid"];
    
    NSLog(@"param -- %@",param);
    NSURLRequest *request = [[DataManager client] requestWithMethod:@"POST" path:@"likeProduct" parameters:param];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"-+-%@",JSON);
        if ([[JSON valueForKey:@"error"] isEqualToNumber:@1]) {
            [self showLoginView];
        }else{
            txtLikeCount = [[JSON valueForKey:@"likecount"] stringValue];
            if ([[JSON valueForKey:@"like"] isEqualToNumber:@1]) {
                txtLike = @" Unlike";
            }else{
                txtLike = @" Like";
            }
            NSLog(@"Text Like");
        }
        [self.tableView reloadData];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoAppName", @"")  message:[NSString stringWithFormat:@"%@",error] delegate:nil cancelButtonTitle:NSLocalizedString(@"infoOK", @"")otherButtonTitles: nil];
        [alert show];
    }];
    [operation start];
}

- (IBAction)follow:(id)sender {
    NSMutableDictionary *param = [DataManager param];
    User *user = [User currentUser];
    [param setValue:user.oauth_login forKey:@"oauth_login"];
    [param setValue:thisProduct.mid forKey:@"friendid"];
    NSURLRequest *request = [[DataManager client] requestWithMethod:@"POST" path:@"followFriend" parameters:param];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"%@",JSON);
        if ([[JSON valueForKey:@"error"] isEqualToNumber:@1]) {
            LoginNavController *nvc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginNav"];
            nvc.ldelegate = self;
            [self presentViewController:nvc animated:YES completion:nil];
        }else{
            txtFollowCount = [[JSON valueForKey:@"followcount"] stringValue];
            if ([[JSON valueForKey:@"follow"] isEqualToNumber:@1]) {
                txtFollow = @" Unfollow";
            }else{
                txtFollow = @" Follow";
            }
        }
        [self.tableView reloadData];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoAppName", @"")  message:[NSString stringWithFormat:@"%@",error] delegate:nil cancelButtonTitle:NSLocalizedString(@"infoOK", @"") otherButtonTitles: nil];
        [alert show];
        NSLog(@"%@",JSON);
    }];
    [operation start];
}

- (IBAction)message:(id)sender {
    NSMutableDictionary *param = [DataManager param];
    NSMutableDictionary *user = [PLIST readFeature:@"user"];
    [param setValue:[user valueForKey:@"oauth_login"] forKey:@"oauth_login"];
    [param setValue:thisProduct.mid forKey:@"friendid"];
    NSURLRequest *request = [[DataManager client] requestWithMethod:@"POST" path:@"checkFollow" parameters:param];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"%@",JSON);
        if ([[JSON valueForKey:@"follow"] isEqualToNumber:@1]) {
            MsgDetailViewController *vc = [[DataManager getStoryboard] instantiateViewControllerWithIdentifier:@"MsgDetailViewController"];
            vc.friendid = thisProduct.mid;
            [self.navigationController pushViewController:vc animated:YES];
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoAppName", @"") message:NSLocalizedString(@"infoMsgFollowBefore", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"infoOK", @"") otherButtonTitles: nil];
            [alert show];
            
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoAppName", @"") message:[NSString stringWithFormat:@"%@",error] delegate:nil cancelButtonTitle:NSLocalizedString(@"infoOK", @"") otherButtonTitles: nil];
        [alert show];
    }];
    [operation start];
    //    MsgDetailViewController *vc = (MsgDetailViewController *)[segue destinationViewController];
    //    vc.friendid = sender.friendid;
}
- (IBAction)share:(id)sender {

    NSString *qrURLString = [NSString stringWithFormat:@"http://test3.infonities.com/ajax/rediectparameter/%@/%@?infoid=%@",feature,thisProduct.productid,[[DataManager param] valueForKey:@"infoid"]];
    [self presentViewController:[DataManager activityViewControllerWithArray:@[thisProduct.title,[NSURL URLWithString:qrURLString]]] animated:YES completion:nil];


        
}
-(void)showLoginView{
    MainViewController *mainController = (MainViewController *)self.navigationController.parentViewController;
    [mainController.sidebarViewController reloadUser];
    LoginNavController *nvc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginNav"];
    nvc.ldelegate = self;
    [self presentViewController:nvc animated:YES completion:nil];
}
#pragma mark - tap action
-(void)landingAction:(LandingAction)action{
    if ([User currentUser]==nil) {
        [self showLoginView];
    }else{
        switch (action) {
            case LandingActionTypeVote:
                [self sendVote];
                break;
            default:
                break;
        }
    }
    
}
- (IBAction)actionVote:(id)sender {
    [self landingAction:LandingActionTypeVote];
}
- (IBAction)actionAddToMyTrip:(UIBarButtonItem *)sender {
    UIActionSheet *myTripOption = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Create New",@"Add to exist Trip", nil];
    myTripOption.tag = ActionSheetAddToMyTrip;
    [myTripOption showInView:self.view];
}
-(void)sendVote{
    User *user = [User currentUser];
    NSMutableDictionary *param = [DataManager param];
    [param addEntriesFromDictionary:@{
     @"oauth_login":user.oauth_login,
     @"infofid":feature,
     @"productid":thisProduct.productid
     }];
    NSURLRequest *request = [[DataManager client] requestWithMethod:@"POST" path:@"saveVote" parameters:param];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"%@",JSON);
        if ([[JSON objectForKey:@"error"] isKindOfClass:[NSNumber class]]) {
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"infoSuccess", @"")];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoError", @"") message:[JSON valueForKey:@"error"] delegate:nil cancelButtonTitle:NSLocalizedString(@"infoOK", @"") otherButtonTitles: nil];
            [alert show];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoError", @"") message:NSLocalizedString(@"infoNoInternet", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"infoOK", @"") otherButtonTitles: nil];
        [alert show];
    }];
    [operation start];
}
#pragma mark - DMPhotoAlbum dataSource
/////////////////////////////////////////////////////
-(NSUInteger)numberOfPhotoAlbum:(DMPhotoAlbumViewController *)photoAlbum{
    return imageSliderData.count;
}

-(NSString *)photoAlbum:(DMPhotoAlbumViewController *)photoAlbum captionForIndex:(NSUInteger)index{
    return imageSliderData[index][@"caption"];
}

-(NSString *)photoAlbum:(DMPhotoAlbumViewController *)photoAlbum imageLinkForIndex:(NSUInteger)index{
    return imageSliderData[index][@"image"];
}

-(void)didTaptoAlbumDoneButton:(DMPhotoAlbumViewController *)photoAlbum{
    [photoAlbum dismissViewControllerAnimated:YES completion:nil];
}
@end
