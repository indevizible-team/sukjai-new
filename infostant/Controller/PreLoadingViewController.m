//
//  PreLoadingViewController.m
//  9Ent
//
//  Created by indevizible on 2/8/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "PreLoadingViewController.h"
#import "DataManager.h"
#import "Feature.h"
#import "AppDelegate.h"
@interface PreLoadingViewController ()<UIScrollViewDelegate>{
    UIRefreshControl *refreshControl;
}

@end

@implementation PreLoadingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{

	// Do any additional setup after loading the view.
    [super viewDidLoad];
  
    _scrollView.contentSize = self.imageView.bounds.size;
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing: ) forControlEvents:UIControlEventValueChanged];
    [self.scrollView addSubview:refreshControl];
    UIImage *spalshImage = [UIImage imageNamed:((IS_IPHONE_5)?@"Portrait-568h":@"Portrait")];
    [self.imageView setImage:spalshImage];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.btnTryAgain.titleLabel setText:NSLocalizedString(@"infoTryAgain", @"")];
    [self.textError setText:NSLocalizedString(@"infoNoInternet", @"")];
    [self loadFeature];
   
}
- (void)dropViewDidBeginRefreshing:(UIRefreshControl *)refreshControl
{
    [[[DataManager client] operationQueue] cancelAllOperations];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.textError.hidden = self.btnTryAgain.hidden = YES;
    [self loadFeature];
}
-(void)loadFeature{
    AFHTTPClient *client = [DataManager client];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:@"getAllFeatureBanner" parameters:[DataManager param]];
    NSString *device = ([DataManager isIpad])?@"ipad":@"iphone";

    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {

        DataController *feature = [[DataController alloc] initWithDictionary: [PLIST writeFeature:@"feature" withData:JSON]];
        NSMutableDictionary *imageData = [PLIST readFeature:@"image"];
        for (NSString *key in feature.key) {
            Feature *f = [Feature featureWithData:feature.data[key]];
            
            if ([imageData valueForKey:f.pic]==nil) {
                AFImageRequestOperation *imageOperation = [AFImageRequestOperation imageRequestOperationWithRequest:[NSURLRequest requestWithURL:f.picURL] imageProcessingBlock:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                    NSMutableDictionary *tmpImage = [PLIST readFeature:@"image"];
                    [tmpImage setObject:UIImageJPEGRepresentation(image, 1.0) forKey:f.pic];
                    [PLIST writeFeature:@"image" withData:tmpImage];
                } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                    
                }];
                [imageOperation start];
            }
        }
        if ([imageData valueForKey:feature.data[@"banner"][@"image"][device]]==nil) {
            AFImageRequestOperation *imageOperation = [AFImageRequestOperation imageRequestOperationWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:feature.data[@"banner"][@"image"][device]]] imageProcessingBlock:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                NSMutableDictionary *tmpImage = [PLIST readFeature:@"image"];
                [tmpImage setObject:UIImageJPEGRepresentation(image, 1.0) forKey:feature.data[@"banner"][@"image"][device]];
                [PLIST writeFeature:@"image" withData:tmpImage];
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                
            }];
            [imageOperation start];
        }
        [refreshControl endRefreshing];
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        UIViewController *targetVC = [DataManager controllerByID:@"MainViewController"];
        [self presentViewController:targetVC animated:NO completion:^{}];
    
    
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
       [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        self.textError.hidden = self.btnTryAgain.hidden = NO;
        [refreshControl endRefreshing];
        
    }];
    [operation start];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
	
    return UIInterfaceOrientationMaskPortrait;
    
}

@end
