//
//  MyTourViewController.m
//  www
//
//  Created by Trash on 3/22/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "MyTourViewController.h"
#define kUnlock @1
#define kLock @2
#import "MyTripListViewController.h"
#import "MyTripCell.h"
#import "MyTripMapCell.h"
#import "Trip.h"
#import "DataManager.h"
#import "QRScannerViewController.h"
#import "DMURL.h"
#import "Bitly.h"
#import "WebViewController.h"
#import "LoginNavController.h"
typedef enum{
    kActionSheetShare,
    kActionSheetEdit,
    kActionSheetAdd,
    kActionSheetFilter,
    kActionSheetSort
} kActionSheet;

typedef enum{
    kAlertConfirmDelete,
    kAlertInput,
    kAlertCall,
    kAlertAddByCode
    
}kAlert;


@interface MyTourViewController ()<UIAlertViewDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,QRScannerDelegate,LoginDelegate,UIPickerViewDataSource,UIPickerViewDelegate>{
    NSMutableArray *tagList;
    NSIndexPath *selectedIndex;
    int tableCount;
    NSMutableArray *myTripList;
    BOOL isMyTrip,canLoadMore,scanning;
    int page;
    NSURLRequest *lastRequest;
    Trip *selectedTrip;
    User *user;
    NSIndexPath *infoIndex;
    NSString *tmpCode;
//    UIPickerView *pickerView;
    NSArray *SortList;
    
}
@property (strong, nonatomic) IBOutlet UISegmentedControl *sortFilterControl;


@end

@implementation MyTourViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    user = [User currentUser];
    SortList  = @[@[@"Create Date",@"Duration",@"Price"],
                    @[@"Ascending",@"Descending"]];
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing: ) forControlEvents:UIControlEventValueChanged];
    
    [self.refreshControl beginRefreshing];
    [self dropViewDidBeginRefreshing:self.refreshControl];
}
- (IBAction)didSortFilter:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 0) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Sort by :"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Close"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:nil];
        
        [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
        
        CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
        
        UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
        pickerView.showsSelectionIndicator = YES;
        pickerView.dataSource = self;
        pickerView.delegate = self;
        [pickerView selectRow:self.sortMethod inComponent:self.sortType animated:NO];
        
        [actionSheet addSubview:pickerView];


  
        
        [actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
        NSArray *subviews = [actionSheet subviews];
        
        [[subviews objectAtIndex:1] setFrame:CGRectMake(20, 266, 280, 46)];
//        [[subviews objectAtIndex:1] setFrame:CGRectMake(20, 317, 280, 46)];
        [actionSheet setBounds:CGRectMake(0, 0, 320, 540)];
        
    }else{
        if (!tagList) {
            NSMutableDictionary *param = [DataManager param];
            AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"GET" path:@"getTagsTrips" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                DataController *tmpData = [[DataController alloc] initWithDictionary:[DataManager JSONToMutableDictionary:JSON]];
                tagList = [NSMutableArray new];
                [tagList addObject:@{@"name":@"All"}];
                for (int i = 0; i<tmpData.key.count; i++) {
                    NSDictionary *data = [tmpData dictionaryAtIndex:i];
                    [tagList addObject:@{@"name":data[@"tag"],@"id":data[@"tagid"]}];
                }
                [tagList addObject:@{@"name":NSLocalizedString(@"infoCancel", @"")}];
                [self selectTagList];
                [SVProgressHUD dismiss];
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                [SVProgressHUD showErrorWithStatus:@"Cannot Fetch data from server"];
            }];
            [operation start];
            [SVProgressHUD show];
        }else{
            [self selectTagList];
        }
    }
    [sender  setSelectedSegmentIndex:UISegmentedControlNoSegment];
}


-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return SortList[component][row];
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [SortList[component] count];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return SortList.count;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"%u - %u",row,component);
    if (!component) {
        self.sortMethod = row;
    }else{
        self.sortType = row;
    }
    
    
    [self reset];
}

-(void)selectTagList{
    UIActionSheet *sheet =[[UIActionSheet alloc] initWithTitle:@"Filter by tag" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
    for (int i=0; i<tagList.count; i++) {
        [sheet addButtonWithTitle:[NSString stringWithFormat:@"%@",tagList[i][@"name"]]];
    }
    sheet.tag = kActionSheetFilter;
    [sheet setCancelButtonIndex:tagList.count-1];
//    [sheet showFromBarButtonItem:self.sortFilterControl animated:YES];
    [sheet showInView:self.sortFilterControl.viewForBaselineLayout];

}
-(void)viewDidDisappear:(BOOL)animated{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}
- (void)dropViewDidBeginRefreshing:(UIRefreshControl *)refreshControl
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [self reset];
}

-(void)reset{
    page = 1;
    canLoadMore =YES;
    [self loadMore];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
- (IBAction)information:(UIButton *)sender {
    NSIndexPath *current = [NSIndexPath indexPathForItem:sender.tag inSection:0];
    if (infoIndex) {
        if ([current isEqual:infoIndex]) {
            infoIndex = nil;
        }else{
            __strong NSIndexPath *tmp = infoIndex;
            infoIndex = current;
            [self.tableView reloadRowsAtIndexPaths:@[tmp] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        [self.tableView reloadRowsAtIndexPaths:@[current] withRowAnimation:UITableViewRowAnimationAutomatic];
    }else{
        infoIndex = current;
        [self.tableView reloadRowsAtIndexPaths:@[current] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return myTripList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([myTripList[indexPath.row] isKindOfClass:[NSURL class]]) {
        MyTripMapCell *mapCell = [tableView dequeueReusableCellWithIdentifier:@"MyTripMapCell" forIndexPath:indexPath];
        [mapCell.webView loadRequest:[NSURLRequest requestWithURL:myTripList[indexPath.row]]];
        return mapCell;
    }
    
    static NSString *CellIdentifier = @"MyTripCell";
    MyTripCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Trip *myTrip = (Trip *)[myTripList objectAtIndex:indexPath.row];
    cell.title.text = myTrip.title;
    cell.description.text = myTrip.dateString;
    [cell.imageThumbnail setImageWithURL:myTrip.image];
    cell.priceLabel.text = myTrip.priceText;
    cell.currencyType.text = myTrip.currency;
    cell.dayCount.text = myTrip.durationText;
    cell.callBtn.enabled = (myTrip.tel != nil);
    cell.infoBtn.tag =  cell.callBtn.tag = indexPath.row;
    cell.information.text = myTrip.infoText;
    
    if (indexPath.row+3 > myTripList.count && canLoadMore) {
        [self loadMore];
    }
    
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // Return NO if you do not want the specified item to be editable.

    return NO;
    
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        selectedIndex = indexPath;
        selectedTrip = myTripList[selectedIndex.row];
        // Delete the row from the data source
        [self showDeleteSeletedIndexPathConfirm];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */


// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    [self.refreshControl endRefreshing];
}

#pragma mark - UIActionSheetDelegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ([actionSheet cancelButtonIndex]!=buttonIndex) {
        NSLog(@"click : %u",buttonIndex);
        if (actionSheet.tag == kActionSheetEdit) {
            if (buttonIndex == 0) {
                [self showDeleteSeletedIndexPathConfirm];
            }else if(buttonIndex == 1){
                UIAlertView *inputBox = [[UIAlertView alloc] initWithTitle:@"Edit Trip" message:@"Design your trip name" delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") otherButtonTitles:NSLocalizedString(@"infoOK", @"") , nil];
                [inputBox setAlertViewStyle:UIAlertViewStylePlainTextInput];
                inputBox.tag = kAlertInput;
                [inputBox show];
            }else if(buttonIndex == 2){
                UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"infoFacebook", @""),NSLocalizedString(@"infoShareInTrip", @""), nil];
                actionSheet.tag = kActionSheetShare;
                [actionSheet showInView:self.view];
            }
        }else if (actionSheet.tag == kActionSheetShare){
            
        }else if (actionSheet.tag == kActionSheetAdd){
            if (buttonIndex == 1) {
                QRScannerViewController *vc = [QRScannerViewController QRScanner:self];
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                UIAlertView *inputBox = [[UIAlertView alloc] initWithTitle:@"Add Tour" message:@"Enter Code" delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") otherButtonTitles:NSLocalizedString(@"infoOK", @"") , nil];
                [inputBox setAlertViewStyle:UIAlertViewStylePlainTextInput];
                inputBox.tag = kAlertAddByCode;
                [inputBox show];
                
            }
        }else if (actionSheet.tag == kActionSheetFilter){
            self.title = [NSString stringWithFormat:@"#%@",tagList[buttonIndex][@"name"]];
            self.tag = tagList[buttonIndex][@"id"];
            [self reset];
            
        }
    
    }
}

#pragma  mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex != [alertView cancelButtonIndex]) {
        if (alertView.tag == kAlertConfirmDelete) {
            
            NSMutableDictionary *param = [DataManager param];
            [param addEntriesFromDictionary:@{@"infomt":selectedTrip.id}];
            [param addEntriesFromDictionary:[User currentUser].oauth_login_dict];
            
            AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"POST" path:@"deleteMyTrips" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                NSLog(@"JSON : %@\n param : %@",JSON,param);
                if ([JSON[@"error"] isKindOfClass:[NSString class]]) {
                    [SVProgressHUD showErrorWithStatus:JSON[@"error"]];
                }else{
                    [myTripList removeObjectAtIndex:selectedIndex.row];
                    ;            [self.tableView deleteRowsAtIndexPaths:@[selectedIndex] withRowAnimation:UITableViewRowAnimationAutomatic];
                }
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                NSLog(@"error :%@",JSON);
            }];
            [operation start];
            
            
            
            
        }else if (alertView.tag == kAlertAddByCode){
            if ([User currentUser]) {
                [self addCodeWithCode:[alertView textFieldAtIndex:0].text];
            }else{
                [self presentViewController:[LoginNavController loginViewDelegate:self] animated:YES completion:nil];
            }
        }else if (alertView.tag == kAlertCall){
            [[UIApplication sharedApplication] openURL:selectedTrip.tel];
        }
    }
}

#pragma mark - LoginNavDelegate
-(void)didLogin:(LoginNavController *)vc{
    [vc dismissViewControllerAnimated:YES completion:^{
        if (tmpCode && [User currentUser]) {
            [self addCodeWithCode:tmpCode];
        }else{
            tmpCode = nil;
        }
        
    }];
}

#pragma mark - Custom Method
-(void)addCodeWithCode:(NSString *)code{
    [SVProgressHUD show];
    NSMutableDictionary *param = [DataManager param];
    [param setValue:code forKey:@"code"];
    [param addEntriesFromDictionary:[User currentUser].oauth_login_dict];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"POST" path:@"saveFriendTrips" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"%@->%@",param, JSON);
        if ([JSON[@"error"] isKindOfClass:[NSString class]]) {
            [SVProgressHUD showErrorWithStatus:JSON[@"error"]];
        }else{
            [SVProgressHUD showSuccessWithStatus:@"Complete"];
            page = 1;
            canLoadMore =YES;
            [self loadMore];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD showErrorWithStatus:@"Can not fetch data from server"];
    }];
    [operation start];
    tmpCode = nil;
}
-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan ) {
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
        if (indexPath ){
            
        }
    }
}

-(void)showDeleteSeletedIndexPathConfirm{
    UIAlertView *deleteConfirm = [[UIAlertView alloc] initWithTitle:@"Confirm To delete" message:@"Do you wanna delete" delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") otherButtonTitles:NSLocalizedString(@"infoConfirm", @"") , nil];
    deleteConfirm.tag = kAlertConfirmDelete;
    [deleteConfirm show];
}

- (IBAction)ClickBtn:(UIButton *)sender {
    selectedTrip = [myTripList objectAtIndex:sender.tag];
    UIAlertView *alertCall = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"infoAppName", @"") message:[NSString stringWithFormat:@"Call %@",selectedTrip.telString] delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") otherButtonTitles:@"Call", nil];
    alertCall.tag = kAlertCall;
    [alertCall show];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([myTripList[indexPath.row] isKindOfClass:[NSURL class]]) {
        return tableView.bounds.size.height;
    }
    if ([infoIndex isEqual:indexPath]) {
        return 250.0f;
    }
    return 117.0f;
}


-(void)loadMore{
    canLoadMore = NO;
        NSMutableDictionary *param = [DataManager param];
        if (self.whereIs) {
            param[@"whereis"]= self.whereIs;
            param[@"where"] = @"catid";
        }
        if (self.tag) {
            param[@"tag[]"]=self.tag;
        }
        
        [param setValue:[NSNumber numberWithInt:page++] forKey:@"page"];
        [param setValue:@[@"infomt",@"duration",@"price",@"rand()"][self.sortMethod] forKey:@"orderby"];
        [param setValue:@[@"asc",@"desc"][self.sortType] forKey:@"sortby"];
        lastRequest = [[DataManager client] requestWithMethod:@"GET" path:@"getMyTrips" parameters:param];
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:lastRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            DataController *tmp = [[DataController alloc] initWithDictionary:[DataManager JSONToMutableDictionary:JSON]];
            if ([[param valueForKey:@"page"] isEqualToNumber:@1]) {
                myTripList = [NSMutableArray new];
                infoIndex = nil;
            }
            NSLog(@"%@ res : %@",request.URL, JSON);
            if ([JSON count]) {
                for (int i=0; i<tmp.key.count; i++) {
                    [myTripList addObject:[Trip tripWithData:[tmp dictionaryAtIndex:i]]];
                }
                canLoadMore = YES;
            }else{
                canLoadMore = NO;
            }
            [self.refreshControl endRefreshing];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            [self.tableView reloadData];
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
            NSLog(@"error , %@",request.URL);
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            [self.refreshControl endRefreshing];
            canLoadMore = NO;
        }];
        [operation start];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    MyTripListViewController *vc = [segue destinationViewController];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:((MyTripCell *)sender)];
    vc.trip = myTripList[indexPath.row];
    vc.canEdit = kLock;
//    vc.title = 
    
}

@end
