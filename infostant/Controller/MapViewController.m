//
//  MapViewController.m
//  infostant
//
//  Created by indevizible on 1/6/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "MapViewController.h"
#import "DataManager.h"
@interface MapViewController ()

@end

@implementation MapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"infoMap", @"");
    self.btnGetDirection.title = NSLocalizedString(@"infoGetDirect", @"");
	// Do any additional setup after loading the view.
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [locationManager startUpdatingLocation];
    ann = [[DisplayMap alloc] init];
    MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
    
    region.center.latitude = _lat;
    region.center.longitude = _lng;
    region.span.longitudeDelta = 0.01f;
    region.span.latitudeDelta = 0.01f;
    [_mapView setRegion:region animated:YES];
    ann.coordinate = region.center;
    [_mapView addAnnotation:ann];
    
    NSLog(@"Place name - %@",self.placeName);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)GetDirection:(id)sender {
//    CLLocationCoordinate2D start = { (locationManager.location.coordinate.latitude), (locationManager.location.coordinate.longitude) };
    CLLocationCoordinate2D end = { (_lat   ), (_lng) };
    
//    NSString *googleMapsURLString = [NSString stringWithFormat:@"maps://?saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f", start.latitude, start.longitude, end.latitude, end.longitude];
//    NSURL *url = [NSURL URLWithString:googleMapsURLString];
//    
//    if ([[UIApplication sharedApplication] canOpenURL:url]) {
//        // Facebook app is installed
//        [[UIApplication sharedApplication] openURL:url];
//    }else{
//        NSString *googleMapsURLString = [NSString stringWithFormat:@"https://itunes.apple.com/en/app/google-maps/id585027354?mt=8"];
//        NSURL *url = [NSURL URLWithString:googleMapsURLString];
//         [[UIApplication sharedApplication] openURL:url];
//    }
    
    CLLocationCoordinate2D endingCoord = CLLocationCoordinate2DMake(end.latitude, end.longitude);
    MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:endingCoord addressDictionary:nil];
    MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
    if (self.placeName!=nil) {
        [endingItem setName:self.placeName];
    }
    
    
    NSMutableDictionary *launchOptions = [[NSMutableDictionary alloc] init];
    [launchOptions setObject:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
    
    [endingItem openInMapsWithLaunchOptions:launchOptions];
    
//    
//    UIStoryboard *storyboard = [DataManager getStoryboard];
//    
//    WebViewController *wv = [storyboard instantiateViewControllerWithIdentifier:@"LandingD"];
//    
//    wv.address = googleMapsURLString;
//    [self.navigationController pushViewController:wv animated:YES];
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsURLString]];
}

@end

