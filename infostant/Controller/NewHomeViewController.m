//
//  NewHomeViewController.m
//  www
//
//  Created by Trash on 3/22/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "NewHomeViewController.h"
#import "MyTourViewController.h"
#import "TourGalleryViewController.h"
#import "NewHomeCell.h"
#import "CategoryViewController.h"
@interface NewHomeViewController (){
    NSMutableArray *dataSource;
    NSArray *sectionName;
}
@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;
@end

@implementation NewHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
#warning set section name here
    sectionName = @[@"SecA",@"SecB"];
    //row 1
    UIImage *tmpImage = [UIImage imageNamed:@"FacebookSDKResources.bundle/FBProfilePictureView/images/fb_blank_profile_square.png"];
    dataSource = [NSMutableArray new];
    
    TourPackage *btn1 = [TourPackage new];
    btn1.title = @"Duration";
    btn1.type = kTourPackageTypeDuration;
    btn1.image = tmpImage;
    
    TourPackage *btn2 = [TourPackage new];
    btn2.title = @"Price";
    btn2.type = kTourPackageTypePrice;
    btn2.image = tmpImage;
    
    TourSuggestion *btn3 =[TourSuggestion new];
    btn3.title = @"Destination";
    btn3.type = kTourSuggestionTypeDestination;
    btn3.image = tmpImage;
    
    TourSuggestion *btn4 = [TourSuggestion new];
    btn4.title = @"Special";
    btn4.type = kTourSuggestionTypeSpecial;
    btn4.image = tmpImage;
    
    [dataSource addObject:@[btn1,btn2,btn3,btn4]];
    NSArray *catidList = @[@2,@4,@6,@10,@13,@14,@17];
    NSArray *catnameList = @[@"Hotel",@"Dining",@"Shopping",@"Service",@"Embassy",  @"Attraction",@"Local Event"];
  
        NSMutableArray *tmp = [NSMutableArray new];
        MagazineButton *btn = [MagazineButton new];
        btn.title = [NSString stringWithFormat:@"Magazine"];
        btn.image = tmpImage;

    NSMutableDictionary *feature = [PLIST readFeature:@"feature"];
        btn.downloadLink = [NSURL URLWithString:feature[@"shelf"][@"link"]];
        btn.appScheme = [NSURL URLWithString:[NSString stringWithFormat:@"%@://",feature[@"shelf"][@"schema"]]];
    NSLog(@"scheme -> %@",btn.appScheme);
        [tmp addObject:btn];
        for (int j=0; j<7; j++) {
            CategoryButton *btn = [CategoryButton new];
            btn.title = catnameList[j];
            btn.image = tmpImage;
            btn.catid = catidList[j];
            [tmp addObject:btn];
        }
        [dataSource addObject:tmp];
    
    [self setSlide];
}

-(void)setSlide{
    UINavigationController *nav = self.navigationController;
    UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			self.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
		}
		
		// Check if we have a revealButton already.
		if (![self.navigationItem leftBarButtonItem]&&(self.navigationController.viewControllers.count==1)) {
			// If not, allocate one and add it.
			UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
            
            UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [menuButton setImage:imageMenu forState:UIControlStateNormal];
            menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
            [menuButton addTarget:controller action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
		}
        
        if (![self.navigationItem rightBarButtonItem]&&[[[[NSBundle mainBundle] infoDictionary] valueForKey:@"RightMenu"] boolValue]) {
			// If not, allocate one and add it.
			UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
            
            UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [menuButton setImage:imageMenu forState:UIControlStateNormal];
            menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
            [menuButton addTarget:controller action:@selector(revealRightToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
		}
	}

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    HomeButton *selectedCell = dataSource[indexPath.section][indexPath.row];
    NSString *cellIdentifier;
    if ([selectedCell isKindOfClass:[TourPackage class]]) {
        cellIdentifier = @"TourPackage";
    }else if ([selectedCell isKindOfClass:[TourSuggestion class]]){
        cellIdentifier = @"TourSuggestion";
    }else if([selectedCell isKindOfClass:[MagazineButton class]]){
        cellIdentifier = @"Magazine";
    }else if([selectedCell isKindOfClass:[CategoryButton class]]){
        cellIdentifier = @"Category";
    }
    
    NewHomeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.title.text = selectedCell.title;
    [cell.title setTextAlignment:NSTextAlignmentCenter];
    [cell.imageView setImage:selectedCell.image];
    return  cell;
}
-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    NewHomeCell *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"NewHomeHeader" forIndexPath:indexPath];
    [cell.title setText:sectionName[indexPath.section]];
    return cell;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return dataSource.count;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [dataSource[section] count];
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(80, 90);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([dataSource[indexPath.section][indexPath.row] isKindOfClass:[MagazineButton class]]) {
        MagazineButton *mag = dataSource[indexPath.section][indexPath.row] ;
        if ([[UIApplication sharedApplication] canOpenURL:mag.appScheme]) {
            [[UIApplication sharedApplication] openURL:mag.appScheme];
        }else{
            [[UIApplication sharedApplication] openURL:mag.downloadLink];
        }
    }else if([dataSource[indexPath.section][indexPath.row] isKindOfClass:[CategoryButton class]]){
        
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
   
     NSIndexPath *indexPath = [self.collectionView indexPathForCell:sender];
    
    if ([segue.identifier isEqualToString:@"TourPackage"]) {
         MyTourViewController    *vc =[segue destinationViewController];
        TourPackage *tour = dataSource[indexPath.section][indexPath.row];
        if (tour.type == kTourPackageTypeDuration) {
            vc.sortMethod = SortMethodDuration;
        }else{
            vc.sortMethod = SortMethodPrice;
        }
        vc.sortType = SortTypeASD;
        
    }else if ([segue.identifier isEqualToString:@"TourSuggestion"]){
        TourGalleryViewController    *vc =[segue destinationViewController];
        vc.tour = dataSource[indexPath.section][indexPath.row];
    }else if ([segue.identifier isEqualToString:@"TOCCategory"]){
        CategoryViewController *vc = [segue destinationViewController];
        CategoryButton *tour = dataSource[indexPath.section][indexPath.row];
        vc.infofid = @"1";
        vc.selectedCat = [NSString stringWithFormat:@"%@",tour.catid];
    }
}
@end
