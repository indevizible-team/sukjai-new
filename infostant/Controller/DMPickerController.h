//
//  DMPickerController.h
//  www
//
//  Created by KEEP CALM AND CODE OK on 3/11/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMPickerController : UIViewController
@property (strong, nonatomic) IBOutlet UIDatePicker *picker;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
