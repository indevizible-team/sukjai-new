//
//  SelectFeatureViewController.h
//  infostant
//
//  Created by indevizible on 1/10/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SelectFeatureDelegate;
@interface SelectFeatureViewController : UITableViewController{
    NSArray *featureList;
}
@property (nonatomic, assign) id<SelectFeatureDelegate> delegate;
@end

@protocol SelectFeatureDelegate <NSObject>
-(NSArray*)selectFeatureList:(SelectFeatureViewController*)controller;
-(void)didSelectFeature:(SelectFeatureViewController*)controller selected:(NSInteger*)selected;
@end