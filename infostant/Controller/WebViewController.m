//
//  WebViewController.m
//  WebView
//
//  Created by Nattawut Singhchai on 12/28/12 .
//  Copyright (c) 2012 Nattawut Singhchai. All rights reserved.
//

#import "WebViewController.h"
#import "ActivitySet.h"
@interface WebViewController ()

@end

@implementation WebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.address]];
	
	[self.web loadRequest:request];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)refresh:(UIBarButtonItem *)sender {
	[self.web reload];
	
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
	[self.activity startAnimating];
	
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
	[self.activity stopAnimating];
    self.title = [self.web stringByEvaluatingJavaScriptFromString:@"document.title"];

	if ([self.web canGoBack]) {
		[self.backBtn setEnabled:YES];
	}else{
		[self.backBtn setEnabled:NO];
		
	}
	if([self.web canGoForward]){
		[self.fwdBtn setEnabled:YES];
	}else{
		[self.fwdBtn setEnabled:NO];
	}
}
- (IBAction)back:(id)sender {
	[self.web goBack];
}
- (IBAction)fwd:(id)sender {
	[self.web goForward];
}

- (IBAction)share:(id)sender {
	NSURL *theUrl = self.web.request.URL.absoluteURL;
	NSArray *activityItems = [NSArray arrayWithObjects:theUrl , nil];
	
//	UIActivityViewController *avc = [[UIActivityViewController alloc]
//									 initWithActivityItems: activityItems applicationActivities:nil];
	[self presentViewController:[DataManager activityViewControllerWithArray:activityItems] animated:YES completion:nil];
}
+(id)webViewWithURLString:(NSString *)string{
    WebViewController *vc = (WebViewController *)[DataManager controllerByID:@"LandingD"];
    vc.address = string;
    return vc;
}
@end
