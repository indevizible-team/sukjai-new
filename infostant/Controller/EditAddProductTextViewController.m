//
//  EditAddProductTextViewController.m
//  infostant
//
//  Created by indevizible on 1/24/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "EditAddProductTextViewController.h"

@interface EditAddProductTextViewController ()

@end

@implementation EditAddProductTextViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.btnCancel.title = NSLocalizedString(@"infoCancel", @"");
    self.btnClear.title = NSLocalizedString(@"infoClear", @"");
    self.btnDone.title = NSLocalizedString(@"infoDone", @"");
    [self.description setText:[self.targetData valueForKey:@"detail"]];
    [self.description becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)btnClear:(id)sender {
    [self.description setText:@""];
    [self.description becomeFirstResponder];
}
- (IBAction)btnDone:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.targetData setValue:self.description.text forKey:@"detail"];
        [self.parentTable reloadData];
    }];
}

@end
