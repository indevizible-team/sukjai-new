//
//  LikeLandingViewController.m
//  infostant
//
//  Created by INFOSTANT on 12/26/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import "LikeLandingViewController.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@interface LikeLandingViewController()
@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;

@end

@implementation LikeLandingViewController

- (void)viewDidUnload {
    
    [super viewDidUnload];
}
-(void)viewDidLoad{
    [self arrangeCollectionView];
}
- (void)viewDidAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.title = NSLocalizedString(@"Landing", @"Landing");
	
    UINavigationController *nav = self.navigationController;
    UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			self.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
		}
	}
}
- (void)arrangeCollectionView {
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.collectionLikelnd.collectionViewLayout;
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        flowLayout.scrollDirection =  UICollectionViewScrollDirectionVertical;
    } else {
        flowLayout.scrollDirection =  UICollectionViewScrollDirectionHorizontal;
    }
    
    self.collectionLikelnd.collectionViewLayout = flowLayout;
    [self.collectionLikelnd reloadData];
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self arrangeCollectionView];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
    }
}

#pragma mark – UICollectionViewDelegateFlowLayout

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	CGSize retval;
    retval = CGSizeMake(320, 263);
	return retval;
}
#pragma mark - UICollectionView Datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    //    return [Repository dataIPad].count;
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return 2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray* menuList = [[NSArray alloc] initWithObjects:
                         @"landingMain",@"landingMain", nil];
    UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier: menuList[indexPath.row] forIndexPath:indexPath];
    return cell;
}

- (UICollectionReusableView *)collectionView: (UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *reusableView = [[UICollectionReusableView alloc] init];
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        reusableView= [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader                                                       withReuseIdentifier:@"LandingHeaderCell" forIndexPath:indexPath];
        
    }
    else if([kind isEqualToString:UICollectionElementKindSectionFooter]){
        reusableView= [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter
                                                         withReuseIdentifier:@"LandingFooterCell" forIndexPath:indexPath];
        
    }
    return reusableView;
}

@end
