//
//  NotificationMsgViewController.h
//  www
//
//  Created by indevizible on 2/18/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationMsgViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *userImage;
@property (strong, nonatomic) IBOutlet UILabel *lblDisplayName;
@property (strong, nonatomic) IBOutlet UILabel *lblCreateDate;
@property (strong, nonatomic) IBOutlet UITextView *txtMessage;


@end
