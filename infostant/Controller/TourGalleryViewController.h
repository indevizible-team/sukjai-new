//
//  TourGalleryViewController.h
//  www
//
//  Created by Trash on 3/22/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TourSuggestion.h"
@interface TourGalleryViewController : UICollectionViewController
@property (nonatomic,strong) TourSuggestion *tour;
@property (nonatomic,strong) UIRefreshControl *refreshControl;
@end
