//
//  LikeListViewController.m
//  infostant
//
//  Created by INFOSTANT on 12/25/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import "LikeListViewController.h"
#import "CategoryCell.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "SelectFeatureViewController.h"
#import "WebViewController.h"
#import "ImageSlideViewController.h"
#import "LandingNewViewController.h"
@interface LikeListViewController()<SelectFeatureDelegate,LoginDelegate>

@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;

@end



@implementation LikeListViewController

@synthesize navigationBarPanGestureRecognizer,infofid;

- (void)viewDidUnload {
    
    [super viewDidUnload];
}

-(void)viewDidLoad{
//    [self arrangeCollectionView];
    canLoadMore = YES;
    page = 1;
    user = [User currentUser];
            client = [DataManager client];
            _allFeature = [[DataController alloc] initWithDictionary:[PLIST readFeature:@"feature"]];
           

    if (_feed) {
        mode = @"feed";
        
    }else{
        mode = @"like";
        infofid = infofid = [_allFeature.key objectAtIndex:0];
   
    }
    self.title = [[_allFeature.data valueForKey:infofid] valueForKey:@"name"];
     [self loadMore];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}
-(void)loadMore{
    if (canLoadMore) {
        canLoadMore = NO;
        if (page == 1) {
            [SVProgressHUD show];
        }

        
    NSMutableDictionary *param = [@{
                                  @"infofid":infofid,
                                  @"oauth_login":user.oauth_login,
                                  mode:@1,
                                  @"page":[NSNumber numberWithInteger:page++]
                                  } mutableCopy];
        [param addEntriesFromDictionary:[DataManager param]];

        NSMutableURLRequest *dataRequest = [client requestWithMethod:@"GET" path:@"getNewAllFeature" parameters:param];
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:dataRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            
                if ([JSON count]) {
                    if ([JSON valueForKey:@"expire"]!=nil) {
                        [self openLogin];
                    }else{
                        if (_like==nil) {
                            _like = [[DataController alloc] initWithDictionary:[[DataManager JSONToMutableDictionary:JSON] mutableCopy]];
                        }else{
                            [_like insertData:[[DataManager JSONToMutableDictionary:JSON] mutableCopy]];
                        }
                        canLoadMore = YES;
                    }
                    
                }else{
                    canLoadMore = NO;
                }
            NSLog(@"Finish request : %@",request.URL);
            [SVProgressHUD dismiss];
            [_collectionLike reloadData];
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
            NSLog(@"error : %@",request.URL);
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNetworkError", @"")];
        }];
        [operation start];
    }
}

-(void)openLogin{
    LoginNavController *nvc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginNav"];
    nvc.ldelegate = self;
    [self presentViewController:nvc animated:YES completion:nil];

}
-(void)didLogin:(LoginNavController *)vc{
    user = [User currentUser];
    canLoadMore = YES;
    [self loadMore];
}
- (void)viewDidAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
//	self.title = @"Shop";
	
    UINavigationController *nav = self.navigationController;
    UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			self.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
		}
		
		// Check if we have a revealButton already.
		if (![self.navigationItem leftBarButtonItem]&&(self.navigationController.viewControllers.count==1) && !self.feed) {
			// If not, allocate one and add it.
			UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
            
            UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [menuButton setImage:imageMenu forState:UIControlStateNormal];
            menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
            [menuButton addTarget:controller action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
		}
	}
}
- (void)arrangeCollectionView {
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.collectionLike.collectionViewLayout;
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        flowLayout.scrollDirection =  UICollectionViewScrollDirectionVertical;
    } else {
        flowLayout.scrollDirection =  UICollectionViewScrollDirectionHorizontal;
    }
    
    self.collectionLike.collectionViewLayout = flowLayout;
    [self.collectionLike reloadData];
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self arrangeCollectionView];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
    }
}

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	CGSize retval;
    int cellMod = indexPath.row%5;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        retval = CGSizeMake( 306, 282);
    }
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        if (cellMod>=2) {
            retval = CGSizeMake(236, 328);
        }
        if (cellMod<2) {
            retval = CGSizeMake(361, 328);
        }
    }
	return retval;
}


#pragma mark - UICollectionView Datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    if (_like!=nil) {
        if (_like.key.count == 0) {
            return 1;
        }
        return _like.key.count;
    }
    return 1;
}

- (CategoryCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (_like.key.count == 0) {
        return [cv dequeueReusableCellWithReuseIdentifier:@"NoRstCell" forIndexPath:indexPath];
    }
    NSDictionary *each = [_like dictionaryAtIndex:indexPath.row];
    CategoryCell *cell;
    int cellMod = indexPath.row%5;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        if (cellMod<2) {
            cell = [cv dequeueReusableCellWithReuseIdentifier: @"DuoCateCell" forIndexPath:indexPath];
        }
        if (cellMod>=2) {
            cell = [cv dequeueReusableCellWithReuseIdentifier: @"TripleCateCell" forIndexPath:indexPath];
        }
    }
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
    {
        if ([each valueForKey:@"banner"]!=nil) {
            cell = [cv dequeueReusableCellWithReuseIdentifier:@"AdsCell" forIndexPath:indexPath];
        }else{
            cell = (CategoryCell *)[cv dequeueReusableCellWithReuseIdentifier:@"likelistM" forIndexPath:indexPath];
        }
        
    }
    
    [cell.imageView setImageWithURL:[NSURL URLWithString:[each valueForKey:@"pic"]]];
    cell.ownerName.text = [NSString stringWithFormat:@"By %@",[each valueForKey:@"displayname"]];
    cell.description.text = [each valueForKey:@"title"];
    cell.followNo.text = [DataManager numberToStr:[[each valueForKey:@"countfollow"] intValue]];
    cell.likeNo.text = [DataManager numberToStr:[[each valueForKey:@"countlike"] intValue]];
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:cell.bounds];
    cell.layer.masksToBounds = NO;
    cell.layer.borderColor =[UIColor blackColor].CGColor;
    cell.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.layer.shadowOffset = CGSizeMake(0.0f, 0.05f);
    cell.layer.shadowOpacity = 0.5f;
    cell.layer.shadowRadius = 1.0f;
    cell.layer.shadowPath = shadowPath.CGPath;
    
    if (indexPath.row  >=  _like.key.count-5) {
        [self loadMore];
    }
    return cell;
}
- (void)collectionView:(UICollectionView *)colView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor colorWithRed:105.0f/255 green:105.0f/255 blue:105.0f/255 alpha:0.2f];
}

- (void)collectionView:(UICollectionView *)colView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = nil;
}
-(NSArray *)selectFeatureList:(SelectFeatureViewController *)controller{
    featureList = [NSMutableArray new];
    for (NSString *key in _allFeature.key) {
        [featureList addObject:[[_allFeature.data valueForKey:key] valueForKey:@"name"]];
    }
 
    return featureList;
}
-(void)didSelectFeature:(SelectFeatureViewController *)controller selected:(NSInteger *)selected{
    page =1;
    NSDictionary *data = [_allFeature dictionaryAtIndex:selected];
    infofid = [data valueForKey:@"infofid"];
    self.title = [data valueForKey:@"name"];
    NSLog(@"select %@",data);
    if (_like.key.count) {
          [_collectionLike scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
    }
    _like = nil;
    canLoadMore = YES;
    [self loadMore];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    SelectFeatureViewController *vc = [segue destinationViewController];
    vc.delegate = self;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (_like.key.count != 0) {
        NSDictionary *selectedInfofid = [_like dictionaryAtIndex:indexPath.row];
        UIStoryboard *storyboard = [DataManager getStoryboard];
        
        if (([infofid isEqualToString:@"1"] && ![[selectedInfofid valueForKey:@"data"] count] )||[[selectedInfofid valueForKey:@"banner"] integerValue]==1) {
            WebViewController *wv  = [storyboard instantiateViewControllerWithIdentifier:@"LandingD"];
            wv.address = [selectedInfofid valueForKey:@"fullshopurl"];
            wv.title = NSLocalizedString([selectedInfofid valueForKey:@"title"],[selectedCat valueForKey:@"title"]);
            [self.navigationController pushViewController:wv animated:YES];
        }else if ([infofid isEqualToString:@"3"]){
            NSMutableArray *TmpImage = [NSMutableArray new];
            for (NSString *image in [selectedInfofid valueForKey:@"pdfdata"]) {
                NSURL *_url = [NSURL URLWithString:image];
                [TmpImage addObject:_url];
            }
            ImageSlideViewController *iv = [storyboard instantiateViewControllerWithIdentifier:@"LandingE"];
            iv.imageList = TmpImage;
            
            [self presentViewController:iv animated:YES completion:nil];
        }else{
            LandingNewViewController *lv = [storyboard instantiateViewControllerWithIdentifier:@"LandingNew"];
            lv.product = _like;
            lv.accessKey = _like.key[indexPath.row];
            lv.landing = [_like accessKey:lv.accessKey];
            lv.feature = infofid;
            [self.navigationController pushViewController:lv animated:YES];
        }

    }
}
@end
