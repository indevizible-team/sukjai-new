//
//  AddContentSelectViewController.h
//  infostant
//
//  Created by indevizible on 1/22/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"

@interface AddContentSelectViewController : UIViewController{
    DataController *subCatData;
}

@property (strong, nonatomic) IBOutlet UIButton *btnCatSel;
@property (strong, nonatomic) IBOutlet UIButton *btnSubCatSel;
@property (strong, nonatomic) IBOutlet UITextField *txtTitle;
@property (strong, nonatomic) IBOutlet UITextView *txtDescription;
@property (strong, nonatomic) IBOutlet UIToolbar *doneBar;


@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *nextBtn;
@property (strong, nonatomic) NSString *infofid;
@end
