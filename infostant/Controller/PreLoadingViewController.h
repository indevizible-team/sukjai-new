//
//  PreLoadingViewController.h
//  9Ent
//
//  Created by indevizible on 2/8/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreLoadingViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *btnTryAgain;
@property (strong, nonatomic) IBOutlet UILabel *textError;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;


@end
