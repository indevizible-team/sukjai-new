//
//  HorizonFeatureCell.h
//  infostant
//
//  Created by indevizible on 1/17/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"
@interface HorizonFeatureCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *productName;
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) NSString *infofid;
@property (strong, nonatomic) NSMutableDictionary *each;
@property (strong, nonatomic) DataController *datasouce;
@end
