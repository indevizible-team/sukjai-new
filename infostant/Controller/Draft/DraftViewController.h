//
//  DraftViewController.h
//  infostant
//
//  Created by indevizible on 1/31/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DraftViewController : UITableViewController<UIActionSheetDelegate>
@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;
@end
