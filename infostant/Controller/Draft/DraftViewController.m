//
//  DraftViewController.m
//  infostant
//
//  Created by indevizible on 1/31/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//
#import "DataManager.h"
#import "DraftViewController.h"
#import "FollowDetailViewController.h"
@interface DraftViewController (){
    NSMutableArray *draft;
    NSMutableDictionary *data;
    NSIndexPath *selected;
}

@end

@implementation DraftViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"infoDraft", @"");
    data = [PLIST readFeature:@"draft"];
    if ([data count]) {
        draft = [data valueForKey:@"data"];
    }
    
    UINavigationController *nav = self.navigationController;
    UIViewController *controller = nav.parentViewController; // MainViewController : ZUUIRevealController
    if ([controller respondsToSelector:@selector(revealGesture:)] && [controller respondsToSelector:@selector(revealToggle:)])
	{
		// Check if a UIPanGestureRecognizer already sits atop our NavigationBar.
		if (![[self.navigationController.navigationBar gestureRecognizers] containsObject:self.navigationBarPanGestureRecognizer])
		{
			// If not, allocate one and add it.
			UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:controller action:@selector(revealGesture:)];
			self.navigationBarPanGestureRecognizer = panGestureRecognizer;
			
			[self.navigationController.navigationBar addGestureRecognizer:self.navigationBarPanGestureRecognizer];
		}
		
		// Check if we have a revealButton already.
		if (![self.navigationItem leftBarButtonItem]&&(self.navigationController.viewControllers.count==1)) {
			// If not, allocate one and add it.
			UIImage *imageMenu = [UIImage imageNamed:@"button-menu.png"];
            
            UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [menuButton setImage:imageMenu forState:UIControlStateNormal];
            menuButton.frame = CGRectMake(0.0, 0.0, imageMenu.size.width+10, imageMenu.size.height);
            [menuButton addTarget:controller action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
		}
	}

     self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (draft!=nil) {
        return draft.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"DraftCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = [[draft objectAtIndex:indexPath.row] valueForKey:@"title"];
    
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") destructiveButtonTitle:nil  otherButtonTitles:@"Publish", nil];
    selected = indexPath;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [action showInView:self.view];
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    [self removeDraftAtIndex:indexPath];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (!buttonIndex) {
        [self postToServer];
    }
}

-(void)removeDraftAtIndex:(NSIndexPath *)indexPath{
    [draft removeObjectAtIndex:indexPath.row];
    data = [PLIST writeFeature:@"draft" withData:data];
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(void)postToServer{
    NSDictionary *param = [draft objectAtIndex:selected.row];
    NSURLRequest *reqest  = [[DataManager client] requestWithMethod:@"POST" path:@"saveFeature" parameters:param];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:reqest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [SVProgressHUD dismiss];
        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"infoPublishStatus", @"")];
        [self removeDraftAtIndex:selected];
        [self.view setUserInteractionEnabled:YES];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNoInternet", @"")];
        [self.view setUserInteractionEnabled:YES];
        
    }];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        float percentDone = ((float)((int)totalBytesWritten) / (float)((int)totalBytesExpectedToWrite));
        [SVProgressHUD showProgress:percentDone status:NSLocalizedString(@"infoPublish", @"")];
    }];
    [self.view setUserInteractionEnabled:NO];
    [operation start];
    
}
@end
