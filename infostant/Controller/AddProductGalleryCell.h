//
//  AddProductGalleryCell.h
//  infostant
//
//  Created by indevizible on 1/24/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//
#define galleryRatio 206.0f/172.0f
#import "GKImagePicker.h"
#import <UIKit/UIKit.h>
#import "AddProductViewController.h"
@interface AddProductGalleryCell : UITableViewCell<GKImagePickerDelegate,UIActionSheetDelegate,UIPopoverControllerDelegate,UINavigationControllerDelegate>{
    BOOL didChangeImage,newMedia;
    NSUInteger currentEditingCell;
    
}
@property (nonatomic, strong) UIPopoverController *myPopoverController;
@property (nonatomic, strong) GKImagePicker *imagePicker;
- (IBAction)btn1:(id)sender;
- (IBAction)btn2:(id)sender;
- (IBAction)btn3:(id)sender;
- (IBAction)btn4:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn1Outlet;
@property (strong, nonatomic) IBOutlet UIButton *btn2Outlet;
@property (strong, nonatomic) IBOutlet UIButton *btn3Outlet;
@property (strong, nonatomic) IBOutlet UIButton *btn4Outlet;

@property (nonatomic, weak) NSMutableDictionary *targetData;
@property (nonatomic, weak) UITableView *targetTable;
@property (nonatomic, weak) AddProductViewController *parent;
@end