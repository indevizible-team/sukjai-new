//
//  MenuViewController.h
//  socioville
//
//  Created by Valentin Filip on 09.04.2012.
//  Copyright (c) 2012 App Design Vault. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuRightViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>{
}

-(void)reloadUser;
//@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIImageView *avatar;
@property (strong, nonatomic) NSArray *tableData;
//-(void)openLoginPage;
@end
