//
//  NewHomeHeader.h
//  www
//
//  Created by Trash on 3/22/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewHomeHeader : UICollectionReusableView
@property (strong, nonatomic) IBOutlet UILabel *title;


@end
