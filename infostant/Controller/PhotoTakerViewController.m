//
//  PhotoTakerViewController.m
//  AddContentProto
//
//  Created by indevizible on 1/24/13.
//  Copyright (c) 2013 indevizible. All rights reserved.
//

#import "PhotoTakerViewController.h"

@interface PhotoTakerViewController ()

@end

@implementation PhotoTakerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self btnTaken:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnDone:(id)sender {
    [self.targetSource setValue:self.imageView.image forKey:@"pic"];
    [self.targetTable reloadData];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnTaken:(id)sender {
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"infoCancel", @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"infoTakePhoto", @""),NSLocalizedString(@"infoChoosePhoto", @""), nil];
    popup.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [popup showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
	if (buttonIndex == 0) {
		if ([UIImagePickerController isSourceTypeAvailable:
			 UIImagePickerControllerSourceTypeCamera])
		{
			UIImagePickerController *imagePicker =
			[[UIImagePickerController alloc] init];
			imagePicker.delegate = self;
			imagePicker.sourceType =
			UIImagePickerControllerSourceTypeCamera;
			imagePicker.mediaTypes = [NSArray arrayWithObjects:
									  (NSString *) kUTTypeImage,
									  nil];
			imagePicker.allowsEditing = NO;
			[self presentViewController:imagePicker
                               animated:YES completion:nil];
			newMedia = YES;
		}
	}else if (buttonIndex == 1){
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            if ([self.myPopoverController isPopoverVisible]) {
                [self.myPopoverController dismissPopoverAnimated:YES];
            } else {
                if ([UIImagePickerController isSourceTypeAvailable:
                     UIImagePickerControllerSourceTypeSavedPhotosAlbum])
                {
                    UIImagePickerController *imagePicker =
                    [[UIImagePickerController alloc] init];
                    imagePicker.delegate = self;
                    imagePicker.sourceType =
                    UIImagePickerControllerSourceTypePhotoLibrary;
                    imagePicker.mediaTypes = [NSArray arrayWithObjects:
                                              (NSString *) kUTTypeImage,
                                              nil];
                    imagePicker.allowsEditing = YES;
                    
                    self.myPopoverController = [[UIPopoverController alloc]
                                                initWithContentViewController:imagePicker];
                    
                    self.myPopoverController.delegate = self;
                    
                    
                    [self.myPopoverController presentPopoverFromRect:CGRectMake(44, 20, 200, 200) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
                    
                    newMedia = NO;
                }
            }

        } else {
            if ([UIImagePickerController isSourceTypeAvailable:
                 UIImagePickerControllerSourceTypeSavedPhotosAlbum])
            {
                UIImagePickerController *imagePicker =
                [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;
                imagePicker.sourceType =
                UIImagePickerControllerSourceTypePhotoLibrary;
                imagePicker.mediaTypes = [NSArray arrayWithObjects:
                                          (NSString *) kUTTypeImage,
                                          nil];
                imagePicker.allowsEditing = NO;

                [self presentViewController:imagePicker animated:YES completion:nil];
                
                newMedia = NO;
            }

        }
    }
}

#pragma mark -
#pragma mark UIImagePickerControllerDelegate

-(void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self.myPopoverController dismissPopoverAnimated:true];
    }else{
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
	
    NSString *mediaType = [info
						   objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image = [info
						  objectForKey:UIImagePickerControllerOriginalImage];
		didChangeImage = YES;
        self.imageView.image = image;
        if (newMedia){
			[self dismissViewControllerAnimated:YES completion:nil];
            
//			[self refreshSize];
            //saveimage
            UIImageWriteToSavedPhotosAlbum(image,
										   self,
										   @selector(image:finishedSavingWithError:contextInfo:),
										   nil);
		}
    }
	
}

-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
	if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
							  initWithTitle: NSLocalizedString(@"infoTitleSaveFail", @"")
							  message: NSLocalizedString(@"infoMsgSaveFail", @"")
							  delegate: nil
							  cancelButtonTitle:NSLocalizedString(@"infoOK", @"")
							  otherButtonTitles:nil];
        [alert show];
	}
}

@end
