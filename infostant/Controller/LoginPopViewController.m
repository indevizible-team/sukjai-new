//
//  LoginPopViewController.m
//  infostant
//
//  Created by NINE on 1/21/56 BE.
//  Copyright (c) 2556 infostant. All rights reserved.
//

#import "LoginPopViewController.h"
#import "DataManager.h"
#import "MainViewController.h"
#import "FeatureViewController.h"
#define kOFFSET_FOR_KEYBOARD 216
#define kOFFSET_FOR_KEYBOARD_PAD 264

@interface LoginPopViewController ()<FBLoginViewDelegate>

@end

@implementation LoginPopViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"infoLogin", @"");
    self.txtUsername.placeholder = NSLocalizedString(@"infoUsername", @"");
    self.txtPassword.placeholder = NSLocalizedString(@"infoPassword", @"" );
    self.btnClose.title = NSLocalizedString(@"infoClose", @"");
    self.btnForgetpass.titleLabel.text = NSLocalizedString(@"infoForgotpass", @"");
    [self.scrollLogin setContentOffset:CGPointZero];

    [self.btnLogin.titleLabel setText:NSLocalizedString(@"infoLogin", @"")];
    [self.butnRegis.titleLabel setText:NSLocalizedString(@"infoRegis", @"")];
	// Do any additional setup after loading the view.
    nc = (LoginNavController *)self.navigationController;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleTap];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [SVProgressHUD dismiss];
}
- (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer {
    // single tap does nothing for now
    [_txtUsername resignFirstResponder];
    [_txtPassword resignFirstResponder];
    
    [UIView animateWithDuration:0.2
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGRect rect =_scrollLogin.frame;
                         rect.origin.y = self.view.frame.size.height-rect.size.height;
                         _scrollLogin.frame = rect;
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done");
                     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldBeginEditing");
    [UIView animateWithDuration:0.2
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGRect rect = _scrollLogin.frame;
                         if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone){
                             rect.origin.y = self.view.frame.size.height-kOFFSET_FOR_KEYBOARD-rect.size.height+105;
                         }
                         _scrollLogin.frame = rect;
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done");
                     }];
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldEndEditing");
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSLog(@"textFieldShouldReturn");
    [textField resignFirstResponder];
    [UIView animateWithDuration:0.2
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGRect rect =_scrollLogin.frame;
                         rect.origin.y = self.view.frame.size.height-rect.size.height;
                         _scrollLogin.frame = rect;
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done");
                     }];
    
    return  YES;
}



- (IBAction)fbLogin:(id)sender {
    [SVProgressHUD show];
    if ([[FBSession activeSession] isOpen]) {
        [self fetchData];
    }else{
        NSArray *permissions =
        [NSArray arrayWithObjects:@"email", nil];
        
        [FBSession openActiveSessionWithReadPermissions:permissions
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                          /* handle success + failure in block */
                                          NSLog(@"status - %@\nerror %@",session,error);
                                          if (status == FBSessionStateOpen) {
                                              
                                              NSArray *permissions2 =
                                              [NSArray arrayWithObjects:@"user_photos", @"friends_photos",@"user_about_me",@"read_stream",@"user_relationships", @"user_birthday",nil];
                                              
                                              [[FBSession activeSession] reauthorizeWithReadPermissions:permissions2
                                                                                      completionHandler:^(FBSession *session, NSError *error) {
                                                                                          /* handle success + failure in block */
                                                                                          NSLog(@"C: %@",session);
                                                                                          [self fetchData];
                                                                                          
                                                                                      }];
                                          }
                                          
                                      }];
    }
}

- (IBAction)closeBtn:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)nativeLogin:(id)sender {
 
    NSMutableDictionary *login = [@{
                            
                                  @"username":_txtUsername.text,
                                  @"password1":_txtPassword.text
                                  } mutableCopy];
    [login addEntriesFromDictionary:[DataManager param]];
    NSDictionary *device = [PLIST readFeature:@"device"];
    [login addEntriesFromDictionary:device];
    
    AFHTTPClient *client = [DataManager client];
    NSURLRequest *request = [client requestWithMethod:@"POST" path:@"loginUser" parameters:login];
    [SVProgressHUD showWithStatus:NSLocalizedString(@"infoSignStatus", @"")];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"%@",JSON);
        
        if ([[JSON valueForKey:@"error"] isKindOfClass:[NSString class]]) {
             [SVProgressHUD showErrorWithStatus:JSON[@"error"]];
        }else{
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"infoSignComplete", @"")];
          [PLIST writeFeature:@"user" withData:JSON];
            
            [nc dismissViewControllerAnimated:YES completion:^{
                [nc didLogin];
            }];
            
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNoInternet", @"")];
    }];
    [operation start];
    
}

-(void)fetchData{
    [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary *user, NSError *error) {
        if (!error) {
            [SVProgressHUD show];
            NSArray *data = @[@"name",@"first_name",@"last_name",@"id",@"email",@"username"];
            NSMutableDictionary *param = [DataManager param];
            for (NSString *value in data) {
                [param setValue:user[value] forKey:value];
            }
            [param addEntriesFromDictionary:[PLIST readFeature:@"device"]];
            NSLog(@"param = %@",param);
            AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[[DataManager client] requestWithMethod:@"POST" path:@"loginUser" parameters:param] success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                if ([[JSON valueForKey:@"error"] isKindOfClass:[NSString class]]) {
                     [SVProgressHUD showErrorWithStatus:JSON[@"error"]];
                }else{
                    NSMutableDictionary *userData =  [PLIST writeFeature:@"user" withData:JSON];
                    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"infoCompleted", @"")];
                    [userData setValue:[user valueForKey:@"name"] forKey:@"displayname"];
                    [PLIST writeFeature:@"user" withData:userData];
                    [nc dismissViewControllerAnimated:YES completion:^{
                        [nc didLogin];
                    }];
                    
                }
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                NSLog(@"%i",error.code);
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"infoNetworkError", @"")];
            }];
            [operation start];
        }
    }];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    NSLog(@"-------%u",interfaceOrientation);
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
    }
}

-(void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user{
    NSLog(@"userrrr %@",user);
}

@end
