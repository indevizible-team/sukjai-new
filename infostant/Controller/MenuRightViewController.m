//
//  MenuViewController.m
//  socioville
//
//  Created by Valentin Filip on 09.04.2012.
//  Copyright (c) 2012 App Design Vault. All rights reserved.
//

#import "MenuRightViewController.h"
#import "MainViewController.h"
#import "FeatureViewController.h"


#import "DataSourceRight.h"
#import "MenuItemRight.h"
#import "AppDelegate.h"
#import "DataManager.h"
//#import "LoginNavController.h"
#import "AppDelegate.h"
#import "Feature.h"
#import "CategoryViewController.h"
@interface MenuRightViewController ()<UIAlertViewDelegate>{
    NSArray *sec1,*sec1ReqLogin,*sec2ReqLogin;
    NSArray *sec2;
    AppDelegate *appDelegate;
    DataController *feature;
}

@property (nonatomic, strong) DataSourceRight    *dataSource;
@property (nonatomic, strong) NSIndexPath   *currentSelection;
@property (nonatomic, strong) User *user;
@end

@implementation MenuRightViewController 

@synthesize tableView;
@synthesize currentSelection;
@synthesize dataSource;

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
    feature = [[DataController alloc] initWithDictionary:[PLIST readFeature:@"feature"]];
    
    appDelegate.menuRigntView = self;
    
    self.dataSource = [DataSourceRight dataSource];
    _user = [User currentUser];
    [self.tableView reloadData];
}

- (void)viewDidUnload
{
//    [self setSearchBar:nil];
    [self setTableView:nil];
    [super viewDidUnload];
}
-(void)reloadUser{
    _user = [User currentUser];
    self.dataSource = [DataSourceRight dataSource];
    [self.tableView reloadData];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (feature == nil) {
        return 0;
    }
    return feature.key.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 22;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSString *sectionName = @" FEATURE";
    
    UILabel *sectionHeader = [[UILabel alloc] init];
    sectionHeader.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"menuSectionBg.png"]];
    sectionHeader.font = [UIFont boldSystemFontOfSize:14];
    sectionHeader.textColor = [UIColor whiteColor];
    sectionHeader.text = sectionName;
    return sectionHeader;
    
    ;
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    Feature *eachFeature = [Feature featureWithData:[feature dictionaryAtIndex:indexPath.row]];
    cell = [aTableView dequeueReusableCellWithIdentifier:@"MenuRightCell"];
    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgSideBarCell.png"]];
    cell.textLabel.text = eachFeature.name;
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0f];
    cell.textLabel.textColor = [UIColor whiteColor];
    
//    MenuItemRight *item = [
//        MenuItemRight *item =[MenuItemRight itemWithData:(self.dataSource.items)[indexPath.section-1][indexPath.row]] ;
//        UILabel *lblText = (UILabel *)[cell viewWithTag:2];
//        lblText.text = item.name;
    
        return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Feature *selectedFeature = [Feature featureWithData:[feature dictionaryAtIndex:indexPath.row]];
//    MainViewController *mainController = (MainViewController *)self.parentViewController;
    MainViewController *mainController = (MainViewController *)self.parentViewController;
    CategoryViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CategoryVC"];
    vc.isFirstPage = YES;
    vc.infofid = selectedFeature.infofid;
    InfostantNavigationViewController *nav = [[InfostantNavigationViewController alloc] initWithRootViewController:vc];
    mainController.contentViewController = nav;
    [mainController toggleRightSidebar:!mainController.leftSidebarShowing duration:kGHRevealSidebarDefaultAnimationDuration];
}
@end
