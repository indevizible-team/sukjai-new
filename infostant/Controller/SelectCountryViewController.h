//
//  SelectCountryViewController.h
//  infostant
//
//  Created by INFOSTANT on 12/26/55 BE.
//  Copyright (c) 2555 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectCountryViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancelButtun;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *donbutton;
- (IBAction)doneButton:(id)sender;

@end
