//
//  DMPickerController.m
//  www
//
//  Created by KEEP CALM AND CODE OK on 3/11/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "DMPickerController.h"

@interface DMPickerController ()<UITableViewDataSource,UITableViewDelegate>{
    NSString *dateString ;
}
- (IBAction)actionScorll:(UIDatePicker *)sender;

@end

@implementation DMPickerController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionScorll:(UIDatePicker *)sender {


    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc]init];
    timeFormatter.dateFormat = @"MM/dd/yyyy HH:mm";
    
    
    dateString = [timeFormatter stringFromDate: sender.date];
    [self.tableView reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   return  1;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"Date //Time";
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
  return  1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DateCell"];
    [cell.textLabel setText:dateString];
//    [cell.textLabel setTextAlignment:NSTextAlignmentJustified];
    return cell;
}
@end
