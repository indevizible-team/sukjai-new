//
//  YoutubeViewController.h
//  Youtube
//
//  Created by Nattawut Singhchai on 12/27/12 .
//  Copyright (c) 2012 Nattawut Singhchai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <QuartzCore/QuartzCore.h>
#import "HCYoutubeParser.h"
#import "UIImageView+AFNetworking.h"
@interface YoutubeViewController : UICollectionViewController{
	NSMutableArray *datasource;
	MPMoviePlayerViewController *mp;
	NSMutableDictionary *realLink;
	NSURL* urlToLoad;
}

@end