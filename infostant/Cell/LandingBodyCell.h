//
//  LandingBodyCell.h
//  www
//
//  Created by indevizible on 2/28/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LandingBodyCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *subImage1;
@property (weak, nonatomic) IBOutlet UIImageView *subImage2;
@property (weak, nonatomic) IBOutlet UIImageView *subImage3;
@property (weak, nonatomic) IBOutlet UIImageView *subImage4;
@property (weak, nonatomic) IBOutlet UITextView *caption;
@property (weak, nonatomic) IBOutlet UIImageView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *playIcon;
@property (weak, nonatomic) IBOutlet UITextView *description;
@property (strong, nonatomic) IBOutlet UILabel *lblCaption;
@property (strong, nonatomic) IBOutlet UIImageView *bgCaption;
@property (strong, nonatomic) IBOutlet UIView *shadowView;
@property (strong, nonatomic) IBOutlet UIButton *seeMoreBtn;
@property (nonatomic, assign)  int laidOut;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) NSURL *youtubeUrl;
@end
