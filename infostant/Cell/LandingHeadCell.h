//
//  LandingHeadCell.h
//  www
//
//  Created by indevizible on 2/28/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LandingHeadCell : UITableViewCell
#pragma mark -
#pragma mark - TitleCell
@property (weak, nonatomic) IBOutlet UITextView *title;

#pragma mark - TextCell
//@property (weak, nonatomic) IBOutlet UITextView *description;

@property (strong, nonatomic) IBOutlet UIWebView *description;

#pragma mark - PriceCell
@property (strong, nonatomic) IBOutlet UILabel *regularPrice;
@property (strong, nonatomic) IBOutlet UILabel *salePrice;
@property (weak, nonatomic) IBOutlet UILabel *nowLabel;

#pragma mark - ContactCell
@property (weak, nonatomic) IBOutlet UITextView *address;
@property (strong, nonatomic) IBOutlet UILabel *condition;
@property (strong, nonatomic) IBOutlet UILabel *delivery;
@property (strong, nonatomic) IBOutlet UILabel *tel;
@property (strong, nonatomic) IBOutlet UILabel *email;


@property (strong, nonatomic) IBOutlet UIImageView *imageBanner;


#pragma mark -  DateCell
@property (strong, nonatomic) IBOutlet UILabel *dateBegin;
@property (strong, nonatomic) IBOutlet UILabel *dateEnd;

#pragma mark - Footer Section2
#pragma mark FooterCell
@property (weak, nonatomic) IBOutlet UILabel *username;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *likeCount;
@property (weak, nonatomic) IBOutlet UILabel *folCount;



@property (strong, nonatomic) IBOutlet UIButton *likeBtn;
@property (strong, nonatomic) IBOutlet UIButton *followBtn;
@property (strong, nonatomic) IBOutlet UIButton *megBtn;
@property (strong, nonatomic) IBOutlet UIButton *boardcastBtn;
@property (strong, nonatomic) IBOutlet UIButton *voteBtn;

@end
